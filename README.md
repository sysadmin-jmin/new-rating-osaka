52.193.22.37


## Osaka Rating Project 

Project Code: sushi

Project Name: Tasklist OSAKA 

tasklist.wesvault.com 

### List sample username / password for test

- Role Admin = admin@example.com / admin 
- Role Manager = manager@example.com / manager 
- Role Leader = leader@example.com / leader 
- Role Normal = user@example.com / normal 

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project

### Package Libraries and Softwares
  - pkg-config
  - libgb-dev
  - libimagemagick-dev
  - libmagickwand-dev
  - unzip
  - 
  
### Middleware
- PHP *7.0.0*, or later
  - **Modules**
    - cURL
    - MySQLnd
    - GD
    - mbstring 
    - XML 
    - Pear
- MariaDB *10.2.0*, or later
- nginx or Apache HTTPD

INSTALLATION
------------

1. clone git
2. import sql from /db_export



