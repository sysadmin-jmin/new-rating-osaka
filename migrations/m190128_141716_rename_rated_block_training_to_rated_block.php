<?php

use yii\db\Migration;

/**
 * Class m190128_141716_rename_rated_block_training_to_rated_block
 */
class m190128_141716_rename_rated_block_training_to_rated_block extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('rated_block_training', 'rated_block');

        $this->addColumn('rated_block', 'is_actual', 'tinyint(1) DEFAULT 0 AFTER `time_spent`');
        $this->addColumn('rated_block', 'status', 'text NULL AFTER `is_actual`');
        $this->addColumn('rated_block', 'type', 'varchar(255) NULL AFTER `status`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameTable('rated_block', 'rated_block_training');
        $this->dropColumn('rated_block_training', 'is_actual');
        $this->dropColumn('rated_block_training', 'status');
        $this->dropColumn('rated_block_training', 'type');
    }
}
