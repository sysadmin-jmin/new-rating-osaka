<?php

use yii\db\Migration;

/**
 * Class m190110_081826_add_column_examples_on_question
 */
class m190110_081826_add_column_examples_on_question extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'examples', ' TEXT DEFAULT NULL AFTER `genInstructions`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'examples');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190110_081826_add_column_examples_on_question cannot be reverted.\n";

        return false;
    }
    */
}
