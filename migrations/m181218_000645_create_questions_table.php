<?php

use yii\db\Migration;

/**
 * Handles the creation of table `questions`.
 */
class m181218_000645_create_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'rcID'=>$this->integer(),
			'name'=>$this->string(),
			'items'=>$this->text(),
			'type'=>$this->string()	
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions');
    }
}
