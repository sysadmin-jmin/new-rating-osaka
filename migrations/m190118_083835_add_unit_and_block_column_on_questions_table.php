<?php

use yii\db\Migration;

/**
 * Class m190118_083835_add_unit_and_block_column_on_questions_table
 */
class m190118_083835_add_unit_and_block_column_on_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'units', ' TEXT DEFAULT NULL AFTER `items`');
        $this->addColumn('questions', 'blocks', ' TEXT DEFAULT NULL AFTER `units`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'units');
        $this->dropColumn('questions', 'blocks');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190118_083835_add_unit_and_block_column_on_questions_table cannot be reverted.\n";

        return false;
    }
    */
}
