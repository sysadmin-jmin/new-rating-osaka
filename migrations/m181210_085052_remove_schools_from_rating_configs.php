<?php

use yii\db\Migration;

/**
 * Class m181210_085052_remove_schools_from_rating_configs
 */
class m181210_085052_remove_schools_from_rating_configs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rating_configs', 'schools');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('rating_configs', 'schools', " TEXT NULL AFTER `rating_context`");
        return true;
    }
}
