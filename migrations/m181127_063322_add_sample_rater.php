<?php

use yii\db\Migration;

/**
 * Class m181127_063322_add_sample_rater
 */
class m181127_063322_add_sample_rater extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('raters',[
            'name'=>'Admin User',
            'email'=>'admin@example.com',
            'role'=>"Admin",
            'date_created'=> date("Y-m-d"),
            'date_updated'=> date("Y-m-d"),
            'password_hash' => Yii::$app->security->generatePasswordHash("admin"),
        ]);

        $this->insert('raters',[
            'name'=>'Manager User',
            'email'=>'manager@example.com',
            'role'=>"Manager",
            'date_created'=> date("Y-m-d"),
            'date_updated'=> date("Y-m-d"),
            'password_hash' => Yii::$app->security->generatePasswordHash("manager"),
        ]);

        $this->insert('raters',[
            'name'=>'Leader User',
            'email'=>'leader@example.com',
            'role'=>"Leader",
            'date_created'=> date("Y-m-d"),
            'date_updated'=> date("Y-m-d"),
            'password_hash' => Yii::$app->security->generatePasswordHash("leader"),
        ]);

        $this->insert('raters',[
            'name'=>'Normal User',
            'email'=>'user@example.com',
            'role'=>"Normal",
            'date_created'=> date("Y-m-d"),
            'date_updated'=> date("Y-m-d"),
            'password_hash' => Yii::$app->security->generatePasswordHash("normal"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_063322_add_sample_rater cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_063322_add_sample_rater cannot be reverted.\n";

        return false;
    }
    */
}
