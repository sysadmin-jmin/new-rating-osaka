<?php

use yii\db\Migration;

/**
 * Class m181218_014431_add_group_column_on_project
 */
class m181218_014431_add_group_column_on_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'groups', ' TEXT DEFAULT NULL AFTER `brand`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'groups');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181218_014431_add_group_column_on_project cannot be reverted.\n";

        return false;
    }
    */
}
