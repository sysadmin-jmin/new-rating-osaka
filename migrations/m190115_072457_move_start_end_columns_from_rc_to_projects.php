<?php

use yii\db\Migration;

/**
 * Class m190115_072457_move_start_end_columns_from_rc_to_projects
 */
class m190115_072457_move_start_end_columns_from_rc_to_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->dropColumn('rating_configs', 'start_date');
            $this->dropColumn('rating_configs', 'end_date');
            $this->addColumn('projects', 'starttime', ' DATETIME DEFAULT NULL AFTER `skipblock`');
            $this->addColumn('projects', 'endtime', ' DATETIME DEFAULT NULL AFTER `starttime`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        try {
            $this->dropColumn('projects', 'starttime');
            $this->dropColumn('projects', 'endtime');
            $this->addColumn('rating_configs', 'start_date', ' DATE DEFAULT NULL AFTER `model_test`');
            $this->addColumn('rating_configs', 'end_date', ' DATE DEFAULT NULL AFTER `start_date`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190115_072457_move_start_end_columns_from_rc_to_projects cannot be reverted.\n";

        return false;
    }
    */
}
