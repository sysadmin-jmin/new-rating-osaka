<?php

use yii\db\Migration;

/**
 * Class m181211_122729_add_group_column_to_raters
 */
class m181211_122729_add_group_column_to_raters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('raters', 'group', " VARCHAR(10) NULL AFTER `id`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('raters', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_122729_add_group_column_to_raters cannot be reverted.\n";

        return false;
    }
    */
}
