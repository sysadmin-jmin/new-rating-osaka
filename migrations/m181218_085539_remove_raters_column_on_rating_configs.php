<?php

use yii\db\Migration;

/**
 * Class m181218_085539_remove_raters_column_on_rating_configs
 */
class m181218_085539_remove_raters_column_on_rating_configs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rating_configs', 'raters');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('rating_configs', 'raters', ' VARCHAR(255) DEFAULT NULL AFTER `test_name`');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181218_085539_remove_raters_column_on_rating_configs cannot be reverted.\n";

        return false;
    }
    */
}
