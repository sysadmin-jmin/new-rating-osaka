<?php

use yii\db\Migration;

/**
 * Class m190129_053134_add_missing_column_block_status_on_rated_block
 */
class m190129_053134_add_missing_column_block_status_on_rated_block extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rated_block', 'block_status', 'tinyint DEFAULT 0 AFTER `status`');
        $this->renameColumn('rated_block', 'status', 'unit_status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rated_block', 'block_status');
        $this->renameColumn('rated_block', 'unit_status', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_053134_add_missing_column_block_status_on_rated_block cannot be reverted.\n";

        return false;
    }
    */
}
