<?php

use yii\db\Migration;

/**
 * Class m181110_025149_testconfigs_add_status_col
 */
class m181110_025149_testconfigs_add_status_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('rating_configs','status'," TINYINT AFTER `model_test`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181110_025149_testconfigs_add_status_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181110_025149_testconfigs_add_status_col cannot be reverted.\n";

        return false;
    }
    */
}
