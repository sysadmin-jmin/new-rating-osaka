<?php

use yii\db\Migration;

/**
 * Class m190201_105535_add_column_assign_to_answers_table
 */
class m190201_105535_add_column_assign_to_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->addColumn('answers', 'assign', ' TEXT DEFAULT NULL AFTER `blocks`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        try {
            $this->dropColumn('answers', 'assign');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190201_105535_add_column_assign_to_answers_table cannot be reverted.\n";

        return false;
    }
    */
}
