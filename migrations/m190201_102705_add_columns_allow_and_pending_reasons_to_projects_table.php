<?php

use yii\db\Migration;

/**
 * Class m190201_102705_add_columns_allow_and_pending_reasons_to_projects_table
 */
class m190201_102705_add_columns_allow_and_pending_reasons_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->addColumn('projects', 'allow', ' VARCHAR(64) DEFAULT NULL AFTER `endtime`');
            $this->addColumn('projects', 'pending_reasons', ' TEXT DEFAULT NULL AFTER `allow`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        try {
            $this->dropColumn('projects', 'pending');
            $this->dropColumn('projects', 'pending_reasons');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190201_102705_add_columns_allow_and_pending_reasons_to_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
