<?php

use yii\db\Migration;

/**
 * Class m190109_052941_add_column_geninstructions_on_question
 */
class m190109_052941_add_column_geninstructions_on_question extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'genInstructions', ' TEXT DEFAULT NULL AFTER `context`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'genInstructions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190109_052941_add_column_geninstructions_on_question cannot be reverted.\n";

        return false;
    }
    */
}
