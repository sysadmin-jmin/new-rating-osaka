<?php

use yii\db\Migration;

/**
 * Class m190102_115127_add_column_groups_and_raters_on_rating_configs
 */
class m190102_115127_add_column_groups_and_raters_on_rating_configs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rating_configs', 'groups', 'text DEFAULT NULL AFTER `rating_blocks`');
        $this->addColumn('rating_configs', 'raters', 'text DEFAULT NULL AFTER `groups`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rating_configs', 'groups');
        $this->dropColumn('rating_configs', 'raters');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190102_115127_add_column_groups_and_raters_on_rating_configs cannot be reverted.\n";

        return false;
    }
    */
}
