<?php

use yii\db\Migration;

/**
 * Class m190202_050746_rater_add_col_certBrands
 */
class m190202_050746_rater_add_col_certBrands extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('raters', 'certBrands', 'TEXT AFTER `inpro_testblock`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190202_050746_rater_add_col_certBrands reverted.\n";
		$this->dropColumn('raters', 'certBrands');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190202_050746_rater_add_col_certBrands cannot be reverted.\n";

        return false;
    }
    */
}
