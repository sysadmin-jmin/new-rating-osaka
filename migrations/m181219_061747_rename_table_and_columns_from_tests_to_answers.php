<?php

use yii\db\Migration;

/**
 * Class m181219_061747_rename_table_and_columns_from_tests_to_answers
 */
class m181219_061747_rename_table_and_columns_from_tests_to_answers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('tests', 'answers');
        $this->renameColumn('rated_units', 'testID', 'answerID');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('rated_units', 'answerID', 'testID');
        $this->renameTable('answers', 'tests');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_061747_rename_table_and_columns_from_tests_to_answers cannot be reverted.\n";

        return false;
    }
    */
}
