<?php

use yii\db\Migration;

/**
 * Class m181106_014643_add_raters_table
 */
class m181106_014643_add_raters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('raters', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
			'email'=> $this->string()->notNull(),
			'role'=> $this->string()->notNull(),
			'password_hash'=>$this->string(),		
            'date_created' => $this->date(),
            'date_updated' => $this->date(),      
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181106_014643_add_raters_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181106_014643_add_raters_table cannot be reverted.\n";

        return false;
    }
    */
}
