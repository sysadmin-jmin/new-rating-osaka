<?php

use yii\db\Migration;

/**
 * Class m190207_013749_rated_delta_set_table
 */
class m190207_013749_rated_delta_set_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rated_delta_set', [
            'id' => $this->primaryKey(),
            'raterID' => $this->integer(),
			'type'=> $this->string(),
            'rbdSets' => $this->string(),
            'tContext' => $this->integer(),
			'tPerfect' => $this->integer(),
			'tWithin' => $this->integer(),
			'diffModel' => $this->integer(),
			'status' => $this->integer(),
            'datetime_created' => $this->datetime(),
            'datetime_updated' => $this->datetime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rated_delta_set');
    }
}
