<?php

use yii\db\Migration;

/**
 * Class m181127_064403_rc_config_to_context
 */
class m181127_064403_rc_config_to_context extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->renameColumn('rating_configs','rating_config','rating_context');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_064403_rc_config_to_context cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_064403_rc_config_to_context cannot be reverted.\n";

        return false;
    }
    */
}
