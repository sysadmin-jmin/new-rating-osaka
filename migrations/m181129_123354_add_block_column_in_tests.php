<?php

use yii\db\Migration;

/**
 * Class m181129_123354_add_block_column_in_tests
 */
class m181129_123354_add_block_column_in_tests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tests', 'blocks', " TEXT NULL AFTER `units`");
        $this->renameColumn('tests', 'partID', "examineeID");
        $this->alterColumn('tests', 'items', "TEXT");
        $this->alterColumn('tests', 'units', "TEXT");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tests', 'blocks');
        $this->renameColumn('tests', 'examineeID', "partID");
        $this->alterColumn('tests', 'items', "varchar(255)");
        $this->alterColumn('tests', 'units', "varchar(255)");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181129_123354_add_block_column_in_tests cannot be reverted.\n";

        return false;
    }
    */
}
