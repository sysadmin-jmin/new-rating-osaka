<?php

use yii\db\Migration;

/**
 * Class m181127_071043_add_status_column_on_raters_table
 */
class m181127_071043_add_status_column_on_raters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('raters', 'status', " TINYINT DEFAULT 0 AFTER `password_hash`");


        $this->update('raters', ["status" => 1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_071043_add_status_column_on_raters_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_071043_add_status_column_on_raters_table cannot be reverted.\n";

        return false;
    }
    */
}
