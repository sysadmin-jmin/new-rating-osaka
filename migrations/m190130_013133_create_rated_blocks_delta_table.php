<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rated_blocks_delta`.
 */
class m190130_013133_create_rated_blocks_delta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rated_blocks_delta', [
            'id' => $this->primaryKey(),
            'qnID' => $this->integer(),
            'ansID' => $this->integer(),
            'qnID' => $this->integer(),
            'blockID' => $this->integer(),
            'modelRatingID' => $this->integer(),
            'ratedBlockID' => $this->integer(),
            'delta' => $this->text(),
            'result' => $this->tinyInteger(),
            'datetime_created' => $this->datetime(),
            'datetime_updated' => $this->datetime(),
        ]);
        if ($this->db->getTableSchema("rated_units", true) !== null) {
            $this->dropTable('rated_units');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rated_blocks_delta');
    }
}
