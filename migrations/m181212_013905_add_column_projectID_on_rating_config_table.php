<?php

use yii\db\Migration;

/**
 * Class m181212_013905_add_column_projectID_on_rating_config_table
 */
class m181212_013905_add_column_projectID_on_rating_config_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rating_configs', 'projectID', 'INT(11) unsigned DEFAULT NULL AFTER `raterID`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rating_configs', 'projectID');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181212_013905_add_column_projectID_on_rating_config_table cannot be reverted.\n";

        return false;
    }
    */
}
