<?php

use yii\db\Migration;

/**
 * Class m181106_030129_add_rating_configs_table
 */
class m181106_030129_add_rating_configs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('rating_configs', [
            'id' => $this->primaryKey(),
			'raterID'=> $this->integer(),
			'test_name' => $this->string(),
			'raters' => $this->string(),
            'rating_items'=> $this->text(),
			'rating_units'=> $this->text(),
			'rating_blocks'=>$this->text(),
			'rating_config'=>$this->text(),
			'schools' => $this->text(),
			'model_test'=> $this->text(),			
			'date_created' => $this->date(),
            'date_updated' => $this->date(),      
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181106_030129_add_rating_configs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181106_030129_add_rating_configs_table cannot be reverted.\n";

        return false;
    }
    */
}
