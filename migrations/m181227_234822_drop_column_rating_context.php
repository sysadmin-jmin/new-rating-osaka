<?php

use yii\db\Migration;

/**
 * Class m181227_234822_drop_column_rating_context
 */
class m181227_234822_drop_column_rating_context extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rating_configs', 'rating_context');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('rating_configs', 'rating_context', 'text DEFAULT NULL AFTER `rating_blocks`');
    }
}
