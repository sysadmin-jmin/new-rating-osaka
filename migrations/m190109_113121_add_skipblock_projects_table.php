<?php

use yii\db\Migration;

/**
 * Class m190109_113121_add_skipblock_projects_table
 */
class m190109_113121_add_skipblock_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('projects','skipblock',' tinyint(1) DEFAULT 0 AFTER `name`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190109_113121_add_skipblock_projects_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190109_113121_add_skipblock_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
