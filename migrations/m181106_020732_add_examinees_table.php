<?php

use yii\db\Migration;

/**
 * Class m181106_020732_add_examinees_table
 */
class m181106_020732_add_examinees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('examinees', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
			'school'=>$this->string(),
			'date_created' => $this->date(),
            'date_updated' => $this->date(),      
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181106_020732_add_examinees_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181106_020732_add_examinees_table cannot be reverted.\n";

        return false;
    }
    */
}
