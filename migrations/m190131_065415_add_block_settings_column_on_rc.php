<?php

use yii\db\Migration;

/**
 * Class m190131_065415_add_block_settings_column_on_rc
 */
class m190131_065415_add_block_settings_column_on_rc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rating_configs', 'block_settings', 'TEXT AFTER `status`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rating_configs', 'block_settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190131_065415_add_block_settings_column_on_rc cannot be reverted.\n";

        return false;
    }
    */
}
