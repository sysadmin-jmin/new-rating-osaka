<?php

use yii\db\Migration;

/**
 * Class m181211_054820_add_column_end_date_on_rating_config_table
 */
class m181211_054820_add_column_end_date_on_rating_config_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rating_configs', 'end_date', ' DATE DEFAULT NULL AFTER `start_date`');
        // Change column allowed null, and default value
        $this->alterColumn('rating_configs', 'start_date', ' DATE DEFAULT NULL');
        // Change default value to null
        $this->update('rating_configs', ['start_date' => null], 'start_date = "0000-00-00"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rating_configs', 'end_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_054820_add_column_end_date_on_rating_config_table cannot be reverted.\n";

        return false;
    }
    */
}
