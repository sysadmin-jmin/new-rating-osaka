<?php

use yii\db\Migration;

/**
 * Class m190206_022846_change_blockID_to_blockKey
 */
class m190206_022846_change_blockID_to_blockKey extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("rated_block", "blockID", "blockKey");
        $this->renameColumn("rated_blocks_delta", "blockID", "blockKey");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn("rated_block", "blockKey", "blockID");
        $this->renameColumn("rated_blocks_delta", "blockKey", "blockID");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190206_022846_change_blockID_to_blockKey cannot be reverted.\n";

        return false;
    }
    */
}
