<?php

use yii\db\Migration;

/**
 * Class m181210_064649_add_start_date_column_on_rating_configs_table
 */
class m181210_064649_add_start_date_column_on_rating_configs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rating_configs', 'start_date', " DATE NOT NULL DEFAULT '0000-00-00' AFTER `model_test`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rating_configs', 'start_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_064649_add_start_date_column_on_rating_configs_table cannot be reverted.\n";

        return false;
    }
    */
}
