<?php

use yii\db\Migration;

/**
 * Class m190110_093330_drop_table_examinees
 */
class m190110_093330_drop_table_examinees extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('examinees');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('examinees',[
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'school'=>$this->string(),
            'date_created' => $this->date(),
            'date_updated' => $this->date(),
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190110_093330_drop_table_examinees cannot be reverted.\n";

        return false;
    }
    */
}
