<?php

use yii\db\Migration;

/**
 * Class m190119_021627_add_status_col_question_tbl
 */
class m190119_021627_add_status_col_question_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'status', 'VARCHAR(10) DEFAULT NULL AFTER `type`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190119_021627_add_status_col_question_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190119_021627_add_status_col_question_tbl cannot be reverted.\n";

        return false;
    }
    */
}
