<?php

use yii\db\Migration;

/**
 * Handles adding context to table `questions`.
 */
class m181218_163656_add_context_column_to_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('questions', 'context', ' TEXT DEFAULT NULL AFTER `items`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('questions', 'context');
    }
}
