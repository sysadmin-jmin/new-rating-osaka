<?php

use yii\db\Migration;

/**
 * Class m190108_050649_add_column_attempt_times_on_raters_table
 */
class m190108_050649_add_column_attempt_times_on_raters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('raters', 'attempt_times', 'INT unsigned DEFAULT 0 AFTER `status`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('raters', 'attempt_times');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_050649_add_column_attempt_times_on_raters_table cannot be reverted.\n";

        return false;
    }
    */
}
