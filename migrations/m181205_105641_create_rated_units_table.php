<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rated_units`.
 */
class m181205_105641_create_rated_units_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rated_units', [
            'id' => $this->primaryKey(),
            'answerID' => $this->integer(),
            'unitID' => $this->integer(),
            'ratings' => $this->text(),
            'status' => $this->boolean(),
            'comments' => $this->text(),
            'datetime_start' => $this->dateTime(),
            'datetime_end' => $this->dateTime(),
            'time_spent' => $this->integer(),
            'datetime_create' => $this->dateTime(),
            'datetime_updated' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rated_units');
    }
}
