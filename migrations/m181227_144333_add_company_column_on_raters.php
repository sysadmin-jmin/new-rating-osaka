<?php

use yii\db\Migration;

/**
 * Class m181227_144333_add_company_column_on_raters
 */
class m181227_144333_add_company_column_on_raters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('raters', 'company', 'varchar(50) DEFAULT NULL AFTER `group`');
        $this->update("raters", ["company"=>"HQ"], ["role"=>"Admin"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('raters', 'company');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181227_144333_add_company_column_on_raters cannot be reverted.\n";

        return false;
    }
    */
}
