<?php

use yii\db\Migration;

/**
 * Class m190118_074408_add_column_sound_check_to_raters_table
 */
class m190118_074408_add_column_sound_check_to_raters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->addColumn('raters', 'sound_check', ' TINYINT(1) DEFAULT 0 AFTER `inpro_testblock`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        try {
            $this->dropColumn('raters', 'sound_check');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190118_074408_add_column_sound_check_to_raters_table cannot be reverted.\n";

        return false;
    }
    */
}
