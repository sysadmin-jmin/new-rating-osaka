<?php

use yii\db\Migration;

/**
 * Handles the creation of table `test`.
 */
class m181114_033958_create_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
	 
	 
    public function safeUp()
    {
        $this->createTable('tests', [
            'id' => $this->primaryKey(),
			'rcID'=> $this->integer(),
			'partID' => $this->integer(),
			'folderPath' => $this->string(),
			'items' => $this->string(),
			'units' => $this->string(),
			'type' => $this->tinyInteger(),
			'datetime_created' => $this->datetime(), 
			'datetime_updated' => $this->datetime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('test');
    }
}
