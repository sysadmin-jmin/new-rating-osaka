<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ratedBlockTraining`.
 */
class m190114_234705_create_ratedBlockTraining_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rated_block_training', [
            'id' => 		$this->primaryKey(),
			'projectID'=>	$this->integer(),
			'qnID'=> 		$this->integer(),
			'ansID'=> 		$this->integer(),
			'userID'=> 		$this->integer(),
			'blockID'=> 	$this->integer(),
			'ratings'=> 	$this->text(),
			'datetime_start' => $this->datetime(),
			'datetime_end' => $this->datetime(),
			'time_spent'   => $this->integer(),
			'datetime_created' => $this->datetime(),
			'datetime_updated' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ratedBlockTraining');
    }
}
