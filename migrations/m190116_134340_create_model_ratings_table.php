<?php

use yii\db\Migration;

/**
 * Handles the creation of table `model_ratings`.
 */
class m190116_134340_create_model_ratings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('model_ratings', [
            'id' => $this->primaryKey(),
            'userID' => $this->integer(),
            'rcID' => $this->integer(),
            'answerID' => $this->integer(),
            'ratings' => $this->text(),
            'type' => $this->string(),
            'datetime_created' => $this->datetime(),
            'datetime_updated' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('model_ratings');
    }
}
