<?php

use yii\db\Migration;

/**
 * Class m181211_023952_add_inpro_testblock_column_on_raters
 */
class m181211_023952_add_inpro_testblock_column_on_raters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('raters', 'inpro_testblock', " VARCHAR(100) NULL AFTER `status`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('raters', 'inpro_testblock');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_023952_add_inpro_testblock_column_on_raters cannot be reverted.\n";

        return false;
    }
    */
}
