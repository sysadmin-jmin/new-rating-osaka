<?php

use yii\db\Migration;

/**
 * Class m190207_040327_add_column_pending_mode_to_projects_table
 */
class m190207_040327_add_column_pending_mode_to_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->addColumn('projects', 'pending_mode', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `endtime`');
            $this->dropColumn('projects', 'allow');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        try {
            $this->dropColumn('projects', 'pending_mode');
            $this->addColumn('projects', 'allow', ' VARCHAR(64) DEFAULT NULL AFTER `endtime`');
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190207_040327_add_column_pending_mode_to_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
