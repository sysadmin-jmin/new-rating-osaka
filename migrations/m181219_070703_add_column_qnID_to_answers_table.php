<?php

use yii\db\Migration;

/**
 * Class m181219_070703_add_column_qnID_to_answers_table
 */
class m181219_070703_add_column_qnID_to_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('answers', 'qnID', 'INT(11) UNSIGNED DEFAULT NULL AFTER `rcID`');
        $this->addColumn('answers', 'isModel', 'BOOLEAN DEFAULT 0 AFTER `type`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('answers', 'qnID');
        $this->dropColumn('answers', 'isModel');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181219_070703_add_column_qnID_to_answers_table cannot be reverted.\n";

        return false;
    }
    */
}
