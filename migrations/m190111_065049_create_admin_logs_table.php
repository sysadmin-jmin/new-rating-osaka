<?php

use yii\db\Migration;

/**
 * Handles the creation of table `admin_logs`.
 */
class m190111_065049_create_admin_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admin_logs', [
            'id' => $this->primaryKey(),
            'userID' => $this->integer(),
            'action' => $this->string(),
            'remark' => $this->string(),
            'data' => $this->text(),
            'datetime_created' => $this->datetime(),
            'datetime_updated' => $this->datetime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_logs');
    }
}
