<?php

use yii\db\Migration;

/**
 * Class m181217_161254_add_column_sheet_status_to_tests
 */
class m181217_161254_add_column_sheet_status_to_tests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('tests', 'sheet_status', 'VARCHAR(10) DEFAULT "open" AFTER `type`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('tests','sheet_status');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181217_161254_add_column_sheet_status_to_tests cannot be reverted.\n";

        return false;
    }
    */
}
