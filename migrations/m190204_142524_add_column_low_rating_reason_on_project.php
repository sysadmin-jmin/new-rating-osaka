<?php

use yii\db\Migration;

/**
 * Class m190204_142524_add_column_low_rating_reason_on_project
 */
class m190204_142524_add_column_low_rating_reason_on_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects', 'low_rating_reasons', 'TEXT AFTER `pending_reasons`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('projects', 'low_rating_reasons');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190204_142524_add_column_low_rating_reason_on_project cannot be reverted.\n";

        return false;
    }
    */
}
