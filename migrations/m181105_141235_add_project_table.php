<?php

use yii\db\Migration;

/**
 * Class m181105_141235_add_project_table
 */
class m181105_141235_add_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'brand'=> $this->string(),
			'name' => $this->string()->notNull(),
            'date_created' => $this->date(),
            'date_updated' => $this->date(),      
        ]);
	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181105_141235_add_project_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_141235_add_project_table cannot be reverted.\n";

        return false;
    }
    */
}
