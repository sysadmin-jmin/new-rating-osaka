<?php
namespace app\assets;

use yii\web\AssetBundle;

class FlowAsset extends AssetBundle
{
    public $sourcePath = '@bower/flow.js/dist';
    public $js = [
        'flow.js',
    ];
}