<?php
namespace app\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $sourcePath = '@bower/vue/dist';

	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];	
	
    public $js = [
        'vue.js',
    ];
}