<?php
/**
 * Created by PhpStorm.
 * User: fujimoto
 * Date: 2018-12-20
 * Time: 11:17
 */

namespace app\components;


use Imagine\Image\ManipulatorInterface;
use yii;
use yii\web\UploadedFile;
use yii\base\Component;
use yii\imagine\Image;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

class ConvertImage extends Component
{
    /**
     * @var int Allowed max width pixel
     */
    const MAX_WIDTH = 1000;

    public $content;

    /**
     * @var ImageInterface
     */
    private $image;


    public function init()
    {
        parent::init();
    }

    /**
     * @param UploadedFile $uploadedFile Yii2 UploadedFile object
     * @param string $savePath Save destination path
     * @param int $maxPixel Max pixels width
     * @return string Saved filepath included filename
     * @example
     *
     * // Upload form model
     * $model = new Import;
     * // Create instance of UploadedFile by form model
     * $imageFile = UploadedFile::getInstance($model, 'dataFile');
     *
     * $width = 500;
     * // $destSavePath is to save file path
     * $convertedFilepath = Yii::$app->convertImage->saveImageForm($imageFile, $destSavePath, $width);
     *    OR
     * $convertedFilepath = \app\components\ConvertImage::saveImageForm($imageFile, $destSavePath, $width);
     */
    public static function saveImageForm(UploadedFile $uploadedFile, $savePath, $maxPixel = ConvertImage::MAX_WIDTH)
    {
        $self = new self;
        $self->loadFile($uploadedFile->tempName);
        $self->image = self::resizeTo($self->image, $maxPixel);
        $pngImage = self::convertTo($self->image, 'png');

        $filename = sprintf('%s.%s', $uploadedFile->getBaseName(), 'png');
        $savePath = (substr($savePath,-1,1)==='/') ? substr($savePath,0,-1) : $savePath;
        $savedPath = sprintf('%s/%s', $savePath, $filename);
        if (file_exists($savedPath))
            unlink($savedPath);
        file_put_contents($savedPath, $pngImage);

        return $savedPath;
    }




    public function loadFile($filename)
    {
        // create Imagine
        $this->image = (new \Imagine\Gd\Imagine)->open($filename);
    }


    /**
     * @param ImageInterface $image
     * @param string $format
     * @return string Image binary string
     */
    protected static function convertTo(ImageInterface $image, $format = 'png')
    {
        $options = [];
        if ($format == 'png') {
            $options = ['png_compression_level' => 4];
        }
        return $image->get($format, $options);
    }

    /**
     * Resizing image canvas size
     * the $image canvas size less than $maxPixel, it will return original.
     * @param ImageInterface $image
     * @param int|null $maxPixel max pixel width or height
     * @return ImageInterface
     */
    protected static function resizeTo(ImageInterface $image, $maxPixel = self::MAX_WIDTH)
    {
        $boxSize = $image->getSize();
        if ($boxSize->getWidth() > $maxPixel) {
            // resize
            //return Image::thumbnail($image, $width, $height, ManipulatorInterface::THUMBNAIL_INSET);
            return Image::resize($image, $maxPixel, $maxPixel);
        }
        return $image;
    }
}
