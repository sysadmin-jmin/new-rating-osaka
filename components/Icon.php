<?php

//icon for FA

namespace app\components;

use yii\base\Component;
use yii\helpers\Html;

class Icon extends Component
{
    public static function fa($icon, $url=[], $opts = [])
    {
        $s = '';
        
        switch ($icon) {
        
            //--- General
            case 'user':
                $s = "fa-user";
                break;
            
			case 'settings':
				$s = "fa-gear";
				break;
            case 'add':
            case 'create':
                $s = "fa-plus";
                break;
            case 'times':
                $s = 'fa-times';
                break;
            case 'delete':
            case 'remove':
            case 'trash':
                $s = "fa-trash-o";
                break;
            case 'upload':
                $s = "fa-upload";
                break;
            case 'arrow-right':
                $s = "fa-arrow-right";
                break;
            case 'edit':
                $s = "fa-edit";
                break;
            
        //block set
        
            case 'block':
                $s = 'fa-cube';
                break;
            case 'unit':
                $s = 'fa-archive';
                break;
            case 'item':
                $s = 'fa-file-text';
                break;
        
        //Workflow
        
            case 'setting':
                $s = 'fa fa-cogs';
                break;
            case 'question':
                $s = 'fa-question-circle-o';
                break;
                
            case 'answer':
                $s = 'fa-quote-left';
                break;
            default:
                $s = "fa-".$icon;
        }

        $i= "<i class=\"fa {$s}\"></i>";

        if (count($url)) {
            return Html::a($i, $url, $opts);
        } else {
            return $i;
        }
    }
}
