-- MySQL dump 10.16  Distrib 10.2.18-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: projects_osaka
-- ------------------------------------------------------
-- Server version	10.2.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_logs`
--

DROP TABLE IF EXISTS `admin_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `data` text DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_logs`
--

LOCK TABLES `admin_logs` WRITE;
/*!40000 ALTER TABLE `admin_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rcID` int(11) DEFAULT NULL,
  `qnID` int(11) unsigned DEFAULT NULL,
  `examineeID` int(11) DEFAULT NULL,
  `folderPath` varchar(255) DEFAULT NULL,
  `items` text DEFAULT NULL,
  `units` text DEFAULT NULL,
  `blocks` text DEFAULT NULL,
  `assign` text DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `isModel` tinyint(1) DEFAULT 0,
  `sheet_status` varchar(10) DEFAULT 'open',
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (8,9,11,NULL,'C:/Users/Lionel/Dropbox/9_Web_Development/htdocs/local/yii2/osaka/data/answers/8','{\"0\":{\"question\":\"1\",\"uid\":\"1\",\"qnType\":\"text\",\"ansType\":\"text\",\"src\":\"Hello Hitty\"},\"1\":{\"question\":\"More sugar?\",\"uid\":\"qn2\",\"qnType\":\"image\",\"ansType\":\"audio\",\"src\":\"2.mp3\"}}','{\"0\":{\"status\":\"open\"},\"1\":{\"status\":\"open\"}}','{\"0\":{\"status\":\"inpro\"},\"1\":{\"status\":\"inpro\"}}',NULL,'Actual',0,'inprocess','2019-01-21 18:15:27','2019-01-22 10:31:57'),(9,9,13,NULL,'/data/answers/9','{\"0\":{\"question\":\"1\",\"uid\":\"1\",\"qnType\":\"text\",\"ansType\":\"text\",\"src\":\"1.png\",\"type\":\"png\"},\"1\":{\"question\":\"More sugar?\",\"uid\":\"qn2\",\"qnType\":\"image\",\"ansType\":\"audio\",\"src\":\"2.png\",\"type\":\"png\"}}','{\"0\":{\"status\":\"open\"},\"1\":{\"status\":\"open\"}}','{\"0\":{\"status\":\"inpro\"},\"1\":{\"status\":\"inpro\"}}',NULL,'Retraining',1,'inprocess','2019-01-22 14:37:48','2019-02-07 08:54:01'),(10,9,14,NULL,'/data/answers/10','{\"0\":{\"src\":\"1.png\",\"type\":\"png\",\"ansType\":\"text\"},\"1\":{\"src\":\"2.mp3\",\"type\":\"mpeg\",\"ansType\":\"audio\"}}','{\"0\":{\"status\":\"open\"},\"1\":{\"status\":\"open\"}}','{\"0\":{\"status\":\"inpro\"},\"1\":{\"status\":\"inpro\"}}',NULL,'Actual',0,'open','2019-02-03 08:33:33','2019-02-05 08:58:37'),(11,9,15,NULL,'/data/answers/11','{\"0\":{\"src\":\"1.png\",\"type\":\"png\",\"ansType\":\"text\"},\"1\":{\"src\":\"2.mp3\",\"type\":\"mpeg\",\"ansType\":\"audio\"}}','{\"0\":{\"status\":\"open\"},\"1\":{\"status\":\"open\"}}','{\"0\":{\"status\":\"inpro\"},\"1\":{\"status\":\"inpro\"}}',NULL,'Cert',1,'open','2019-02-07 09:12:39','2019-02-07 10:12:54');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1541427471),('m181105_141235_add_project_table',1541427488),('m181106_014643_add_raters_table',1541469494),('m181106_020732_add_examinees_table',1541470123),('m181106_030129_add_rating_configs_table',1541483483),('m181110_025149_testconfigs_add_status_col',1541818609),('m181114_033958_create_test_table',1542167110),('m181127_063322_add_sample_rater',1543301018),('m181127_064403_rc_config_to_context',1543377120),('m181127_071043_add_status_column_on_raters_table',1543302993),('m181129_123354_add_block_column_in_tests',1543543467),('m181205_105641_create_rated_units_table',1544406205),('m181210_064649_add_start_date_column_on_rating_configs_table',1544438515),('m181210_085052_remove_schools_from_rating_configs',1544438515),('m181211_023952_add_inpro_testblock_column_on_raters',1544528065),('m181211_054820_add_column_end_date_on_rating_config_table',1544528066),('m181211_122729_add_group_column_to_raters',1544608776),('m181212_013905_add_column_projectID_on_rating_config_table',1544608776),('m181217_161254_add_column_sheet_status_to_tests',1545063518),('m181218_000645_create_questions_table',1545092230),('m181218_014431_add_group_column_on_project',1545123054),('m181218_085539_remove_raters_column_on_rating_configs',1545181325),('m181218_163656_add_context_column_to_questions_table',1545181325),('m181219_061747_rename_table_and_columns_from_tests_to_answers',1545284639),('m181219_070703_add_column_qnID_to_answers_table',1546439133),('m181227_144333_add_company_column_on_raters',1546439133),('m181227_234822_drop_column_rating_context',1546439133),('m190102_115127_add_column_groups_and_raters_on_rating_configs',1546439133),('m190108_050649_add_column_attempt_times_on_raters_table',1546959034),('m190109_052941_add_column_geninstructions_on_question',1547033642),('m190109_113121_add_skipblock_projects_table',1547033642),('m190110_081826_add_column_examples_on_question',1547429441),('m190110_093330_drop_table_examinees',1547543225),('m190111_065049_create_admin_logs_table',1547429441),('m190114_234705_create_ratedBlockTraining_table',1547510108),('m190115_072457_move_start_end_columns_from_rc_to_projects',1547544304),('m190116_134340_create_model_ratings_table',1547690959),('m190118_074408_add_column_sound_check_to_raters_table',1547861918),('m190118_083835_add_unit_and_block_column_on_questions_table',1547861918),('m190119_021627_add_status_col_question_tbl',1547864272),('m190128_141716_rename_rated_block_training_to_rated_block',1548897659),('m190129_053134_add_missing_column_block_status_on_rated_block',1548897659),('m190130_013133_create_rated_blocks_delta_table',1548897659),('m190131_065415_add_block_settings_column_on_rc',1549029657),('m190201_102705_add_columns_allow_and_pending_reasons_to_projects_table',1549328405),('m190201_105535_add_column_assign_to_answers_table',1549328405),('m190202_050746_rater_add_col_certBrands',1549084226),('m190204_142524_add_column_low_rating_reason_on_project',1549328405),('m190206_022846_change_blockID_to_blockKey',1549500833),('m190207_013749_rated_delta_set_table',1549504065),('m190207_040327_add_column_pending_mode_to_projects_table',1549538630);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_ratings`
--

DROP TABLE IF EXISTS `model_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `rcID` int(11) DEFAULT NULL,
  `answerID` int(11) DEFAULT NULL,
  `ratings` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_ratings`
--

LOCK TABLES `model_ratings` WRITE;
/*!40000 ALTER TABLE `model_ratings` DISABLE KEYS */;
INSERT INTO `model_ratings` VALUES (1,NULL,9,9,'{\"0\":{\"0\":{\"rating\":\"4\",\"status\":\"closed\"},\"1\":{\"rating\":\"3\",\"status\":\"closed\"}},\"1\":{\"0\":{\"rating\":\"5\",\"status\":\"closed\"}}}','Retraining','2019-01-31 09:54:21','2019-01-31 09:54:21'),(2,NULL,9,11,'{\"0\":{\"0\":{\"rating\":\"3\",\"status\":\"closed\"}},\"1\":{\"0\":{\"rating\":\"4\",\"status\":\"closed\"}}}','Cert','2019-02-07 09:16:37','2019-02-07 09:16:37');
/*!40000 ALTER TABLE `model_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) DEFAULT NULL,
  `groups` text DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `skipblock` tinyint(1) DEFAULT 0,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `pending_mode` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `pending_reasons` text DEFAULT NULL,
  `low_rating_reasons` text DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (5,'HARV',NULL,'History',0,'2019-01-21 00:00:00','2019-01-21 00:00:00',0,NULL,'{\"0\":\"this is tough\"}','2019-01-08','2019-02-07');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `rcID` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `items` text DEFAULT NULL,
  `units` text DEFAULT NULL,
  `blocks` text DEFAULT NULL,
  `context` text DEFAULT NULL,
  `genInstructions` text DEFAULT NULL,
  `examples` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (15,5,9,'Certification1','{\"0\":{\"question\":\"1\",\"uid\":\"1\",\"qnType\":\"text\",\"ansType\":\"text\"},\"1\":{\"question\":\"More sugar?\",\"uid\":\"qn2\",\"qnType\":\"image\",\"ansType\":\"audio\"}}','{\"0\":{\"id\":1,\"name\":\"1\",\"items\":{\"0\":\"0\"}},\"1\":{\"id\":2,\"name\":\"2\",\"items\":{\"0\":\"1\"}}}','{\"0\":{\"name\":\"1\",\"id\":1,\"units\":{\"0\":\"0\"}},\"1\":{\"name\":\"2\",\"id\":2,\"units\":{\"0\":\"1\"}}}','{\"0\":{\"0\":{\"name\":\"Grammer\",\"src\":\"This is the help\",\"conType\":\"1to5\",\"srcType\":\"text\"}},\"1\":{\"0\":{\"name\":\"Grammer\",\"src\":\"This is the help\",\"conType\":\"1to5\",\"srcType\":\"text\"}}}',NULL,NULL,'cert','lock');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rated_block`
--

DROP TABLE IF EXISTS `rated_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rated_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `qnID` int(11) DEFAULT NULL,
  `ansID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `blockKey` int(11) DEFAULT NULL,
  `ratings` text DEFAULT NULL,
  `datetime_start` datetime DEFAULT NULL,
  `datetime_end` datetime DEFAULT NULL,
  `time_spent` int(11) DEFAULT NULL,
  `is_actual` tinyint(1) DEFAULT 0,
  `unit_status` text DEFAULT NULL,
  `block_status` tinyint(3) DEFAULT 0,
  `type` varchar(255) DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rated_block`
--

LOCK TABLES `rated_block` WRITE;
/*!40000 ALTER TABLE `rated_block` DISABLE KEYS */;
INSERT INTO `rated_block` VALUES (1,5,13,9,1,1,'{\"0\":{\"0\":{\"rating\":\"4\",\"remark\":\"\"},\"1\":{\"rating\":\"3\",\"remark\":\"\"}}}','2019-02-01 15:10:28',NULL,NULL,0,'{\"0\":{\"status\":2,\"comments\":\"\"}}',1,'Retraining','2019-02-01 22:10:28','2019-02-03 13:10:43'),(2,5,14,10,4,1,'{\"0\":{\"0\":{\"rating\":\"\",\"remark\":\"\"}}}','2019-02-03 05:26:22',NULL,NULL,1,'{\"0\":{\"status\":1,\"comments\":\"\"}}',1,'Actual','2019-02-03 12:26:22','2019-02-05 08:58:37'),(3,5,14,10,1,2,'{\"1\":{\"0\":{\"rating\":\"\",\"remark\":\"\"}}}','2019-02-03 05:26:33',NULL,NULL,1,'{\"1\":{\"status\":1,\"comments\":\"\"}}',1,'Actual','2019-02-03 12:26:33','2019-02-03 12:26:33'),(4,5,15,11,4,0,'{\"0\":{\"0\":{\"rating\":\"\",\"remark\":\"\"}}}','2019-02-07 03:12:54',NULL,NULL,0,'{\"0\":{\"status\":1,\"comments\":\"\"}}',1,'Cert','2019-02-07 10:12:54','2019-02-07 10:12:54');
/*!40000 ALTER TABLE `rated_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rated_blocks_delta`
--

DROP TABLE IF EXISTS `rated_blocks_delta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rated_blocks_delta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qnID` int(11) DEFAULT NULL,
  `ansID` int(11) DEFAULT NULL,
  `blockKey` int(11) DEFAULT NULL,
  `modelRatingID` int(11) DEFAULT NULL,
  `ratedBlockID` int(11) DEFAULT NULL,
  `delta` text DEFAULT NULL,
  `result` tinyint(3) DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rated_blocks_delta`
--

LOCK TABLES `rated_blocks_delta` WRITE;
/*!40000 ALTER TABLE `rated_blocks_delta` DISABLE KEYS */;
INSERT INTO `rated_blocks_delta` VALUES (1,13,9,1,1,1,'{\"0\":{\"0\":-3,\"1\":-2}}',0,'2019-02-03 13:10:43','2019-02-03 13:10:43');
/*!40000 ALTER TABLE `rated_blocks_delta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rated_delta_set`
--

DROP TABLE IF EXISTS `rated_delta_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rated_delta_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raterID` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `rbdSets` varchar(255) DEFAULT NULL,
  `tContext` int(11) DEFAULT NULL,
  `tPerfect` int(11) DEFAULT NULL,
  `tWithin` int(11) DEFAULT NULL,
  `diffModel` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rated_delta_set`
--

LOCK TABLES `rated_delta_set` WRITE;
/*!40000 ALTER TABLE `rated_delta_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `rated_delta_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raters`
--

DROP TABLE IF EXISTS `raters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(10) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `attempt_times` int(10) unsigned DEFAULT 0,
  `inpro_testblock` varchar(100) DEFAULT NULL,
  `certBrands` text DEFAULT NULL,
  `sound_check` tinyint(1) DEFAULT 0,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raters`
--

LOCK TABLES `raters` WRITE;
/*!40000 ALTER TABLE `raters` DISABLE KEYS */;
INSERT INTO `raters` VALUES (1,NULL,'HQ','Admin User','admin@example.com','Admin','$2y$13$GQZYDvjcwv8FJjU1pZtnj.QL8PwbEPM7VbpTC1gAQOc7TH/yqWrG6',1,0,'{\"9\":{\"blockKey\":\"0\",\"datetimeStart\":\"2019-02-03 06:10:43\"}}',NULL,0,'2018-11-27','2019-02-03'),(2,NULL,NULL,'Manager User','manager@example.com','Manager','$2y$13$XYkcw73tiI5EtRaA2nclwuArm65brY9FLFtSEABJm5nH3.yW49GEa',1,0,NULL,NULL,0,'2018-11-27','2019-02-03'),(3,'JG',NULL,'Leader User','leader@example.com','Leader','$2y$13$FHh2RKPAUFd2.CVQFU5KJednBBDv6omrIKaVy8yZv8.gosuolOcYm',1,0,NULL,NULL,0,'2018-11-27','2019-02-03'),(4,'JG',NULL,'Normal User','user@example.com','Normal','$2y$13$TlSFqJH2/i01/jdAjwznueebEt7bJM1n4lO0V5n7TGo0V39FU5hSW',1,0,'{\"11\":{\"blockKey\":\"0\",\"datetimeStart\":\"2019-02-07 03:12:54\"}}','[]',0,'2018-11-27','2019-02-07');
/*!40000 ALTER TABLE `raters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_configs`
--

DROP TABLE IF EXISTS `rating_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raterID` int(11) DEFAULT NULL,
  `projectID` int(11) unsigned DEFAULT NULL,
  `test_name` varchar(255) DEFAULT NULL,
  `rating_items` text DEFAULT NULL,
  `rating_units` text DEFAULT NULL,
  `rating_blocks` text DEFAULT NULL,
  `groups` text DEFAULT NULL,
  `raters` text DEFAULT NULL,
  `model_test` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `block_settings` text DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_configs`
--

LOCK TABLES `rating_configs` WRITE;
/*!40000 ALTER TABLE `rating_configs` DISABLE KEYS */;
INSERT INTO `rating_configs` VALUES (9,NULL,5,'Level 10','{\"0\":{\"question\":\"1\",\"uid\":\"1\",\"qnType\":\"text\",\"ansType\":\"text\"},\"1\":{\"question\":\"More sugar?\",\"uid\":\"qn2\",\"qnType\":\"image\",\"ansType\":\"audio\"}}','{\"0\":{\"id\":1,\"name\":\"1\",\"items\":{\"0\":\"0\"}},\"1\":{\"id\":2,\"name\":\"2\",\"items\":{\"0\":\"1\"}}}','{\"0\":{\"name\":\"1\",\"id\":1,\"units\":{\"0\":\"0\"}},\"1\":{\"name\":\"2\",\"id\":2,\"units\":{\"0\":\"1\"}}}','{\"1\":{\"0\":{\"gID\":\"JG\",\"status\":\"\"}}}','{\"1\":{\"0\":{\"rid\":\"2\",\"status\":\"dna\",\"isLeader\":null}}}',NULL,1,NULL,'2019-01-20','2019-02-03');
/*!40000 ALTER TABLE `rating_configs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07 19:24:28
