#!/usr/bin/env bash

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Setup framework command"
export PATH=/app:$PATH

cd /app

info "Install composer"
php composer.phar -n install
info "Migrate apps"
yii migrate/up --interactive 0

