<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "rated_blocks_delta".
 *
 * @property int $id
 * @property int $qnID
 * @property int $ansID
 * @property int $blockKey
 * @property int $modelRatingID
 * @property int $ratedBlockID
 * @property string $delta
 * @property int $result
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class RatedBlocksDelta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rated_blocks_delta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qnID', 'ansID', 'blockKey', 'modelRatingID', 'ratedBlockID', 'result'], 'integer'],
            [['delta'], 'string'],
            [['datetime_created', 'datetime_updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qnID' => 'Questionn ID',
            'ansID' => 'Answer ID',
            'blockKey' => 'Block Key',
            'modelRatingID' => 'Model Rating ID',
            'ratedBlockID' => 'Rated Block ID',
            'delta' => 'Delta',
            'result' => 'Result',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->datetime_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->datetime_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    // related table
    public function getAnswer()
    {
        return $this->hasOne(Answers::className(), ['id' => 'ansID']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'qnID']);
    }

    public function getModelRating()
    {
        return $this->hasOne(ModelRatings::className(), ['id' => 'modelRatingID']);
    }

    public function getRatedBlock()
    {
        return $this->hasOne(RatedBlock::className(), ['id' => 'ratedBlockID']);
    }

    public function getDelta($unitID="", $key="")
    {
        $delta = json_decode($this->delta, true) ?? [];

        if ($unitID!=="" && isset($delta[$unitID])) {
            if ($key!=="" && isset($delta[$unitID][$key])) {
                return  $delta[$unitID][$key];
            }

            return $delta[$unitID];
        }

        return $delta;
    }

    public static function calculateDelta($blockKey, $ratedBlock, $answerModel)
    {
        $modelRating = ModelRatings::find()->where(["answerID"=>$answerModel->id])->one();

        if ($modelRating) {
            $ratedBlocksDelta = RatedBlocksDelta::find()
                                ->where([
                                    "blockKey" => $blockKey,
                                    "ansID" => $answerModel->id,
                                    "qnID" => $answerModel->qnID,
                                    "ratedBlockID" => $ratedBlock->id,
                                    "modelRatingID" => $modelRating->id
                                ])
                                ->one();
            if (!$ratedBlocksDelta) {
                $ratedBlocksDelta = new RatedBlocksDelta();
                $ratedBlocksDelta->blockKey = $blockKey;
                $ratedBlocksDelta->ansID = $answerModel->id;
                $ratedBlocksDelta->qnID = $answerModel->qnID;
                $ratedBlocksDelta->ratedBlockID = $ratedBlock->id;
                $ratedBlocksDelta->modelRatingID = $modelRating->id;
            }

            $modelRates = $modelRating->getRatings();
            $actualRates = $ratedBlock->getRatings();

            $delta = [];
            
            foreach ($actualRates as $unitKey=>$contexts) {
                $delta[$unitKey] = [];
                foreach ($contexts as $key=>$value) {
                    if (isset($modelRates[$unitKey][$key]) && $modelRates[$unitKey][$key]["rating"]!="") {
                        $delta[$unitKey][$key] = (int)$value["rating"]-(int)$modelRates[$unitKey][$key]["rating"];
                    } else {
                        $delta[$unitKey][$key]="";
                    }
                }
            }

            $ratedBlocksDelta->delta = json_encode($delta, JSON_FORCE_OBJECT);
            $ratedBlocksDelta->result = 0;
            
            if ($ratedBlocksDelta->save()) {
                return $ratedBlocksDelta;
            }
        }
    }
}
