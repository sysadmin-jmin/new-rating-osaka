<?php

namespace app\models\ProjectForms;

use Yii;
use yii\base\Model;
use yii\helpers\{Url, ArrayHelper};

class Brand extends Model {
	
	public $key;
	public $name;
	
	
	#Allow Certification By Brand Level 
	public $certNo;
	public $certByBrand;		
	public $certLimit; 
	
	public $certPerfectPerc;   //0-100
	public $certWithinPerc;
	public $certTolerance;
	public $lowScoreOption;   //drop
	
	public $attemptIntDays;
	public $attemptLimit;
	
	#Initial Training 
	public $isInTraining;
	public $inTrainNo;
	
	
	public $training_interval;  #json
	public $trainDummieNo;
	public $trainActNo; 
	
	#Quality Control
	public $qc_interval; #json
	public $qcPerfectPerc;
	public $qcTolerance;
	public $qcWithinPerc;
	

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
	return [
			[['certNo','certByBrand','certLimit','attemptIntDays','attemptLimit',
			  'isInTraining','inTrainNo',
			  'trainDummieNo','trainActNo',
			  
			  ], 'integer'],
			[['name','key','training_interval','qc_interval','lowScoreOption'],'string'], 
			
			[['certPerfectPerc','certWithinPerc','certTolerance'],'integer',"max"=>100,"min"=>0],

			[['qcPerfectPerc','qcTolerance','qcWithinPerc'],'integer',"max"=>100,"min"=>0]
			
		 ]; 
	}

 
    public function attributeLabels()
    {
        return [
			'certNo'=>'Number of Answer Sheets Used in Certification',
			'certByBrand'=>'Rater only need to be certified once per brand',
			'lowScoreOption'=> 'Option For Low Score',
			'certPerfectPerc' => 'Perfect Ratings percentage',
			'certWithinPerc' => 'Within Tolerance Rating Percentage',
			'attemptIntDays'=>"Days before next Certification Attempt",
			'attemptLimit' => "Number of attempts for certification",
			'isInTraining'=> 'Is Initial Training Required',
			'inTrainNo' => 'Number of Answer sheets per Training Session',
			'trainDummieNo' => 'Training Set Based on Actual Test',
			'trainActNo' => 'Training Set Based on Dummie Test',
			
			'qcPerfectPerc' => "% perfect to pass",
			'qcWithinPerc' => "% within tolerance to pass",
			'qcTolerance' => "Tolerance for quality control",
		];
    }	
	
	
	
    public function brandDataPath()
    {
        $path = Url::to('@app/data/projects/brand.json');
        return $path;
    }

	
	/*Gets */ 
	
	
    public function getBrandA()
    {
        $path = $this->brandDataPath();
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            return json_decode($contents, true);
        }
        return [];
    }
	
	public function getBrandList() {
		$brandA = $this->getBrandA();
		return ArrayHelper::map($brandA,'key','name');	
	}
	
	public function lowScoreOptionA() {
		$a = ['not_tolerated'=> "some big long text"];
		return $a;
	}
	

    public function find($key)
    {
	    $this->key = $key;
		$brandA = $this->getBrandA();
		
		$a = ['Brand'=>$brandA[$this->key]];
		$this->load($a);
	}
    
    public function save()
    {
        $brandA = $this->getBrandA();
		
        $brandA[$this->key] = $this->attributes;
        $path = $this->brandDataPath();
        file_put_contents($path, json_encode($brandA,JSON_FORCE_OBJECT | JSON_PRETTY_PRINT));
        return true;
    }
    
    public function deleteBrand($key)
    {
        $brandA = $this->getBrandA();
        if (array_key_exists($key, $brandA)) {
            unset($brandA[$key]);
            $path = $this->brandDataPath();
            file_put_contents($path, json_encode($brandA));
        }
        return true;
    }	
	
	//*** GETs 
	
	public function getTrainingInterval() {
		return json_encode(explode(',',$this->training_interval));
	}
	
	public function getQcInterval() {
		if ($this->qc_interval=='') return "[]";
		
		return json_encode(explode(',',$this->qc_interval));
	}	
	
}