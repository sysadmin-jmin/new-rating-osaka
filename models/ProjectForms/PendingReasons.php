<?php

namespace app\models\ProjectForms;

use app\models\Projects;
use Yii;
use yii\base\Model;
use yii\helpers\{Url, ArrayHelper};
use yii\web\NotFoundHttpException;

class PendingReasons extends Model
{
    /**
     * @var array pending reasons
     */
    protected static $reasons = [];
    
    /**
     * @var int
     */
	public $key;
    /**
     * @var string
     */
	public $text;
	
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
	return [
	        [['key'], 'integer'],
			[['text'],'string'],
		 ]; 
	}

 
    public function attributeLabels()
    {
        return [
			'text'=>'Pending reason',
		];
    }	
    
	
	/*Gets */
    
    /**
     * @param array $reasons
     * @return void
     */
    public static function setReasons(array $reasons)
    {
        static::$reasons = $reasons;
    }
    
    /**
     * @return array
     */
    public function getReasons()
    {
        return static::$reasons;
    }
    
    /**
     * @return array
     * @throws NotFoundHttpException
     */
	public function getReasonList() {
		$list = $this->getReasons();
		return array_column($list, 'text', 'key');
	}
    
    /**
     * @param int $key
     * @return string|null
     */
    public function find($key)
    {
        $list = $this->getReasons();
        if (isset($list[$key]))
            return $list[$key];
        return null;
	}
    
    /**
     * Add data
     * @param string $text
     * @return bool
     */
	public function add($text)
    {
        $list = $this->getReasons();
        
        $tmp = array_column($list, 'text');
        // has already recorded 'text' comment
        if (in_array($text, $tmp))
            return false;
        
        $list[] = ['key' => count($list), 'text' => $text];
        static::$reasons = $list;
        return true;
    }
    
    /**
     * Remove data by key
     * @param int $key
     * @return bool
     */
    public function remove($key)
    {
        $list = $this->getReasons();
        
        if (!isset($list[$key]))
            return false;
        unset($list[$key]);
        
        static::$reasons = $list;
        
        return true;
    }
	
}