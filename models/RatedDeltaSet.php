<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "rated_delta_set".
 *
 * @property int $id
 * @property int $raterID
 * @property string $type   'cert', 'training', 'pretraining',

 * @property string $rbdSets - Array of model rating sheet w/ blocks
                                0=>[sheet,blockKey]
 * @property int $tContext
 * @property int $tPerfect
 * @property int $tWithin
 * @property int $diffModel
 * @property int $status    0 - Open, 1 - Completed
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class RatedDeltaSet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rated_delta_set';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['raterID', 'tContext', 'tPerfect', 'tWithin', 'diffModel', 'status'], 'integer'],
            [['datetime_created', 'datetime_updated'], 'safe'],
            [['rbdSets'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'raterID' => 'Rater ID',
            'rbdSets' => 'Rating-block-delta sets',
            'tContext' => 'Total Count Context',
            'tPerfect' => 'Total Count Perfect ',
            'tWithin' => 'Total Count Within',
            'diffModel' => 'Diff Between Model Answer Score',
            'status' => 'Status',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->datetime_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->datetime_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    public function getRbdSets()
    {
        return json_decode($this->rbdSets, true);
    }

    public static function computeBlock($ratedblockDeltaA, $rater)
    {
        $rds = new self;
        if (count($ratedblockDeltaA)) {
            $rbdSets   = [];
            $tContext  = 0;
            $tPerfect  = 0;
            $tWithin   = 0;
            $diffModel = 0;
            foreach ($ratedblockDeltaA as $key=>$ratedblockDelta) {
                $ratedBlock = $ratedblockDelta->ratedBlock;
                $modelRating = $ratedblockDelta->modelRating;

                if ($ratedBlock && $modelRating) {
                    $type = $ratedBlock->type;
                    $modelRates = $modelRating->getRatings();
                    $actualRates = $ratedBlock->getRatings();
                    foreach ($actualRates as $unitKey=>$contexts) {
                        foreach ($contexts as $key=>$value) {
                            $tContext++;
                            if ((int)$value["rating"] == (int)$modelRates[$unitKey][$key]["rating"]) {
                                $tPerfect++;
                            }
                            if ((int)$value["rating"]+1 == (int)$modelRates[$unitKey][$key]["rating"]) {
                                $tWithin++;
                            }
                            $diffModel+= ((int)$value["rating"] - (int)$modelRates[$unitKey][$key]["rating"]);
                        }
                    }
                }
                $rbdSets[] = [$ratedblockDelta->id, $ratedblockDelta->blockKey];
            }

            $rds->rbdSets   = json_encode($rbdSets, JSON_FORCE_OBJECT);
            $rds->raterID   = $rater->id;
            $rds->type      = $type;
            $rds->tContext  = $tContext;
            $rds->tPerfect  = $tPerfect;
            $rds->tWithin   = $tWithin;
            $rds->diffModel = $diffModel;

            if ($rds->save()) {
                return $rds;
            }
        }
        return null;
    }
}
