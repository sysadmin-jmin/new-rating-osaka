<?php

namespace app\models;

use Yii;
use yii\web\Controller;

/**
 * This is the model class for table "admin_logs".
 *
 * @property int $id
 * @property int $userID
 * @property string $action
 * @property string $remark
 * @property string $data
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class AdminLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID'], 'integer'],
            [['data'], 'string'],
            [['datetime_created', 'datetime_updated'], 'safe'],
            [['action', 'remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userID' => 'User ID',
            'action' => 'Action',
            'remark' => 'Remark',
            'data' => 'Data',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->date_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Log activity
     * @param Controller $controller
     * @param string $remark
     * @param array $data
     * @return boolean
     */
    public static function log(Controller $controller, $remark="", $data=[])
    {
        $log = new self();

        if (!Yii::$app->user->isGuest) {
            $log->userID = Yii::$app->user->id;
        }

        $this->remark = $remark;
        $this->data = json_encode($data, JSON_FORCE_OBJECT);
        $this->action = $controller->getRoute();

        return $this->save();
    }
}
