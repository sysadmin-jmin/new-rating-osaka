<?php

namespace app\models;

use Yii;
use app\models\RatingConfigs;
use app\models\QuestionForms\Items;
use app\models\QuestionForms\Context;
use app\models\QuestionForms\GeneralInstruction;
use app\models\QuestionForms\Example;
use yii\helpers\Url;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property int $projectID
 * @property int $rcID
 * @property string $name
 * @property string $items
 * {"0":{"src":"","qnType":"text","ansType":"text"},"1":{"src":"","qnType":"image","ansType":"image"}, ... }
 * @property string $context
 * {
 *   "2": {  // unit key
 *     "0": { // context key
 *       "name": "Grammars",
 *       "src": "Lorem ipsum dolor sit amet.",
 *       "conType": "1to5",
 *       "srcType": "text" // text or image
 *     },
 *     ...
 *   },
 *   ...
 * }
 * @property string $genInstructions
 * {
 *   "1": { // blockID
 *     "0": { // General instruction key
 *       "name": "How to answer", // General instruction name
 *       "src": "1_0.png" // image
 *     }
 *     ...
 *   },
 *   ...
 * }
 * @property string $examples
 * {
 *   "0": { // unitKey
 *     "1": { // key
 *       "name": "Health",
 *       "src": "0_1.png"
 *     },
 *     ...
 *   },
 *   ...
 * }
 * @property string $type
 */
class Questions extends \yii\db\ActiveRecord
{
    #Set of Trait gets
    use \app\models\traits\QuestionsTrait;

    private $question_save_dir = "data/rc/";
    
    const STATUS_LOCK = 'lock';
    const STATUS_UPLOAD = 'upload';
    const STATUS_SETUP = 'setup';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rcID'], 'integer'],
            [['rcID','name'], 'required'],

            [['items', 'context', 'genInstructions', 'status','examples', 'blocks', 'units'], 'string'],

            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'projectID' => 'Project ID',
            'rcID' => 'Rc ID',
            'name' => 'Name',
            'items' => 'Items',
            'units' => 'Units',
            'blocks' => 'Blocks',
            'context' => 'Context',
            'genInstructions' => 'General Instructions',
            'examples' => 'Examples',
            'type' => 'Type',
        ];
    }
    
    
    public function statusA()
    {
        $a = [
                'setup'=>'setup',
                'upload'=>'upload',
                'lock'=>'lock',
             ];
        return $a;
    }
    
    public function getStatus()
    {
        $a = $this->statusA();
        return @$a[$this->status] ?? '';
    }
    
    
    
    // Type of Question Set
    public function typeA()
    {
        $a = [
                'actual'=>'Actual',
                'cert'=>'Certification',
                'training'=>'Pre Certification Training',
                'retraining'=>'Refresh Training',
                'qc' => 'quality control',
        ];
        
        return $a;
    }
    
    public function getType()
    {
        $a = $this->typeA();
        return $a[$this->type];
    }
    
    public function genItems()
    {
        
        // Takes Existing Set
        $rcModel = $this->rcModel;
        $items = $rcModel->getItems();
        
        $a = [];
        foreach ($items as $key => $value) {
            $a[$key] = [
                        "uid"=>$value['uid'],
                        "src"=>'',
                        'qnType'=>$value['qnType'],
                        'ansType'=>$value['ansType']
                        ];
        }
        return json_encode($a, JSON_FORCE_OBJECT);
    }

    public function genUnits()
    {
        
        // Takes Existing Set
        $rcModel = $this->rcModel;
        $units = $rcModel->getUnits();
        
        $a = [];
        foreach ($units as $key => $value) {
            $a[$key] = [

                        "name"=>$value['name'],
                        'items'=>$value['items']
                        ];
        }
        return json_encode($a, JSON_FORCE_OBJECT);
    }

    public function genBlocks()
    {
        
        // Takes Existing Set
        $rcModel = $this->rcModel;
        $blocks = $rcModel->getBlocks();
        
        $a = [];
        foreach ($blocks as $key => $value) {
            $a[$key] = [
                        "name"=>$value['name'],
                        'id'=>$value['id'],
                        'units'=>$value['units']
                        ];
        }
        return json_encode($a, JSON_FORCE_OBJECT);
    }

    // related model
    public function getRcModel()
    {
        return $this->hasOne(RatingConfigs::className(), ['id' => 'rcID']);
    }

    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'projectID']);
    }

    // get Context
    public function getUnitContexts($key=null)
    {
        $context = json_decode($this->context, true);
        if ($key!== null) {
            return isset($context[$key])?$context[$key]:[];
        }
        return $context?:[];
    }

    // get upload Dir for Question field
    public function getUploadDir($field)
    {
        $path = Url::to("@app/data/rc/".$this->rcModel->id."/question/".$this->id."/".$field);

        if (!file_exists($path)) {
            if (!mkdir($path, 0777, true)) {
                return false;
            }
        }

        return $path;
    }

    // get upload dir for items
    public function getItemsDir()
    {
        return $this->getUploadDir("items");
    }

    // get upload dir for context
    public function getContextDir()
    {
        return $this->getUploadDir("context");
    }

    // get context Path()
    public function getContextPath($unitKey, $key)
    {
        // check if context still use old Path
        $oldPath = $this->getContextDir()."/".$unitKey."/".$key.".png";

        if (file_exists($oldPath)) {
            return $oldPath;
        }

        return $this->getContextDir()."/".$unitKey."_".$key.".png";
    }

    /**
     * Get upload dir for General Instructions
     * @return string directory path
     */
    public function getGenInstructionsDir()
    {
        return $this->getUploadDir("genInstructions");
    }

    /**
     * Get upload dir for Examples
     * @return string directory path
     */
    public function getExamplesDir()
    {
        return $this->getUploadDir("examples");
    }
}
