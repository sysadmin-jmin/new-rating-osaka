<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "model_ratings".
 *
 * @property int $id
 * @property int $userID
 * @property int $rcID
 * @property int $answerID
 * @property string $ratings
 * @property string $type
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class ModelRatings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'model_ratings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userID', 'rcID', 'answerID'], 'integer'],
            [['ratings'], 'string'],
            [['datetime_created', 'datetime_updated'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userID' => 'User ID',
            'rcID' => 'Rc ID',
            'answerID' => 'Answer ID',
            'ratings' => 'Ratings',
            'type' => 'Type',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->datetime_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->datetime_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    // related table
    public function getRcModel()
    {
        return $this->hasOne(RatingConfigs::className(), ['id' => 'rcID']);
    }

    public function getUser()
    {
        return $this->hasOne(Raters::className(), ['id' => 'userID']);
    }

    public function getAnswer()
    {
        return $this->hasOne(Answers::className(), ['id' => 'answerID']);
    }

    // --- end related table -- //

    public static function saveRating($data)
    {
        $modelRating = self::find()->where(["rcID"=>$data["rcID"], "answerID"=>$data["answerID"]])->one();

        if (!$modelRating) {
            $modelRating = new self();

            $modelRating->answerID = $data['answerID'];
            $modelRating->rcID = $data['rcID'];

            $answerModel = $modelRating->answer;

            $modelRating->type = $answerModel->type;

            if (!Yii::$app->user->isGuest) {
                $modelRating->userID = Yii::$app->user->id;
            }
        }

        $ratings = [];
        foreach ($data["rating"] as $unitKey=>$contexts) {
            $ratings[$unitKey] = [];
            foreach ($contexts as $contextKey=>$ctx) {
                $ratings[$unitKey][$contextKey] = ["rating"=>"", "status"=>"open"];
                if ($ctx!=="") {
                    $ratings[$unitKey][$contextKey]['rating'] = $ctx;
                    $ratings[$unitKey][$contextKey]['status'] = $ctx > 0 ? "closed" : "open";
                }
            }
        }

        $modelRating->ratings = json_encode($ratings, JSON_FORCE_OBJECT);

        return $modelRating->save();
    }

    public function getRatings($unitKey="")
    {
        $ratings = json_decode($this->ratings, true);


        if ($unitKey!=="") {
            return isset($ratings[$unitKey]) ? $ratings[$unitKey] : [];
        }

        return $ratings;
    }
}
