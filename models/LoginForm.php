<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Incorrect username or password.');
            } elseif (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password. attempts no.' . ($user->attempt_times+1));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            // Success
            if ($user instanceof Raters)
                $user->resetAttemptTimes();

            return Yii::$app->user->login($this->getUser(), 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Raters|User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Raters::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * @param string[]|string $attributeNames attribute name or list of attribute names that should be validated.
     * If this parameter is empty, it means any attribute listed in the applicable
     * validation rules should be validated.
     * @param bool $clearErrors whether to call [[clearErrors()]] before performing validation
     * @return bool whether the validation is successful without any error.
     * @throws InvalidArgumentException
     * @see \yii\base\Model
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if ($clearErrors) {
            $this->clearErrors();
        }

        $user = $this->getUser();
        //ob_end_clean();var_dump($attributeNames);exit;
        if ($user instanceof Raters && $user->status != 1) {
            $this->addError('username', 'Your account has been locked.');
            return false;
        }

        if (!parent::validate($attributeNames, false)) {
            if ($user instanceof Raters) {
                // TODO Change failure count
                $user->failureLogin(5);
            }
            return false;
        }

        return true;
    }
}
