<?php

namespace app\models\AnswerForms;

use Yii;
use yii\base\Model;

class Create extends Model
{
    public $rcID;
    public $qnID;
    public $key;
    public $index;
	public $answerFile;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['rcID','qnID','key','index'], 'integer'],
			[['answerFile'],'file'],
		];
    }

 
    public function attributeLabels()
    {
        return [];
    }


}
