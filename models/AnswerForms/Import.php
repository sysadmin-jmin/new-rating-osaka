<?php

namespace app\models\AnswerForms;

use Yii;
use yii\base\Model;

class Import extends Model
{
    public $qnID;
	public $dataFile;
	public $answerZipFile;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['qnID'], 'integer'],
			[['dataFile'],'file'],
			[['answerZipFile'],'file']
		];
    }

 
    public function attributeLabels()
    {
        return [
            'dataFile'=> 'CSV of Answers',
			'answerZipFile' => 'Zip File of Answers in folder',
        ];
    }


}
