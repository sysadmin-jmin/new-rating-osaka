<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\db\Expression;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $brand
 * @property string $groups => {"0": "JG", "1": "RT", ...}
 * @property string $name
 * @property tinyint $skipblock   - allow rater to skip a block
 * @property string $starttime
 * @property string $endtime
 * @property int $pending_mode
 * @property string $pending_reasons
 * @property string $low_rating_reasons
 * @property string $date_created
 * @property string $date_updated
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    use \app\models\traits\projects\BrandTrait;
    
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['brand', 'name'], 'string', 'max' => 255],
            [['groups', 'pending_reasons',   'low_rating_reasons'], 'string'],
            [['skipblock', 'pending_mode'],'integer'],
            [['starttime', 'endtime'], 'date', 'format' => 'yyyy-MM-dd HH:mm'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand' => 'Brand',
            'groups' => 'Groups',
            'name' => 'Name',
            'skipblock' => 'Skip A Block',
            'starttime' => 'Start time',
            'endtime' => 'End time',
            'pending_mode' => 'Pending_mode',
            'pending_reasons' => 'Pending reasons',
            'low_rating_reasons' => 'Low Rating reasons',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }
    
    public static function getName($id)
    {
        return self::findOne($id)->name;
    }
        


        
    // get groups on project
    public function getGroups()
    {
        return json_decode($this->groups, true)?:[];
    }
    
    /**
     * add group to project
     * @param string $code
     * @return Projects|bool
     */
    public function addGroup($code)
    {
        $groups = $this->getGroups();
        
        if (in_array($code, $groups)) {
            return false;
        }
        $groups[] = $code;

        $this->groups = json_encode($groups, JSON_FORCE_OBJECT);
        return $this;
    }
    
    /**
     * remove group from project
     * @param string $code
     * @return Projects
     */
    public function deleteGroup($code)
    {
        $groups = $this->getGroups();

        $key = array_search($code, $groups);
        if (is_numeric($key)) {
            unset($groups[$key]);
        }

        $this->groups = json_encode($groups, JSON_FORCE_OBJECT);
        return $this;
    }


    /**
     * End_date - Start_date
     * @return int days
     */
    public function getDateDuration()
    {
        $start = strtotime(@$this->starttime) ?? time();
        $end = @strtotime($this->endtime) ?? time();
        return floor(($end-$start) / 86400);
    }

    /**
     * Today - Start_date
     * @return int days
     */
    public function getElapsedDaysSinceStart()
    {
        $start = strtotime(@$this->starttime) ?? time();
        return floor((time()-$start) / 86400);
    }


    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->date_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * @param int|null $index ;default=null
     * @return array|string
     * @throws \OutOfRangeException
     */
    public function getPendingReasons($index = null)
    {
        $reasons = json_decode($this->pending_reasons, true) ?? [];
        
        if (null == $index) {
            return $reasons;
        }
        
        if (isset($reasons[$index])) {
            return $reasons[$index];
        }
        
        throw new \OutOfRangeException('Incorrect index specified in pending_reasons');
    }
    
    /**
     * @param array $reasons
     * return Projects
     */
    public function setPendingReasons(array $reasons)
    {
        $this->pending_reasons = json_encode($reasons, JSON_FORCE_OBJECT);
        return $this;
    }

    public function getLowRatingReasons()
    {
        return json_decode($this->low_rating_reasons, true) ?? [];
    }

    public function addLowRatingReasons($text)
    {
        $reasons = $this->getLowRatingReasons();

        $reasons[] = $text;

        $this->low_rating_reasons = json_encode($reasons, JSON_FORCE_OBJECT);

        return $this->save();
    }

    public function removeLowRatingReasons($key)
    {
        $reasons = $this->getLowRatingReasons();

        unset($reasons[$key]);

        $reasons = array_values($reasons);

        $this->low_rating_reasons = json_encode($reasons, JSON_FORCE_OBJECT);

        return $this->save();
    }
}
