<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

/**
 * This is the model class for table "raters".
 *
 * @property int $id
 * @property string $group
 * @property string $company
 * @property string $name
 * @property string $email
 * @property string $role
 * @property string $password_hash
 * @property int $status
 * @property int $attempt_times
 * @property Json $inpro_testblock => { "(int)$answerID" : {"blockKey": "(int)$blockKey", "datetimeStart": "(datetime)Y-m-d H:i:s"} }
 * @property int $sound_check
 * @property int $certBrands =>  
 {
  "key": "no|yes"
 }
      
 
 * @property string $date_created
 * @property string $date_updated
 */
class Raters extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    use \app\models\traits\GroupsTrait;
    use \app\models\traits\CompanyTrait;
	use \app\models\traits\raters\CertBrandsTrait;

    private $customRules =[];
    
    const GROUP_DATA_PATH = "data/projects/group.json";
    const COMPANY_DATA_PATH = "data/projects/company.json";
    const SCENARIO_SEARCH = "search";
	
	const CERTBRAND_NO = "no";
	const CERTBRAND_YES = "yes";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'raters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge($this->customRules, [
            [['email', 'role'], 'required'],
            [['name'],'required','except' => 'search'],
            [['date_created', 'date_updated'], 'safe'],
            [['name', 'email', 'role', 'password_hash'], 'string', 'max' => 255],
            [["status", 'attempt_times'], "integer"],
            [['sound_check'], 'boolean'],
            [["inpro_testblock", "group", "certBrands", "company"], "string"]
        ]);
    }

    public function setCustomRules($rules)
    {
        $this->customRules = $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group' => 'Group',
            'company' => 'Company',
            'name' => 'Name',
            'email' => 'Email',
            'role' => 'Role',
            'status' => "Status",
            'attempt_times' => 'Attempt times',
            'password_hash' => 'Password',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields["status"] = function () {
            return $this->getStatus();
        };

        unset($fields["password_hash"]);


        return $fields;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->date_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    public function statusA()
    {
        $a = [
                0 => 'Suspended',
                1 => 'Live',
                ];
        return $a;
    }

    public function getStatus()
    {
        $a = $this->statusA();
        $status = @$a[$this->status] ?? 'unset';
        return $status;
    }
    

    /**
     * @param mixed|null $value
     * @return array
     */
    public function getRole($value = null)
    {
        $map = [
            ''          => '',
            'Admin'     => 'Rating Administrator',
            'Manager'   => 'Rater Manager',
            'Leader'    => 'Lead Rater',
            'Normal'    => 'Rater',
        ];

        if ($value == null) {
            return $map;
        }
        return $map[$value] ?? $value;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        throw new NotSupportedException('"getAuthKey" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException('"validateAuthKey" is not implemented.');
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setInproTestBlock($answerID, $blockKey)
    {
        $inproTestBlock = [
            $answerID =>[
                "blockKey" => $blockKey,
                "datetimeStart" => date("Y-m-d H:i:s")
            ]
        ];
        $this->inpro_testblock = json_encode($inproTestBlock, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function completeInproTestBlock()
    {
        $this->inpro_testblock = null;
        return $this->save();
    }

    /**
     * @return array
     */
    public function getInproTestBlock()
    {
        return json_decode($this->inpro_testblock, true);
    }
	
	

	
	

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Raters::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params))) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'company' => $this->company,
            'group' => $this->group,
            'role' => $this->role,
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }


    /**
     * Increment login attempt counts
     * @param int $allowFailCount
     */
    public function failureLogin($allowFailCount)
    {
        $this->attempt_times++;
        // when over $allowFailCount
        if ($this->attempt_times >= $allowFailCount)
            $this->status = 0;
        $this->save();
    }

    /**
     * Reset login attempt counts
     */
    public function resetAttemptTimes()
    {
        $this->attempt_times = 0;
        $this->save();
    }

}
