<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "rated_block_training".
 *
 * @property int $id
 * @property int $projectID
 * @property int $qnID
 * @property int $ansID
 * @property int $userID
 * @property int $blockKey
 * @property string $ratings
 *      {
 *        "0": {        // rc unitKey
 *          "0": null,  // context key
 *          "1": null
 *        },
 *        "2": {
 *          "0": null
 *        }
 *      }
 * @property string $datetime_start
 * @property string $datetime_end
 * @property int $time_spent
 * @property int $is_actual
 * @property string $unit_status
 *      {
 *        "0": {            // rc unitKey
 *          "status": 0,    // 0=pending, 1=in progress, 2=complete
 *          "comments": ""  // comments
 *        },
 *        "2": {
 *          "status": 1,
 *          "comments": ""
 *        }
 *      }
 * @property string $block_status 0=pending, 1=in progress, 2=complete
 * @property string $type
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class RatedBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rated_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['projectID', 'qnID', 'ansID', 'userID', 'blockKey', 'time_spent', 'is_actual', 'block_status'], 'integer'],
            [['ratings', 'unit_status', 'type'], 'string'],
            [['datetime_start', 'datetime_end', 'datetime_created', 'datetime_updated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'projectID' => 'Project ID',
            'qnID' => 'Qn ID',
            'ansID' => 'Ans ID',
            'userID' => 'User ID',
            'blockKey' => 'Block Key',
            'ratings' => 'Ratings',
            'datetime_start' => 'Datetime Start',
            'datetime_end' => 'Datetime End',
            'time_spent' => 'Time Spent',
            'is_actual' => 'Is Actual',
            'unit_status' => 'Unit Status',
            'block_status' => 'Block Status',
            'type' => 'Type',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->datetime_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->datetime_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    // related table
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'projectID']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Questions::className(), ['id' => 'qnID']);
    }

    public function getRcModel()
    {
        return $this->question ? $this->question->rcModel : null;
    }

    public function getUser()
    {
        return $this->hasOne(Raters::className(), ['id' => 'userID']);
    }

    public function getAnswer()
    {
        return $this->hasOne(Answers::className(), ['id' => 'ansID']);
    }

    public function getRatedBlocksDelta()
    {
        return $this->hasOne(RatedBlocksDelta::className(), ["ratedBlockID" => "id"]);
    }

    public function getBlock()
    {
        $blocks = $this->rcModel ? $this->rcModel->getBlocks() : [];

        return $blocks[$this->blockKey] ?? [];
    }

    public function getRatings($unitID="", $key="")
    {
        $ratings = json_decode($this->ratings, true) ?? [];

        if ($unitID!=="" && isset($ratings[$unitID])) {
            if ($key!=="" && isset($ratings[$unitID][$key])) {
                return  $ratings[$unitID][$key];
            }

            return $ratings[$unitID];
        }

        return $ratings;
    }

    public function setRatings($value, $unitID, $key)
    {
        $ratings = $this->getRatings();

        if (isset($ratings[$unitID][$key])) {
            $ratings[$unitID][$key] = $value;
        }

        $this->ratings = json_encode($ratings, JSON_FORCE_OBJECT);
    }

    public function getUnitStatus($unitID="")
    {
        $unitStatus = json_decode($this->unit_status, true) ?? [];

        if ($unitID !== "") {
            return $unitStatus[$unitID] ?? [];
        }

        return $unitStatus;
    }

    public function setUnitStatus($data, $unitID)
    {
        $unitStatus = $this->getUnitStatus();
        if (isset($unitStatus[$unitID])) {
            $unitStatus[$unitID]["status"] = $data["status"];
            $unitStatus[$unitID]["comments"] = $data["comments"];
        }
        $this->unit_status = json_encode($unitStatus, JSON_FORCE_OBJECT);

        return $this->save();
    }

    public static function prepareBlockForRating($blockKey, $answerModel, $rater)
    {
        $rcModel = $answerModel->rcModel;

        $ratedBlock = self::find()->
                        where([
                            "blockKey" => $blockKey,
                            "ansID" => $answerModel->id,
                            "qnID" => $answerModel->qnID,
                            "projectID" => $rcModel->projectID
                        ])->one();

        if (!$ratedBlock) {
            $ratedBlock = new self();
            $ratedBlock->blockKey = $blockKey;
            $ratedBlock->ansID = $answerModel->id;
            $ratedBlock->qnID = $answerModel->qnID;
            $ratedBlock->projectID = $rcModel->projectID;
            $ratedBlock->datetime_start = date("Y-m-d H:i:s");
            $ratedBlock->is_actual = ($answerModel->type==Answers::TYPE_ACTUAL) ? 1 : 0;
            $ratedBlock->type = $answerModel->type;

            $questionModel = $answerModel->questionModel;

            $block=$rcModel->getBlocks($blockKey);
            $units= $block["units"];
            $unitStatus = [];
            $ratings = [];

            foreach ($units as $unitID) {
                $unitStatus[$unitID] = ["status"=>1, "comments"=>""];
                $unitContext = $questionModel->getUnitContexts($unitID);
                $ratings[$unitID] = [];
                foreach ($unitContext as $conKey =>$context) {
                    $ratings[$unitID][$conKey] = ["rating"=>"", "remark"=>""];
                }
            }

            $ratedBlock->unit_status = json_encode($unitStatus, JSON_FORCE_OBJECT);
            $ratedBlock->ratings = json_encode($ratings, JSON_FORCE_OBJECT);
        }

        $ratedBlock->block_status = 1;
        $ratedBlock->userID = $rater->id;

        if ($ratedBlock->save()) {
            return $ratedBlock;
        }

        return null;
    }

    public static function submitRating($data, $blockKey, $answerModel, $rater)
    {
        $rcModel = $answerModel->rcModel;
        $ratedBlock = self::find()->
                        where([
                            "blockKey" => $blockKey,
                            "ansID" => $answerModel->id,
                            "qnID" => $answerModel->qnID,
                            "projectID" => $rcModel->projectID
                        ])->one();

        if ($ratedBlock) {
            $ratedBlock->userID = $rater->id;
            $unitID = $data["id"];
            $unitStatus = $ratedBlock->getUnitStatus($unitID);

            $unitStatus["comments"] = $data["comments"];
            if (empty($data["pending"])) {
                $unitStatus["status"] = 2;
            } else {
                $unitStatus["status"] = 0;
            }
            $ratedBlock->setUnitStatus($unitStatus, $unitID);

            $contexts = $data["context"];

            foreach ($contexts as $key=>$value) {
                $ratedBlock->setRatings($value, $unitID, $key);
            }

            if ($ratedBlock->save()) {
                return $ratedBlock;
            }
        }

        return null;
    }

    public static function finishRating($blockKey, $answerModel, $rater)
    {
        $rcModel = $answerModel->rcModel;
        $ratedBlock = self::find()->
                        where([
                            "blockKey" => $blockKey,
                            "ansID" => $answerModel->id,
                            "qnID" => $answerModel->qnID,
                            "projectID" => $rcModel->projectID
                        ])->one();
        if ($ratedBlock) {
            $unitStatus = $ratedBlock->getUnitStatus();

            $block_status = 2;
            foreach ($unitStatus as $key=>$unit) {
                if ($unit["status"]=="0") {
                    $block_status=0;
                }
            }

            $ratedBlock->block_status = $block_status;
            $ratedBlock->datetime_end = date("Y-m-d H:i:s");

            $ratedBlock->time_spent = strtotime($ratedBlock->datetime_end) - strtotime($ratedBlock->datetime_start);

            if ($ratedBlock->save()) {
                $rater->completeInproTestBlock();
                return $ratedBlock;
            }
        }

        return null;
    }
}
