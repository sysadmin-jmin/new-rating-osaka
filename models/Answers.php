<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

use Faker;

/**
 * This is the model class for table "tests".
 *
 * @property int $id
 * @property int $rcID
 * @property int $examineeID
 * @property string $folderPath
 * @property string $items
 * @property string $units = {"0":{"status":"open/closed/pending"}}
 * @property string $block
 *		{"0":{"status":"open/closed/inpro/pending"}}
 * @property string $assign : {"123":{"<raters.id>":<raters.id>,"name":<Rater Name>},...}
 * @property string $type
 * @property int $sheet_status
 * @property string $datetime_created
 * @property string $datetime_updated
 */
class Answers extends \yii\db\ActiveRecord
{
    const SHEETS_STATUS_OPEN = 'open';
    const SHEETS_STATUS_RATED = 'rated';
    const SHEETS_STATUS_INPROCESS = 'inprocess';
    const SHEETS_STATUS_PENDING = 'pending';
    const SHEETS_STATUS_PARK = 'park';

    const TYPE_ACTUAL = 'Actual';
    const TYPE_CERT = 'Cert';
    const TYPE_TRIAL = 'Training';
    const TYPE_QC = 'QC';
    const TYPE_RETRAING = 'Retraining';

    const ISMODEL_MODEL = 1;
    const ISMODEL_ACTUAL = 0;


    private $_imageDir = 'data/tests';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rcID', 'qnID', 'examineeID'], 'integer'],
            [['isModel'], 'boolean'],
            [['datetime_created', 'datetime_updated'], 'safe'],
            [['folderPath', 'items', 'units', 'type', 'blocks', 'sheet_status', 'assign'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rcID' => 'Ratingconfig ID',
            'qnID' => 'Question Set ID',
            'examineeID' => 'Examinee ID',
            'folderPath' => 'Folder Path',
            'items' => 'Items',
            'units' => 'Units',
            'isModel' => 'Model Answer',
            'type' => 'Type',
            'blocks' => 'Blocks',
            'assign' => 'Assign',
            'sheet_status' => 'Answer Sheet Status',
            'datetime_created' => 'Datetime Created',
            'datetime_updated' => 'Datetime Updated',
        ];
    }
    
    public function getIsModelList()
    {
        $a = [
                self::ISMODEL_ACTUAL => 'Actual',
                self::ISMODEL_MODEL => 'Model',
              ];
        return $a;
    }
    

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->datetime_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->datetime_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }

    public function createFolderPath()
    {
        $folderPath = $this->_imageDir.DIRECTORY_SEPARATOR.$this->id.DIRECTORY_SEPARATOR."answer";
        //
        if (!file_exists(APP_PATH.DIRECTORY_SEPARATOR.$folderPath)) {
            if (!mkdir(APP_PATH.DIRECTORY_SEPARATOR.$folderPath, 0777, true)) {
                $this->addError("folderPath", 'Unable to create upload directory. Please check permission to write directory');
                return false;
            }
        }
        $this->folderPath = $folderPath;
        return true;
    }

    
    public function generateBlocks($qnBlockA)
    {
        $blockA = [];
        foreach ($qnBlockA as $key=>$block) {
            $blockA[$key] = ['status'=>'open'];
        }
        return json_encode($blockA, JSON_FORCE_OBJECT);
    }
    
    public function generateUnits($qnUnitA)
    {
        $unitA = [];
        foreach ($qnUnitA as $key=>$unit) {
            $unitA[$key] = ['status'=>'open'];
        }
        return json_encode($unitA, JSON_FORCE_OBJECT);
    }
    

    
    //**********gets
    
    public function getUnits()
    {
        return json_decode($this->units, true);
    }

    public function getBlocks()
    {
        return json_decode($this->blocks, true);
    }

    public function getItems()
    {
        return json_decode($this->items, true);
    }

    // related model
    public function getRcModel()
    {
        return $this->hasOne(RatingConfigs::className(), ['id' => 'rcID']);
    }

    public function getQuestionModel()
    {
        return $this->hasOne(Questions::className(), ['id' => 'qnID']);
    }

    // Find A random block
    public static function findByBlockStatus($status = "open", $projectID='', $blockID='')
    {
        $test = self::find()
            ->with("rcModel")
            ->joinWith(["rcModel"])
            ->where("(JSON_SEARCH(blocks, 'one', '".$status."', NULL, '$**.status' ) is not NULL)")
            ->andFilterWhere(['rating_configs.projectID'=>$projectID]);
            

        // if blockID is specified then get test which have blockID
        if ($blockID) {
            $test->andWhere("( JSON_SEARCH( JSON_KEYS( blocks ), 'one', '".$blockID."' ) is not NULL )");
        }
        
        $test->orderBy("rand()");

        return $test->one();
    }
  
    // Block Functions
    public function updateBlockStatus($blockKey, $status)
    {
        if ($status =="closed") {
            //check all there are no unit pending
            $unitA = $this->getUnits();
            foreach ($unitA as $unit) {
                //if there is one unit status=pending then block status=pending
                if ($unit["status"]=="pending") {
                    $status="pending";
                    break;
                }
            }
        }

        $blockA = $this->getBlocks();
        $blockA[$blockKey]["status"] = $status;
        $this->blocks = json_encode($blockA, JSON_FORCE_OBJECT);
        return $this->save(false);
    }

    // Gets Block Id by status
    public function getBlockByStatus($status)
    {
        $blocks = $this->getBlocks();
        $blockID = null;

        foreach ($blocks as $key=>$block) {
            if ($block["status"]==$status) {
                $blockID= $key;
                break;
            }
        }

        return $blockID;
    }

    public function getOpenBlockId()
    {
        return $this->getBlockByStatus("open");
    }

    public function updateUnitStatus($unitKey, $status)
    {
        $units = $this->getUnits();
        $units[$unitKey]["status"] = $status;
        $this->units = json_encode($units, JSON_FORCE_OBJECT);

        return $this->save(false);
    }
  
    public function getItemImage($key)
    {
        $items = $this->getItems();
        if (!isset($items[$key])) {
            throw new Exception('Item key does not found');
        }
        $filename = $items[$key]['src'];
        return Url::to("@webroot/".$this->folderPath."/{$filename}");
    }

    /**
     * @param string $key
     * @return string filepath
     */
    public function getItemAudio($key)
    {
        $items = $this->getItems();
        if (!isset($items[$key])) {
            throw new Exception('Item key does not found');
        }
        $filename = $items[$key]['src'];

        return Url::to("@webroot/".$this->folderPath."/{$filename}");
    }
    
    // Test
    
    public function createAnswerImg($answerID, $key)
    {
        $faker = Faker\Factory::create();
       
        gc_collect_cycles();
        $str = wordwrap($faker->realText(350), 50, "\n");
        
        $w = 450;
        $h = 260;
 
        $font_path = Url::to('@webroot/web/fonts/arial.ttf');
        
        if (!$this->folderPath) {
            $this->createFolderPath();
        }

        #refix this
        $image_folder = Url::to("@webroot/".$this->folderPath);
        $image_path = $image_folder."/{$key}.png";
        
        $palette = new \Imagine\Image\Palette\RGB();
        $image = \yii\imagine\Image::getImagine()->create(new \Imagine\Image\Box($w, $h), $palette->color([252, 248, 227], 100));

        $text = $str;

        $textHeader = "AnswerID: {$answerID} , Answer {$key}";

        $h_d = 30;
        $image = \yii\imagine\Image::text($image, $textHeader, $font_path, [10, $h_d], ["size"=>20, "color" =>"000"]);
        
        $h_d = $h_d + 50;
        $image = \yii\imagine\Image::text($image, $text, $font_path, [10, $h_d], ["size"=>12, "color" =>"000"]);

        try {
            $image->save($image_path);
        } catch (\Exception $e) {
            $this->addError("folderPath", 'Unable to write files.');
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
     
    public function getType()
    {
        $a = self::getTypeList();
        return $a[$this->type];
    }
     
     
    public static function getTypeList()
    {
        return [
            self::TYPE_RETRAING => ucfirst(self::TYPE_RETRAING),
            self::TYPE_CERT => ucfirst(strtolower(self::TYPE_CERT)),
            self::TYPE_QC => ucfirst(strtolower(self::TYPE_QC)),
            self::TYPE_TRIAL => ucfirst(strtolower(self::TYPE_TRIAL)),


            self::TYPE_ACTUAL => ucfirst(strtolower(self::TYPE_ACTUAL)),
            
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::SHEETS_STATUS_OPEN => ucfirst(strtolower(self::SHEETS_STATUS_OPEN)),
            self::SHEETS_STATUS_PENDING => ucfirst(strtolower(self::SHEETS_STATUS_PENDING)),
            self::SHEETS_STATUS_INPROCESS => ucfirst(strtolower(self::SHEETS_STATUS_INPROCESS)),
            self::SHEETS_STATUS_RATED => ucfirst(strtolower(self::SHEETS_STATUS_RATED)),
            self::SHEETS_STATUS_PARK => ucfirst(strtolower(self::SHEETS_STATUS_PARK)),
        ];
    }
    
    public function getRc()
    {
        return $this->hasOne(RatingConfigs::className(), ['id' => 'rcID']);
    }
    
    public function getQn()
    {
        return $this->hasOne(Questions::className(), ['id' => 'qnID']);
    }

    /**
     *
     * @param int $id Answer.id
     * @param int $idx index of in items
     * @return string Url for output raw-content
     */
    public function getRenderRawUrl($idx)
    {
        return sprintf(Url::to('@web/admin/answers/render-raw?id=%d&idx=%d'), $this->id, $idx);
    }
    
    /**
     * @return array
     */
    public function getAssign()
    {
        $json = json_decode($this->assign,true);
        if (!is_array($json))
            return [];
        return $json;
    }
}
