<?php

namespace app\models\traits;

use app\models\RatingConfigForms\Groups;
use app\models\RatingConfigForms\Raters;
use app\models\RatingConfigForms\Items;

// trait for app/models/RatingConfigs
trait RatingConfigsTrait
{
    /**
     * get rating items
     */
    public function getItems()
    {
        if ($this->rating_items==null) {
            return "{}";
        }
        
        
        return json_decode($this->rating_items, true)?:[];
    }

    /**
     * Find or create new Rating config Item
     * @param integer $key
     * @return app\models\RatingConfigForms\Items object
     */
    public function findItem($key)
    {
        $itemA = $this->getItems();

        $item = isset($itemA[$key]) ? $itemA[$key] : [];

        $item["rcID"] = $this->id;
        $item["key"] = $key;

        return new Items($item);
    }

    /**
     * Assign Item to Unit
     * @param integer $itemKey
     * @param integer $unitKey
     * @return boolean
     */
    public function assignItemToUnit($itemKey, $unitKey)
    {
        $unitA = $this->getUnits();
        
        if (isset($unitA[$unitKey])) {
            if (!in_array($itemKey, $unitA[$unitKey]["items"])) {
                $unitA[$unitKey]["items"][] = $itemKey;
            } else {
                $this->addError("rating_units", "Item already assigned to Unit.");
                return false;
            }
        } else {
            $this->addError("rating_units", "Unit not found.");
            return false;
        }

        $this->rating_units = json_encode($unitA, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Remove Item From Unit
     * @param integer $itemKey
     * @param integer $unitKey
     * @return boolean
     */
    public function unassignItemFromUnit($itemKey, $unitKey)
    {
        $unitA = $this->getUnits();
        
        if (isset($unitA[$unitKey])) {
            if (in_array($itemKey, $unitA[$unitKey]["items"])) {
                $unitA[$unitKey]["items"] = array_diff($unitA[$unitKey]["items"], [$itemKey]);
                //reset the keys;
                $unitA[$unitKey]["items"] = array_values($unitA[$unitKey]["items"]);
            } else {
                $this->addError("rating_units", "Item not found.");
                return false;
            }
        } else {
            $this->addError("rating_units", "Unit not found.");
            return false;
        }

        $this->rating_units = json_encode($unitA, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * get rating units
     */
    public function getUnits()
    {
        return json_decode($this->rating_units, true)?:[];
    }

    /**
     * get next Units on Block ID
     * @param integer $currentUnit
     * @param integer $blockID
     * @return integer
     */
    public function getNextUnit($currentUnit, $blockID)
    {
        $blockA = $this->getBlocks();

        $unit = $blockA[$blockID]['units'];

        $nextUnit = current($unit);

        if ($nextUnit==$currentUnit) {
            $nextUnit = next($unit);
        } else {
            while ($nextUnit = next($unit) && $nextUnit!==false) {
                if ($nextUnit==$currentUnit) {
                    $nextUnit = next($unit);
                    break;
                }
            }
        }

        return $nextUnit;
    }

    /**
     * get previous Units on Block ID
     * @param integer $currentUnit
     * @param integer $blockID
     * @return integer
     */
    public function getPrevUnit($currentUnit, $blockID)
    {
        $blockA = $this->getBlocks();

        $unit = $blockA[$blockID]['units'];

        $prevUnit = current($unit);

        while ($prevUnit = next($unit)) {
            if ($prevUnit==$currentUnit) {
                $prevUnit = prev($unit);
                break;
            }
        }

        return $prevUnit;
    }

    public function assignUnitToBlock($unitKey, $blockKey)
    {
        $blockA = $this->getBlocks();

        if (isset($blockA[$blockKey])) {
            $blockA[$blockKey]["units"][]= $unitKey;
        }

        $this->rating_blocks = json_encode($blockA, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function unassignUnitFromBlock($unitKey, $blockKey)
    {
        $blockA = $this->getBlocks();

        if (isset($blockA[$blockKey])) {
            if (in_array($unitKey, $blockA[$blockKey]["units"])) {
                $blockA[$blockKey]["units"] = array_diff($blockA[$blockKey]["units"], [$unitKey]);
                $blockA[$blockKey]["units"] = array_values($blockA[$blockKey]["units"]);
            }
        }

        $this->rating_blocks = json_encode($blockA, JSON_FORCE_OBJECT);
        return $this->save();
    }


    /**
     * get rating blocks
     * @return array of block
     */
    public function getBlocks($key="")
    {
        $blocks = json_decode($this->rating_blocks, true)?:[];

        if ($key!=="") {
            return $blocks[$key] ?? [];
        }
        return $blocks;
    }

    /**
     * get single block array by block id
     * @param integer $id
     * @return block
     */
    public function getBlock($id)
    {
        $blocksA = $this->getBlocks();

        $blockKey = $this->getBlockKey($id);

        return isset($blocksA[$blockKey]) ? $blocksA[$blockKey] : [];
    }

    /**
     * get single block array by block id
     * @param integer $id
     * @return block
     */
    public function getBlockKey($id)
    {
        $blocksA = $this->getBlocks();

        $blockKey = array_search($id, array_column($blocksA, "id"));

        return $blockKey;
    }

    /**
     * get model_test
     * @return array
     */
    public function getModelTest()
    {
        return json_decode($this->model_test, true)?:[];
    }

    /**
     * check all Units have items
     * @return boolean
     */
    public function isAllUnitsHasItems()
    {
        $units = $this->units;
        if (count($units)) {
            foreach ($units as $unit) {
                if (!count($unit['items'])) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * get array or single groups
     * @param integer $blockKey
     * @return array or single group
     */
    public function getGroups($blockKey=null)
    {
        $groups = json_decode($this->groups, true)?:[];
        if ($blockKey!==null) {
            return isset($groups[$blockKey]) ?  $groups[$blockKey] : [];
        }

        return $groups;
    }

    /**
     * Remove group from blocks
     * @param integer $blockKey
     * @return boolean
     */
    public function removeGroup($blockKey)
    {
        $groupA = $this->getGroups();

        if (isset($groupA[$blockKey])) {
            unset($groupA[$blockKey]);
            $groupA = array_values($groupA);
            $this->groups = json_encode($groupA, JSON_FORCE_OBJECT);
            return $this->save();
        }
        return true;
    }

    // check all Block have Units
    public function isAllBlocksHasUnit()
    {
        $blocks = $this->blocks;
        if (count($blocks)) {
            foreach ($blocks as $block) {
                if (!count($block['units'])) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public static function getAllQuailfiedBlocks($raterID, $projectID)
    {
        $rcModels = self::find()->where(["projectID"=>$projectID])->all();

        $rcBlocks = [];

        foreach ($rcModels as $rc) {
            if ($blockIDs = $rc->getQualifiedBlock($raterID)) {
                $rcBlocks[$rc->id] = $blockIDs;
            }
        }

        return $rcBlocks;
    }

    ### block_settings

    public function getBlockSettings($blockKey="")
    {
        $block_settings = json_decode($this->block_settings, true) ?? [];

        if (isset($block_settings[$blockKey])) {
            return $block_settings[$blockKey];
        }

        return $block_settings;
    }

    /**
     * Remove block_settings from blocks
     * @param integer $blockKey
     * @return boolean
     */
    public function removeBlockSettings($blockKey)
    {
        $blockSettingsA = $this->getBlockSettings();

        if (isset($blockSettingsA[$blockKey])) {
            unset($blockSettingsA[$blockKey]);
            $blockSettingsA = array_values($blockSettingsA);
            $this->block_settings = json_encode($blockSettingsA, JSON_FORCE_OBJECT);
            return $this->save();
        }
        return true;
    }
    ### ###

    public function getCompletedStatus()
    {
        $fail = 7;
        $pass = 0;

        // check status
        if ($this->status) {
            $fail--;
            $pass++;
        }

        //check items
        if (count($this->items)) {
            $fail--;
            $pass++;
        }

        //check units
        if (count($this->units)) {
            $fail--;
            $pass++;
        }

        //check Assign Test Items to Unit
        if ($this->isAllUnitsHasItems()) {
            $fail--;
            $pass++;
        }

        //check blocks
        if (count($this->blocks)) {
            $fail--;
            $pass++;
        }

        //check Assign Units To Block
        if ($this->isAllBlocksHasUnit()) {
            $fail--;
            $pass++;
        }

        // check start_date and end_date
        if ((!empty($this->start_date) && !empty($this->end_date))) {
            $fail--;
            $pass++;
        }

        //check Modal Answer And Rating
        // if (count($this->getModelTest())) {
        //     $fail--;
        //     $pass++;
        // }

        return ["pass"=>$pass, "fail"=>$fail];
    }

    /**
     * Synchronize column for block changes
     * @param string $columnName
     * @param int $key1
     * @param int $key2
     * @return boolean
     */
    public function syncColumn($columnName, $key1, $key2="")
    {
        $cols = [];
        switch ($columnName) {
            case "groups":
                $cols = $this->getGroups();
                break;
            case "raters":
                $cols = $this->getRaters();
                break;
            case "block_settings":
                $cols = $this->getBlockSettings();
                break;
        }

        if ($cols) {
            $tmpcols = $cols[$key1] ?? [];
            if ($key2=="") {
                $cols[$key1] = [];
                $key2 = $key1+1;
            } else {
                $cols[$key1] = $cols[$key2] ?? [];
            }
            $cols[$key2] = $tmpcols;
            ksort($cols);

            $this->{$columnName} = json_encode($cols, JSON_FORCE_OBJECT);
            return true;
        }

        return false;
    }
}
