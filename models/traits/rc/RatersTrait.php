<?php

namespace app\models\traits\rc;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

// trait for Raters Companies
trait RatersTrait
{
    public function getRaters($blockKey="")
    {
        $ratersA = json_decode($this->raters, true)?:[];

        if ($blockKey!='') {
            return isset($ratersA[$blockKey]) ? $ratersA[$blockKey] : [];
        }

        return $ratersA;
    }

    public function getRaterStatus($blockKey, $raterID)
    {
        $assignedRaters = $this->getRaters($blockKey);
        $assignedRatersIDs = array_column($assignedRaters, "rid");
        if (in_array($raterID, $assignedRatersIDs)) {
            $key = array_search($raterID, array_column($assignedRaters, "rid"));
            return $assignedRaters[$key]["status"];
        }
        return "dna";
    }

    public function isLeaderRater($blockKey, $raterID)
    {
        $assignedRaters = $this->getRaters($blockKey);
        $assignedRatersIDs = array_column($assignedRaters, "rid");
        if (in_array($raterID, $assignedRatersIDs)) {
            $key = array_search($raterID, array_column($assignedRaters, "rid"));
            return isset($assignedRaters[$key]["isLeader"])? $assignedRaters[$key]["isLeader"] : "";
        }
        return false;
    }

    public function getGroupRatersStatus($blockKey, $groupID, $status)
    {
        $groupMembers = \app\models\Raters::find()->where(["group"=>$groupID])->all();
        $members = [];
        foreach ($groupMembers as $member) {
            $memberStatus = $this->getRaterStatus($blockKey, $member->id);
            if ($memberStatus==$status) {
                $members[]=$member->toArray();
            }
        }
        return $members;
    }

    public function getGroupRatersStatusCount($blockKey, $groupID, $status)
    {
        return count($this->getGroupRatersStatus($blockKey, $groupID, $status));
    }

    
    // remove rater from blocks
    public function removeRater($blockKey)
    {
        $raterA = $this->getRaters();

        if (isset($raterA[$blockKey])) {
            unset($raterA[$blockKey]);
            $raterA = array_values($raterA);
            $this->raters = json_encode($raterA, JSON_FORCE_OBJECT);
            return $this->save();
        }
        return true;
    }
    
    public function getQualifiedBlock($raterID)
    {
        $blockRaterA = $this->getRaters();

        $blocks = [];
        foreach ($blockRaterA as $blockKey=>$raterA) {
            foreach ($raterA as $rater) {
                if ($rater["rid"]==$raterID && $rater["status"]=="ok") {
                    $blocks[] = $blockKey;
                }
            }
        }
        return $blocks;
    }
}
