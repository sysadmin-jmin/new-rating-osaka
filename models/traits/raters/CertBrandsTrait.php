<?php

namespace app\models\traits\raters;

use yii\helpers\{Url, ArrayHelper};
use app\models\ProjectForms\Brand;


// trait for Raters Companies
trait CertBrandsTrait {
	
	public function addCertBrands($brandKey) {
		$certBrandsA = $this->getCertBrands();
		$certBrandsA[$brandKey] = self::CERTBRAND_YES; 
		$this->certBrands = json_encode($certBrandsA);
		$this->save(false);
	}
	
	public function delCertBrands($brandKey) {
		$certBrandsA = $this->getCertBrands();
		unset($certBrandsA[$brandKey]); 
		$this->certBrands = json_encode($certBrandsA);
		$this->save(false);
	}		

	
	public function getCertBrands() {
		return json_decode($this->certBrands,true) ?? [];
	}
	
	// Get 'available' brands 
	//list of brands that a rater is not certified for 
	public function getAvaBrandsA() {
		$Brand = new \app\models\ProjectForms\Brand;
		$allBrands = $Brand->getBrandList();	

		$certBrands = $this->getCertBrands();
		
		foreach($certBrands as $brandKey => $v) {
			unset($allBrands[$brandKey]);
		}
		
		return $allBrands; 
	}
	
	public function isCert($brandKey) {
		$brands = $this->getCertBrands();
		return array_key_exists($brandKey,$brands);
	}

}