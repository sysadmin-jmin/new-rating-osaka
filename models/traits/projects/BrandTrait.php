<?php

namespace app\models\traits\projects;

use yii\helpers\{Url, ArrayHelper};
use app\models\ProjectForms\Brand;


// trait for Raters Companies
trait BrandTrait {
	
	
		//Brand is handled by forms 
	public function getBrandA() {
	}
	
	public function getBrandDropList() {
		$Brand = new Brand;
		$brandA = $Brand->getBrandA();
		
		return ArrayHelper::map($brandA,'key','name');
	}
	
	public function getBrandName() {
		
		$Brand = new Brand;
		$Brand->find($this->brand);
		return $Brand->name;
		
	}
	
	
	

}