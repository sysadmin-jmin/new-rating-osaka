<?php

namespace app\models\traits;

use app\models\QuestionForms\Items;
use app\models\QuestionForms\Units;
use app\models\QuestionForms\Context;
use app\models\QuestionForms\GeneralInstruction;
use app\models\QuestionForms\Example;

// trait for app/models/Questions
trait QuestionsTrait
{
    // get Items
    public function getItems($key=null)
    {
        $items = json_decode($this->items, true);

        if ($key!==null) {
            return @$items[$key]??[];
        }

        return @$items??[];
    }

    // find question items by key
    public function findItems($key)
    {
        $items = $this->getItems();
        $itemData = [];
        if ($key!=="") {
            $itemData = isset($items[$key]) ? $items[$key] : [] ;
        }

        $itemData["key"] = $key;
        $itemData["qID"] = $this->id;

        return new Items($itemData);
    }

    // save item
    public function saveItem($itemObj)
    {
        $data = $this->getItems($itemObj->key);

        $attributes = $itemObj->getAttributes();

        foreach ($attributes as $key=>$value) {
            if ($itemObj->isAttributeActive($key)) {
                $data[$key] = $value;
            }
        }

        $items = $this->getItems();
        $items[$itemObj->key] = $data;

        $this->items = json_encode($items, JSON_FORCE_OBJECT);

        return $this->save();
    }

    // delete item
    public function deleteItem($itemObj)
    {
        $itemA = $this->getItems();

        if (isset($itemA[$itemObj->key])) {
            // remove assigned item key in units;
            $unitA = $this->getUnits();
            foreach ($unitA as $unitKey=>$unit) {
                $unit['items'] = array_diff($unit["items"], [$itemObj->key]);
                $unit['items'] = array_values($unit["items"]);
                $unitA[$unitKey] = $unit;
            }

            $this->units = json_encode($unitA, JSON_FORCE_OBJECT);

            unset($itemA[$itemObj->key]);
        }

        $this->items = json_encode($itemA, JSON_FORCE_OBJECT);

        return $this->save();
    }


    // find question context by key
    public function findUnitContext($unitKey, $key)
    {
        $unitContexts = $this->getUnitContexts($unitKey);
        $contextData = isset($unitContexts[$key]) ? $unitContexts[$key] : [];

        $contextData["key"] = $key;
        $contextData["unitKey"] = $unitKey;
        $contextData["qID"] = $this->id;

        return new Context($contextData);
    }

    /**
     * Save context object to app\models\Questions model
     * @param app\models\QuestionForms\Context $contextObj
     * @return boolean
     */
    public function saveContext($contextObj)
    {
        // get Item Context
        $unitContext = $this->getUnitContexts($contextObj->unitKey);

        if ($contextObj->key!="") {
            $context = $unitContext[$contextObj->key];

            if ($contextObj->srcType=="image") {
                $contextObj->uploadFile();
            }

            $attributes = $contextObj->getAttributes();
            foreach ($attributes as $key=>$value) {
                if (isset($context[$key])) {
                    $context[$key]=$value;
                }
            }
            $unitContext[$contextObj->key] = $context;
        } else {
            $contextObj->key = count($unitContext)>0 ? max(array_keys($unitContext))+1 : 0;

            if ($contextObj->srcType=="image") {
                $contextObj->uploadFile();
            }

            $unitContext[$contextObj->key] = [
                    "name" => $contextObj->name,
                    "src" => $contextObj->src,
                    "conType" => $contextObj->conType,
                    "srcType" => $contextObj->srcType
                ];
        }

        $allContext = $this->getUnitContexts();

        $allContext[$contextObj->unitKey] = $unitContext;

        $this->context = json_encode($allContext, JSON_FORCE_OBJECT);

        return $this->save();
    }

    public function deleteContext($contextObj)
    {
        $unitContext = $this->getUnitContexts($contextObj->unitKey);
        unset($unitContext[$contextObj->key]);

        $allContext = $this->getUnitContexts();
        $allContext[$contextObj->unitKey] = $unitContext;
        $this->context = json_encode($allContext, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * get array of general Instructions
     * @param integer|null $blockID
     * @return array
     */
    public function getGenInstructions($blockKey=null)
    {
        $genInstructions = json_decode($this->genInstructions, true);
        if ($blockKey!== null) {
            return isset($genInstructions[$blockKey])?$genInstructions[$blockKey]:[];
        }
        return @$genInstructions??[];
    }

    /**
     * Find existing or create new object of General Instruction
     * @param integer $blockID
     * @param integer $key
     * @return app\models\QuestionForms\GeneralInstruction object
     */
    public function findGenInstruction($blockKey, $key)
    {
        $blockInstructions = $this->getGenInstructions($blockKey);
        $genInstruction = isset($blockInstructions[$key]) ? $blockInstructions[$key] : [];

        $genInstruction["key"] = $key;
        $genInstruction["blockKey"] = $blockKey;
        $genInstruction["qID"] = $this->id;

        return new GeneralInstruction($genInstruction);
    }

    /**
     * Save or update General Instruction Object
     * @param app\models\QuestionForms\GeneralInstruction $genInstructionObj
     * @return boolean
     */
    public function saveGenInstructions($genInstructionObj)
    {
        $blockInstructions = $this->getGenInstructions($genInstructionObj->blockKey);

        if ($genInstructionObj->key!="") {
            $genInstructionObj->uploadFile();
            
            $genInstruction = $blockInstructions[$genInstructionObj->key];

            $attributes = $genInstructionObj->getAttributes();
            foreach ($attributes as $key=>$value) {
                if (isset($genInstruction[$key])) {
                    $genInstruction[$key]=$value;
                }
            }
            $blockInstructions[$genInstructionObj->key] = $genInstruction;
        } else {
            $genInstructionObj->key = count($blockInstructions) > 0 ? max(array_keys($blockInstructions))+1 : 0;

            $genInstructionObj->uploadFile();

            $blockInstructions[$genInstructionObj->key] = [
                "name" => $genInstructionObj->name,
                "src" => $genInstructionObj->src
            ];
        }

        $allGenInstructions = $this->getGenInstructions();
        $allGenInstructions[$genInstructionObj->blockKey] = $blockInstructions;
        $this->genInstructions = json_encode($allGenInstructions, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Delete General Instruction Object
     * @param app\models\QuestionForms\GeneralInstruction $genInstructionObj
     * @return type
     */
    public function deleteGenInstructions($genInstructionObj)
    {
        $blockInstructions = $this->getGenInstructions($genInstructionObj->blockKey);
        unset($blockInstructions[$genInstructionObj->key]);

        $allGenInstructions = $this->getGenInstructions();
        $allGenInstructions[$genInstructionObj->blockKey] = $blockInstructions;
        $this->genInstructions = json_encode($allGenInstructions, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Get array of examples
     * @param integer|null $unitKey
     * @return array
     */
    public function getExamples($unitKey=null)
    {
        $examples = json_decode($this->examples, true);
        if ($unitKey!== null) {
            return isset($examples[$unitKey])?$examples[$unitKey]:[];
        }
        return @$examples??[];
    }

    /**
     * Find existing or create new object of Example
     * @param integer $unitKey
     * @param integer $key
     * @return app\models\QuestionForms\Example object
     */
    public function findExample($unitKey, $key)
    {
        $unitExamples = $this->getExamples($unitKey);
        $example = isset($unitExamples[$key]) ? $unitExamples[$key] : [];

        $example["key"] = $key;
        $example["unitKey"] = $unitKey;
        $example["qID"] = $this->id;

        return new Example($example);
    }


    /**
     * Save or update Example Object
     * @param app\models\QuestionForms\Example $exampleObj
     * @return boolean
     */
    public function saveExamples($exampleObj)
    {
        $unitExamples = $this->getExamples($exampleObj->unitKey);

        if ($exampleObj->key!="") {
            $exampleObj->uploadFile();
            
            $example = $unitExamples[$exampleObj->key];

            $attributes = $exampleObj->getAttributes();
            foreach ($attributes as $key=>$value) {
                if (isset($example[$key])) {
                    $example[$key]=$value;
                }
            }
            $unitExamples[$exampleObj->key] = $example;
        } else {
            $exampleObj->key = count($unitExamples) > 0 ?  max(array_keys($unitExamples))+1 : 0;

            $exampleObj->uploadFile();

            $unitExamples[$exampleObj->key] = [
                "name" => $exampleObj->name,
                "src" => $exampleObj->src
            ];
        }

        $allExamples = $this->getExamples();
        $allExamples[$exampleObj->unitKey] = $unitExamples;
        $this->examples = json_encode($allExamples, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Delete General Instruction Object
     * @param app\models\QuestionForms\GeneralInstruction $exampleObj
     * @return type
     */
    public function deleteExamples($exampleObj)
    {
        $unitExamples = $this->getExamples($exampleObj->unitKey);
        unset($unitExamples[$exampleObj->key]);

        $allExamples = $this->getExamples();
        $allExamples[$exampleObj->unitKey] = $unitExamples;
        $this->examples = json_encode($allExamples, JSON_FORCE_OBJECT);

        return $this->save();
    }

    // get Units
    public function getUnits($key=null)
    {
        $units = json_decode($this->units, true);

        if ($key!==null) {
            return @$units[$key]??[];
        }

        return @$units??[];
    }


    public function findUnit($key)
    {
        $units = $this->getUnits();
        $itemData = [];
        if ($key!=="") {
            $itemData = isset($units[$key]) ? $units[$key] : [] ;
        }

        $itemData["key"] = $key;
        $itemData["qID"] = $this->id;

        return new Units($itemData);
    }

    public function saveUnit($unitObj)
    {
        $data = $this->getUnits($unitObj->key);

        $attributes = $unitObj->getAttributes();

        foreach ($attributes as $key=>$value) {
            if ($unitObj->isAttributeActive($key)) {
                $data[$key] = $value;
            }
        }

        $units = $this->getUnits();
        $units[$unitObj->key] = $data;

        $this->units = json_encode($units, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Assign Item to Unit
     * @param integer $itemKey
     * @param integer $unitKey
     * @return boolean
     */
    public function assignItemToUnit($itemKey, $unitKey)
    {
        $unitA = $this->getUnits();


        if (isset($unitA[$unitKey])) {
            if ($itemKey!="" && !in_array($itemKey, $unitA[$unitKey]["items"])) {
                $unitA[$unitKey]["items"][] = $itemKey;
            } else {
                if ($itemKey=="") {
                    $this->addError("units", "No Item selected.");
                } else {
                    $this->addError("units", "Item already assigned to Unit.");
                }
                return false;
            }
        } else {
            $this->addError("units", "Unit not found.");
            return false;
        }

        $this->units = json_encode($unitA, JSON_FORCE_OBJECT);

        return $this->save();
    }

    /**
     * Remove Item From Unit
     * @param integer $itemKey
     * @param integer $unitKey
     * @return boolean
     */
    public function unassignItemFromUnit($itemKey, $unitKey)
    {
        $unitA = $this->getUnits();
        
        if (isset($unitA[$unitKey])) {
            if (in_array($itemKey, $unitA[$unitKey]["items"])) {
                $unitA[$unitKey]["items"] = array_diff($unitA[$unitKey]["items"], [$itemKey]);
                //reset the keys;
                $unitA[$unitKey]["items"] = array_values($unitA[$unitKey]["items"]);
            } else {
                $this->addError("units", "Item not found.");
                return false;
            }
        } else {
            $this->addError("units", "Unit not found.");
            return false;
        }

        $this->units = json_encode($unitA, JSON_FORCE_OBJECT);

        return $this->save();
    }

    public function getAssignedUnits()
    {
        $a = [];
        $blocks = $this->getBlocks();
        
        if (count($blocks)) {
            foreach ($blocks as $block) {
                $unitA = $block['units'];
                $unitA = array_values($unitA);
                $a = array_merge($a, $unitA);
            }
        }
        return $a;
    }


    // get Blocks
    public function getBlocks($key=null)
    {
        $blocks = json_decode($this->blocks, true);

        if ($key!==null) {
            return @$blocks[$key]??[];
        }

        return @$blocks??[];
    }

    /**
     * get single block array by block id
     * @param integer $id
     * @return block
     */
    public function getBlock($id)
    {
        $blocksA = $this->getBlocks();

        $blockKey = $this->getBlockKey($id);

        return isset($blocksA[$blockKey]) ? $blocksA[$blockKey] : [];
    }

    /**
     * get single block array by block id
     * @param integer $id
     * @return block
     */
    public function getBlockKey($id)
    {
        $blocksA = $this->getBlocks();

        $blockKey = array_search($id, array_column($blocksA, "id"));

        return $blockKey;
    }

    public function assignUnitToBlock($unitKey, $blockKey)
    {
        $blockA = $this->getBlocks();

        if (isset($blockA[$blockKey])) {
            $blockA[$blockKey]["units"][]= $unitKey;
        }

        $this->blocks = json_encode($blockA, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function unassignUnitFromBlock($unitKey, $blockKey)
    {
        $blockA = $this->getBlocks();

        if (isset($blockA[$blockKey])) {
            if (in_array($unitKey, $blockA[$blockKey]["units"])) {
                $blockA[$blockKey]["units"] = array_diff($blockA[$blockKey]["units"], [$unitKey]);
                $blockA[$blockKey]["units"] = array_values($blockA[$blockKey]["units"]);
            }
        }

        $this->blocks = json_encode($blockA, JSON_FORCE_OBJECT);
        return $this->save();
    }
}
