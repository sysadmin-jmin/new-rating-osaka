<?php

namespace app\models\traits;

use yii\helpers\Url;

// trait for Raters Companies
trait CompanyTrait
{
    // static function get Company Data Path
    public static function getCompanyDataPath()
    {
        return Url::to("@app/".self::COMPANY_DATA_PATH);
    }

    // static function get all Companies
    public static function getCompanies($code="")
    {
        $path = self::getCompanyDataPath();

        if (!file_exists($path)) {
            self::saveCompanies(["HQ"=>"HeadQuarters"]);
        }

        $contents = file_get_contents($path);

        $companies = json_decode($contents, true)?:[];
        if ($code) {
            return $companies[$code];
        }
        return $companies;
    }

    // save companies array contents
    public static function saveCompanies($contents)
    {
        $path = self::getCompanyDataPath();

        $contents = json_encode($contents, JSON_FORCE_OBJECT);
        return file_put_contents($path, $contents);
    }

    // add company with code and name, if code already exists will update the company
    public static function addCompany($code, $name)
    {
        $contents = self::getCompanies();
        $contents[strtoupper($code)] = $name;

        return self::saveCompanies($contents);
    }

    // delete company by code
    public static function deleteCompany($code)
    {
        $contents = self::getCompanies();
        unset($contents[$code]);

        return self::saveCompanies($contents);
    }

    // get company from raters
    public function getCompany()
    {
        if ($this->company) {
            return self::getCompanies($this->company);
        } else {
            return "";
        }
    }
}
