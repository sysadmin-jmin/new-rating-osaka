<?php

namespace app\models\traits;

use yii\helpers\Url;

// trait for Raters Groups
trait GroupsTrait
{
    // static function get GroupDataPath
    public static function getGroupsDataPath()
    {
        return Url::to("@app/".self::GROUP_DATA_PATH);
    }
    
    // static function get all Groups
    public static function getGroups()
    {
        $path = self::getGroupsDataPath();

        if (!file_exists($path)) {
            self::saveGroups([]);
        }

        $contents = file_get_contents($path);
        return json_decode($contents, true)?:[];
    }

    // static function get group by code
    public static function getGroupByCode($code)
    {
        $groups = self::getGroups();

        return isset($groups[$code]) ? $groups[$code] : null;
    }

    // save groups array contents
    public static function saveGroups($contents)
    {
        $path = self::getGroupsDataPath();

        $contents = json_encode($contents, JSON_FORCE_OBJECT);
        return file_put_contents($path, $contents);
    }

    // add group with code and name, if code already exists will update the group
    public static function addGroup($code, $name)
    {
        $contents = self::getGroups();
        $contents[strtoupper($code)] = ["code" => strtoupper($code), "name"=>$name];

        return self::saveGroups($contents);
    }

    // delete group by code
    public static function deleteGroup($code)
    {
        $contents = self::getGroups();
        unset($contents[$code]);

        return self::saveGroups($contents);
    }

    // assign Rater to Group
    public static function assignRaterToGroup($raterID, $groupCode)
    {
        if (is_array($raterID)) {
            foreach ($raterID as $id) {
                self::assignRaterToGroup($id, $groupCode);
            }
            return true;
        } else {
            $rater = self::findOne($raterID);
            if ($rater) {
                $rater->group = $groupCode;
                return $rater->save();
            }
        }
        return false;
    }

    // unassign Rater From Group
    public static function unAssignRaterFromGroup($raterID)
    {
        $rater = self::findOne($raterID);
        if ($rater) {
            $rater->group = null;
            return $rater->save();
        }
        return false;
    }

    // get group from rater
    public function getGroup()
    {
        $groups = self::getGroups();
        if ($this->group && isset($groups[$this->group])) {
            return $groups[$this->group];
        }
        return null;
    }

    //find Raters by group
    public function findByGroup($code)
    {
        if ($this->role=="Admin") {
            return self::find()->where(["group"=>$code])->all();
        } elseif ($this->role=="Manager") {
            // if role=manager only get raters with same company
            return self::find()->where(["group"=>$code, "company"=>$this->company])->all();
        }
        return [];
    }

    public static function assignedRaterCount($code)
    {
        return self::find()->where(["group"=>$code])->count();
    }
}
