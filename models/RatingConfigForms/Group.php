<?php

namespace app\models\RatingConfigForms;

use Yii;
use yii\base\Model;
use app\models\RatingConfigs;

class Group extends Model
{
    public $gID;
    public $blockKey;
    public $status;
    public $rcID;

    public $rcModel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['gID', 'status'], 'string'],
            [['rcID'], 'integer'],
            [['gID', 'blockKey'], 'required'],
        ];
    }

    public function getRcModel()
    {
        if (!$this->rcModel) {
            $this->rcModel = RatingConfigs::find()->where(["id"=>$this->rcID])->one();
        }
        return $this->rcModel;
    }


    public static function findOne($rcModel, $blockKey, $gid)
    {
        $group = new self();
        $group->blockKey = $blockKey;
        $group->rcID = $rcModel->id;

        $groupA = $rcModel->getGroups($blockKey);

        if ($groupA) {
            $key = array_search($gid, array_column($groupA, "gID"));
            if ($key!==null) {
                $group->gID = $groupA[$key]["gID"];
                $group->status = $groupA[$key]["status"];

                return $group;
            }
        }
        return null;
    }

    /**
     * Save group
     * @param app\models\RatingConfigForms\Group $this
     * @return boolean
     */
    public function save()
    {
        $rcModel = $this->getRcModel();

        if ($rcModel && $this->validate()) {
            $groupA = $rcModel->getGroups();

            $group = [
                "gID"=>$this->gID,
                "status"=>""
            ];

            if (!isset($groupA[$this->blockKey])) {
                $groupA[$this->blockKey] = [];
            }


            if (array_search($this->gID, array_column($groupA[$this->blockKey], "gID"))!==false) {
                // $this->addError("groups", "Groups already assigned.");
                $this->addError("gID", "Group already assigned.");
                return false;
            }

            $groupA[$this->blockKey][] = $group;

            $rcModel->groups = json_encode($groupA, JSON_FORCE_OBJECT);

            return $rcModel->save();
        }

        return false;
    }

    /**
     * Remove group from blocks
     * @return boolean
     */
    public function delete($gID="")
    {
        $rcModel = $this->getRcModel();
        $groupA = $rcModel->getGroups();

        if (isset($groupA[$this->blockKey])) {
            if ($gID) {
                $key = array_search($gID, array_column($groupA[$this->blockKey], "gID"));

                if ($key!==false) {
                    unset($groupA[$this->blockKey][$key]);
                }
            } else {
                unset($groupA[$this->blockKey]);
            }
            $rcModel->groups = json_encode($groupA, JSON_FORCE_OBJECT);
            return $rcModel->save();
        }
        return false;
    }
}
