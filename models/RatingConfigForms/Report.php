<?php

namespace app\models\RatingConfigForms;

use app\models\Projects;
use app\models\RatingConfigs;
use app\models\Answers;
use Yii;
use yii\base\Event;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * ContactForm is the model behind the contact form.
 *
 */
class Report extends Model
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
	public $rcID;

    /**
     * @var RatingConfigs
     */
	private $rc;

    /**
     * @var Projects
     */
	private $project;


    /**
     * @var Answers[]
     */
	private $answers = [];

    /**
     * Report constructor.
     * @param array $config
     * @throws \InvalidArgumentException
     * @throws \yii\web\NotFoundHttpException
     */
	public function __construct($config = [])
    {
        if (!isset($config['rcID']))
            throw new \InvalidArgumentException('"rcID" parameter required.');

        // set rcID to prop
        parent::__construct($config);

        $this->answers = Answers::findAll(['rcID' => $this->rcID]);

        $this->rc = RatingConfigs::findOne(['id' => $this->rcID]);
        if (!isset($this->rc))
            throw new NotFoundHttpException('Unknown rcID: ' . $this->rcID);

        $this->project = Projects::findOne(['id' => $this->rc->projectID]);

	}




    /**
     * @return Answers[]
     */
	public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @return Projects|null
     */
    public function getProjects()
    {
        return $this->project;
    }

    /**
     * @return RatingConfigs
     */
    public function getRatingConfig()
    {
        return $this->rc;
    }

    /**
     * @throws \Exception
     */
    public function updateSheetStatus()
    {
        // start transaction
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($this->answers as $answer) {
                // defaults
                $blockStatues = [
                    'open' => 0,
                    'closed' => 0,
                    'inpro' => 0,
                    'pending' => 0,
                ];
                $status = [];
                if (!empty($answer->getBlocks())) {
                    $status = array_map(
                        function ($block) use ($answer, &$blockStatues) {
                            if (isset($blockStatues[$block['status']]))
                                $blockStatues[$block['status']]++;
                            return $block['status'];
                        }, $answer->getBlocks());
                }

                if (count($answer->getBlocks()) == $blockStatues['closed'])
                    $answer->sheet_status = Answers::SHEETS_STATUS_RATED;
                elseif (count($answer->getBlocks()) == $blockStatues['open'])
                    $answer->sheet_status = Answers::SHEETS_STATUS_OPEN;
                else {
                    if (in_array('pending', $status))
                        $answer->sheet_status = Answers::SHEETS_STATUS_PENDING;
                    else
                        $answer->sheet_status = Answers::SHEETS_STATUS_INPROCESS;
                }
                $answer->save(true, ['sheet_status']);
            }
            $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return Answers[]
     */
    public function getClassifyTestStatus()
    {
        $classes = [
            Answers::SHEETS_STATUS_OPEN => [],
            Answers::SHEETS_STATUS_RATED => [],
            Answers::SHEETS_STATUS_INPROCESS => [],
            Answers::SHEETS_STATUS_PENDING => [],
        ];
        array_map(function(Answers $test)use(&$classes){
            if (!isset($classes[$test->sheet_status]))
                $classes[$test->sheet_status] = [];
            $classes[$test->sheet_status][] = $test;
            return $test->sheet_status;
        },$this->getAnswers());

        return $classes;
    }


    /**
     * Get start date at epoctime
     * @return false|int
     */
    public function getStartTime()
    {
        return strtotime($this->project->starttime);
    }

    /**
     * Elapsed time since start_date by seconds
     * @return false|int
     */
    public function getElapsedTime()
    {
        if (empty($this->rc->endtime))
            return time() - $this->getStartTime();
        return strtotime($this->project->endtime) - $this->getStartTime();

    }
}