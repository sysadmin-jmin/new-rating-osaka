<?php

namespace app\models\RatingConfigForms;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\UploadedFile;

use app\models\RatingConfigs;

/**
 * ContactForm is the model behind the contact form.
 */
class Items extends Model
{
    public $key;
    public $uid;
    public $question;
    public $qnType;
    public $ansType;
    
    public $rcID;
    private $rcModel;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['key'], 'integer'],
            [['question','uid','qnType','ansType', 'title'], 'string'],
            [['uid', 'question', 'qnType','ansType'], 'required'],
            [['uid'], 'unique']
            
        ];
    }
 
    public function attributeLabels()
    {
        return [
            'uid' => 'Unique ID',
            'question' => 'Question title',
            'qnType' => 'Question Type',
            'ansType' => 'Answer Type',
        ];
    }
     
    public function qnTypeA()
    {
        $a = [
                'text'=>'Text',
                'image'=>'Image',
        
             ];
        return $a;
    }
    
    public function ansTypeA()
    {
        $a = [
                'text'=>'Text',
                'image'=>'Image',
                'audio'=>'Audio',
             ];
        return $a;
    }
    
    public function __construct($rcID)
    {
        $this->rcID = $rcID;
        $this->rcModel = RatingConfigs::findOne($this->rcID);
    }
    
    
    public function save()
    {
        $row = [
                'question'=>$this->question,
                'uid'=>$this->uid,
                'qnType' => $this->qnType,
                'ansType'=>$this->ansType
               ];
        
        $data = json_decode($this->rcModel->rating_items, true);

        if ($this->key=="") {
            if (count($data)==0) {
                $this->key = 0;
            } else {
                $this->key = max(array_keys($data))+1;
            }
        }

        $data[$this->key] = $row;
        $this->rcModel->rating_items = json_encode($data, JSON_FORCE_OBJECT);

        return $this->rcModel->save();
    }
    
    public function findOne($itemID)
    {
        $rating_items = $this->findAll();
        return $rating_items->$itemID;
    }

    /**
     * @return \stdClass
     */
    public function findAll()
    {
        $rating_items = $this->rcModel->rating_items;
        return json_decode($rating_items)?: [];
    }

    public function delete()
    {
        $itemA = $this->rcModel->getItems();
        if (isset($itemA[$this->key])) {
            // remove assigned item key in units;
            $unitA = $this->rcModel->getUnits();
            foreach ($unitA as $unitKey=>$unit) {
                $unit['items'] = array_diff($unit["items"], [$this->key]);
                $unit['items'] = array_values($unit["items"]);
                $unitA[$unitKey] = $unit;
            }

            $this->rcModel->rating_units = json_encode($unitA, JSON_FORCE_OBJECT);

            unset($itemA[$this->key]);
        }
        $this->rcModel->rating_items = json_encode($itemA, JSON_FORCE_OBJECT);
        return $this->rcModel->save();
    }

    /**
     * Get max key index of json data
     * @return int
     */
    public function getMaxKey()
    {
        $data = (array)$this->findAll();
        if (count($data) < 1) {
            return 0;
        }
        // key starts by 0.. so returned value + 1
        return max(array_keys($data)) + 1;
    }
}
