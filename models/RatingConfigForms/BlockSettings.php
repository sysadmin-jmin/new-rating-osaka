<?php

namespace app\models\RatingConfigForms;

use Yii;
use yii\base\Model;
use app\models\RatingConfigs;

class BlockSettings extends Model
{
    public $blockKey;
    public $rcModel;

    public $preTraining;    #0/1
    public $qcLimit;

    
    public $hasTraining;
    public $fEveryday;
    public $afterBlocks;
    public $dayLimit;
    public $afterDays;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['hasTraining', 'fEveryday', 'afterBlocks', 'dayLimit', 'afterDays', 'qcLimit'], 'integer'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'hasTraining' => 'Allow Training',
            'fEveryday' => 'Everyday',
            'afterBlocks' => 'After Blocks',
            'dayLimit' => 'Day Limit',
            'afterDays' => "After Days",
            'qcLimit' => "QC Limit"
        ];
    }

    
    public function __construct(RatingConfigs $rcModel, $blockKey)
    {
        $this->rcModel = $rcModel;
        $this->blockKey = $blockKey;

        $block_settings = $this->rcModel->getBlockSettings($this->blockKey);

        if (isset($block_settings["training"])) {
            $this->hasTraining = $block_settings["training"]["hasTraining"] ?? "";
            $this->fEveryday = $block_settings["training"]["fEveryday"] ?? "";
            $this->afterBlocks = $block_settings["training"]["afterBlocks"] ?? "";
            $this->dayLimit = $block_settings["training"]["dayLimit"] ?? "";
            $this->afterDays = $block_settings["training"]["afterDays"] ?? "";
        }

        if (isset($block_settings["QC"])) {
            $this->qcLimit = $block_settings["QC"]["limit"] ?? "";
        }
    }

    public function save()
    {
        $blockSetting = $this->rcModel->getBlockSettings($this->blockKey);

        $blockSetting["training"]= [
            'hasTraining' => $this->hasTraining,
            'fEveryday' => $this->fEveryday,
            'afterBlocks' => $this->afterBlocks,
            'dayLimit' => $this->dayLimit,
            'afterDays' => $this->afterDays
        ];

        $blockSetting["QC"] =  [
            "limit" => $this->qcLimit
        ];

        $allBlockSettings = $this->rcModel->getBlockSettings();
        $allBlockSettings[$this->blockKey] = $blockSetting;

        $this->rcModel->block_settings = json_encode($allBlockSettings, JSON_FORCE_OBJECT);
        return $this->rcModel->save();
    }
}
