<?php

namespace app\models\RatingConfigForms;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
class Units extends Model
{
    public function removeAssignedUnits($units, $blocks)
    {
        $assignedUnits = [];
    }
    
    // blocks in obj
    public static function getAssignedUnits($blocks)
    {
        $a = [];
        
        if (count($blocks)) {
            foreach ($blocks as $block) {
                $unitA = $block['units'];
                $unitA = array_values($unitA);
                $a = array_merge($a, $unitA);
            }
        }
        return $a;
    }
}
