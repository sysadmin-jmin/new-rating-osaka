<?php

namespace app\models\RatingConfigForms;

use Yii;
use yii\base\Model;
use app\models\RatingConfigs;

class Rater extends Model
{
    public $rid;
    public $blockKey;
    public $status;
    public $isLeader;
    public $rcID;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['isLeader'], 'boolean'],
            [['rcID', 'rid'], 'integer'],
            [['rid', 'blockKey'], 'required'],
        ];
    }

    public function getRcModel()
    {
        return RatingConfigs::findOne($this->rcID);
    }

    //Array of status
    public static function raterStatus($status=null)
    {
        $a = [
                "ok" => 'Qualified',
                "fail" => 'Failed',
                "override" => "Override",
                "override-cert" => "Certified",
                "dna" => "Did not Attempt",
            ];
        if ($status && isset($a[$status])) {
            return $a[$status];
        } else {
            return $a;
        }
    }

    public function save()
    {
        $rcModel = $this->getRcModel();

        if ($rcModel && $this->validate()) {
            $raterA = $rcModel->getRaters();

            $rater = [
                        "rid" => $this->rid,
                        "status" => $this->status,
                        "isLeader"=> $this->isLeader
                    ];

            if (!isset($raterA[$this->blockKey])) {
                $raterA[$this->blockKey] = [];
            }

            $raterKey = array_search($this->rid, array_column($raterA[$this->blockKey], "rid"));

            if ($raterKey!==false) {
                $raterA[$this->blockKey][$raterKey] = $rater;
            } else {
                $raterA[$this->blockKey][] = $rater;
            }

            $rcModel->raters = json_encode($raterA, JSON_FORCE_OBJECT);

            return $rcModel->save();
        }

        return false;
    }

    public function delete()
    {
        $rcModel = $this->getRcModel();

        if ($rcModel) {
            $raterA = $rcModel->getRaters();

            if (isset($raterA[$this->blockKey])) {
                $key = array_search($this->rid, array_column($raterA[$this->blockKey], "rid"));

                if ($key!==false) {
                    unset($raterA[$this->blockKey][$key]);
                }
                $this->raters = json_encode($raterA, JSON_FORCE_OBJECT);
                return $this->save();
            }
        }
        return false;
    }
}
