<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "rating_configs".
 *
 * @property int $id
 * @property int $raterID
 * @property int $projectID
 * @property string $test_name
 * @property string $rating_items
 * @property string $rating_units
 * @property string $rating_blocks
 *       {
 *         "0": {
 *           "name": "Matrix",
 *           "id": 3,
 *           "units": {
 *             "0": "0"
 *           }
 *         },
 *         "1": {
 *           "name": "Algebra",
 *           "id": 5,
 *           "units": {
 *             "0": "1"
 *           }
 *         }
 *       }
 * @property string $groups
 *      {
 *        "3": { // block key
 *          "0": {
 *            "gID": "JG", // groups code
 *            "status": ""
 *          },
 *          "1": {
 *            "gID": "RR", // groups code
 *            "status": ""
 *          }
 *        }
 *      }
 * @property string $raters
 *      {
 *        "1": { 			// block key
 *          "0": { 			// index
 *            "rid": "4",   // rater id
 *            "status": "ok"    // ok, fail, dna (did not attempt), override
 *            "isLeader": "1" // boolean (0 or 1)
 *          }
 *        }
 *      }
 *  @property string $force_rate
 *      {
 *        "1": { // rating_block id not block key
 *          "0": {   // index
 *				"rid" : "4", //rater id
 *			}
 *        }
 *      }
 * @property string $model_test
 * @property int $status
 * @property string $block_settings
 *      {
 *        "0": { // block key
 *          "training": {
 *            "hasTraining": "1",
 *            "fEveryday": "1",
 *            "afterBlocks": "2",
 *            "dayLimit": "1",
 *            "afterDays": "3"
 *          },
 *          "QC": {
 *            "limit": "5"
 *          }
 *        }
 *      }
 * @property string $date_created
 * @property string $date_updated
 */
class RatingConfigs extends \yii\db\ActiveRecord
{
    #Set of Trait gets
    use \app\models\traits\RatingConfigsTrait;
    use \app\models\traits\rc\RatersTrait;

    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rating_configs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['raterID','projectID','status'], 'integer'],
            [['rating_items', 'rating_units', 'rating_blocks', 'model_test', 'groups', 'raters', 'block_settings'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['test_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'raterID' => 'Rater ID',
            'projectID' => 'Project ID',
            'test_name' => 'Test Set Name',
            'rating_items' => 'Rating Items',
            'rating_units' => 'Rating Units',
            'rating_blocks' => 'Rating Blocks',
            'groups' => 'Groups',
            'raters' => 'Raters',
            'model_test' => 'Model Test',
            'status'=>'Status',
            'block_settings'=>'Block Settings',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }
    
    public function statusA()
    {
        $a = [
                0 => '1 - Setup Items',
                1 => '2 - Rating Structuring',
                2 => '3 - Question Sets',
                
                ];
        return $a;
    }
    
    public function getStatus()
    {
        $a = $this->statusA();
        return $a[$this->status] ?? '';
    }
    
    public static function getName($id)
    {
        $model = RatingConfigs::findOne($id);
        return $model->test_name;
    }

    //stores as auto increase number
    public function saveUnits($arrData)
    {
        $data = json_decode($this->rating_units, true);

        //check for null array
        $nextID = (count($data)==0) ? 1 : max(array_column($data, 'id')) + 1;
    
        $row = [
            'id'=> $nextID,
            'name'=>$arrData['name'],
            'items'=>[]
        ];

        $data[] = $row;
        $this->rating_units = json_encode($data, JSON_FORCE_OBJECT);
        if ($this->save()) {
            return $this->rating_units;
        } else {
            return $this->getErrors();
        }
    }
    
    /***********
        Blocks
    ***********/
    
    public function saveBlock($data)
    {
        $blocks = json_decode($this->rating_blocks, true)?:[];

        $row = [
            'name'=>$data['name'],
        ];

        $row['id'] = (count($blocks)==0) ? 1 : max(array_column($blocks, 'id')) + 1;
        $row['units'] = [];

        if (is_numeric($data["blockKey"])) {
            array_splice($blocks, $data["blockKey"]+1, 0, [$row]);
            $this->syncColumn("groups", $data["blockKey"]+1);
            $this->syncColumn("raters", $data["blockKey"]+1);
            $this->syncColumn("block_settings", $data["blockKey"]+1);
        } else {
            $blocks[] = $row;
        }

        $this->rating_blocks = json_encode($blocks, JSON_FORCE_OBJECT);
        if ($this->save()) {
            return $this->rating_blocks;
        } else {
            return $this->getErrors();
        }
    }

    public function deleteBlock($key)
    {
        $blocksA = json_decode($this->rating_blocks, true);

        // remove groups and raters related to block
        if (isset($blocksA[$key])) {
            $this->removeGroup($key);
            $this->removeRater($key);
            $this->removeBlockSettings($key);
        }
        unset($blocksA[$key]);

        $blocksA = array_values($blocksA);

        $this->rating_blocks = json_encode($blocksA, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function blockUp($key)
    {
        $blocksA = json_decode($this->rating_blocks, true);

        if (isset($blocksA[$key])) {
            $block = $blocksA[$key];
            $blocksA[$key] = $blocksA[$key-1];
            $blocksA[$key-1] = $block;
            $this->syncColumn("groups", $key, $key-1);
            $this->syncColumn("raters", $key, $key-1);
            $this->syncColumn("block_settings", $key, $key-1);
        }

        $this->rating_blocks = json_encode($blocksA, JSON_FORCE_OBJECT);
        return $this->save();
    }
    
    public function blockDown($key)
    {
        $blocksA = json_decode($this->rating_blocks, true);

        if (isset($blocksA[$key])) {
            $block = $blocksA[$key];
            $blocksA[$key] = $blocksA[$key+1];
            $blocksA[$key+1] = $block;
            $this->syncColumn("groups", $key, $key+1);
            $this->syncColumn("raters", $key, $key+1);
            $this->syncColumn("block_settings", $key, $key+1);
        }

        $this->rating_blocks = json_encode($blocksA, JSON_FORCE_OBJECT);
        return $this->save();
    }
    
    public function sortBlocks($arrdata)
    {
        $item = array();
        foreach ($arrdata as $key => $row) {
            $item[$key] = $row['order'];
        }
        array_multisort($item, SORT_ASC, $arrdata);
        return $arrdata;
    }
    
    public function deleteUnit($key)
    {
        $items = json_decode($this->rating_units, true);
        unset($items[$key]);
        $items = array_values($items);  // re-numbering key index
        $this->rating_units = json_encode($items, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function unitUp($key)
    {
        $units = json_decode($this->rating_units, true);
        $current = $key;
        $prevKey = $key-1;
        $unit = $units[$current];
        $units[$current] = $units[$prevKey];
        $units[$prevKey] = $unit;

        $this->rating_units = json_encode($units, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function unitDown($key)
    {
        $units = json_decode($this->rating_units, true);
        $current = $key;
        $nextKey = $key+1;
        $unit = $units[$current];
        $units[$current] = $units[$nextKey];
        $units[$nextKey] = $unit;

        $this->rating_units = json_encode($units, JSON_FORCE_OBJECT);
        return $this->save();
    }

    public function addUnit($key, $postUnit)
    {
        $units = json_decode($this->rating_units, true);
        $newUnit = [];
        if ($postUnit['name'] != '') {
            $newUnit = [
                'name' => $postUnit['name'],
                'id' => (count($units) == 0) ? 1 : max(array_column($units, 'id')) + 1,
                'items' => [],
            ];
        }
        if (empty($newUnit)) {
            return;
        }

        if (isset($units[$key])) {
            $works = $units;
            $max = count($works);
            $units = [];
            $i = 0;
            while (count($works) > 0) {
                if ($i == $key) {
                    $units[] = $newUnit;
                } else {
                    $units[] = array_shift($works);
                }
                $i++;
            }
        } else {
            $units[$key] = $newUnit;
        }

        $this->rating_units = json_encode($units, JSON_FORCE_OBJECT);
        return $this->save();
    }
    
    /* Related model */

    /**
     * @return Projects
     */
    public function getProject()
    {
        return Projects::findOne($this->projectID);
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_updated = new Expression("NOW()");
            
            if ($this->isNewRecord) {
                $this->date_created = new Expression("NOW()");
            }
            return true;
        } else {
            return false;
        }
    }
}
