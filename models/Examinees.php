<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "examinees".
 *
 * @property int $id
 * @property string $name
 * @property string $school
 * @property string $date_created
 * @property string $date_updated
 */
class Examinees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examinees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['name', 'school'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'school' => 'School',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        ];
    }

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			$this->date_updated = new Expression("NOW()");
			
			if($this->isNewRecord) {
				$this->date_created = new Expression("NOW()"); 
			}
			return true;
		} else {
			return false;
		}
	}	
	
}
