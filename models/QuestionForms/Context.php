<?php

namespace app\models\QuestionForms;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use app\models\Questions;
use yii\web\UploadedFile;

class Context extends Model
{
    public $key;
    public $name;
    public $conType;
    public $src;
    public $srcType;
    public $order;
    public $qID; // parent Question ID
    public $unitKey;

    private $questionModel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['key','order', 'qID', 'unitKey'], 'integer'],
            [['src','conType','name', 'srcType'], 'string'],
            [['key','name','conType'], 'required']
        ];
    }

 
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'name' => 'Name',
            'src' => 'Context Help Text',
            'conType' => 'Context Type',
            'srcType' => 'Source Type'
        ];
    }

    public static function getContextTypeA()
    {
        $path = Url::to('@app').'/data/projects/context.json';
        if (file_exists($path)) {
            $content = file_get_contents($path);
            $a = json_decode($content, true);
        } else {
            $a = [];
        }

        return $a;
    }

    public static function getSrcTypeA()
    {
        $a = [
                'text'=>'Text',
                'image'=>'Image',
             ];
        return $a;
    }

    //get question model
    public function getQuestion()
    {
        if (!$this->questionModel) {
            $this->questionModel = Questions::findOne($this->qID);
        }
        return $this->questionModel;
    }

    // save context to question table
    public function save()
    {
        $question = $this->getQuestion();

        return $question->saveContext($this);
    }

    // upload Image file
    public function uploadFile()
    {
        $question = $this->getQuestion();

        $fileUpload=UploadedFile::getInstanceByName("image-src");

        if ($fileUpload && $fileUpload->size > 1) {
            if (!in_array($fileUpload->type, ["image/png", "image/jpeg"])) {
                $this->addError("src", 'Invalid file type. Only jpg and png image type are allowed.');
                return false;
            }

            $savedFile = $this->unitKey."_".$this->key.".png";

            $path = $question->getContextDir()."/".$savedFile;

            // delete image if exists
            if (file_exists($path)) {
                @unlink($path);
            }

            // use Imagine to always save image as png
            $im = \yii\imagine\Image::getImagine()->open($fileUpload->tempName);
            try {
                $im->save($path);
            } catch (\Exception $e) {
                $this->addError("src", 'Unable to write files to upload directory.');
                return false;
            }

            // overwrite src if exists to {$key}.png
            $this->src = $savedFile;
        }
        return true;
    }

    public function delete()
    {
        $question = $this->getQuestion();

        if ($this->srcType=="image") {
            $path = $question->getContextPath($this->unitKey, $this->key);
            // delete image if exists
            if (file_exists($path)) {
                @unlink($path);
            }
        }

        return $question->deleteContext($this);
    }
}
