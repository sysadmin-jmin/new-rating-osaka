<?php

namespace app\models\QuestionForms;

use Yii;
use yii\base\Model;
use app\models\Questions;
use yii\web\UploadedFile;

class Items extends Model
{
    public $uid;
    public $key;
    public $src;
    public $question;
    public $qnType;
    public $ansType;
    public $qID; // parent Question ID

    private $questionModel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['src','qnType', 'ansType', 'uid', "question"], 'string'],
            [['qnType', 'ansType'],'required']
        ];
    }

 
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'src' => 'Src or reference',
            'question' => 'Question',
            'uid' => 'UID',
            'qnType' => 'Question Type',
            'ansType' => 'Answer Type',
            "qID" => "Question ID"
        ];
    }
    
    //get question model
    public function getQuestion()
    {
        if (!$this->questionModel) {
            $this->questionModel = Questions::findOne($this->qID);
        }
        return $this->questionModel;
    }

    // save item to question table
    public function save()
    {
        $question = $this->getQuestion();

        if ($this->qnType=="image") {
            $this->uploadFile();
        }
        
        return $question->saveItem($this);
    }

    // not really deleted the items just clear the src;
    public function delete()
    {
        $question = $this->getQuestion();

        if ($this->qnType=="image") {
            $path = $question->getItemsDir()."/".$this->src;
            // delete image if exists
            if (file_exists($path)) {
                @unlink($path);
            }
        }

        return $question->deleteItem($this);
    }

    // upload Image file
    public function uploadFile()
    {
        $question = $this->getQuestion();

        $fileUpload=UploadedFile::getInstance($this, "src");

        if ($fileUpload && $fileUpload->size > 1) {
            if (!in_array($fileUpload->type, ["image/png", "image/jpeg"])) {
                $this->addError("src", 'Invalid file type. Only jpg and png image type are allowed.');
                return false;
            }

            $savedFile = $this->key.".png";

            $path = $question->getItemsDir()."/".$savedFile;

            // delete image if exists
            if (file_exists($path)) {
                @unlink($path);
            }

            // use Imagine to always save image as png
            $im = \yii\imagine\Image::getImagine()->open($fileUpload->tempName);
            try {
                $im->save($path);
            } catch (\Exception $e) {
                $this->addError("src", 'Unable to write files to upload directory.');
                return false;
            }

            $this->src = $savedFile;
        }
        return true;
    }
}
