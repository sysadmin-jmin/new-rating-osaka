<?php

namespace app\models\QuestionForms;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use app\models\Questions;
use yii\web\UploadedFile;

/**
 *
 */
class Example extends Model
{
    public $unitKey;
    public $key;
    public $src;
    public $name;
    public $qID;

    private $questionModel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['unitKey', "qID", "key"], 'integer'],
            [['src','name'], 'string'],
            [['unitKey','key', 'qID', "name", "src"],'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'unitKey' => 'Unit Key',
            'src' => 'Src or reference',
            'name' => 'Name',
            'key' => 'Gen Instruction Key',
            "qID" => "Question ID"
        ];
    }

    /**
     * Get question Model
     * @return object app\models\Questions
     */
    public function getQuestion()
    {
        if (!$this->questionModel) {
            $this->questionModel = Questions::findOne($this->qID);
        }

        return $this->questionModel;
    }

    /**
     * Get Unit from config model
     * @return array
     */
    public function getUnit()
    {
        $questionModel = $this->question;
        $unitA = $questionModel->getUnits();

        return isset($unitA[$this->unitKey]) ? $unitA[$this->unitKey] : [];
    }

    /**
     * Add/Update $this object to question model
     * @return boolean
     */
    public function save()
    {
        $question = $this->getQuestion();

        return $question->saveExamples($this);
    }

    /**
     * Upload image and add the file name to src in $this object
     * @return boolean
     */
    public function uploadFile()
    {
        $question = $this->getQuestion();

        $fileUpload=UploadedFile::getInstanceByName("imageSrc");

        if ($fileUpload && $fileUpload->size > 1) {
            if (!in_array($fileUpload->type, ["image/png", "image/jpeg"])) {
                $this->addError("src", 'Invalid file type. Only jpg and png image type are allowed.');
                return false;
            }

            // filename format {$unitKey}_{$key}.png
            $savedFile = $this->unitKey."_".$this->key.".png";

            $destination = $question->getExamplesDir();

            // upload image to destination
            $filePath = Yii::$app->convertImage->saveImageForm($fileUpload, $destination, 480);

            // rename the file
            if (file_exists($filePath)) {
                $savedPath = $question->getExamplesDir()."/".$savedFile;
                rename($filePath, $savedPath);
            }

            // update src if exists with savedFile
            $this->src = $savedFile;
        }
        return true;
    }

    /**
     * Delete $this object and related images from Questions model
     * @return boolean
     */
    public function delete()
    {
        $question = $this->getQuestion();

        $path = $question->getExamplesDir()."/".$this->src;
        // delete image if exists
        if (file_exists($path)) {
            @unlink($path);
        }

        return $question->deleteExamples($this);
    }
}
