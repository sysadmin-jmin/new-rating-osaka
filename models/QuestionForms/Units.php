<?php

namespace app\models\QuestionForms;

use Yii;
use yii\base\Model;
use app\models\Questions;
use yii\web\UploadedFile;

class Units extends Model
{
    public $key;
    public $id;
    public $name;
    public $items;
    public $qID; // parent Question ID

    private $questionModel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['name'],'required']
        ];
    }

 
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'name' => 'Name',
            "qID" => "Question ID"
        ];
    }
    
    //get question model
    public function getQuestion()
    {
        if (!$this->questionModel) {
            $this->questionModel = Questions::findOne($this->qID);
        }
        return $this->questionModel;
    }

    // save item to question table
    public function save()
    {
        $question = $this->getQuestion();
        
        return $question->saveUnit($this);
    }
}
