#!/bin/bash

WORKPATH=$(cd $(dirname `dirname $0`); pwd)

PATH=/usr/local/bin:$PATH

cd $WORKPATH
echo "Current path: $WORKPATH"

if ! [ -e /usr/local/bin/composer ]; then
    curl -sS https://getcomposer.org/installer | /opt/bitnami/php/bin/php -- --install-dir=/usr/local/bin --filename=composer
fi
if ! [ -x /usr/local/bin/composer ]; then
    chmod a+x /usr/local/bin/composer
fi
#npm install bower -g

/usr/bin/env php /usr/local/bin/composer -n install

# link settings for stage
if ! [ -L config/db.php ]
then
    rm config/db.php
    ln -s db.stage.php config/db.php
fi

/usr/bin/env php yii migrate/up --interactive 0
