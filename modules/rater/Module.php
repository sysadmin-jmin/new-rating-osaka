<?php

namespace app\modules\rater;

use Yii;
/**
 * rater module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\rater\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
		Yii::$app->homeUrl = ['/rater'];
        // custom initialization code goes here
    }
	
	
}
