<?php

namespace app\modules\rater\controllers;

use Yii;
use app\models\Answers;
use app\models\RatedUnits;
use app\models\Projects;
use app\models\RatingConfigs;
use yii\web\Cookie;

class TestsController extends \yii\web\Controller
{
    public function actionBlock()
    {
        $this->layout = '@app/modules/rater/views/layout/block';
    
        
        $answerID = 1;
        $blockKey = null;
        $answerModel = null;
        $blockName = '';
        
        $projectID = Yii::$app->request->get('projectID');
        $unitKey = Yii::$app->request->get('unitKey', 0);

        // check rater is already login
        $rater = Yii::$app->user->getIdentity();

        // if not login cannot give rating
        if (!$rater) {
            Yii::$app->getSession()->setFlash('warning', 'You need login first to give rating.');
            return $this->redirect("/rater/project");
        }

        // Check if there is 'inpro block'
        if ($inproBlock = $rater->getInproTestBlock()) {
            $answerID = key($inproBlock);
            $blockKey = $inproBlock[$answerID]["blockKey"];

            $answerModel = Answers::findOne($answerID);
        } else {
            // get random test with blocks status = open by projectID
            // $answerModel = Answers::findByBlockStatus("open", $projectID); // use later

            $answerModel = Answers::findOne($answerID);

            if ($answerModel) {
                // $blockID = $answerModel->getOpenBlockId(); // use later
                $blockKey = 0;
                $rcModel = $answerModel->rcModel;
                $blockA = $rcModel->getBlocks();
                if (count($blockA)) {
                    // set block status to inpro and update inpro_testblock in rater
                    if ($answerModel->updateBlockStatus($blockKey, "inpro")) {
                        $rater->setInproTestBlock($answerModel->id, $blockKey);
                    } else {
                        self::flash('warning', 'Cannot proceed with block.');
                        return $this->redirect(['/rater/project/view', "projectID"=>$projectID]);
                    }
                } else {
                    self::flash('warning', 'There is No block on this Test Answer Sheet for this project.');
                    return $this->redirect(['/rater/project/view', "projectID"=>$projectID]);
                }
            } else {
                self::flash('warning', 'There is No Test Answer Sheet with open in que block for this project.');
                return $this->redirect(['/rater/project']);
            }
        }

        return $this->render('block', [
                                    "answerModel" => $answerModel,
                                    "blockKey" => $blockKey
                                ]);
    }

    public function actionSubmit()
    {
        // check rater is already login
        /** @var \app\models\Raters $rater */
        $rater = Yii::$app->user->getIdentity();

        // if not login cannot submit rating
        if (!$rater) {
            self::flash('warning', 'You need login first to submit rating.');
            return $this->redirect("/rater/project");
        }

        $unit = Yii::$app->request->post("unit");

        // check all context rated if not pending
        if (!isset($unit["pending"])) {
            $context = $unit["context"];
            foreach ($context as $items) {
                foreach ($items as $rating) {
                    if (empty($rating)) {
                        $this->flash("danger", "You need to fill rate to all contexts.");
                        return $this->redirect(['block', "unitKey"=>$unit['key'] ]);
                    }
                }
            }
        }

        // get inpro testblock from rater
        if ($inproBlock = $rater->getInproTestBlock()) {
            $answerID = key($inproBlock);
            $blockKey = $inproBlock[$answerID]["blockKey"];
            $datetimeStart = $inproBlock[$answerID]["datetimeStart"];
            
            $answerModel = Answers::findOne($answerID);

            $data = Yii::$app->request->post("unit");

            if ($ru = RatedUnits::submitData($answerID, $data, $datetimeStart)) {
                $unitStatus = "closed";
                if ($ru->status=="-1") {
                    $unitStatus = "pending";
                }
                $answerModel->updateUnitStatus($ru->unitID, $unitStatus);
            }

            $unitID = $answerModel->rcModel->getNextUnit($ru->unitID, $blockKey);

            if ($unitID===false) { // last Unit
                $answerModel->updateBlockStatus($blockKey, "closed");
                $rater->completeInproTestBlock();
                return $this->redirect(['/rater/project']);
            }
        }

        return $this->redirect(['block', "unitKey"=>$unit['key']+1]);
    }
    
    //shows only the pending blocks
    // only for rater leader
    public function actionPending()
    {
        $models = Answers::find()
                ->with("rcModel")
                ->where("(JSON_SEARCH(blocks, 'one', 0, NULL, '$**.status' ) is not NULL)")
                ->andWhere("(JSON_SEARCH(blocks, 'one', 'p', NULL, '$**.status' ) is NULL)")
                ->orderBy("RAND()");
    

        return $this->render('block', ["answerModel" => $answerModel, "blockID" => $blockID]);
    }

	# flash in inherted	
}
