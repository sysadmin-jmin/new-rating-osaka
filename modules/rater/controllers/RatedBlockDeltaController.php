<?php 


namespace app\modules\rater\controllers;

use Yii;
use app\models\RatedUnits;
use app\models\RatingConfigs;
use app\models\Projects;
use app\models\Questions;
use app\models\Answers;
use app\models\RatedBlock;
use app\models\RatedBlocksDelta;
use app\models\RatedDeltaSet;
use app\models\ProjectForms\Brand;

class RatedBlockDeltaController extends Controller
{
    public $layout = '@app/modules/rater/views/layout/columns2';
    
    
    // Certify a Rater for a Block
    
    public function actionCertBlock($ansID, $qnID)
    {
        $rater = Yii::$app->user->getIdentity();

        $questionModel = Questions::findOne($qnID);

        $project = $questionModel->project;

        $brand = new Brand();

        $brand->find($project->brand);

        $noSheetsRequired = $brand->certNo; #get from brand

        // model find all
        $ratedblockDeltaA = RatedBlocksDelta::find()
                        ->where(["ansID"=>$ansID, "qnID"=>$qnID])
                        ->all();

        
        if (count($ratedblockDeltaA) < $noSheetsRequired) { // redirect
            return $this->redirect(['project/index','projectID'=>$project->id]);
        }

        $ratedDeltaSet = RatedDeltaSet::computeBlock($ratedblockDeltaA, $rater);

        /* if ( pass ) {
            RABS to rID
        }
        else {
            RABs to fail
        }
        */
        
        $data["ratedDeltaSet"]= $ratedDeltaSet;
        
        return $this->render('cert', $data);
    }
    
    public function actionCertBrand()
    {
        $noSheetsRequired = 5; #get from brand
        $blockDeltaA = []; // model find all
        
        /* if ( pass ) {
            set brand certify , set all RABS to rID
        }
        else {
            set brand fail, set all RABs to fail
        }
        */
        
        
        return $this->render('cert');
    }
}
