<?php

namespace app\modules\rater\controllers;

use Yii;
use yii\web\Controller;
use app\models\Projects;
use app\models\Answers;
use yii\filters\AccessControl;

/**
 * Default controller for the `rater` module
 */
class ProjectController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
     
     
    public function actionIndex()
    {
        $this->layout = '@app/modules/rater/views/layout/columns2';
        $projects = Projects::find()->all();
        
        return $this->render('index', ['projects'=>$projects]);
    }

    public function actionClearInproBlock()
    {
        $inproBlock = Yii::$app->user->identity->getInproTestBlock();
        
        if (count($inproBlock) > 0) {
            $answerID = key($inproBlock);
            $blockID = $inproBlock[$answerID]["blockKey"];
            
            #Set the Block to Open
            $answerModel = Answers::findOne($answerID);
            if ($answerModel) {
                $answerModel->updateBlockStatus($blockID, "open");
            }
            
            #Unset the Rater inpro_testblock
            Yii::$app->user->identity->inpro_testblock = "{}";
            Yii::$app->user->identity->save(false);
            Yii::$app->session->setFlash('danger', 'In Process Block Cleared');
        }
        return $this->redirect(['index']);
    }
}
