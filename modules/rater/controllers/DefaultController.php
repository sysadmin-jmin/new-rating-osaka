<?php

namespace app\modules\rater\controllers;

use yii\web\Controller;

/**
 * Default controller for the `rater` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['project/']);
    }
}
