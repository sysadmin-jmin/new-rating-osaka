<?php
/**
 * Created by PhpStorm.
 * User: fujimoto
 * Date: 2019-01-18
 * Time: 15:54
 */

namespace app\modules\rater\controllers;

use app\models\Raters;
use Yii;
use yii\web\Controller;
use app\models\Projects;
use app\models\Answers;
use yii\filters\AccessControl;

/**
 * Myaccount controller for the `rater` module
 */
class MyaccountController extends Controller
{

    /**
     * @return string
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [

                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


	public function actionIndex() {
		$this->layout = '@app/modules/rater/views/layout/columns2';
		
		return $this->render('index');
		
	}


    /**
     * /rater/myaccount/system-check
     * @return string
     */
    public function actionSystemCheck()
    {
        $this->layout = '@app/modules/rater/views/layout/columns2';
        $userId = Yii::$app->user->identity->getId();
        $rater = Raters::findOne($userId);
    
        $soundCheck = Yii::$app->request->get('sound_check',false);
        if ($soundCheck !== false) {
            if ($soundCheck == '1') {
                $rater->sound_check = $soundCheck;
                if ($rater->save()) {
                    Yii::$app->response->redirect('system-check');
                    return null;
                }
            }
        }
        

        return $this->render('systemCheck', ['rater'=>$rater]);
    }
}