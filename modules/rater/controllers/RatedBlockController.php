<?php

namespace app\modules\rater\controllers;

use app\models\RatedUnits;
use Yii;
use app\models\{RatingConfigs, Projects};
use app\models\Questions;
use app\models\Answers;
use app\models\RatedBlock;
use app\models\RatedBlocksDelta;

class RatedBlockController extends Controller
{
    public function actionIndex($projectID)
    {
        $this->layout = '@app/modules/rater/views/layout/columns2';

		$projectM = Projects::findOne($projectID);
		if(!Yii::$app->user->identity->isCert($projectM->brand)) {
			Yii::$app->session->setFlash('danger',"you are not certified to rate this brand");
		}
		
		
        $user = Yii::$app->user->getIdentity();

        $data = [];
        $ansTraining = [];
        $ansActual = [];

        
        /*
        //KIV DUE TO TESTING
        #$rcBlocks = RatingConfigs::getAllQuailfiedBlocks($user->id, $projectID);

        #if (count($rcBlocks)) {
            #$rcIDs = array_keys($rcBlocks);



            $qnTrainingM = Questions::find()->where(["rcID"=>$rcIDs])
                                                       ->andWhere(["<>", "type", "actual"])->all();
            $questionActualModels = Questions::find()->where(["rcID"=>$rcIDs])
                                                     ->andWhere(["type"=>"actual"])->all();
        #}
        */
        
        $ansActual = Answers::find()
                        ->joinWith(['rc','qn'])
                        ->where([
                            'rating_configs.projectID'=>$projectID,
                            'questions.type'=>'actual'
                        ])
                        ->all();
        
        $ansTraining = Answers::find()
                        ->joinWith(['rc','qn'])
                        ->where(['rating_configs.projectID'=>$projectID])
                        ->andWhere(["not in", "questions.type", ["actual", "park"]])
                        ->all();
        
        

        #$data["rcBlocks"] = $rcBlocks;
        $data["ansTraining"] = $ansTraining;
        $data["ansActual"] = $ansActual;

        return $this->render('index', $data);
    }
    
    public function actionRateAnswer($ansID, $blockKey)
    {
        $this->layout = '@app/modules/rater/views/layout/block';
    
        $answerModel = null;
        $blockName = '';
        
        $unitKey = Yii::$app->request->get('unitKey', 0);

        // check rater is already login
        $rater = Yii::$app->user->getIdentity();

        // if not login cannot give rating
        if (!$rater) {
            Yii::$app->getSession()->setFlash('warning', 'You need login first to give rating.');
            return $this->redirect("/rater/project");
        }

        // Check if there is 'inpro block'
        // if ($inproBlock = $rater->getInproTestBlock()) {
        //     $answerID = key($inproBlock);
        //     $blockKey = $inproBlock[$answerID]["blockKey"];

        //     $answerModel = Answers::findOne($answerID);
        // } else {
        // get random test with blocks status = open by projectID
        // $answerModel = Answers::findByBlockStatus("open", $projectID); // use later

        $answerModel = Answers::findOne($ansID);

        if ($answerModel) {
            // $blockID = $answerModel->getOpenBlockId(); // use later
            $questionModel = $answerModel->questionModel;
            $projectID = $questionModel->projectID;
            $blockA = $questionModel->getBlocks($blockKey);
            if ($blockA) {
                // set block status to inpro and update inpro_testblock in rater
                if ($answerModel->updateBlockStatus($blockKey, "inpro")) {
                    $rater->setInproTestBlock($answerModel->id, $blockKey);

                    // prepare block for rating
                    $ratedBlock = RatedBlock::prepareBlockForRating($blockKey, $answerModel, $rater);
                } else {
                    self::flash('warning', 'Cannot proceed with block.');
                    return $this->redirect(['index', "projectID"=>$projectID]);
                }
            } else {
                self::flash('warning', 'There is No block on this Test Answer Sheet for this project.');
                return $this->redirect(['index', "projectID"=>$projectID]);
            }
        } else {
            self::flash('warning', 'There is No Test Answer Sheet with open in que block for this project.');
            return $this->redirect(['/rater/project']);
        }
        // }

        return $this->render(
                    'rate-answer',
                    [
                        "answerModel" => $answerModel,
                        "ratedBlockModel" => $ratedBlock,
                        "blockKey" => $blockKey
                    ]
                );
    }

    public function actionSubmitAnswer($ansID, $blockKey)
    {
        $answerModel = Answers::findOne($ansID);

        // check rater is already login
        /** @var \app\models\Raters $rater */
        $rater = Yii::$app->user->getIdentity();

        // if not login cannot submit rating
        if (!$rater) {
            self::flash('warning', 'You need login first to submit rating.');
            return $this->redirect("/rater/project");
        }


        $unit = Yii::$app->request->post("unit");

        // check all context rated if not pending
        if (!isset($unit["pending"])) {
            $unitContext = $unit["context"];
            foreach ($unitContext  as $conKey=>$rating) {
                if (empty($rating)) {
                    $this->flash("danger", "You need to give rate all contexts in this unit.");
                    return $this->redirect(
                                [
                                    'rate-answer',
                                    "ansID"=>$ansID,
                                    "blockKey"=>$blockKey,
                                    "unitKey"=>$unit['key']
                                ]
                            );
                }
            }
        }

        // get inpro testblock from rater
        if ($inproBlock = $rater->getInproTestBlock()) {
            if ($ratedBlock = RatedBlock::submitRating($unit, $blockKey, $answerModel, $rater)) {
                RatedBlocksDelta::calculateDelta($blockKey, $ratedBlock, $answerModel);
                if ($answerModel->type==Answers::TYPE_ACTUAL) {
                    $unitStatus = isset($unit["pending"]) ? "pending" : "closed";
                    $answerModel->updateUnitStatus($unit["id"], $unitStatus);
                }
                self::flash('success', 'Rating successfully submitted.');
            }
        }


        return $this->redirect(
                    [
                        'rate-answer',
                        "ansID"=>$ansID,
                        "blockKey"=>$blockKey,
                        "unitKey"=>$unit['key']
                    ]
                );
    }

    public function actionFinishAnswer($ansID, $blockKey)
    {
        $answerModel = Answers::findOne($ansID);
        $rater = Yii::$app->user->getIdentity();

        if ($inproBlock = $rater->getInproTestBlock()) {
            if ($ratedBlock = RatedBlock::finishRating($blockKey, $answerModel, $rater)) {
                if ($ratedBlock->block_status==2) {
                    $answerModel->updateBlockStatus($blockKey, "closed");
                } else {
                    $answerModel->updateBlockStatus($blockKey, "pending");
                }
            }
        }
        return $this->redirect(
                    [
                        'index',
                        "projectID"=>$answerModel->rcModel->projectID
                    ]
                );
    }
}
