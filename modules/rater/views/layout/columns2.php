<?php 
use yii\helpers\Html;
use app\components\Icon;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="row">

<div class="col-md-3">
	
	 <div class="list-group">
		<div class="list-group-item disabled">Options</div>	 
	 
	  <?= Html::a('<i class="fa fa-star-o"></i> Projects', ['project/'], ['class'=>'list-group-item']) ?>
	  <?= Html::a(Icon::Fa('user').' My Account', ['myaccount/'], ['class'=>'list-group-item']) ?>
	  			  
	</div> 	
	

</div>

<div class="col-md-9">
	<?= $content ?>
</div>


</div>
<?php $this->endContent(); ?>
