<?php 
use app\models\RatedBlocksDelta;

$this->title = "Get Qualified For A Rating Block";

$css = <<<CSS
.overall {
	font-size:16px;
	padding:5px;
}

.summaryRow td {
	background-color: #efefef;
}
CSS;

$this->registerCss($css);

function showResults($ratedDeltaSet)
{
    $htmlResult = "";
    $rbdset = $ratedDeltaSet->getRbdSets();

    foreach ($rbdset as $rbd) {
        $rbdID = $rbd[0];
        $ratedBlockDelta = RatedBlocksDelta::findOne($rbdID);
        $ratedBlock = $ratedBlockDelta->ratedBlock;
        $modelRating = $ratedBlockDelta->modelRating;
        if ($modelRating && $ratedBlock) {
            $modelRates = $modelRating->getRatings();
            $actualRates = $ratedBlock->getRatings();

            $tContext = 0;
            $tPerfect = 0;
            $tWithin = 0;
            $diffModel = 0;

            foreach ($actualRates as $unitKey=>$contexts) {
                foreach ($contexts as $key=>$value) {
                    $tContext++;
                    if ((int)$value["rating"] == (int)$modelRates[$unitKey][$key]["rating"]) {
                        $tPerfect++;
                    }
                    if ((int)$value["rating"]+1 == (int)$modelRates[$unitKey][$key]["rating"]) {
                        $tWithin++;
                    }
                    $diffModel+= ((int)$value["rating"] - (int)$modelRates[$unitKey][$key]["rating"]);
                }
            }
            $htmlResult .= "
	    			<tr>
	    				<td>".$rbdID."</td>
	    				<td>".$tContext."</td>
	    				<td>".$tPerfect."</td>
	    				<td>".$tWithin."</td>
	    				<td>".$diffModel."</td>
	    			</tr>";
        }
    }

    $tPerfectPercentage = round(($ratedDeltaSet->tPerfect/$ratedDeltaSet->tContext)*100);
    $tWithinPercentage = round(($ratedDeltaSet->tWithin/$ratedDeltaSet->tContext)*100);

    $htmlResult .= "			
			<tr class='summaryRow'>
				<td></td>
				
				<td>".$ratedDeltaSet->tContext."</td>
				<td>".$ratedDeltaSet->tPerfect." (".$tPerfectPercentage."%) 
					<span class='label label-danger'>Fail</span>
				</td>
				<td>".$ratedDeltaSet->tWithin." (".$tWithinPercentage."%)
					<span class='label label-danger'>Fail</span>
				</td>
				<td>
					".$ratedDeltaSet->diffModel."
				</td>
				
			</tr>
			<tr>
				<td colspan='3'>Your overall score is</td>
				
				<td><span class='label label-danger overall'>Failed </span></td>
				<td></td>
			</tr>";

    return $htmlResult;
}

?>

<div class="rated-block-delta-cert">

	<h1><?= $this->title ?></h1>

	<div class="">

		<table class="table table-bordered">
			<tr>
				<th>Cert Q-A Set</th>
				<th>Rating Context Points</th>
				<th>Perfect Answers</th>
				<th>Within 1 Point</th>
				<th>Score vs Modal Answer</th>
			</tr>
			<?=showResults($ratedDeltaSet)?>			
		</table>

	</div>

</div>