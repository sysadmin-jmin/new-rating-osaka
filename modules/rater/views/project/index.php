<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

use app\models\{RatingConfigs, Answers};


?>
<h1>Rating Projects</h1>

<div class="index">

show only projects that the rater group can view 

	<table class='table table-bordered'>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>brand</th>
			<th>Certified?</th>
		</tr>
	<?php foreach($projects as $key => $project): ?>
		<?php $isCert = Yii::$app->user->identity->isCert($project->brand);
			  $showIsCert = ($isCert) ? "Yes" : "No";
		?>
		<tr>
			<td><?= $project->id ?> </td>
			<td><?= Html::a($project->name,['rated-block/index','projectID'=>$project->id]); ?></td>
			<td><?= $project->brand ?></td>
			<td><?= $showIsCert ?></td>
		</tr>
	<?php endforeach ?>	
		
		
		
	</table>

</div>
