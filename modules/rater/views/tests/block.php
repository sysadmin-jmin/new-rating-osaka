<?php

/* @var $answerModel \app\models\Answers */
/* @var $blockKey int */
/* @var $blockName string */

use app\modules\rater\views\tests\UnitWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Rate A Test Set Block';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.tab-content {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    
    padding: 20px;

}

.nav-tabs {
    margin-bottom: 0;
}

.navbar-text {
	padding: 0 10px;
}

.question-panel {
    max-height: 400px;
    overflow-y: scroll;
}

.answers-panel {
    max-height:400px;
    overflow-y: scroll;
}


.qn-img {
	max-width:100%;
}

.qn-label {
    margin-bottom:35px;
}

/* in unit widget */
.des-panel {
	display:none;
}

CSS;

$this->registerCSS($css);

$rcModel = $answerModel->rcModel;
$questionModel = $answerModel->questionModel;
$blockA = $rcModel->getBlocks();
$block = $blockA[$blockKey];
$blockName = $block['name'];

$unitKey = Yii::$app->request->get("unitKey", 0);

?>

  <nav class="navbar navbar-inverse" style='margin-top:5px'>
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Rating System</a>
    </div>
	<p class="navbar-text"></p>
	<p class="navbar-text"><?=$answerModel->rcModel->test_name?></p>
	<p class="navbar-text">Answer ID: <b><?=$answerModel->id?></b></p>
	<p class="navbar-text">Block: <b><?=$blockName?></b></p>
	<p class="navbar-text">Block Start: <b>
</b></p>
    <ul class="nav navbar-nav navbar-right">

      <li>
	  <?php
        $link = '<i class="glyphicon glyphicon-log-out"></i> Exit';
      
        echo Html::a($link, ['project/view','projectID'=>$answerModel->rcModel->projectID], ['data-confirm'=>"Exit Rating?"])
      ?>
	  </li>
    </ul>	
	
    
  </div>
</nav> 

<?php
    if (isset($block["units"])) {
        $blockUnits = $block["units"];
        $lastKey = count($blockUnits)-1;
    }
?>
<div class="qn-label">
    <h3 class="pull-right"><span class="label label-warning">Training</span></h3>
</div>
<?=Html::beginForm('submit', 'post', ["id"=>"formSubmitRatingUnit"]);?>
<ul class="nav nav-tabs">
 	<?php
        if (count($blockUnits)) {
            foreach ($blockUnits as $key=>$unit) {
                $units = $rcModel->getUnits();
                if ($unitKey==$key) {
                    $unitID = $blockUnits[$unitKey];
                    echo '<li class="active"><a href="#">'.$units[$unit]["name"].'</a></li>';
                } else {
                    echo '<li class="disabled"><a>'.$units[$unit]["name"].'</a></li>';
                }
            }
        }
    ?>
</ul>
 
 <div class="tab-content"> 
	<div  class="tab-pane active">
        <div class="row">
            <div class="col-md-6">
                <?=$this->render("block/_instruction")?>
                <?=$this->render("block/_context", ["rcModel"=>$rcModel, "questionModel"=>$questionModel, "unitID"=>$unitID])?>
            </div>

            <div class="col-md-6">
                <?=$this->render("block/_question", ["rcModel"=>$rcModel, "questionModel"=>$questionModel, "unitID"=>$unitID])?>
                <?=$this->render("block/_answer")?>
                <?=$this->render("block/_rating", ["rcModel"=>$rcModel, "questionModel"=>$questionModel, "answerModel"=>$answerModel, "unitID"=>$unitID, "isLastUnit"=>($lastKey==$unitKey) ])?>
            </div>
        </div>
	</div>
 </div>
<?=Html::input("hidden", "unit[id]", $unitID)?>
<?=Html::input("hidden", "unit[key]", $unitKey)?>
<?=Html::endForm();?>

<?php

$js = <<<JS
$('.toggle-commentbox').click(function(e) {
	e.preventDefault();
	$('#commentbox').toggle();
})

$(".chkbox-unit-pending").on("click",function(e){
    var me=$(this);
    var context = $(".radio-context");
    if (me.prop("checked")) {
        context.prop("checked", false).prop("disabled",true);
    } else {
        context.prop("disabled",false);
    }
    
});

JS;
$this->registerJS($js);
?>
 