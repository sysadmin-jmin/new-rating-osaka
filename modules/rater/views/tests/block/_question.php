<?php
/* @var $rcModel app\models\RatingConfigs */
/* @var $questionModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;

function showQuestions($rcModel, $questionModel, $unitID)
{
    $htmlResult = "";
    if ($questionModel && $rcModel) {
        $unitA = $rcModel->getUnits();
        $unit = $unitA[$unitID];

        $number=1;
        foreach ($unit['items'] as $itemKey) {
            $qnItem = $questionModel->getItems($itemKey);
            $htmlResult.="Question No.".$number;
            if ($qnItem["qnType"]=="text") {
                $htmlResult .= '<div class="well">'.$qnItem["src"].'</div>';
            } elseif ($qnItem["qnType"]=="image") {
                $srcImage = Url::to(["/admin/questions/item-image", "itemKey"=>$itemKey, "id"=>$questionModel->id]);
                $htmlResult .= '<div class="well"><image src="'.$srcImage.'" class="img-responsive" /></div>';
            }
            $number++;
        }
    } else {
        $htmlResult = '<div class="well">Data not found.</div>';
    }

    return $htmlResult;
}
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Questions</h3>
  </div>
  <div class="panel-body">
        <?=showQuestions($rcModel, $questionModel, $unitID)?>
  </div>
</div>