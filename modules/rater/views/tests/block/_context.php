<?php
/* @var $rcModel app\models\RatingConfigs */
/* @var $questionModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;

function showContexts($rcModel, $questionModel, $unitID)
{
    $htmlResult = "";
    if ($questionModel && $rcModel) {
        $unitA = $rcModel->getUnits();
        $unit = $unitA[$unitID];

        foreach ($unit['items'] as $itemKey) {
            $contextA = $questionModel->getItemContexts($itemKey);
            foreach ($contextA as $key=>$context) {
                $htmlResult.= "<p>".$context["name"]."</p>";
                if ($context["srcType"]=="text") {
                    $htmlResult .= '<div class="well">'.$context["src"].'</div>';
                } elseif ($context["srcType"]=="image") {
                    $srcImage = Url::to(["/admin/questions/context-image", "itemKey"=>$itemKey, "contextKey"=>$key, "id"=>$questionModel->id]);
                    $htmlResult .= '<div class="well"><image src="'.$srcImage.'" class="img-responsive" /></div>';
                }
            }
        }
    } else {
        $htmlResult = '<div class="well">Data not found.</div>';
    }

    return $htmlResult;
}
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Contexts</h3>
  </div>
  <div class="panel-body">
    <?=showContexts($rcModel, $questionModel, $unitID)?>
  </div>
</div>


