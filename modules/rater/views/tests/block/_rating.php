<?php
/* @var $rcModel app\models\RatingConfigs */
/* @var $questionModel app\models\Questions */
/* @var $answerModel app\models\Answers */
/* @var $unitID integer */

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\RatedUnits;

/**
 * Generate context ratings html table row string
 * @param app\models\RatingConfigs $rcModel
 * @param app\models\Questions $questionModel
 * @param app\models\RatedUnits $ruModel
 * @param integer $unitID
 * @return html string of table row
 */
function showRatings($rcModel, $questionModel, $ruModel, $unitID)
{
    $htmlResult = "";
    if ($questionModel && $rcModel) {
        $unitA = $rcModel->getUnits();
        $unit = $unitA[$unitID];

        $rateDisabled = "";
        $ruRatings = [];
        if ($ruModel) {
            $ruRatings = $ruModel->getRatings();
            if ($ruModel->status==-1) {
                $rateDisabled = "disabled";
            }
        }

        foreach ($unit['items'] as $itemKey) {
            $contextA = $questionModel->getItemContexts($itemKey);
            foreach ($contextA as $key=>$context) {
                $htmlResult.='<tr><th>'.$context["name"].Html::input("hidden", 'unit[context]['.$itemKey.']['.$key.']').'</th>';

                for ($i=1; $i<=5 ; $i++) {
                    $check="";
                    if ($ruRatings && $ruRatings[$itemKey][$key]==$i) {
                        $check= "checked";
                    }
                    $htmlResult.= '<td><input type="radio" class="radio-context unit-context" name="unit[context]['.$itemKey.']['.$key.']" value="'.$i.'" '.$rateDisabled.' '.$check.'></td>';
                }

                $htmlResult.= '<td><input type="radio" class="radio-context unit-context" name="unit[context]['.$itemKey.']['.$key.']" value="0" '.$rateDisabled.'></td>';
                $htmlResult.= '</tr>';
            }
        }
    } else {
        $htmlResult = '<tr><td colspan="7">Data not found.</td></tr>';
    }

    return $htmlResult;
}

$ruModel = RatedUnits::find()->where(["answerID"=>$answerModel->id, "unitID"=>$unitID])->one();

$pendingCheck = "";
$comments = "";
if ($ruModel) {
    if ($ruModel->status=="-1") {
        $pendingCheck="checked";
    }
    $comments = $ruModel->comments;
}

$unitKey = Yii::$app->request->get("unitKey", 0);
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Ratings</h3>
    </div>
    <div class="panel-body">
        <table class='table table-bordered table-striped'>
            <tr>
                <th>Context</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>NR</th>
            </tr>
            <?=showRatings($rcModel, $questionModel, $ruModel, $unitID);?>
        </table>

        <div class="checkbox">
            <label for="pending">
                <input id="pending" class="chkbox-unit-pending" type="checkbox" value="1" name="unit[pending]" <?=$pendingCheck?>>Pending
            </label>
        </div>

        <p><button class='toggle-commentbox btn btn-default btn-xs'>Comments</button></p>

        <div class="row">
            <div class="col-md-9">
                <textarea id='commentbox' name="unit[comments]" class='form-control' style='height:60px; <?=$comments=="" ? "display:none" : ""?>'><?=$comments?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="pull-left">
        <?=Html::a("<i class='fa fa-chevron-left'></i> Prev", ['block', "unitKey"=>$unitKey>0?$unitKey-1:0], ["class"=>"btn btn-default btn-prev"])?>
    </div>
    <div class=""></div>
    <div class="pull-right">
        <?php 
          if ($isLastUnit) {
              echo Html::submitButton("Submit", ["class"=>"btn btn-default btn-next"]);
          } else {
              echo Html::submitButton("Next <i class='fa fa-chevron-right'></i>", ["class"=>"btn btn-default btn-next"]);
          }
        ?>
    </div>
  </div>                    
</div>
