<?php

use yii\helpers\Html;

$css = <<<CSS
.tab-content {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    
    padding: 20px;

}

.nav-tabs {
    margin-bottom: 0;
}

.navbar-text {
  padding: 0 10px;
}

.question-panel {
    max-height: 400px;
    overflow-y: scroll;
}

.answers-panel {
    max-height:400px;
    overflow-y: scroll;
}

.qn-img {
  max-width:100%;
}

.qn-label {
    margin-bottom:35px;
}

/* in unit widget */
.des-panel {
  display:none;
}

.answer-text {
    white-space: pre-wrap;
    max-height: 480px;
    overflow-y: scroll;
}
CSS;
$this->registerCSS($css);

$questionModel = $answerModel->questionModel;
$blockA = $questionModel->getBlocks();
$block = $blockA[$blockKey];
$blockName = $block['name'];

$unitKey = Yii::$app->request->get("unitKey", 0); // key in the block[units]

if (isset($block["units"])) {
    $blockUnits = $block["units"];
    $lastKey = count($blockUnits)-1;
}

$this->registerJsFile('@web/web/js/wavesurfer/wavesurfer.min.js');
$this->registerJsFile('@web/web/js/wavesurfer/wavesurfer-html-init.min.js');
?>

<nav class="navbar navbar-inverse" style='margin-top:5px'>
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Rating System</a>
    </div>
	<p class="navbar-text"></p>
	<p class="navbar-text"><?=$answerModel->rc->test_name ?? "Test Name" ?></p>
	<p class="navbar-text">Answer ID: <b><?=$answerModel->id ?? "not set" ?></b></p>
	<p class="navbar-text">Block: <b><?=$blockKey ?? "not set" ?></b></p>
	<p class="navbar-text">Block Start: <b></b></p>
    <ul class="nav navbar-nav navbar-right">

      <li>
	  <?php
        $link = '<i class="glyphicon glyphicon-log-out"></i> Exit';
        echo Html::a(
            $link,
            ['index','projectID'=>$answerModel->rcModel->projectID],
            ['data-confirm'=>"Exit Rating?"]
        );
      ?>
	  </li>
    </ul>	 
  </div>
</nav> 

<div class="qn-label">
<h3 class="pull-right"><span class="label label-warning"><?=$questionModel->getType()?></span></h3>
</div>

<?=Html::beginForm(
    [
      "submit-answer",
      "ansID"=>Yii::$app->request->get("ansID"),
      "blockKey"=>Yii::$app->request->get("blockKey")
    ],
    'post',
    [
      "id"=>"formSubmitRatingUnit"
    ]
  )
;?>
<ul class="nav nav-tabs">
  <?php
        if (count($blockUnits)) {
            foreach ($blockUnits as $key=>$unit) {
                $units = $questionModel->getUnits();
                if ($unitKey==$key) {
                    $unitID = $blockUnits[$unitKey]; // key in the units
                    $unitStatus = $ratedBlockModel->getUnitStatus($unitID);
                    echo '<li class="active"><a href="#"> Item:'.$units[$unit]["name"].'</a></li>';
                } else {
                    echo '<li class="disabled"><a>'.$units[$unit]["name"].'</a></li>';
                }
            }
        }
    ?>
</ul>
 
 <div class="tab-content"> 
  <div  class="tab-pane active">
        <div class="row">
            <div class="col-md-6">
                <?=$this->render(
                    "rate-answer/_instruction",
                    [
                      "questionModel"=>$questionModel,
                      "block"=>$block
                    ]
                  )
                ?>
                
                <?=$this->render(
                    "rate-answer/_examples",
                    [
                      "questionModel" => $questionModel,
                      "unitID"=>$unitID
                    ]
                  );
                ?>				
				
                <?=$this->render(
                    "rate-answer/_context",
                    [
                      "questionModel"=>$questionModel,
                      "unitID"=>$unitID
                    ]
                  )
                ?>
            </div>

            <div class="col-md-6">
                <?=$this->render(
                    "rate-answer/_question",
                    [
                      "questionModel"=>$questionModel,
                      "unitID"=>$unitID
                    ]
                  )
                ?>

                <?=$this->render(
                    "rate-answer/_answer",
                    [
                      "questionModel"=>$questionModel,
                      "answerModel" => $answerModel,
                      "unitID"=>$unitID
                    ]
                  )
                ?>

                <?php 
                    if ($unitStatus["status"]!=2) {
                        echo $this->render(
                            "rate-answer/_rating",
                            [
                                "questionModel"=>$questionModel,
                                "answerModel"=>$answerModel,
                                "ratedBlockModel"=>$ratedBlockModel,
                                "unitID"=>$unitID
                            ]
                        );
                    } elseif ($unitStatus["status"]==2 && $ratedBlockModel->type!="actual") {
                        echo $this->render(
                            "rate-answer/_answerDelta",
                            [
                                "questionModel"=>$questionModel,
                                "answerModel"=>$answerModel,
                                "ratedBlockModel"=>$ratedBlockModel,
                                "unitID"=>$unitID
                            ]
                        );
                    }
                ?>
				
    			<section>	
                    <?=$this->render(
                        'rate-answer/_finishNextButton',
                        [
                          "isLastUnit"=>($lastKey==$unitKey),
                          "unitStatus"=>$unitStatus
                        ]
                      )
                    ?>
    			</section>

            </div>
        </div>
  </div>
 </div>
<?=Html::input("hidden", "unit[id]", $unitID)?>
<?=Html::input("hidden", "unit[key]", $unitKey)?>
<?=Html::endForm();?>

<?php

$js = <<<JS
$('.toggle-commentbox').click(function(e) {
  e.preventDefault();
  $('#commentbox').toggle();
});

// if pending checked should disabled all rating 
$(".chkbox-unit-pending").on("click",function(e){
	
    var me=$(this);
    var context = $(".rating_radio");
    if (me.prop("checked")) {
        context.prop("checked", false).prop("disabled",true);
    } else {
        context.prop("disabled",false);
    }

    // hide remark rows when pending
    $(".row-remark").hide();
    $(".input-remark").val("");
    $(".cell-name").removeAttr("rowspan");
    //----
});


$('.rating_radio').click(function(e) {
	var val = $(this).val();	
	if (val == '0') {
    var c = confirm("Set all ratings to UNrated?");
    
    if(!c) {
      e.preventDefault();
      return;
    }		
		
		$('[class="rating_radio"][value="0"]').each(function(e) {
			$(this).prop('checked', true);
		});
	} else if ( val == 1) { // show remark
    var key = $(this).data("key");

    $(".row-remark-"+key).show();
    $(".cell-name-"+key).attr("rowspan", "2");
  } else { // hide remark and empty remark value
    var key = $(this).data("key");
    $(".row-remark-"+key).hide();
    $(".input-remark[data-key="+key+"]").val("");
    $(".cell-name-"+key).removeAttr("rowspan");
  }
});

$(".btn-audio-control").on("click",(e)=>{
    e.preventDefault();
});

JS;
$this->registerJS($js);
?>
