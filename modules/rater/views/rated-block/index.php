<?php
/*
 * @var $rcBlocks
 * @var $questionTrainingModels
 * @var $questionActualModels
*/

use yii\helpers\Html;

$show = false;

?>

<h1>Rating Block Index</h1>

<div class="rated-block-index">


<h3> Blocks (Actual) </h3>
<?php if ($show) : ?>
<ul>
	<li>Actual</li>
	<li>Forced - Admin forced specific rater</li>
	<li>Pending - Admin sets pending mode for rater leaders</li>
	<li>In-Process - Block returned by rater</li>	
</ul>
<?php endif; ?>

<table class="table table-bordered">
	<tr>
		<th>Test(RC id)</th>
		<th>Question ID</th>
		<th>Answer ID</th>
		<th>blockKey</th>
		<th></th>
	</tr>
	<?php
        foreach ($ansActual as $answer):
        
            $blocks = $answer->getBlocks();
            #echo "<pre>";
            #print_r($blocks);
            #echo "</pre>";
            foreach ($blocks as $blockKey=>$block):
    ?>
				<tr>
					<td><?=$answer->rcID?>: <?= $answer->rc->test_name ?></td>
					<td><?= $answer->qnID ?> <?= $answer->qn->type ?></td>
					<td><?=$answer->id?></td>	
					<td><?= $blockKey ?></td>
					<td><?= Html::a('Rate model Answer', ['rate-answer','ansID'=>$answer->id,
                                                      'blockKey'=>$blockKey])  ?>
					
					<?php 
					print_r($answer->rc->getRaters($blockKey));
					
					?>
					</td>
					
				</tr>
	<?php
            endforeach;
        endforeach;
    ?>
</table>



<div class="" style='padding: 50px 0'>
	<hr>
</div>




<section>
<h3> Blocks (training) </h3>

<?php if ($show) : ?>
<ul>
	<li>Training - Done B4 Certification</li>
	<li>Re-Training - Done every x number of rater</li>
	<li>Cert - To do a block</li>
	<li>Quality Control* Cloned from Actual - Done every x number</li> 
</ul>
<?php endif; ?>



<table class="table table-bordered">
	<tr>
		<th>Test Set</th>
		<th>Q Set Type</th>
		<th>Question Set</th>
		<th>block Key</th>
		<th></th>
	</tr>
	<?php
        foreach ($ansTraining as $answer):
            $blocks = $answer->getBlocks();
            foreach ($blocks as $blockKey=>$block):
    ?>
				<tr>
					<td><?=$answer->rcID?></td>
					<td><?=$answer->questionModel->getType()?></td>
					<td><?=$answer->id?></td>	
					<td><?=$blockKey?>: </td>
					<td><?= Html::a('Rate model Answer', ['rate-answer','ansID'=>$answer->id,
                                                      'blockKey'=>$blockKey])  ?>
					</td>
				</tr>
	<?php
            endforeach;
        endforeach;
    ?>

</table>
</section>
</div>