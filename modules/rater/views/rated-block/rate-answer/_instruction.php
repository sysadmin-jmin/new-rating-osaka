<?php
/* @var $questionModel app\models\Questions */
/* @var $blockKey integer */

use yii\helpers\Url;

function showInstructions($questionModel, $block)
{
    $htmlResult = '<div class="well">Data not found.</div>';
    if ($questionModel) {
        $instructionA = $questionModel->getGenInstructions($block["id"]);

        if (count($instructionA)) {
            $htmlResult="";
            foreach ($instructionA as $key=>$instruction) {
                $htmlResult.= "<h4>".$instruction['name']."</h4>";
                $srcImage = Url::to(
                              [
                                "/admin/questions/gen-instructions-image",
                                "blockID"=>$block["id"],
                                "id"=>$questionModel->id,
                                "key"=>$key
                              ]
                            );
                $htmlResult.= '<div class="well"><image src="'.$srcImage.'" class="img-responsive img-thumbnail" /></div>';
            }
        }
    }

    return $htmlResult;
}

?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Instructions</h3>
  </div>
  <div class="panel-body">
        <?=showInstructions($questionModel, $block)?>
  </div>
</div>