<?php
/* @var $questionModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;

function showQuestions($questionModel, $unitID)
{
    $htmlResult = '<div class="well">Data not found.</div>';
    if ($questionModel) {
        $unitA = $questionModel->getUnits();
        $unit = $unitA[$unitID];

        $number=1;
        if (isset($unit['items']) && count($unit['items'])) {
            $questionCount=0;
            $htmlResult = "";
            foreach ($unit['items'] as $itemKey=>$item) {
                $qnItem = $questionModel->getItems($itemKey);
                $htmlResult.="<h4>Question ".$number."</h4>";
                if (!empty($qnItem["src"])) {
                    if ($qnItem["qnType"]=="text") {
                        $htmlResult .= '<div class="well">'.$qnItem["src"].'</div>';
                    } elseif ($qnItem["qnType"]=="image") {
                        $srcImage = Url::to(
                                        [
                                            "/admin/questions/item-image",
                                            "itemKey"=>$itemKey,
                                            "id"=>$questionModel->id
                                        ]
                                    );
                        $htmlResult .= '<div class="well"><image src="'.$srcImage.'" class="img-responsive" /></div>';
                    }
                } else {
                    $htmlResult .= '<div class="well"><em>(not set)</em></div>';
                }
                $number++;
                $questionCount++;
            }
        }
    }

    return $htmlResult;
}
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Questions</h3>
  </div>
  <div class="panel-body">
        <?=showQuestions($questionModel, $unitID)?>
  </div>
</div>