<?php
/* @var $questionModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;

function showExamples($questionModel, $unitID)
{
    $htmlResult = '<div class="well">Data not found.</div>';
    if ($questionModel) {
        $exampleA = $questionModel->getExamples($unitID);
        if (count($exampleA)) {
            $htmlResult="";
            foreach ($exampleA as $key=>$example) {
                $htmlResult.= "<h4>".$example["name"]."</h4>";
                $srcImage = Url::to(
                                [
                                    "/admin/questions/examples-image",
                                    "unitKey"=>$unitID,
                                    "key"=>$key,
                                    "id"=>$questionModel->id
                                ]
                            );
                $htmlResult .= '<div class="well"><image src="'.$srcImage.'" class="img-responsive" /></div>';
            }
        }
    }

    return $htmlResult;
}
?>

<div class="panel panel-default">
	<div class="panel-heading">Examples</div>

	<div class="panel-body">
		<?=showExamples($questionModel, $unitID)?>
	</div>
</div>