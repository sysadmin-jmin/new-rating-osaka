<?php
/* @var $questionModel app\models\Questions */
/* @var $answerModel app\models\Questions */
/* @var $ratedBlockModel app\models\RatedBlock */
/* @var $unitID integer */

use yii\helpers\Url;
use yii\helpers\Html;

$css = <<<CSS
.result-table {
	max-width: unset;
	width: 120%;
}
.badge-green {
	background-color:green;
}
.badge-maroon {
    background-color:maroon;	
}
CSS;

$this->registerCss($css);


function showDelta($questionModel, $answerModel, $ratedBlockModel, $unitID)
{
    $ratedBlocksDelta = $ratedBlockModel->ratedBlocksDelta;
    $htmlResult = "";
    if ($ratedBlocksDelta) {
        $modelRating = $ratedBlocksDelta->modelRating;

        $contextA 		= $questionModel->getUnitContexts($unitID);
        $modelRates 	= $modelRating->getRatings($unitID);
        $actualRates 	= $ratedBlockModel->getRatings($unitID);
        $delta 			= $ratedBlocksDelta->getDelta($unitID);

        foreach ($contextA as $key=>$context) {
            $badgeColor  = ($delta[$key]=="0") ? 'badge-green' : 'badge-maroon';
            $remark      = $actualRates[$key]["remark"] ?? "";
            $actualValue = $actualRates[$key]["rating"] ?? "";
            $modelValue  = $modelRates[$key]["rating"] ?? "";
            $deltaValue  = $delta[$key];

            $htmlResult .=
                        "<tr>
							<td>".$context["name"]."</td>
							<td>".$actualValue."</td>
							<td>".$modelValue."</td>
							<td><span class=\"badge {$badgeColor}\">".$deltaValue."</span></td>
							<td><small>{$remark}</small></td>
						</tr>";
        }
    }

    return $htmlResult;
}

?>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Results</h3>
  </div>
  <div class="panel-body" style="overflow-x:scroll">
    <table class='result-table table table-bordered'>
		<tr>
			<th>Context</th>
			<th>You</th>
			<th>Model</th>
			<th>Diff</th>
			<th>Reason</th>
		</tr>
        <?=showDelta($questionModel, $answerModel, $ratedBlockModel, $unitID)?>
	</table>
  </div>
</div>
