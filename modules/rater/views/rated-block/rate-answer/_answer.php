<?php
/* @var $questionModel app\models\Questions */
/* @var $answerModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Icon;


$js = <<<JS
window.WS_InitOptions={
  defaults:{
    mediaControls:false,
  }
};

$(".audioplayer")
  .on("click", "a[data-action=play]", function(e){
    var audio_id = $(this).attr("data-id");

    var wsInstances = window.WaveSurferInit.instances;
    var ws = null;
    $.each(wsInstances, function(index, item){
        if(item.container.id==audio_id){
          ws = item;
          return;
        }
      });
    ws.playPause();
  });

JS;

$this->registerJs($js);

function showAnswers($questionModel, $answerModel, $unitID)
{
    $htmlResult = '<div class="well">Data not found.</div>';
    if ($answerModel && $questionModel) {
        $itemsA = $answerModel->getItems();
        $unit = $questionModel->getUnits($unitID);
        if (isset($unit["items"]) && count($unit["items"])) {
            $htmlResult="";
            $number = 1;
            foreach ($unit["items"] as $itemKey) {
                $htmlResult.= "<h4>Answer $number</h4>";
				
                $item = @$itemsA[$itemKey] ?? [];
                if (count($item) > 0 && isset($item["src"]) && strlen(trim($item["src"])) > 0) {
                    
					if ($item['ansType']=="image" || $item['ansType'] =='text') {
						
						
						
                        $srcImage = Url::to(
                                        [
                                            "/admin/answers/item-image",
                                            "itemID"=>$itemKey,
                                            "id"=>$answerModel->id
                                        ]
                                    );
                        $htmlResult .= '<div class="well">';
						
						$htmlResult .= Html::img($srcImage,['class'=>'img-responsive img-thumbnail']);
						
						$htmlResult .= "</div>";
					} elseif ($item['ansType']=="audio") {
                        $srcAudio= Url::to(
                                        [
                                            "/admin/answers/item-audio",
                                            "itemID"=>$itemKey,
                                            "id"=>$answerModel->id
                                        ],
                                        true
                                    );
                        $htmlResult .= '<div class="well audioplayer">
                                          <wavesurfer data-url="'.$srcAudio.'" id="audio-'.$itemKey.'"></wavesurfer>
                                          <div class="text-center">
                                            '.Html::a(
                                                Icon::fa("play"). " Play / ". Icon::fa("pause"). " Pause",
                                                "#",
                                                [
                                                  "class"=> 'btn btn-primary btn-xs btn-audio-control',
                                                  "data-action"=> "play",
                                                  "data-id" => "audio-".$itemKey
                                                ]
                                              ).
                                          '</div>
                                        </div>';
                    } else {
                        //$src= Url::to(["/admin/answers/item-text", "itemID"=>$itemKey, "id"=>$answerModel->id],true);
                        $src= sprintf(Url::to('@app/data/answers/%d/%s'),$answerModel->id,$item["src"]);

                        if (!file_exists($src))
                            $htmlResult .= '<div class="well"><span class="text-bold text-italic">File could not be loaded.</span></div>';
                        else {
                            $content = file_get_contents($src);
                            $htmlResult .= '<div class="well answer-text">' . $content . '</div>';
                        }
                    }
                } else {
                    $htmlResult .= '<div class="well"><em>(not set)</em></div>';
                }
            }
        }
    }

    return $htmlResult;
}
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Answers</h3>
  </div>
  <div class="panel-body">
    <?=showAnswers($questionModel, $answerModel, $unitID);?>
  </div>
</div>