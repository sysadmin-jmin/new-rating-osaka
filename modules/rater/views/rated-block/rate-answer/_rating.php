<?php
/* @var $questionModel app\models\Questions */
/* @var $answerModel app\models\Answers */
/* @var $unitID integer */

use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Generate context ratings html table row string
 * @param app\models\Questions $questionModel
 * @param integer $unitID
 * @return html string of table row
 */
function showRatings($questionModel, $ratedBlockModel, $unitID)
{
    $htmlResult = "";
    if ($questionModel) {
        $rateDisabled = "";
        $unitRatings = [];
        if ($ratedBlockModel) {
            $unitRatings = $ratedBlockModel->getRatings($unitID);
        }

        $contextCount =0;
        $contextA = $questionModel->getUnitContexts($unitID);
        foreach ($contextA as $key=>$context) {
            $rateValue  = $unitRatings[$key] ?? ["rating"=>"", "remark"=>""];
            $showRemark = $rateValue["rating"]=="1" ? 'rowspan="2"' : '';
            $htmlResult.='
                    <tr>
                        <th class="cell-name cell-name-'.$key.'" '.$showRemark.'>'.
                            $context["name"].Html::input("hidden", 'unit[context]['.$key.'][rating]').'
                        </th>';
            for ($i=1; $i<=5 ; $i++) {
                $checked = $rateValue["rating"]==$i ? true : false;
                $htmlResult.= '
                        <td>'.
                            Html::radio(
                                "unit[context][$key][rating]",
                                $checked,
                                [
                                    "value" => $i,
                                    "class"=>"rating_radio",
                                    "data-key"=> $key
                                ]
                            ).'
                        </td>';
            }
            $checked = $rateValue["rating"]=="0" ? true : false;
            $remark = $rateValue["remark"];
            $showRemark = $rateValue["rating"]=="1" ? '' : 'style="display:none;"';
            $htmlResult.= '
                        <td>'.
                            Html::radio(
                                "unit[context][$key][rating]",
                                $checked,
                                [
                                    "value" => "0",
                                    "class"=>"rating_radio"
                                ]
                            ).'
                        </td>
                    </tr>
                    <tr '.$showRemark.' class="row-remark row-remark-'.$key.'">
                        <td colspan="6"> *Remark'.
                            Html::textarea(
                                "unit[context][$key][remark]",
                                $remark,
                                [
                                    "class" => "form-control input-remark",
                                    "data-key"=>$key
                                ]
                            ).'
                        </td>
                    </tr>';
            $contextCount++;
        }
    }

    if (!$contextCount) {
        $htmlResult = '<tr><td colspan="7">Data not found.</td></tr>';
    }

    return $htmlResult;
}

$unitStatus = $ratedBlockModel->getUnitStatus($unitID);

$pendingCheck = $unitStatus["status"]=="0" ? true : false;
$comments = $unitStatus["comments"];
$unitKey = Yii::$app->request->get("unitKey", 0);
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Ratings</h3>
    </div>
    <div class="panel-body" style="overflow-x:scroll">
        <table class='rating-table table table-bordered table-striped'>
            <tr>
                <th>Context</th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>NR</th>
            </tr>
            <?=showRatings($questionModel, $ratedBlockModel, $unitID);?>
        </table>

        <div class="checkbox">
            <label for="pending">
                <?=
                    Html::checkbox(
                        "unit[pending]",
                        $pendingCheck,
                        [
                            "id"=>"pending",
                            "class" =>"chkbox-unit-pending",
                            "value"=>"1"
                        ]
                    );
                ?>
                Pending
            </label>
        </div>

        <p>
            <?=
                Html::button(
                    'Comments',
                    [
                        "class"=>'toggle-commentbox btn btn-default btn-xs'
                    ]
                );
            ?>
        </p>

        <div class="row">
            <div class="col-md-9">
                <?=
                    Html::textarea(
                        "unit[comments]",
                        $comments,
                        [
                            "id"=>"commentbox",
                            "class"=>"form-control",
                            "style"=>'height:60px;'.($comments=="" ? "display:none;" : "")
                        ]
                    );
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
            <?=
                Html::submitButton(
                        "Submit Rating",
                        [
                            "class"=>"btn btn-primary"
                        ]
                    );
            ?>
            </div>        
        </div>
    </div>
</div>
