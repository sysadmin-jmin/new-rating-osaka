<?php
use app\components\Icon;
use yii\helpers\Html;

function showNextButton($isLastUnit, $unitKey)
{
    if ($isLastUnit) {
        $text = Icon::fa("check")." Save and Finish";
        $links = [
                "finish-answer",
                "ansID"=>Yii::$app->request->get("ansID"),
                "blockKey"=>Yii::$app->request->get("blockKey")
            ];
        $opts = [
                "class" => "btn btn-success",
                "data-confirm" => "Are you sure to finish rating?"
            ];
    } else {
        $text = "Continue to Next Unit ".Icon::fa("chevron-right");
        $links = [
                "rate-answer",
                "ansID" =>Yii::$app->request->get("ansID"),
                "blockKey"=>Yii::$app->request->get("blockKey"),
                "unitKey" =>$unitKey+1
            ];
        $opts = [ "class" => "btn btn-default" ];
    }
    return Html::a($text, $links, $opts);
}

function showPrevButton($unitKey)
{
    if ($unitKey>0) {
        return Html::a(
                    Icon::fa("chevron-left")." Back to Previous Unit",
                    [
                        "rate-answer",
                        "ansID" =>Yii::$app->request->get("ansID"),
                        "blockKey"=>Yii::$app->request->get("blockKey"),
                        "unitKey" =>$unitKey-1
                    ],
                    [
                        "class" => "btn btn-default"
                    ]
                );
    }
}

$unitKey = Yii::$app->request->get("unitKey", 0);
?>

<div class="pull-right">
    <?php
        if ($unitStatus["status"]!=1) {
            echo showNextButton($isLastUnit, $unitKey);
        }
    ?>
</div>

<div>
	<?= showPrevButton($unitKey); ?>
</div>
