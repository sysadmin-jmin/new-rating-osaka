<?php
/* @var $questionModel app\models\Questions */
/* @var $unitID integer */

use yii\helpers\Url;

function showContexts($questionModel, $unitID)
{
    $htmlResult = "";
    if ($questionModel) {
        // $unitA = $questionModel->getUnits($unitID);

        // if (count($unitA)) {
        $contextCount =0;
        // foreach ($unitA as $unitKey=>$unit) {
        $contextA = $questionModel->getUnitContexts($unitID);
        // print_r($contextA);
        // exit;
        foreach ($contextA as $key=>$context) {
            $htmlResult.= "<h4>".$context["name"]."</h4>";
            if ($context["srcType"]=="text") {
                $htmlResult .= '<div class="well">'.$context["src"].'</div>';
            } elseif ($context["srcType"]=="image") {
                $srcImage = Url::to(
                                [
                                    "/admin/questions/context-image",
                                    "unitKey"=>$unitID,
                                    "contextKey"=>$key,
                                    "id"=>$questionModel->id
                                ]
                            );
                $htmlResult .= '<div class="well"><image src="'.$srcImage.'" class="img-responsive" /></div>';
            }
            $contextCount++;
        }
        // }
        // }
    }

    if (!$contextCount) {
        $htmlResult = '<div class="well">Data not found.</div>';
    }

    return $htmlResult;
}
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Contexts</h3>
  </div>
  <div class="panel-body">
    <?=showContexts($questionModel, $unitID)?>
  </div>
</div>


