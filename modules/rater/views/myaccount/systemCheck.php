<?php
/**
 * @var $this yii\web\View
 * @var \app\models\Raters $rater
 */

use yii\helpers\Html;
use app\models\{RatingConfigs, Answers};
use yii\helpers\Url;

$this->registerJsFile('@web/web/js/wavesurfer/wavesurfer.min.js');

$voicePath = Url::to('@web/web/samples/speakingmodelans.mp3');

$css =<<<CSS
 .controls {
    margin: 5px;
 }
 .controls-play {
    display: inline-block;
    widrth: auto;
    min-width: 90px;
 }
 #btn-mute {
    width: 34px;
    height: 34px;
 }
 #volume {
    display: inline-block;
    width: 100px;
    margin: 18px 10px 12px 10px;
 }
 .btn-control-text:before {
    padding-right: 5px;
 }
#waveform {
    height: 128px;
}
CSS;
$this->registerCss($css);

?>
<h1>System check</h1>

<div class="index">

<div class="row">
	<div class="col-md-8">
	    <div id="waveform"></div>
        <div class="controls">
            <div class="controls-play">
                <button class="btn btn-default disabled" id="btn-control">
                    <i class="fa fa-spinner btn-control-text">Loading sound...</i>
                </button>
            </div>
            <button class="btn btn-default" id="btn-mute">
                <i class="fa fa-volume-up"></i>
            </button>
            <input id="volume" type="range" min="0" max="1" value="0.5" step="0.1">
        </div>
	</div>
</div>
<div class="row">
    <div class="controls">
        <div class="col-md-8">
        <?php if ($rater->sound_check == '1'): ?>
            <button class="btn btn-primary disabled" id="btn-sound-check" disabled>Already checked system working</button>
        <?php else: ?>
            <button class="btn btn-primary" id="btn-sound-check">Yes, My System Works</button>
        <?php endif; ?>
        </div>
    </div>
</div>

	
</div>
<?php

$js = <<<JS
$(()=>{
    let jqVol=$('#volume'),jqBtnMute=$('#btn-mute');
    let wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: 'gre',
        progressColor: 'purple'
    });
    let btnCtrl = $("#btn-control");
    wavesurfer.on("finish",function(){
        btnCtrl.children(".fa")
            .removeClass()
            .addClass("fa fa-play btn-control-text")
            .text("Play");
        wavesurfer.seekTo(0);
    });
    wavesurfer.on("ready",function(){
        jqVol.val(wavesurfer.getVolume());
        btnCtrl.removeClass("disabled");
        btnCtrl.children(".btn-control-text").removeClass("fa-spinner").addClass("fa-play").text("Play");
    });
    wavesurfer.load("$voicePath");
    $("#btn-control").on("click",function(e){
        wavesurfer.playPause();
        $(this).children('.fa').removeClass("fa-play fa-pause")
            .addClass(wavesurfer.isPlaying()?"fa-pause":"fa-play")
            .text(
                wavesurfer.isPlaying() ? 'Pause' : 'Play'
            )
    });
    jqBtnMute.on('click',function(e){
        wavesurfer.setMute(!wavesurfer.getMute());
        if(wavesurfer.getMute())
            jqBtnMute.html('<i class="fa fa-volume-off"></i>');
        else
            jqBtnMute.html('<i class="fa fa-volume-up"></i>');
    });
    
	
	jqVol.on('input',function(e){
		let vol_ = jqVol.val();
		console.log(vol_);
		wavesurfer.setVolume(vol_);
	});
	
	
	// buttons
	$("#btn-sound-check").on("click",function(e){
	    e.preventDefault();
	    $(this).attr("disabled",true).addClass('disabled');
	    window.location.href = 'system-check?sound_check=1';
	});
	
});
JS;
$this->registerJs($js);
