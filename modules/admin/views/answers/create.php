<?php

use app\components\Icon;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = 'Create A Model Answer';

$this->params['breadcrumbs'][] = ['label' => 'Rating Configuration Set', 'url' => ['rating-configs/view', 'id' => $rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Question Sets', 'url' => ['/admin/questions', 'rcID' => $rcID, 'qnID' => $qnID]];
$this->params['breadcrumbs'][] = ['label' => 'Answers', 'url' => ['index', 'rcID' => $rcID, 'qnID' => $qnID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function fa($icon='', $url=['']) {
    switch ($icon) {

        case 'edit':
            $i = "<i class='fa fa-edit'> </i>";
            break;
        case 'trash':
            $i = "<i class='fa fa-trash-o'> </i>";
            break;
        case 'create':
        default:
            $i = "<i class='fa fa-plus'> </i>";
            break;
    }

    return Html::a($i,$url,[]);
}


use yii\widgets\ActiveForm;
?>
<style>

    .item-div {
        padding: 0 0 0 5px;
    }

</style>
<h1>Create A Model Answer For This Question Set</h1>

<p>
	<?= Html::a('Back',['actual-index',
						'qnID'=>$model->qnID,
						'rcID'=>$model->rcID],
						['class'=>'btn btn-default']) ?>

</p>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Items</div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin() ?>

                <?= $form->field($model,'rcID') ?>

                <?= $form->field($model,'qnID') ?>
				
				<?= $form->field($model,'units') ?>
				
				<?= $form->field($model,'blocks') ?>
				
				
				
				

                <?= $form->field($model,'isModel')->dropDownList($model->getIsModelList()) ?>

                <?= $form->field($model,'type')->dropDownList($model->getTypeList()) ?>

                <?= $form->field($model,'sheet_status')->dropDownList($model->getStatusList())  ?>

                <?= Html::submitButton('Create',['class'=>'btn btn-primary']) ?>

                <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>


    <div class="col-md-6">

    </div>

</div>
