<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Answers;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Answers - Actual";

$this->params['breadcrumbs'][] = ['label' => 'Rating Configuration Set', 'url' => ['rating-configs/view', 'id' => $rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Question Sets', 'url' => ['/admin/questions', 'rcID' => $rcID, 'qnID' => $qnID]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="answers-index">

<h1><?= $this->title ?></h1>

<p>
    <?= Html::a("Back",['questions/index','rcID'=>$rcID],['class'=>'btn btn-default']) ?>
</p>

<p class='btn-group'>
<?php 
echo Html::a('Generate Dummie Actual Answers',
			['#'],['class'=>'btn btn-default', 'id' => 'btn-gen-dummy-answers']);

echo Html::a('Import',['import','rcID' => $rcID,'qnID'=>$qnID],
			['class'=>'btn btn-default']);
			

echo Html::a('Create Answer',['create','rcID'=>$rcID,
										'qnID'=>$qnID],
							 ['class'=>'btn btn-default']);				
	

echo Html::a('Truncate',['delete', 'rcID'=>$rcID, 'qnID'=>$qnID],['class'=>'btn btn-danger','data-confirm'=>"Truncate all answers and images?"]) 

?>
</p>

<p>
<?= Html::a('View Pending RABS',['actual-pending-rab','rcID'=>$rcID,'qnID'=>$qnID],['class'=>'btn btn-default']) ?>

</p>

	

<?php 

$options = function (Answers $model) {
	$s = '<ul>'; 
	$s .= '<li>';
	$s .= Html::a(Icon::Fa('upload'). ' Upload',['update', 'id' => $model->id]);
	$s .= '</li>';

	$s .= "</ul>";
	return $s;
};


$advOptions = function (Answers $model) {
	$s = '<ul>'; 
	$s .= '<li>';
	$s .= Html::a(Icon::Fa('user'). ' Force Assign',['assign', 'id' => $model->id]);
	$s .= '</li>';

	return $s;
};


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => 'ID',
        ],
        'type',
		'datetime_created:datetime',
		'sheet_status',
		
        [
            'attribute' => 'model',
            'value' => function (Answers $model) {
                return $model->isModel == Answers::ISMODEL_MODEL ? 'Yes' : 'No';
            },
        ],
        
		['label'=>'Options',
		 'value'=> $options,
		 'format'=> 'html'
		],

		['label'=>'Advance Options',
		 'value'=> $advOptions,
		 'format'=> 'html'
		],
		
	],

]); ?>


</div>
<?php
$js = <<< JS
$("#btn-gen-dummy-answers").on("click",function(e){
    alert('Preparing...');
    e.preventDefault();
    return false;
});
JS;
$this->registerJs($js);