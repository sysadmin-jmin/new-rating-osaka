<?php 
use yii\grid\GridView;

$this->title = "Pending RABs in this Actual Question Set";

$questionModel = $answerModel->questionModel;

?>

<h1><?php echo $this->title ?></h1>

<div class="pending-rabs">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'Answer Sheet',
                'value' => function ($model) {
                    return $model->ansID;
                }
            ],
            [
                'attribute' => 'Rated Blocks',
                'value' => function ($model) use ($questionModel) {
                    $block = $questionModel->getBlock($model->blockID);
                    return $model->blockID. ": ". $block["name"];
                }
            ],
            [
                'label'=>'Options',
                'format'=>'html',
            ],
        ],
    ]); ?>

</div>

