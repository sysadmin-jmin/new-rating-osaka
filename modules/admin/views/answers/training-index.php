<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Answers;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Answers";

$this->params['breadcrumbs'][] = ['label' => 'Rating Configuration Set', 'url' => ['rating-configs/view', 'id' => $rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Question Sets', 'url' => ['/admin/questions', 'rcID' => $rcID, 'qnID' => $qnID]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="answers-index">

<h1><?= $this->title ?></h1>

<p>
    <?= Html::a("Back",['questions/index','rcID'=>$rcID],['class'=>'btn btn-default']) ?>
</p>

<p class='btn-group'>
<?php 
switch ($qnType) {
	case 'actual':
		echo Html::a('Generate Dummie Actual Answers',
					['#'],['class'=>'btn btn-default', 'id' => 'btn-gen-dummy-answers']);
		echo Html::a('Import',['import','rcID' => $rcID,'qnID'=>$qnID],
					['class'=>'btn btn-default']);
					
		if(YII_ENV == 'dev') {
			echo Html::a('Create Model Answer',['create','rcID'=>$rcID,
														'qnID'=>$qnID],
														['class'=>'btn btn-default']);			
			
		}			
					
					
		break;
		
	case 'training':
	case 'cert':
	case 'retraining':
	case 'qc':
		echo Html::a('Create Model Answer',['create','rcID'=>$rcID,'qnID'=>$qnID],
				['class'=>'btn btn-default']);
		break;
	}
		
echo Html::a('Truncate',['delete', 'rcID'=>$rcID, 'qnID'=>$qnID],['class'=>'btn btn-danger','data-confirm'=>"Truncate all answers and images?"]) 

?>
</p>

	

<?php 

$options = function (Answers $model) {
	$s = '<ul>'; 
	$s .= '<li>';
	$s .= Html::a(Icon::Fa('upload'). ' Upload',['update', 'id' => $model->id]);
	$s .= '</li>';
	
	$s .= '<li>';
	$s .= Html::a('Model Rating',['model-ratings/create',
								'ansID'=>$model->id,
								'qnID'=>$model->qnID
								]);
	$s .= '</li>';
	
	$s .= '<li>';
	$s .= Html::a('Actual Rating',['rated-block/create',
								'ansID'=>$model->id,
								'qnID'=>$model->qnID
								]);
	$s .= "</li>";
	
	$s .= "<li>";
	$s .= Html::a('Rating Delta',['rated-block/create',
								'ansID'=>$model->id,
								'qnID'=>$model->qnID
								]);
	$s .= "</li>";
	return $s;
};


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => 'ID',
        ],
        'type',
		'datetime_created:datetime',
		'sheet_status',
		
        [
            'attribute' => 'model',
            'value' => function (Answers $model) {
                return $model->isModel == Answers::ISMODEL_MODEL ? 'Yes' : 'No';
            },
        ],
        
		['label'=>'Options',
		 'value'=> $options,
		 'format'=> 'html'
		],

		
	],

]); ?>


</div>
<?php
$js = <<< JS
$("#btn-gen-dummy-answers").on("click",function(e){
    alert('Preparing...');
    e.preventDefault();
    return false;
});
JS;
$this->registerJs($js);