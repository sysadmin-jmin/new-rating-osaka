<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use dosamigos\fileupload\FileUploadUI;
use app\assets\FlowAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Answer Set ";

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
FlowAsset::register($this);
?>

<h1><?= $this->title ?></h1>

<p>
    <?= Html::a("Back",['answers/index','rcID'=>$rcID,'qnID'=>$model->qnID],['class'=>'btn btn-default']) ?>
</p>
<div class="answer-import">

    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-primary">
                <div class="panel-heading">Import Answers CSV</div>

                <div class="panel-body">

                    <p class='text-warning'>
                        CSV file
                        Zip file of folders
                    </p>

                    <?php echo Html::beginForm('', 'post'); ?>
                    <div class="">
                        <div>File size: <span id="filesize">0 bytes</span></div>
                        Chunk num: <span id="chunks">0</span> / <span id="chunk-total">0</span>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <div class="img-box"></div>

                    <button type="button" id="btn-upload" class="btn btn-info"  data-status="start" data-rcID="<?php echo $rcID ?>" data-qnID="<?php echo $model->qnID ?>">Upload</button>
                    <button type="button" id="btn-progress" class="btn btn-warning disabled" data-status="pause">Pause</button>
                    <?php echo Html::endForm(); ?>
                </div>

            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-md-7">


            <div class="panel panel-primary">

                <div class="panel-heading">Upload File</div>

                <div class="panel-body">

                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


                    <?= $form->field($model, 'qnID')->textInput(['readonly'=>true]); ?>

                    <?php /* =$form->field($model, 'dataFile')->fileInput()*/ ?>

                    <?php /*= $form->field($model, 'answerZipFile')->fileInput()*/ ?>

                    <?= FileUploadUI::widget([
                        'model' => $model,
                        'attribute' => 'dataFile',
                        'url' => ['media/upload'],
                        'gallery' => false,
                        'fieldOptions' => [
                            'accept' => 'image/*'
                        ],
                        'clientOptions' => [
                            'maxFileSize' => 2000000
                        ],
                        // ...
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                        ],
                    ]);

                    ?>

                    <?php Activeform::end() ?>

                </div>

            </div>

        </div>

    </div>

</div>

<?php
$js = <<<JS
    const CHUNK_SIZE=$supportUploadBytes;

    (function(){
        let rcID = $rcID;
        let qnID = {$model->qnID};
        let csrfToken = document.getElementsByName('csrf-token')[0].getAttribute('content');
        let flow = new Flow({
            target:'upload?qnID='+qnID
            ,chunkSize:CHUNK_SIZE
            ,forceChunkSize:true
            ,singleFile:true
            ,query:{__csrf:csrfToken}
            ,headers:{'X-CSRF-Token':csrfToken}
        });
        var flowfile= flow.files;
        
        
        let btnUpload = $("#btn-upload");
        let btnProgress = $("#btn-progress")
        let progressBar = $('.progress-bar');
        
        
        let upload = {
            start:()=>{
                flow.opts.qnID = qnID;
                flow.upload();
                
                btnUpload.data("status", "cancel")
                    .text("Cancel")
                    .removeClass("btn-info")
                    .addClass("btn-danger");
            },
            cancel:()=>{
                flow.cancel();
                btnUpload.text("Upload")
                    .data("status","start")
                    .removeClass("btn-danger")
                    .addClass("btn-info");
                progressBar
                    .css({width:'0%'}).prop("aria-valuenow",0).text("0 %");
                $("#chunks").text('0');
                $("#chunk-total").text(0);
            },
            retry:()=>{
                flow.retry();
                btnUpload.text("Cancel")
                    btnUpload.data("status","cancel")
                    btnUpload.removeClass("btn-warning")
                    btnUpload.addClass("btn-danger");
            },
            
        };
        
        flow.assignBrowse(btnUpload.get(0),false,true,{accept:'text/csv,application/zip'});
        
        
        // File selected
        flow.on('filesSubmitted', function(/*FlowFile*/file) {
            // TODO set targetfile
            btnProgress.removeClass("disabled");
            progressBar
                .css({width:'0%'})
                .prop("aria-valuenow",0)
                .text("0 %")
                .addClass("progress-bar-striped active")
                .removeClass("progress-bar-info progress-bar-success");
            if (flow.isUploading() || (btnUpload.data("status")==="resume" || btnUpload.data("status")==="cancel")) {
                if (!confirm('Are you sure want to abort while current uploading files ?'))
                    return;
                flow.cancel();
                btnUpload.text("Start")
                    .data("status", "start")
                    .removeClass("btn-danger")
                    .addClass("btn-info");
            }
            //console.log(file);
            file[0].bootstrap();
            var size = file[0].file.size;
            var unitLevel = 0;
            let units = ['bytes','KB','MB','GB','TB','PB','EB'];
            while (1024 < size) {
                size = size / 1024;
                unitLevel++;
            }
            
            $("#filesize").text(Math.floor(size) + ' '+units[unitLevel]);
            $("#chunk-total").text(file[0].chunks.length);
            
            // auto start
            upload.start();
        });
        
        // On progress
        flow.on('progress',function(){
            // Show progress
            var perc = Math.floor(flow.progress()*100);
            /*Flow flow*/
            progressBar
                .css({width: perc + '%'})
                .prop("aria-valuenow",perc)
                .text(perc+" %");
            $("#chunks").text(flow.opts.query.flowChunkNumber);
        });
        
        // File uploaded
        flow.on('fileSuccess',function(file,message){
            progressBar.removeClass('progress-bar-info progress-bar-striped active')
                .addClass("progress-bar-success");
            // TODO process to write when complete
            btnUpload.text("Upload");
            btnUpload.data("status", "start");
            btnUpload.removeClass("btn-danger");
            btnUpload.addClass("btn-info");
        });
        
        // On Error
        flow.on('fileError',function(file){
            btnUpload.text("Cancel");
            btnUpload.data("status", "cancel");
            progressBar.removeClass("active")
                .addClass("progress-bar-danger");
        });
        
        btnUpload.on("click",function(e){
            let status = btnUpload.data("status");
            console.log(status);
            if (status === "start") {
                // Start upload
            } else if (status === "cancel") {
                upload.cancel();
                e.stopPropagation();
                return false;
            } else if (status === "retry") {
                upload.retry();
                e.preventDefault();
                return false;
            }
        });
        btnProgress.on("click",function(){
            let status = btnProgress.data("status");
            if (status==="pause"){
                flow.pause();
                btnProgress.text("Resume")
                    .data("status", "resume")
                    .removeClass("btn-warning")
                    .addClass("btn-success");
            } else if (status === "resume") {
                flow.resume();
                btnProgress.data("status", "pause")
                    .text("Pause")
                    .removeClass("btn-success")
                    .addClass("btn-warning");
            }
        });
    })();
JS;

$this->registerJS($js);