<?php
use yii\helpers\Html;
use app\components\Icon;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\Answers $model
 * @var array $qItem
 */

// Answers.items
$items = $model->getItems();

$form = ActiveForm::begin([
    'action' => ['update-upload', 'rcID' => $rcID, 'qnID' => $qnID, 'ansID' => $ansID],
    'options' => [
        'method' => 'post',
        'enctype' => 'multipart/form-data',
        'class' => 'items_form',
        'id' => 'form' . ($key+1),
    ]
]); ?>

<?php echo $form->field($formModel, 'key')
    ->label(false)
    ->hiddenInput(['value' => $key]); ?>
<?php echo Html::hiddenInput('MAX_FILE_SIZE', $max_file_size); ?>

<div class="item-div">
    <!-- Filename -->
    <strong>Item <?= $key+1 ?>: <span id="item-src-<?= $key+1 ?>">
    <?php if(!empty($name)): ?>
        <a class="preview" data-src="<?php echo $model->getRenderRawUrl($key) ?>" data-type="<?php echo $items[$key]['ansType'] ?>"><?php echo $name ?></a>
    <?php else: ?>
        <a class="preview"></a><span class="label-none text-muted">None</span>
    <?php endif; ?>
    </span></strong>
    <!-- File type -->
    - (<?php echo $qItem['ansType']?>)
    <?php if (empty($name)): ?>
        <!-- Upload button -->
        - <?php echo  Html::a(sprintf('%s Add',Icon::fa('add')),null,[
            'class'=>'btn btn-default btn-sm btn-browse btn-text-success',
            'id' => 'btn-browse'.($key+1),
            'data-type' => $qItem['ansType'],
            'data-index' => $key+1,
        ]) ?>
    <?php endif; ?>
    <!-- Delete button -->
    <?= Html::a(sprintf('%s Remove',Icon::fa('trash')),null,[
        'class'=> 'btn btn-danger btn-sm btn-remove',
        'id' => 'btn-remove'.($key+1),
        'data-type' => $qItem['ansType'],
        'data-index' => $key+1,
        'style' => empty($name) ? 'display:none' : '',
    ]) ?>
    <!-- Uploaded filename -->
    <span id="txt-filename-<?php echo $key+1 ?>" class="small"></span>
    <!-- Uploading progress -->
    <span id="progress-<?php echo $key+1 ?>" class="small"></span>

    <?php
    $acceptMime = '*/*';
    switch ($qItem['ansType']){
        case 'text': $acceptMime = 'text/*'; break;
        case 'audio': $acceptMime = 'audio/mp3,application/ogg,audio/ogg,audio/vorbis'; break;
        case 'image': $acceptMime = 'image/png,image/jpeg'; break;
    }


    $create = new \app\models\AnswerForms\Create;
    echo $form->field($formModel, 'index')->label(false)->hiddenInput(['value'=>($key+1)]);
    echo $form->field($formModel, 'answerFile')
        ->label(false)
        ->fileInput([
            'id' => 'form-item-file' . ($key+1),
            'class'=>'hidden form-item-file',
            'data' => ['index' => ($key+1)],
            'accept' => $acceptMime
        ]);

    ?>
</div>
<?php ActiveForm::end(); ?>

