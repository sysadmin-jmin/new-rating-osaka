<?php

use yii\helpers\Html;
use app\components\Icon;
/** @var $model \app\models\Answers */
/** @var $raters \app\models\Raters */

$blocks = $model->getBlocks();
$assigns = $model->getAssign();
?>
<style>
    .block-div {
        padding: 20px 10px;
    }
</style>


<p>
    <?= Html::a('Back', ['index',
        'qnID' => $model->qnID,
        'rcID' => $model->rcID],
        ['class' => 'btn btn-default']) ?>
</p>


<div class="">
    
    
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">Answers</div>
                <div class="panel-body">
                    <?php foreach ($blocks as $blockkey => $block) : ?>
                    <div class="block-div">
                        <h2><i class='fa fa-cubes'></i> Block <?php echo $blockkey + 1 ?></h2>
                        <ul>
                            <li>Status: <?php echo ucfirst($block['status']); ?></li>
                            <li>Assigned to: <ul>
                                <?php if (isset($assigns[$blockkey]) && count($assigns[$blockkey]) > 0): ?>
                                    <?php foreach ($assigns[$blockkey] as $rid): ?>
                                        <li><?php echo $rid ?> - <?php echo @$raters[$rid]->name ?? '<i>Rater Unknown</i>' ?></li>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <li class="text-muted">No users assigned</li>
                                <?php endif; ?>
                            </ul></li>
                        </ul>
                        <!-- end block -->
                    </div>
                    <?php endforeach; ?>
                </div>
            
            </div>
        
        </div>
        
        <div class="col-md-5">
            
            <div class="panel panel-primary">
                
                <div class="panel-heading">
                    Assign block to a user / Unassign block from user
                </div>
                <div class="panel-body">
                    <?php echo Html::beginForm(\yii\helpers\Url::to(['assign', 'id' => $model->id]),'POST'); ?>
                    <div class="form-group">
                        <div><?php echo Html::label('Assign/Unassign Block'); ?></div>
                        <select name="blockkey" class="form-control">
                            <?php echo Html::renderSelectOptions('rid',array_column(
                                    array_map(function($blockkey){
                                        return ['key' => $blockkey, 'block' => ('Block'.($blockkey+1))];
                                    },array_keys($blocks)),'block','key')
                            ); ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <?php echo Html::label('Search User'); ?>
                        <select name="rid" class="form-control">
                            <?php echo Html::renderSelectOptions('v¥',array_column(
                                array_map(function(\app\models\Raters $rater){
                                    return ['rid' => $rater->id, 'name' => $rater->name];
                                },$raters),'name','rid')
                            ); ?>
                        </select>
                    </div>
                    
                    <?php echo Html::submitButton('Assign', [
                        'name' => 'action',
                        'value' => 'assign',
                        'class' => 'btn btn-primary'
                    ]); ?>
                    <?php echo Html::submitButton(Icon::Fa('delete') . 'Unassign', [
                        'name' => 'action',
                        'value' => 'unassign',
                        'class' => 'btn btn-default'
                    ]); ?>
                    <label>
                    <?php echo Html::endForm(); ?>
                </div>
            </div>
        
        
        </div>
    
    
    </div>


</div>