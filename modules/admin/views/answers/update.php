<?php

use app\components\Icon;
use yii\helpers\Html;

/**
 * @var \app\models\Questions $qnM
 * @var \app\models\RatingConfigs $rcM
 * @var \app\models\Answers $model
 */
?>

<h1>Upload Files for Answer #<?= $model->id ?></h1>

<p>
    <?php 
	if($model->isModel) {
		echo Html::a('Back', ['training-index',
					'qnID' => $model->qnID,
					'rcID' => $model->rcID],
					['class' => 'btn btn-default']); 
	}
	else {
		echo Html::a('Back', ['actual-index',
					'qnID' => $model->qnID,
					'rcID' => $model->rcID],
					['class' => 'btn btn-default']); 
	}
		
	?>

</p>

<style>
    .item-div {
        padding: 0 0 0 5px;
    }
    .preview{
        cursor: pointer;
    }
    #preview-text-plain {
        border: solid 1px #ccc;
        border-radius: 4px;
        width: 569px;
        height: 426px;
    }
    .preview-content {
        display:none;
    }
</style>

<div class="row">
    <div class="col-md-8">

        <div class="panel panel-default">

            <div class="panel-heading">Items</div>
            <div class="panel-body">
                * Each files can upload allowed up to <span id="allowed-bytes" data-bytes="<?php echo $supportUploadBytes; ?>">
                    <?php echo ini_get('upload_max_filesize'); ?>B
                </span>

                <?php $qItems = $qnM->getItems(); ?>
                <?php $items = $model->getItems(); ?>
                <?php $rcItem = $rcM->getItems(); ?>
                <ul>
                    <?php foreach ($qItems as $key => $qItem): ?>
                        <?= $this->render('_item', [
                            "model" => $model,
                            'formModel' => $formModel,
                            'rcID' => $rcM->id,
                            'qnID' => $qnM->id,
                            'ansID' => $model->id,
                            "key" => $key,
                            "name" => (@$items[$key]['src'] ?? ''),
                            "qItem" => $qItem,
                            'max_file_size' => $supportUploadBytes,
                        ]); ?>
                    <?php endforeach ?>
                </ul>
            </div>

            <div class="panel-footer">

            </div>

            <div class="modal" id="modal-dialog" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Preview</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="fa fa-times"></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="preview-image" class="preview-content">
                                <img id="preview-image-img" width="568" alt="image preview" src="">
                            </div>
                            <div id="preview-audio" class="preview-content">
                                <audio id="preview-audio-audio" controls>
                                    <source>
                                </audio>
                            </div>
                            <div id="preview-text" class="preview-content">
                                <iframe id="preview-text-plain" width="568" height="426"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<?php
$js = <<<JS
var modalView,audioTag,previewType;
const thisID={$model->id};

let btnBrowse = $(".btn-browse");

$(".form-item-file").on("change",function(e){
    let \$this = $(this);
    if (e.target.files.length<1){
        // when user cancel
        e.preventDefault();
        return false;
    }
    let formFile = this.files[0];
    var index=\$this.data("index"),
        fileInfo=formFile.name + " ("+bytesToHumanize(formFile.size)+")";
    $("#txt-filename-"+index).text(fileInfo);
    let prog=$("#progress-"+index);
    let filename=$("#item-src-"+index);
    if (this.files.length > 0){
        let formElm=$("#form"+index);
        let action=formElm.prop("action");
        $.ajax({
            url: action,
            method:"POST",
            //contentType: "multipart/form-data",
            //contentType: "application/x-www-form-urlencoded",
            contentType: false,
            dataType: "json",
            processData: false,
            data: new FormData(formElm.get(0)),
            beforeSend: function(){
                console.log("Upload start");
                prog.html('<i class="fa fa-spinner fa-spin"></i>');
                btnBrowse.attr({disabled:true}).off("click");
            }
        }).done((res,status,xhr)=>{
            console.log("Upload finished");
            console.log("status:"+xhr.status+" "+status);
            console.log(res);
            formFile.value='';
            btnBrowse.attr({disabled:false}).on("click",funcBrowse);
            if (xhr.status!=200)
                prog.html('<i class="fa fa-exclamation-triangle text-danger"></i>');
            else {
                if (res.status === "success") {
                    prog.html('<i class="fa fa-check text-success"></i>');
                    // filename.html(
                    //     '<a class="preview" data-src="'+res.content.raw+'" data-type="'+res.content.type+'">'+res.content.name+'</a>'
                    // );
                    filename.children(".label-none").remove();
                    let previewLabel=filename.children(".preview");
                    previewLabel.data({src:res.content.raw,type:res.content.type})
                    .prop({"data-src":res.content.raw,"data-type":res.content.type})
                    .attr({"data-src":res.content.raw,"data-type":res.content.type})
                    .text(res.content.name);
                    //reloadPreview();
                    $("#btn-browse"+index).hide();
                    $("#btn-remove"+index).show();
                } else {
                    if (formFile.size > allowedBytes.data("bytes"))
                        prog.html('<i class="fa fa-exclamation-triangle text-danger">File is larger</i>');
                    else if (typeof res.message == "string")
                        prog.html('<i class="fa fa-exclamation-triangle text-danger">'+res.message+'</i>');
                    else 
                        prog.html('<i class="fa fa-exclamation-triangle text-danger"></i>');
                }
            }
        }).fail((xhr,status,err)=>{
            console.log("Error occurred");
            console.log("status: "+ xhr.status);
            console.error("error: "+ err);
            if (formFile.size > allowedBytes.data("bytes"))
                prog.html('<i class="fa fa-exclamation-triangle text-danger">File is larger</i>');
            else
                prog.html('<i class="fa fa-exclamation-triangle text-danger"></i>');
        }).always((res,status,xhr)=>{

        }).then((res,status,xhr)=>{

        },(xhr,status,err)=>{

        });
    }
});

btnBrowse.on("click",funcBrowse);
$(".btn-remove").on("click",function(e){
    let index=$(this).data("index");
    if (!confirm("Are you sure want to remove this file ?")){
        e.preventDefault();
        return false;
    }
    window.location.href="update?id="+thisID+"&action=remove&key="+index;
});
let allowedBytes = $("#allowed-bytes");
allowedBytes.text(bytesToHumanize(allowedBytes.data('bytes')));

// modal view
modalView=$("#modal-dialog");
reloadPreview();
modalView.on("hide.bs.modal",function(e){
    if(typeof audioTag=="object")
        audioTag.get(0).pause();
});

function funcBrowse(e){
    let index=$(this).data("index");
    $("#form-item-file"+index).click();
    e.preventDefault();
    return false;
}

function reloadPreview(){
    let prevLink=$(".preview");
    prevLink.off("click");
    prevLink.on("click",function(e){
        let self=$(this);
        $(".preview-content").hide();
        var src=self.data("src");
        var type=self.data("type");
        previewType=type;
        if (type==="image") {
            $("#preview-image-img").prop("src",src);
            $("#preview-"+type).show();
            modalView.modal('show');
        } else if (type==="audio"){
            audioTag=$("#preview-audio-audio");
            audioTag.children("source").prop("src",src);
            audioTag.get(0).load();
            $("#preview-"+type).show();
            modalView.modal('show');
        } else if(type==="text") {
            $("#preview-text-plain").attr({"src":src});
            $("#preview-"+type).show();
            modalView.modal('show');
        }
        e.preventDefault();
    });
}
function bytesToHumanize(val){
    var loop = 0;
    while (val>1000&&loop<=3) {
        val = val / 1000;
        loop++;
    }
    var unit="";
    switch (loop){
        case 0: unit="B"; break;
        case 1: unit="KB"; break;
        case 2: unit="MB"; break;
        case 3: unit="GB"; break;
        //case 4: unit="TB"; break;
        //case 5: unit="PB"; break;
        default:
            return "Too HUGE!!!";
    }
    return val.toFixed(2)+" "+unit;
}
JS;
$this->registerJS($js);
