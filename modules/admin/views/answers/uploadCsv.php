<?php

use yii\helpers\Html;
use app\assets\FlowAsset;

/* @var $this yii\web\View */

$this->title = "Upload CSV";

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
FlowAsset::register($this);


?>
<style>
    .progress{
        width:100%;
        border:1px solid #000;
        box-sizing: border-box;
        margin:30px 0;
    }
</style>

<div class="container">

	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">Answers CSV</div>

				<div class="panel-body">
                    <?php echo Html::beginForm('', 'post'); ?>
                    <input type="hidden" name="rcID" id="rcID" value="<?php echo $rcID ?>">
                    <button type="button" id="btn-browse" class="btn btn-default">upload</button>
                    <div class="">
                        <div>File size: <span id="filesize">0 bytes</span></div>
                        Chunk num: <span id="chunks">0</span> / <span id="chunk-total">0</span>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <div class="img-box"></div>

                    <button type="button" id="btn-start" class="btn btn-info" data-status="start">Start</button>
                    <button type="button" id="btn-progress" class="btn btn-warning" data-status="pause">Pause</button>
                    <?php echo Html::endForm(); ?>
				</div>
			
			</div>		
		
		</div>
	
	</div>	

</div>

<?php
$js = <<<JS
    const CHUNK_SIZE=$supportUploadBytes;

    (function(){
        let rcID = document.getElementsByName("rcID")[0].value;
        let csrfToken = document.getElementsByName('csrf-token')[0].getAttribute('content');
        let flow = new Flow({
            target:'upload?rcID='+rcID
            ,chunkSize:CHUNK_SIZE
            ,forceChunkSize:true
            ,singleFile:true
            ,query:{__csrf:csrfToken}
            ,headers:{'X-CSRF-Token':csrfToken}
        });
        var flowfile= flow.files;
        
        
        let btnStart = document.getElementById("btn-start");
        let btnProgress = document.getElementById("btn-progress")
        let progressBar = $('.progress-bar');
        
        
        let upload = {
            start:()=>{
                flow.opts.rcId = rcID;
                flow.upload();
                
                btnStart.dataset.status = "cancel";
                btnStart.innerText = "Cancel";
                btnStart.classList.remove("btn-info");
                btnStart.classList.add("btn-danger");
            },
            cancel:()=>{
                flow.cancel();
                btnStart.innerText = "Start";
                btnStart.dataset.status = "start";
                btnStart.classList.remove("btn-danger");
                btnStart.classList.add("btn-info");
                progressBar
                    .css({width:'0%'}).prop("aria-valuenow",0).text("0 %");
                $("#chunks").text('0');
                $("#chunk-total").text(0);
            },
            retry:()=>{
                flow.retry();
                btnSTart.innerText = "Cancel";
                btnSTart.dataset.status = "cancel";
                btnSTart.classList.remove("btn-warning");
                btnSTart.classList.add("btn-danger");
            },
            
        };
        
        flow.assignBrowse(document.getElementById('btn-browse'));
        flow.on('fileSuccess', function(file,message){
            progressBar.removeClass("progress-bar-info")
                .addClass("progress-bar-success");
            //alert("Upload completed");
        });
        flow.on('filesSubmitted', function(/*FlowFile*/file) {
            // TODO set targetfile
            progressBar.addClass("progress-bar-striped active")
            .removeClass("progress-bar-info").removeClass("progress-bar-success");
            if (flow.isUploading() || (btnStart.dataset.status==="resume" || btnStart.dataset.status==="cancel")) {
                if (!confirm('Are you sure want to abort while current uploading files ?'))
                    return;
                flow.cancel();
                btnStart.innerText = "Start";
                btnStart.dataset.status = "start";
                btnStart.classList.remove("btn-danger");
                btnStart.classList.add("btn-info");
            }
            //console.log(file);
            file[0].bootstrap();
            var size = file[0].file.size;
            var unitLevel = 0;
            let units = ['bytes','KB','MB','GB','TB','PB','EB'];
            while (1024 < size) {
                size = size / 1024;
                unitLevel++;
            }
            
            $("#filesize").text(Math.floor(size) + ' '+units[unitLevel]);
            $("#chunk-total").text(file[0].chunks.length);
            
            // auto start
            upload.start();
        });
        
        flow.on('progress',function(){
            // Show progress
            var perc = Math.floor(flow.progress()*100);
            /*Flow flow*/
            progressBar
            .css({width: perc + '%'})
            .prop("aria-valuenow",perc)
            .text(perc+" %");
            $("#chunks").text(flow.opts.query.flowChunkNumber);
        });
        flow.on('fileSuccess',function(file){
            // TODO process to write when complete
            btnStart.innerText = "Start";
            btnStart.dataset.status = "start";
            btnStart.classList.remove("btn-danger");
            btnStart.classList.add("btn-info");
        });
        flow.on('fileError',function(file){
            btnStart.innerText = "Cancel";
            btnStart.dataset.status = "cancel";
            progressBar.removeClass("active")
                .addClass("progress-bar-danger");
        });
        btnStart.addEventListener("click",function(){
            let status = this.dataset.status;
            if (status === "start") {
                // Start upload
                upload.start();
            } else if (status === "cancel") {
                upload.cancel();
            } else if (status === "retry") {
                upload.retry();
            }
        });
        btnProgress.addEventListener("click",function(){
            let status = this.dataset.status;
            if (status==="pause"){
                flow.pause();
                this.innerText = "Resume";
                this.dataset.status = "resume";
                this.classList.remove("btn-warning");
                this.classList.add("btn-success");
            } else if (status === "resume") {
                flow.resume();
                this.dataset.status = "pause";
                this.innerText = "Pause";
                this.classList.remove("btn-success");
                this.classList.add("btn-warning");
            }
        });
    })();
JS;

$this->registerJS($js);