<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Answer Set ";

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function fa($icon='', $url=['']) {
	switch ($icon) {

		case 'edit':
			$i = "<i class='fa fa-edit'> </i>";		
			break;
		case 'trash':	
			$i = "<i class='fa fa-trash-o'> </i>";		
			break;	
		case 'create':
		default:
			$i = "<i class='fa fa-plus'> </i>";
			break;
	}

	return Html::a($i,$url,[]);
}





?>
<style> 


.unit-div {
	padding:20px;
	border: 1px solid lightgrey;
	border-radius: 4px;
}

.item-div {
	margin:10px 20px;
}

.context-div, .src-div, .context-items-div {
	margin-left:50px;
	padding: 4px 0;
}

.src-a, .context-a {
	padding-right: 10px;
}

.block-divider {
	padding: 25px 0;
}

</style>


<p>
	<?= Html::a('Update',['update','id'=>$model->id],['class'=>'btn btn-default']) ?>
</p>



<div class="">


	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">Answers</div>
			
				<div class="panel-body">
				
				<div class="block-div">
				
					<h2><i class='fa fa-cubes'></i> Block 1</h2>
					
					<div class="unit-div">
						<h4>  Unit 1</h4>
						
						<div class="item-div">

							<div class="">
								<i class='fa fa-long-arrow-right'> </i> 
								<i class="fa fa-folder-o" aria-hidden="true"></i>
								 Item 1: 
								
								
							</div> 
							<div class="src-div">
								Source:  <a class='src-a' href="#"> NULL </a> <i class="fa fa-file-audio-o" aria-hidden="true"></i> 							 
							</div>
							
						</div>
					
					</div>
					<!-- end block -->
					<div class="block-divider"></div>
					
					<h2><i class='fa fa-cubes'>  </i> Block 2</h2>
					

						

					<!-- end block -->					
				</div>
				
				
				
				</div>
			
			</div>		
		
		</div>
	
		<div class="col-md-5">

		<?= $this->render('_form') ?>
		
		
		
		</div>
	
	
	</div>	






</div>
