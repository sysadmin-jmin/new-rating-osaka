<?php 
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = "Set Units of Question " . $model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$css = <<< CSS

li.unit {
    padding: 10px;
}
CSS;
$this->registerCss($css);

function showForm($view, $model)
{
    if (Yii::$app->request->get("forms")=="addBlock") {
        return $view->render('forms/_addUnit', ["id"=>$id]);
    } elseif (Yii::$app->request->get("forms")=="assignGroupToBlock") {
        return $view->render('forms/_assignGroupToBlock', ["groups" => $groups, "block" => $units[$id]]);
    }
}


$units = $model->getUnits();

?>
<h3 class="pull-right"><span class="label label-warning"><?= $model->Type ?></span></h3>
<h1><?= $this->title ?></h1>
<p>
    <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default']) ?>
</p>


<div class="row">
    <?php Pjax::begin(["scrollTo"=>200]); ?>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Manage</div>
                <div class="panel-body">

                    <ul class="list-unstyled">
                        <?php foreach ($units as $key => $unit): ?>
                            <?= $this->render('units/_list', [
                                'id' => $key+1,
                                "key" => $key,
                                "name" => $unit['name'],
                                "item" => $unit,
                            ]); ?>
                        <?php endforeach ?>
                    </ul>

                </div>
            </div>
        </div>
 
        <div class="col-md-4">
            <?=$this->render('units/_unitForm', ["model"=>$formObj]);?>        
        </div>
    <?php Pjax::end(); ?>

</div>

