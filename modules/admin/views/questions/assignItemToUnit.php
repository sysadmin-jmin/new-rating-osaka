<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\components\Icon;

$this->title = "Assign Items To Unit ". $model->name;

$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function deleteLink($key)
{
    return Html::a(
        '<i class="fa fa-minus-circle"></i>',
        ['assign-item-to-unit', 'rcID'=>Yii::$app->request->get('rcID')],
        ["data-confirm"=>"Are you sure want to delete this unit?", "data-method"=>"post", "data-params"=>['action'=>'delete', 'unitKey'=>$key]]
    );
}

function updownLink($key, $list)
{
    $s = '';

    if ($key != 0) {
        $s = Html::a(
            '<i class="fa fa-arrow-up block-arrow" style="color:green"></i>',
            ['assign-item-to-unit', 'rcID'=>Yii::$app->request->get('rcID')],
            ["data-method"=>"post", "data-params"=>['action'=>'up', 'unitKey'=>$key]]
        );
    }

    if ($key!=count($list)-1) {
        $s .= Html::a(
            '<i class="fa fa-arrow-down block-arrow" style="color:green;"></i>',
            ['assign-item-to-unit', 'rcID'=>Yii::$app->request->get('rcID')],
            ["data-method"=>"post", "data-params"=>['action'=>'down', 'unitKey'=>$key]]
        );
    }
    return "<span class='updownLink'>{$s}</span>";
}

function showForm($view, $units, $key, $items)
{
    if (Yii::$app->request->get("forms")=="addUnit") {
        return $view->render('forms/_addUnit', ["key"=>$key]);
    } elseif (Yii::$app->request->get("forms")=="assignItemToUnit") {
        return $view->render('forms/_assignItemToUnit', ["items" => $items, "unit" => $units[$key], "unitKey"=>$key]);
    }
}

function fa($icon='', $url=[], $opts = [])
{
    switch ($icon) {

        case 'edit':
            $i = "<i class='fa fa-edit'> </i>";
            break;
        case 'trash':
            $i = "<i class='fa fa-trash-o'> </i>";
            break;
        case 'create':
            $i = "<i class='fa fa-plus'> </i>";
            break;
        default:
            $i = "<i class='fa fa-$icon'> </i>";
            break;
    }
    if (count($url)) {
        return Html::a($i, $url, $opts);
    } else {
        return $i;
    }
}

$css = <<<CSS
i {
    padding-right: 4px;
}
.block-div {
    padding: 10px;
}

.block-name {
    display:inline-block;
    padding: 0 10px;
    font-size: 20px;    
}

.block-arrow {
    margin-right:5px;
}

.group-div {
    padding:10px 50px;
}

.group-item-div {
    padding: 10px 20px;
}

.group-qfy-div {
    padding: 5px 30px;
}
.group-qfy-div li {
    font-size: 12px;
}

.updownLink {
    display: inline-block;
    width: 60px;

}

.displayhide{
    display: none;
}

CSS;

$this->registerCss($css);

$items = $model->getItems();
$units = $model->getUnits();
$unitID = Yii::$app->request->get("unitID");
$unitKey = Yii::$app->request->get("unitKey");


?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default']) ?>
</p>


<div class="row">

<?php Pjax::begin(["scrollTo"=>200]); ?>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Unit Structures</div>

        <div class="panel-body">
            <?php if (count($units)):
                foreach ($units as $idx => $item) {
                    echo $this->render('assignItemToUnit/_unit', ["model"=>$model, "unitKey"=>$idx, "units"=>$units, 'items' => $items]);
                } else:
                ?>
                <div class="block-div">
                    <div class='alert alert-warning'>
                        No units found.
                    </div>
                    <?=Html::a(fa('plus-circle').' Add unit', ['units', "id"=>$model->id, "update"=>"unit", "unitKey"=>0], ["class"=>"btn btn-default"]); ?>
                    <hr>
                </div>
                <?php
            endif; ?>
        </div>
    </div>
</div>

    <div class="col-md-4">
        <?=$this->render('assignItemToUnit/_assignItem', ["items" => $items, "units" => $units, "unitKey"=>$unitKey]);?>
    </div>
<?php Pjax::end(); ?>
</div>

<div class="div-space"></div>