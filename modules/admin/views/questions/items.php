<?php
use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\components\Icon;

$this->title = "Set Rating Items of Question " . $model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


$fa_picture= "<i class='fa fa-picture-o'></i> ";
$fa_delete ="<i class='fa fa-trash-o'></i> ";

function deleteLink($key)
{
    return Icon::fa(
        'minus-circle',
        [
            '',
            'id'=>Yii::$app->request->get('id'),
            "update"=>"item"
        ],
        [
            "data-confirm"=>"Are you sure want to delete this item?",
            "data-method"=>"post",
            "data-params"=>[
                'action'=>'delete',
                'itemKey'=>$key
            ]
        ]
    );
}

function showForm($view, $model)
{
    echo $view->render("items/_itemForm", ["model"=>$model]);
}

$css = <<<CSS
i {
    padding-right: 4px;
}
.item-div {
    padding:10px;
}

ul.item-info li {
    font-size:10px;
    padding-bottom:2px;
}

.item-arrow {
    margin-right:5px;
}

CSS;

$this->registerCss($css);

$items = $model->getItems();
?>

<div class="">
    <h3 class="pull-right"><span class="label label-warning"><?= $model->Type ?></span></h3>
    <h1><?= $this->title ?></h1>
    <p>
        <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default']) ?>
    </p>


    <div class="row">
        <?php Pjax::begin(["scrollTo"=>200]); ?>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php $count = count($items) ?>
                    Items (<?= $count ?>)
                </div>
                <div class="panel-body">
                    <?php 
                        if ($count>0) {
                            foreach ($items as $key=>$item) {
                                echo $this->render("items/_list", ["item"=>$item, "itemA"=>$items, "itemKey"=>$key, "id"=>$model->id]);
                            }
                        }
                    ?>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <?=showForm($this, $formObj)?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>