<?php 
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use yii\helpers\Html;
use app\models\RatingConfigForms\Units;

$this->title = "Assign Items To Unit ". $model->name;

$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


$css = <<<CSS
i {
    padding-right: 4px;
}
.block-div {
    padding: 10px;
}

.block-name {
    display:inline-block;
    padding: 0 10px;
    font-size: 20px;    
}

.block-arrow {
    margin-right:5px;
}

.group-div {
    padding:10px 50px;
}

.group-item-div {
    padding: 10px 20px;
}

.group-qfy-div {
    padding: 5px 30px;
}
.group-qfy-div li {
    font-size: 12px;
}

.updownLink {
    display: inline-block;
    width: 60px;

}
.displayhide{
    display: none;
}

CSS;

$this->registerCss($css);

$blocks = $model->getBlocks();
$units = $model->getUnits();

$blockKey = Yii::$app->request->get("blockKey");

?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default']) ?>
</p>

<div class="row">

<?php Pjax::begin(["scrollTo"=>200]); ?>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?=$model->name?> Block Structure
            </div>
            <div class="panel-body">
                <?php
                    if (count($blocks) > 0) {
                        foreach ($blocks as $key => $block) {
                            echo $this->render('assignUnitToBlock/_block', ["model"=>$model, "blockKey"=>$key, "blocks"=>$blocks, "units"=>$units]);
                        }
                    } ?>
            </div>          
        </div>
    </div>
    <div class="col-md-4">
        <?=$this->render('assignUnitToBlock/_assignUnit', ["model"=>$model, "blockKey"=>$blockKey, "blocks"=>$blocks, "units"=>$units]);?>
    </div>
<?php Pjax::end(); ?>

</div>


<div class="div-space"></div>






