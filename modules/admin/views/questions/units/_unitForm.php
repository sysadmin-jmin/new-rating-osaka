<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="panel panel-primary">

    <div class="panel-heading">Edit Unit</div>

    <div class="panel-body">
        <?php if (Yii::$app->request->get("update")=="unit"):?>

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="form-group">
                    <label>Unit: <?=$model->key+1?></label>
                </div>
                <?=Html::hiddenInput('unitKey', $model->key); ?>
                <?=Html::hiddenInput('action', 'save'); ?>

                <?= $form->field($model, 'name')->textInput() ?>

                <p>
                    <?=Html::submitButton("save", ["class"=>'btn btn-primary'])?>
                </p>
            <?php ActiveForm::end(); ?>

        <?php else: ?>

            <div class="well"> Please click edit button on the left side to edit the unitss.</div>

        <?php endif; ?>
    </div>

</div>
