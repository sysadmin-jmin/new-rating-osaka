<?php
use yii\helpers\Html;
use app\components\Icon;
use yii\widgets\ActiveForm;

?>

<li class="unit">
    <strong>Unit <?= $id ?>: <span id="unit-index-name<?= $id ?>"><?= !empty($name) ? $name : '<span class="text-muted">Unknown</span>' ?></span></strong>
    - <?=
        Icon::fa(
            "edit",
            ['',
                'update'=>'unit',
                'id'=> Yii::$app->request->get("id")
            ],
            [
                'class' => 'profile-link',
                'data-method' => "post",
                'data-params' => [
                    'unitKey'=>$key
                ],
                'data-pjax'=>1
            ]
        );
    ?>

</li>

