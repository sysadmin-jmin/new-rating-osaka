<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Question Set " . $model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function showSrc($qItem, $questionModel, $itemKey)
{
    if (@$qItem["src"]) {
        if ($qItem["qnType"]=="text") {
            return "<em>text</em> ".Icon::fa("file-text-o", ['view', "id"=>$questionModel->id, "update"=>"items", "key"=>$itemKey], ["class"=>"src-a"]);
        } elseif ($qItem["qnType"]=="image") {
            return "<em>image</em> ".Icon::fa("file-image-o", ['view', "id"=>$questionModel->id, "update"=>"items", "key"=>$itemKey], ["class"=>"src-a"]);
        }
    } else {
        $s = "<em>(null)</em> ";
        $path = ['view', "id"=>$questionModel->id, "update"=>"items", "key"=>$itemKey];
        $s .= Html::a(Icon::fa("upload") ." Upload", $path, ["class"=>"btn btn-xs btn-default"]);

        return $s;
    }
}

function showAnswer($qItem, $questionModel, $itemKey)
{
    if ($qItem["ansType"]=="text") {
        return "<em>text</em> ".Icon::fa("file-text-o");
    } elseif ($qItem["ansType"]=="image") {
        return "<em>image</em> ".Icon::fa("file-image-o");
    } elseif ($qItem["ansType"]=="audio") {
        return "<em>audio</em> ".Icon::fa("file-audio-o");
    }
}

$css = <<<CSS

.unit-div {
	padding:20px;
	border: 1px solid lightgrey;
	border-radius: 4px;
}

.item-div {
	margin:10px 20px;
	padding: 5px 0 10px;
}

.context-div, .src-div, .context-items-div {
	margin-left:20px;
	padding: 4px 0;
}

.src-a, .context-a {
	padding-right: 10px;
}
CSS;

$this->registerCSS($css);

$blocks = $model->getBlocks();
$units = $model->getUnits();
$items = $model->getItems();
?>

<div class="questions-view">

    <h3 class="pull-right"><span class="label label-warning"><?= $model->Type ?></span></h3>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default'])?>
    </p>

	<div class="row">
    <?php Pjax::begin(["scrollTo"=>200]); ?>
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading"><?= $model->name ?></div>
			
				<div class="panel-body">
					
				<?php
                    // show each block for test
                    foreach ($blocks as $block) {
                        $blockUnits = $block["units"];
                        echo $this->render('view/_block', ['block'=>$block, "blockUnits"=>$blockUnits, "items"=>$items, "units"=>$units, "questionModel"=>$model]);
                    }
                ?>	
				</div>			
			</div>
		</div>
	
		<div class="col-md-5">
			<?= $this->render('view/_itemForm', ['model'=>$formObj]);?>		
		</div>
    <?php Pjax::end(); ?>
	</div>
</div>
