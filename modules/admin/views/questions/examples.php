<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Examples Set ".$model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Question Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$css = <<<CSS

.block-div {
	padding-bottom: 20px;
}


ul.unit {
	margin-left: 20px;
	
}


ul.example {

}

ul.example li {
	padding: 4px 0;
}


.src-a, .context-a {
	padding-right: 10px;
}
CSS;

$this->registerCSS($css);

$blocks = $model->getBlocks();
$units = $model->getUnits();

$update = Yii::$app->request->get("update");
?>

<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default'])?>
    </p>

	<div class="row">
		<?php Pjax::begin(["scrollTo"=>200]); ?>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">Examples</div>
				
					<div class="panel-body">
						<?php
                            foreach ($blocks as $block) {
                                $blockUnits = $block["units"];
                                echo $this->render("examples/_block", ["block"=>$block, "units"=>$units, "model"=>$model]);
                            }
                        ?>
					</div>			
				</div>
			</div>
		
			<div class="col-md-4">
				<?=$this->render("examples/_exampleForm", ["model"=>$formObj]);?>		
			</div>
		<?php Pjax::end(); ?>
	</div>
</div>

