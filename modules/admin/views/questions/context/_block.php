<?php
use app\components\Icon;

/* @var $block */
/* @var $blockUnits */
/* @var $items */
/* @var $units */
/* @var $questionModel */

?>

<div class="block-div">					
	<h2> <?=Icon::fa("cubes")?> Block: <?=$block["name"]?></h2>	
	<div class="unit-div">
		<?php
            // show units in each block
            foreach ($blockUnits as $unitKey) {
                $unit = $units[$unitKey];
                $qContext = $questionModel->getUnitContexts($unitKey);
                echo "<h4> ".Icon::fa("archive")."</i> Unit: ".$unit["name"]."</h4>";
                echo '<div class="item-div">
                        '.Icon::fa("file-text").' </i> 
                        Context : '.Icon::fa("create", ["context", "id"=>$questionModel->id, "update"=>"context", "unitKey"=>$unitKey]);

                // show context in each units
                foreach ($qContext as $conKey=>$context) {
                    echo $this->render('_context', ["context"=>$context, "questionModel"=>$questionModel, "unitKey"=>$unitKey, "conKey"=>$conKey]);
                }
                echo '</div>';
            }
        ?>
	</div>
</div>
<div class="" style='padding: 25px 0'></div>
