<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="panel panel-primary">
	<div class="panel-heading"> <?=isset($model) && $model->key? "Edit" : "Add" ?> Question Context</div>
		<div class="panel-body">
			<?php 
                if (Yii::$app->request->get("update")=="context") {
                    $question = $model->getQuestion();
                    $unit = $question->getUnits($model->unitKey);
                    $form = ActiveForm::begin([ "options"=>["enctype"=>"multipart/form-data"] ]); ?>
                    <div class="form-group form-inline">
                        <label>Units</label>
                        <div class="form-control-static">
                            <?=$unit["name"]?>
                        </div>
                    </div>

    			    <?= Html::hiddenInput('action', 'save'); ?>

    				<?= $form->field($model, 'qID')->hiddenInput()->label(false); ?>

    				<?php if ($model->key!=""): ?>
    					<?= $form->field($model, 'key')->hiddenInput()->label(false); ?>
    				<?php endif; ?>

                    <?php if ($model->srcType=="image" && !empty($model->src)):?>
                        <div class="placeholder">
                            <img src="<?=Url::to(["context-image", "unitKey"=>$model->unitKey, "contextKey"=>$model->key, "id"=>$question->id])?>" class="img-responsive img-rounded">
                        </div>
                    <?php endif; ?>

    				<?= $form->field($model, 'name')->textInput(); ?>

    				<?= $form->field($model, 'conType')->dropDownList($model::getContextTypeA()); ?> 

    				<div class="" style='padding: 10px 0'></div>
    				<h4> Context Help </h4>
    				<?= $form->field($model, 'srcType')->dropDownList($model::getSrcTypeA()); ?> 

    				<?php
                        $hideImage = "";
                    $hideText = "";
                    if ($model->srcType=="image") {
                        $hideText = 'style="display:none;"';
                    } else {
                        $hideImage = 'style="display:none;"';
                    } ?>

    				<div class="image-src" <?=$hideImage?> >
    					<div class="form-group field-image-src">
    						<label class="control-label" for="image-src">Src or reference</label>
    			 			<?= Html::fileInput("image-src", null, []); ?>
    					</div>
    				</div>

    				<div class="text-src" <?=$hideText?> >
    	 				<?= $form->field($model, 'src')->textArea(["rows"=>5]); ?>
    				</div>

    				<div class="" style='padding:20px 0'>
					<?=Html::submitButton('Save', ['class'=>'btn btn-primary']); ?>

					<?php if ($model->key!=""): ?>
						<?= Html::a("<i class='fa fa-trash-o'></i> Delete", ["view", "id"=>$question->id, "update"=>"context", "key"=>$model->key, "unitKey"=> $model->unitKey], ["class"=>"btn btn-danger", "data-confirm"=>"Are you sure want to delete this context?", "data-method"=>"post", "data-params"=>["action"=>"delete"] ])?>
					<?php endif; ?>
    				</div>
    		<?php 
                    ActiveForm::end();
                } else {
                    ?>
                    <div class="well"> Please click the icon on the left side.</div>
            <?php
                }
            ?>
		</div>
	</div>
</div>
