<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Icon;

/* @var $context */
/* @var $conKey */
/* @var $questionModel */
/* @var $unitKey */

?>
<div class="src-div">
	<?=Icon::fa("long-arrow-right")?>
	<?=Html::a($context["name"], ["context","id"=>$questionModel->id, 
										  "update"=>"context", 
										  "unitKey"=>$unitKey, 
										  "key"=>$conKey], 
								  ["class"=>"context-a"])
	?>
	(<em><?= $context['conType'] ?></em>)
	
</div>
	
