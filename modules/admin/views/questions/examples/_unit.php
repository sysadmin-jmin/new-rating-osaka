<?php
use yii\helpers\Html;
use app\components\Icon;

/* @var $unitKey integer */
/* @var $unit array */
/* @var $model \app\models\Questions */

$exampleA = $model->getExamples($unitKey);
?>
<li> 
    <h4><?= Icon::fa('unit') ?> Unit: <?=$unit["name"]?></h4> 
    <ul class='example'>
        <?php
            foreach ($exampleA as $key=>$example) {
                echo '<li>'.$example["name"]." ".
                        Icon::fa(
                            "edit",
                            [
                                "examples",
                                "id"=>$model->id,
                                "update"=>"example"
                            ],
                            [
                                "data-method"=>"post",
                                "data-params"=>[
                                    "key"=>$key,
                                    "unitKey"=>$unitKey,
                                ],
                                "data-pjax"=>1
                            ]
                        )." ".
                        Icon::fa(
                            "trash",
                            [
                                "examples",
                                "id"=>$model->id,
                                "update"=>"example"
                            ],
                            [
                                "data-method"=>"post",
                                "data-params"=>[
                                    "key"=>$key,
                                    "unitKey"=>$unitKey,
                                    "action" => "delete"
                                ],
                                "data-confirm" => "Are you sure want to delete this example?",
                                "data-pjax"=>0
                            ]
                        ).
                    '</li>';
            }
        ?>
        <li>
            <?=
                Html::a(
                    Icon::fa('upload')."  Add",
                    [
                        "examples",
                        "id"=>$model->id,
                        "update"=>"example"
                    ],
                    [
                        "class"=>"btn btn-default btn-xs",
                        "data-method"=>"post",
                        "data-params"=>[
                            "unitKey"=>$unitKey
                        ],
                        "data-pjax"=>1
                    ]
                )
            ?>
        </li>
    </ul>
</li>    
