<?php
use app\components\Icon;

?>
<div class="block-div">                 
    <h4><?=Icon::fa("cubes")?> Block: <?=$block["name"]?></h4>
    <ul class='unit'>
        <?php
            foreach ($block["units"] as $unitKey) {
                $unit = $units[$unitKey];
                echo $this->render("_unit", ["unitKey"=>$unitKey, "unit"=>$unit, "model"=>$model]);
            }
        ?>
    </ul>
</div>  
