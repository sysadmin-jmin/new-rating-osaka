<?php
/* @var $model \app\models\QuestionsForms\Example */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <?=isset($model) && $model->key!=""? "Edit" : "Add" ?> Example
    </div>

    <div class="panel-body">
        <?php 
            if (Yii::$app->request->get("update")=="example") {
                $unit = $model->getUnit();
                $form = ActiveForm::begin([ "options"=>["enctype"=>"multipart/form-data"] ]); ?>
                <div class="form-group">
                    <label>Unit Name</label>
                    <div class="form-control-static">
                        <?=$unit["name"]?>
                    </div>
                </div>

                <?= Html::hiddenInput('action', 'save'); ?>
                <?= Html::hiddenInput('unitKey', $model->unitKey); ?>

                <?php if ($model->key!==""): ?>
                    <?= Html::hiddenInput('key', $model->key); ?>
                <?php endif; ?>

                <?= $form->field($model, 'qID')->hiddenInput()->label(false); ?>

                <?php
                    if ($model->src) {
                        echo '<div class="thumbnail">'.
                            Html::img(
                                [
                                    "examples-image",
                                    "id"=>$model->qID,
                                    "unitKey"=>$model->unitKey,
                                    "key"=>$model->key
                                ],
                                ["class"=>"img-responsive img-rounded"]
                            )."</div>";
                    } ?>

                <?= $form->field($model, 'src')->fileInput(["name"=>"imageSrc"]) ?>

                <?= $form->field($model, 'name')->textInput()->label("Description") ?>

                <div class="" style='padding:20px 0'>
                    <?=Html::submitButton('Save', ['class'=>'btn btn-primary']); ?>
                </div>
        <?php 
                ActiveForm::end();
            } else {
                ?>
                <div class="well"> Please click the icon on the left side.</div>                
        <?php
            }
        ?>
    </div>
</div>