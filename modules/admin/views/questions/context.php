
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "Rating Context " . $model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

function showForm($update, $formObj, $view, $units, $blocks)
{
    if ($update=="context") {
        return $view->render('context/_contextForm', ['model'=>$formObj, "unit"=>$unit]);
    }
}

$css = <<<CSS

.unit-div {
	padding:20px;
	border: 1px solid lightgrey;
	border-radius: 4px;
}

.item-div {
	margin:10px 20px;
	padding: 5px 0 10px;
}

.context-div, .src-div, .context-items-div {
	margin-left:20px;
	padding: 4px 0;
}

.src-a, .context-a {
	padding-right: 10px;
}
CSS;

$this->registerCSS($css);

$blocks = $model->getBlocks();
$units = $model->getUnits();
$items = $model->getItems();
?>

<div class="questions-view">

    <h3 class="pull-right"><span class="label label-warning"><?= $model->Type ?></span></h3>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index','rcID'=>$model->rcID], ['class'=>'btn btn-default'])?>
    </p>

	<div class="row">
    <?php Pjax::begin(["scrollTo"=>200]); ?>
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading"><?= $model->name ?></div>
			
				<div class="panel-body">
					
				<?php
                    // show each block for test
                    foreach ($blocks as $block) {
                        $blockUnits = $block["units"];
                        echo $this->render('context/_block', ['block'=>$block, "blockUnits"=>$blockUnits, "items"=>$items, "units"=>$units, "questionModel"=>$model]);
                    }
                ?>	
				</div>			
			</div>
		</div>
	
		<div class="col-md-5">
			<?= $this->render('context/_contextForm', ['model'=>$formObj]);?>		
		</div>
    <?php Pjax::end(); ?>
	</div>
</div>

<?php
$js = <<<JS
	$(".questions-view").on("change", "#context-srctype", function(e){
		var type = $(this).val();
		if(type=="image"){
			$(".image-src").show();
			$(".text-src").hide();
		}else{
			$(".image-src").hide();
			$(".text-src").show();			
		}
	});
JS;

$this->registerJs($js);
?>
