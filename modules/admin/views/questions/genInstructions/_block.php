<?php
use yii\helpers\Html;
use app\components\Icon;

/* @var $block */
/* @var $questionModel \app\models\Questions */

$genInstructions = $questionModel->getGenInstructions($blockKey);

?>
<div class="block-div">					
	<h3><?=Icon::fa("cubes")?> Block: <?=$block["name"]?></h3>
	
	
	<ul class='gen'>
		<?php
            foreach ($genInstructions as $key=>$instruction) {
                echo "<li>".$instruction['name']." ".
                        Icon::fa(
                                'edit',
                                [
                                    '',
                                    "id"=>$questionModel->id,
                                    "update"=>"genInstruction"
                                ],
                                [
                                    "data-method"=>"post",
                                    "data-params"=>[
                                        "blockKey"=>$blockKey,
                                        "key"=>$key
                                    ],
                                    "data-pjax"=>1
                                ]
                            )." ".
                        Icon::fa(
                            'trash',
                            [
                                '',
                                "id"=>$questionModel->id,
                                "update"=>"genInstruction"
                            ],
                            [
                                "data-method"=>"post",
                                "data-params"=>[
                                    "action"=>"delete",
                                    "blockKey"=>$blockKey,
                                    "key"=>$key
                                ],
                                "data-confirm" => "Are you sure want to delete this general Instruction?",
                                "data-pjax"=>0
                            ]
                        ).
                    "</li>";
            }
        ?>
		<li><?=
                Html::a(
                    Icon::fa('create')." Add",
                    [
                        "gen-instructions",
                        "id"=>$questionModel->id,
                        "update"=>"genInstruction"
                    ],
                    [
                        "class"=>"btn btn-default btn-xs",
                        "data-method"=>"post",
                        "data-params"=>[
                            "blockKey"=>$blockKey
                        ],
                        "data-pjax"=>1
                    ]
                )
        ?></li>
	</ul>
</div>
<div class="" style='padding: 25px 0'></div>
