<?php
/* @var $block */
/* @var $model \app\models\QuestionsForms\GeneralInstruction */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="panel panel-primary">
	<div class="panel-heading"> <?=isset($model) && $model->key!=""? "Edit" : "Add" ?> General Instruction</div>
	<div class="panel-body">
        <?php
            if (Yii::$app->request->get("update")=="genInstruction") {
                $block = $model->getBlock();
                $form = ActiveForm::begin([ "options"=>["enctype"=>"multipart/form-data"] ]); ?>
    			<?= Html::hiddenInput('action', 'save'); ?>
    			<?= Html::hiddenInput('blockKey', $model->blockKey); ?>

    			<?php if ($model->key!==""): ?>
                    <?= Html::hiddenInput('key', $model->key); ?>
    			<?php endif; ?>

    			<?= $form->field($model, 'qID')->hiddenInput()->label(false); ?>
    			
                <div class="form-group">
                    <label>Block Name</label>
                    <div class="form-control-static">
                        <?=$block["name"]?>
                    </div>
        		</div>

    			<?= $form->field($model, 'name')->textInput()->label("General Instruction Name") ?>

    			<?php
                    if ($model->src) {
                        echo '<div class="thumbnail">'.Html::img(["gen-instructions-image", "id"=>$model->qID, "blockKey"=>$model->blockKey, "key"=>$model->key], ["class"=>"img-responsive img-rounded"])."</div>";
                    } ?>

    			<?= $form->field($model, 'src')->fileInput(["name"=>"imageSrc"]) ?>

    			<div class="" style='padding:20px 0'>
    				<?=Html::submitButton('Save', ['class'=>'btn btn-primary']); ?>
    			</div>
    	<?php
                ActiveForm::end();
            } else {
                ?>
                <div class="well"> Please click the icon on the left side.</div>
        <?php
            }
        ?>
	</div>
</div>
