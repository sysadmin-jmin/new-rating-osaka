<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$update = Yii::$app->request->get("update");

?>

<div class="panel panel-primary">

    <div class="panel-heading">Edit Question Item</div>
    
    <div class="panel-body">

        <?php 
            if ($update=="item") {
                $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="form-group">
                    <label>Item: <?=$model->key+1?></label>
                </div>

                <?= Html::hiddenInput('action', 'save'); ?>
                <?= Html::hiddenInput('itemKey', $model->key); ?>

                <?= $form->field($model, 'uid') ?>

                <?= $form->field($model, 'qnType')->textInput(["readonly"=>"readonly"]) ?>
                
                <?= $form->field($model, 'ansType')->textInput(["readonly"=>"readonly"]) ?>
                
                <?= $form->field($model, 'qID')->hiddenInput(['value'=>$model->qID])->label(false) ?>

                <?= Html::submitButton('Save', ['id'=>'add-items-button','class'=>'btn btn-primary']) ?>

        <?php 
                ActiveForm::end();
            } else {
                ?>
            <div class="well"> Please select item on the left side to edit.</div>
        <?php
            }
        ?>
    </div>

</div>