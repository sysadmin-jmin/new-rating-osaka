<?php
use yii\helpers\Html;
use app\components\Icon;

?>
<div class="item-div">
    
    <div class="item-attributes">
        <ul>
            <li class="attr">
    	    <?=
                Html::a(
                    'Item:'.($itemKey+1),
                    [
                        "",
                        "update" => "item",
                        "id" => $id
                    ],
                    [
                        "data-method" => "post",
                        "data-params" =>[
                            "itemKey" =>$itemKey
                        ],
                        "data-pjax" =>1
                    ]
                );
            ?>
            <?=deleteLink($itemKey)?>
			<ul class='item-info'>
				<li>Name: <?=!empty($item["uid"])? $item["uid"] : "<i>(unknown)</i>"?> </li>
				<li>Question: <?=Icon::fa("file-".$item["qnType"]."-o")?> </li>
				<li>Answer <?=Icon::fa("file-".$item["ansType"]."-o")?> </li>
			</ul>
			</li>
        </ul>
    </div>
</div>