<?php
/* @var $blockID integer */
/* @var $rcModel */

use yii\helpers\Html;

$block = $blocks[$blockKey];
?>

<div class="block-div">
    <span class='block-name'>
        <i class='fa fa-cubes'></i> 
         Block: <?= $block['name'] ?> 
    </span>
    <div class="group-div">
        <i class="fa fa-archive"></i> Units <span class='small'>(<?=count($block["units"])?>)</span>
        
        <div class="group-item-div">
            <ul>
                <?php
                    if (isset($block["units"])) {
                        foreach ($block["units"] as $unitKey) {
                            $blockID = $block["id"];
                            echo "<li>".$units[$unitKey]["name"]." ".
                                Html::a(
                                        '<i class="fa fa-minus-circle"></i>',
                                        [
                                            'assign-unit-to-block',
                                            "id"=>$model->id
                                        ],
                                        [
                                            "data-confirm"=>"Are you sure want to remove this unit from block?",
                                            "data-method"=>"post",
                                            "data-params"=>[
                                                'action'=>'unassignUnit',
                                                'blockKey'=>$blockKey,
                                                "unitKey"=>$unitKey
                                            ]
                                        ]
                                )."</li>";
                        }
                    }
                ?>                
            </ul>
            
            <div class="" style='padding-left:25px'>
                <?=Html::a(
                    '<i class="fa fa-plus"></i> Assign Unit',
                    ['assign-unit-to-block', "id"=>$model->id, "blockKey"=>$blockKey, "update"=>"assignUnit"],
                    ['class'=>'btn btn-default btn-xs']
                ); ?>
        
            </div>  
        </div>      
    </div>
    
</div>  