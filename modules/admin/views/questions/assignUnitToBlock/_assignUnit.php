<?php
/* @var $items array*/
/* @var $unitKey integer */
/* @var $unit array*/

use yii\helpers\Html;

?>

<div class="panel panel-primary">

    <div class="panel-heading">
    Assign Unit to Block

    </div>
    <div class="panel-body">
        <?php
            if (Yii::$app->request->get("update")=="assignUnit") {
                $assignedUnits = $model->getAssignedUnits();
                $block = $blocks[$blockKey];

                echo Html::beginForm(['assign-unit-to-block', "id"=>Yii::$app->request->get("id")]);
                echo Html::hiddenInput("blockKey", $blockKey);
                echo Html::hiddenInput("action", "assignUnit"); ?>
            <div class="form-group">
                <label>Block Name</label>
                <div class="form-control-static">
                    <?=$block["name"]?>
                </div>
            </div>
            
            <div class="form-group">
                <label>Unit</label>
                <div class="form-control-static">
                    <?php
                        $unitA=[];
                foreach ($units as $key=>$unit) {
                    if (!in_array($key, $assignedUnits)) {
                        $unitA[$key] = $unit["name"];
                    }
                }
                echo Html::dropDownList('unitKey', "", $unitA, ['class'=>'form-control', 'prompt' => 'Select Unit', ]); ?>
                </div>
            </div>
            <p style='margin: 10px 0'>
               <button class='btn btn-primary' type="submit" id="submit">Save</button>
            </p>
        <?php 
                echo Html::endForm();
            } else {
                ?>

            <div class="well"> Please click 'Assign Unit' on the left side.</div>        

        <?php
            }
        ?>

    </div>

</div>