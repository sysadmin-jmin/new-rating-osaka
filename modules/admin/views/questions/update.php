<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = 'Update Questions: ' . $model->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Questions Sets', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="questions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
