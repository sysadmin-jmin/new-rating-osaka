<?php
use app\components\Icon;

/* @var $block */
/* @var $blockUnits */
/* @var $items */
/* @var $units */
/* @var $questionModel */

?>

<div class="block-div">					
	<h2> <?=Icon::fa("cubes")?>  Block: <?=$block["name"]?></h2>	
	<div class="unit-div">
		<?php
            // show units in each block
            foreach ($blockUnits as $unitKey) {
                $unit = $units[$unitKey];
                echo "<h4> ".Icon::fa("archive")." Unit:".$unit["name"]."</h4>";
                // show items in each units
                foreach ($unit["items"] as $itemKey) {
                    $item = $items[$itemKey];
                    $qItem = $questionModel->getItems($itemKey);
                    echo $this->render('_item', ['item'=>$item, "qItem"=>$qItem, "questionModel"=>$questionModel, "itemKey"=>$itemKey]);
                }
            }
        ?>
	</div>
</div>
<div class="" style='padding: 25px 0'></div>
