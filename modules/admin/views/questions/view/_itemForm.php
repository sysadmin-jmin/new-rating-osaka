<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="panel panel-primary">

	<div class="panel-heading">Update Question Item</div>

	<div class="panel-body">
		<?php 
            if (Yii::$app->request->get("update")=="items") {
                $question = $model->getQuestion();
                $item = $question->getItems($model->key); ?>
			<h4>Item: <?=!empty($item["uid"])? $item["uid"] : "<i>(unknown)</i>"?></h4>

			<?php $form = ActiveForm::begin(); ?>
				<?=Html::input("hidden", "action", "save")?>
				<?= $form->field($model, 'qID')->hiddenInput()->label(false) ?>

				<?php if ($model->qnType=="image" && !empty($model->src)):?>
					<div class="placeholder">
						<img src="<?=Url::to(["item-image", "itemKey"=>$model->key, "id"=>$question->id])?>" class="img-responsive img-rounded">
					</div>
				<?php endif; ?>

				<?= $form->field($model, 'key')->textInput(['readonly'=> true]) ?>

				<?php if ($model->qnType=="image"): ?>
					<?= $form->field($model, 'src')->fileInput() ?>
				<?php else: ?>
					<?= $form->field($model, 'src')->textInput() ?>
				<?php endif; ?>

				<?= $form->field($model, 'qnType')->textInput(['readonly'=> true]) ?>

				<div class="" style='padding:20px 0'>
					<?= Html::submitButton('<i class="fa fa-check"></i> Save', ['class'=>'btn btn-primary']) ?>

					<?= Html::a("<i class='fa fa-trash-o'></i> Delete", ["view", "id"=>$question->id, "update"=>"items", "key"=>$model->key], ["class"=>"btn btn-danger", "data-confirm"=>"Are you sure want to delete the item content?", "data-method"=>"post", "data-params"=>["action"=>"delete"] ])?>
				</div>
		<?php 
                ActiveForm::end();
            } else {
                ?>
            <div class="well"> Please select the source items on the left side.</div>		
		<?php
            }
        ?>
	</div>

</div>