<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\components\Icon;

# showSrc from \view


/* @var $item */
/* @var $qItem */
/* @var $qContext */
/* @var $questionModel */
/* @var $itemKey */


?>

<div class="item-div panel">
	<div class="">
		<?=Icon::fa("folder-o")?>
		 Item: <?=!empty($item["uid"])? $item["uid"] : "<i>(unknown)</i>"?> 
		 
		 
	</div> 
	<div class="src-div">
        <?=Icon::fa("long-arrow-right")?>  Source: <?=showSrc($qItem, $questionModel, $itemKey)?>
	</div>
	<?php /*
	<div class="src-div">
        <?=Icon::fa("long-arrow-right")?>  Answer: <?=showAnswer($qItem, $questionModel, $itemKey)?>
	</div>
	*/ ?>
</div>
