<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\PPrint;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'rcID')->hiddenInput()->label(false) ?>
	
	
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'projectID')->textInput(['readonly'=>'readonly']) ?>
	
	
	<?= $form->field($model, 'items')->textArea(['readonly'=>'readonly']) ?>

	<?php // PPrint::print($model->items) ?>


	
	<?= $form->field($model, 'units')->textArea(['readonly' => 'readonly']) ?>

	<?php // PPrint::print($model->units) ?>

	
	<?= $form->field($model, 'blocks')->textArea(['readonly' => 'readonly']) ?>
	

	<?php // PPrint::print($model->blocks) ?>
	
	
    <?= $form->field($model, 'type')->dropDownList($model->typeA()) ?>
	
	<?= $form->field($model, 'status')->dropDownList($model->statusA(),['readonly'=>'readonly']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		
		<?= $form->errorSummary($model) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
