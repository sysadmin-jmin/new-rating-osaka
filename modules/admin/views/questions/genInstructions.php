<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = "General Instructions Set " . $questionModel->name;
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$questionModel->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Question Sets', 'url' => ['index','rcID'=>$questionModel->rcID]];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);

$css = <<<CSS
ul.gen {
	margin-left: 20px;
	margin-top: 20px;
	
}

ul.gen li {
	padding: 4px 0;
}


.src-a, .context-a {
	padding-right: 10px;
}
CSS;

$this->registerCSS($css);

$blocks = $questionModel->getBlocks();

?>

<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Back', ['index','rcID'=>$questionModel->rcID], ['class'=>'btn btn-default'])?>
    </p>
	<div class="row">
		<?php Pjax::begin(["scrollTo"=>200]); ?>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">General Instructions</div>
			
				<div class="panel-body">
					<?php
                        foreach ($blocks as $blockKey=>$block) {
                            echo $this->render(
                                "genInstructions/_block",
                                [
                                    "blockKey"=>$blockKey,
                                    "block"=>$block ,
                                    "questionModel"=>$questionModel
                                ]
                            );
                        }
                    ?>	
				</div>			
			</div>
		</div>
	
		<div class="col-md-6">
			<?=$this->render("genInstructions/_genInstructionForm", ["model"=>$formObj]);?>
		</div>
		<?php Pjax::end(); ?>
	</div>
</div>


