<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Question Sets';
$this->params['breadcrumbs'][] = ['label'=> 'Rating Configuration Set', 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?= Html::a('Back', ['rating-configs/view','id'=>$rcID], ['class' => 'btn btn-default']) ?>
    
	
     </p>

	<div style="padding: 5px 0"></div>

<section>	
<?php 
$css = <<<CSS
.step {
	text-align: center;
	padding: 20px;
	color: #e6e6e6;
}

.black {
	color: #000;
}

.step i {
	
	font-size: 35px;
	display: block;
}
CSS;

$this->registerCSS($css);
?> 

<div class="panel panel-default">

	<div class="panel-body">
	<div class="row">
		<div class="col-md-4 step">
			<?= Icon::Fa('setting') ?>
			1. Test Items
		</div>
		<div class="col-md-4 black step">
			<?= Icon::Fa('question') ?>
			2. Questions
		</div>
		<div class="col-md-4 step">
			<?= Icon::Fa('answer') ?>
			3. Answers
		</div>
	</div>

	</div>
</div>

</section>

	
	<h3>Actual Question Set </h3>
	
	
	<p>
	    <?= Html::a('Create Actual Question Set', ['create','rcID'=>$rcID,'type'=>'actual'], 
										   ['class' => 'btn btn-success']) ?>
   
	</p>
    <?= GridView::widget([
        'dataProvider' => $dataProviderActual,
        'columns' => [
            'id',
            
            'name',
            'Type',
            
            ['label'=>'Rating Structure',
             'value'=> function ($model) {
                 $s = 'Rating Structure is <br>Locked';
                 return $s;
             },
            'format'=>'html',
            ],
            
            ['label'=>'Upload Files',
             'value'=> function ($model) {
                 if ($model->status == 'lock') {
                     $s = "Locked";
                 } else {
                     $s = '<ul>';
                     
                     $s .= "<li>".Html::a("Upload Question Files", ['view','id'=>$model->id])."</li>";
                     $s .= "<li>".Html::a("Set Unit Rating Context", ['context','id'=>$model->id])."</li>";
                     $s .= "<li>".Html::a("Upload General Instructions", ['gen-instructions','id'=>$model->id])."</li>";
                     $s .= "<li>".Html::a("Upload Examples", ['examples','id'=>$model->id])."</li>";
                     $s .= '</ul>';
                 }
                 return $s;
             },
            'format'=>'html',
            ],
            
            ['label'=>'Answer Sheet',
             'value'=> function ($model) {
                 if ($model->status=='lock') {
                     $s = Html::a('Answers', ['answers/actual-index',
															'rcID'=>$model->rcID,
															'qnID'=>$model->id],
														  ['class'=>'btn btn-default']);
                 } else {
                     $s = Html::a('Lock Upload', ['lock'], ['class'=>'btn btn-default',
													'data'=>[
														'method'=>'post',
														'params'=>['id'=>$model->id,'status'=>$model::STATUS_LOCK],
														'confirm'=>'Lock?'
														]
													 ]);
                                         
                     $s .= "<br>";
                 }
                 return $s;
             },
            'format'=>'raw',
            ],
            'Status',
			['class' => 'yii\grid\ActionColumn',
             'template' => '{update} {delete} '
            ]
        ],
    ]); ?>	
	
	
	<div style="padding: 25px 0">
		<hr>
	</div>
	<h3>Training / Certification Test</h3>
	<p>
	    <?= Html::a('Create Training Question Set', ['create','rcID'=>$rcID,'type'=>'training'], 
										   ['class' => 'btn btn-success']) ?>
   
	</p>	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            
            'name',
            'Type',
            
            ['label'=>'Rating Structure',
             'value'=> function ($model) {
                 
				 if($model->status=='setup') {
					 $s = '<ul>';
					 $s .= "<li>".Html::a("Set Rating Items", ['items','id'=>$model->id])."</li>";
					 $s .= "<li>".Html::a("Set Units", ['units','id'=>$model->id])."</li>";
					 $s .= "<li>".Html::a("Assign Items to Units", ['assign-item-to-unit','id'=>$model->id])."</li>";
					 $s .= "<li>".Html::a("Assign Units to Blocks", ['assign-unit-to-block','id'=>$model->id])."</li>";
					 $s .= '</ul>';
					 }
			     else {
					 $s = 'Locked';
				 }
                 return $s;
             },
            'format'=>'html',
            ],
            
            ['label'=>'Upload Files',
             'value'=> function ($model) {
                 
				 if($model->status=='setup') {
					 $s = Html::a('Lock Structure',['lock'],[ 'class'=>'btn btn-default',
															  'data'=>[
																   'method'=>'post',
																   'params'=>['id'=>$model->id,
																			  'status'=>$model::STATUS_UPLOAD
																			  ]
																	]
															  ]);
				 }
				 elseif($model->status=='upload') {
				 $s = '<ul>';
					$s .= "<li>".Html::a("Upload Questions", ['view','id'=>$model->id])."</li>";
					$s .= "<li>".Html::a("Set Unit Rating Context", ['context','id'=>$model->id])."</li>";
					$s .= "<li>".Html::a("Upload General Instructions", ['gen-instructions','id'=>$model->id])."</li>";
					$s .= "<li>".Html::a("Upload Examples", ['examples','id'=>$model->id])."</li>";
                 $s .= '</ul>';
                 }
				 else $s = 'Locked';
				 return $s;
             },
            'format'=>'raw',
            ],
            
            ['label'=>'Modal Answer Sheet',
                 'value'=> function ($model) {
                     if($model->status==$model::STATUS_UPLOAD) {
                         $s = Html::a('Lock Upload',['lock'],['data'=>
                                                        ['method'=>'post',
                                                         'params'=>['id'=>$model->id,
                                                                    'status'=>$model::STATUS_LOCK]
                                                        ],
                                                       'class'=>'btn btn-default'
                                                      ]);
                     }
                     elseif($model->status == $model::STATUS_LOCK) {
                        $s = Html::a('Modal Answers', ['answers/training-index',
                                                    'rcID'=>$model->rcID,'qnID'=>$model->id],
                                                    ['class'=>'btn btn-default']
                                    );
                     }
                     else $s = '';
                     return $s;
                 },
                'format'=>'raw',
            ],
            'Status',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
</div>
