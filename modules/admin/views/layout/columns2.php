<?php 
use yii\helpers\Html;

 ?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="row">

<div class="col-md-3">
	
	 <div class="list-group">
		<div class="list-group-item disabled">Options</div>
		
		<?php 
		$projectsLink = "<i class=\"fa fa-product-hunt\"></i> Projects"; 
		$ratersLink   = "<i class=\"fa fa-star\"></i> Raters";
		$settingsLink = "<i class=\"fa fa-cog\"></i> Settings";
		
		
		echo Html::a($projectsLink,['projects/'],['class'=>'list-group-item']);
		echo Html::a($ratersLink,['raters/'],['class'=>'list-group-item']);
		echo Html::a($settingsLink,['settings/'],['class'=>'list-group-item']);
		
		
		
		?>
		
         
	  
	</div> 	
	

</div>

<div class="col-md-9">
	<?= $content ?>
</div>


</div>
<?php $this->endContent(); ?>
