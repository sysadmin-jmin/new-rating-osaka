<?php
/* @var $blockID integer */
/* @var $rcModel */

use yii\helpers\Html;
use \app\models\Raters;
use app\components\Icon;

$blockID = $item["id"];
$rcGroups = $rcModel->getGroups();
$assignedGroupsCount = isset($rcGroups[$blockID]) ? count($rcGroups[$blockID]) : 0;


$css = <<<CSS

.user-type-list li{
	font-size:11px;
	padding-top:4px;
}
CSS;

$this->registerCss($css);
$groups = Yii::$app->request->get('groups');

?>


<div class="block-div">
    <span class='block-name'>
        <?= Icon::fa('block') ?> 
         Block: <?= $item['name'] ?> 
    </span>
    <?= updownLink($blockKey, $blocks) ?>
    
    <?= deleteLink($blockKey) ?>
	
	<div class="group-div">
		<?= Icon::fa('settings') ?> 
		<?= Html::a('Settings', ['block-settings','rcID'=>$rcModel->id,'blockKey'=>$blockKey]) ?>
	</div>	
	
	<div class="" style='padding-left:15px'>
		<?=
            Html::a(
                    '<i class="fa fa-plus"> </i> Add block here',
                    [
                        'blocks',
                        "rcID"=>$rcModel->id,
                        "forms"=>"addBlock",
                        "blockKey"=>$blockKey
                    ],
                    [
                        "class"=>"btn btn-default btn-xs"
                    ]
            );
        ?> 	
	
	</div>
    
	
	<hr>
</div>  
