<?php
/* @var integer $key */
/* @var \app\models\RatingConfigs $rcModel */

use yii\helpers\Html;
use \app\models\Raters;
use app\components\Icon;

$assignedItemsCount = isset($units[$unitKey]['items']) ? count($units[$unitKey]['items']) : 0;


$css = <<<CSS
.user-type-list li{
	font-size:11px;
	padding-top:4px;
}
CSS;

$this->registerCss($css);

?>

<div class="block-div">
    <span class='block-name'>
        <?=fa("archive")?> 
         Unit:<?= $units[$unitKey]['name'] ?> 
        <span class='small'>(<?=$assignedItemsCount?>)</span>
	</span>

    <?= updownLink($unitKey, $units) ?>
    
    <?php ?>
    <div class="group-div">
       
            <ul>
                <?php
                    if (isset($units[$unitKey]['items']) && is_array($units[$unitKey]['items']) && count($units[$unitKey]['items']) > 0):
                        foreach ($units[$unitKey]['items'] as $itemKey):
                            if (isset($items[$itemKey])):
                                echo sprintf(
                                    '<li>%s : %s %s</li>',
                                    Icon::Fa('item'),
                                    $items[$itemKey]['uid'],
                                    Html::a(
                                        fa('minus-circle'),
                                        ['assign-item-to-unit','rcID'=>$rcModel->id],
                                        [
                                            "data-confirm" =>"Are you sure want to remove this item from unit?",
                                            "data-method"  =>"post",
                                            "data-params"  =>[
                                                'action'  =>'unassignItem',
                                                'unitKey' =>$unitKey,
                                                "itemKey" =>$itemKey
                                            ]
                                        ]
                                    )
                                );
                            endif;
                        endforeach;
                    endif;
                ?>
            </ul>
			
			<div class="" style='padding-left:25px'>
				<?=Html::a(
                    fa("create").' Assign Item',
                    [
                        'assign-item-to-unit',
                        "rcID"    =>$rcModel->id,
                        "forms"   =>"assignItemToUnit",
                        "unitKey" =>$unitKey
                     ],
                     ['class'=>'btn btn-default btn-xs']
                ); ?>
		
			</div>
    </div> <!--end group div -->
	
	<hr>
</div>  
