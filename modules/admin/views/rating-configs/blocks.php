<?php
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use \app\models\RatingConfigs;
use \app\models\Raters;

$this->title = "Rating Blocks";

$rcID = $model->id;

#fix this breadcrumb
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $model->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $model->test_name, 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;

function deleteLink($key)
{
    return Html::a(
        '<i class="fa fa-minus-circle"></i>',
        ['blocks', 'rcID'=>Yii::$app->request->get('rcID')],
        ["data-confirm"=>"Are you sure want to delete this block?", "data-method"=>"post", "data-params"=>['action'=>'delete', 'blockKey'=>$key]]
    );
}

function updownLink($key, $list)
{
    $s = '';
    
    if ($key != 0) {
        $s = Html::a(
            '<i class="fa fa-arrow-up block-arrow" style="color:green"></i>',
            ['blocks', 'rcID'=>Yii::$app->request->get('rcID')],
            ["data-method"=>"post", "data-params"=>['action'=>'up', 'blockKey'=>$key]]
        );
    }
    
    if ($key!=count($list)-1) {
        $s .= Html::a(
            '<i class="fa fa-arrow-down block-arrow" style="color:green;"></i>',
            ['blocks', 'rcID'=>Yii::$app->request->get('rcID')],
            ["data-method"=>"post", "data-params"=>['action'=>'down', 'blockKey'=>$key]]
        );
    }
    return "<span class='updownLink'>{$s}</span>";
}

function showForm($view, $blocks, $blockKey, $groups, $model)
{
    if (Yii::$app->request->get("forms")=="addBlock") {
        return $view->render('forms/_addBlock', ["blockKey"=>$blockKey,'latestIndex' => count($blocks)+1]);
    } elseif (Yii::$app->request->get("forms")=="assignGroupToBlock") {
        $blockID = $blocks[$blockKey]["id"];
        $assignedGroup = ArrayHelper::getColumn($model->getGroups($blockID), "gID");
        $groupMap = [];

        foreach ($groups as $group) {
            if (!in_array($group["code"], $assignedGroup)) {
                $groupMap[$group["code"]] = $group["name"];
            }
        }

        return $view->render('forms/_assignGroupToBlock', ["groups" => $groupMap, "block" => $blocks[$blockKey]]);
    }
}

$css = <<<CSS
i {
	padding-right: 4px;
}
.block-div {
	padding: 10px;
}

.block-name {
	display:inline-block;
	padding: 0 10px;
	font-size: 20px;	
}

.block-arrow {
	margin-right:5px;
}

.group-div {
	padding:10px 50px;
}

.group-item-div {
	padding: 10px 20px;
}

.group-qfy-div {
	padding: 5px 30px;
}
.group-qfy-div li {
	font-size: 12px;
}

.updownLink {
	display: inline-block;
	width: 60px;

}

.displayhide{
    display: none;
}

CSS;

$this->registerCss($css);

$blocks = $model->getBlocks();
$groupsA = Raters::getGroups();
$blockID = Yii::$app->request->get("blockID");
$blockKey = Yii::$app->request->get("blockKey");

$groups = Yii::$app->request->get('groups');

?>

<h1>Rating Blocks</h1>


<p>
	<?= Html::a('Back', ['view','id'=>$rcID], ['class'=>'btn btn-default']) ?>	
</p>

<div class="row">
<?php Pjax::begin(["scrollTo"=>200]); ?>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?=$model->test_name?> Block Structure
			</div>
			<div class="panel-body">
				<?php
                    if (count($blocks) > 0) {
                        foreach ($blocks as $key => $item) {
                            echo $this->render('blocks/_listBlock', ["rcModel"=>$model, "blockKey"=>$key, "groupsA"=>$groupsA, "blocks"=>$blocks, "item"=>$item]);
                        }
                    } else {
                        ?>
    					<div class="block-div">
    						<div class='alert alert-warning'>
    							 No blocks found. 
    						</div>
    						<?=Html::a('<i class="fa fa-plus-circle"> </i> Add block', ['blocks', "rcID"=>$rcID, "forms"=>"addBlock", "blockKey"=>0], ["class"=>"btn btn-default"]); ?>
    						<hr>
    					</div>					
				<?php
                    } ?>
			</div>			
		</div>
	</div>
	<div class="col-md-4">
        <?=showForm($this, $blocks, $blockKey, $groupsA, $model)?>
	
		
		<div class="panel panel-default">
			<div class="panel-heading">Help</div>
			<div class="panel-body">
			<p>Add A group by clicking add block</p>
			<p>You can manage a group by clicking 'Show Groups'</p>
			</div>
		</div>
	
	
	
	</div>
	
	
	
	
	
<?php Pjax::end(); ?>
</div>
