<?php
use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\components\Icon;

$this->title = "Test Set - Setup Items";

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => $rcModel->project->name . ' - Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcModel->id]];

$this->params['breadcrumbs'][] = $this->title;



$fa_picture= "<i class='fa fa-picture-o'></i> ";
$fa_delete ="<i class='fa fa-trash-o'></i> ";



function icon($type)
{
    $icon = "";
    switch ($type) {
        case "text":
            $icon = '<i class="fa fa-file-text-o"> </i>';
            break;
        case "image":
            $icon = '<i class="fa fa-file-image-o"> </i>';
            break;
        case "audio":
            $icon = '<i class="fa fa-file-audio-o"> </i>';
            break;
    }

    return $icon;
}

$css = <<<CSS
i {
	padding-right: 4px;
}
.item-div {
	padding:10px;
}

ul.item-info li {
	font-size:10px;
	padding-bottom:2px;
}

.item-arrow {
	margin-right:5px;
}


.updownLink {
	display: inline-block;
	width: 60px;

}

CSS;

$this->registerCss($css);

$update = Yii::$app->request->get("update");
?>

<div class="">

	<h1><?= $this->title ?></h1>
	<p>
		<?= Html::a('Back', ['view','id'=>$rcModel->id], ['class'=>'btn btn-default']) ?>
	</p>


	<div class="row">
		<?php Pjax::begin(["scrollTo"=>200]); ?>
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php $count = count($items) ?>
					Items (<?= $count ?>)
				</div>
				<div class="panel-body">
					<?php 
                        if ($count>0) {
                            foreach ($items as $key=>$item) {
                                echo $this->render("items/_item", ["item"=>$item, "itemA"=>$items, "itemKey"=>$key, "rcID"=>$rcModel->id]);
                            }
                        }
                    ?>
					<div class="item-attributes">
					    <?=Html::a(
                                    Icon::Fa('add') . " Items",
									['items', "rcID"=>$rcModel->id,
									 "update"=>"item"
									 ],
									 ['class'=>'btn btn-default btn-xs']
                        ); ?>
					</div>  
				</div>
			</div>
		</div>


		<div class="col-md-4">

			<?= $this->render("items/_itemForm", ["model"=>$itemM]); ?>

		</div>
		<?php Pjax::end(); ?>
	</div>
</div>