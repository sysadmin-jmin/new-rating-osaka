<?php
use yii\helpers\Html;
use app\components\Icon;

?>



<li class="unit">
    <strong>Unit <?= $id ?></strong>: 
	<span id="unit-index-name<?= $id ?>"><?= !empty($name) ? $name : '<span class="text-muted">Unknown</span>' ?></span>
    - <?= Html::a(Icon::fa('remove'),
        ['/admin/rating-configs/units',
            'action'=>'delete',
            'id'=>$key,
            'rcID'=>$rcID
        ],
		['data-confirm'=>"Delete this?"]
        ); ?>

</li>


