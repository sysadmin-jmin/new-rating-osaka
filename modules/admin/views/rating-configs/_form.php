<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RatingConfigs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rating-configs-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'test_name')->textInput(['maxlength' => true]) ?>

	<label for="">Project Name</label>
	<p>
	<?= $model->project->name; ?>
	
	</p>
    <?=$form->field($model, 'projectID')->hiddenInput()->label(false) ?>

	<?php 
    if (!$model->isNewRecord) {
        echo $form->field($model, 'status')->dropDownList($model->statusA());
    } else {
        echo $form->field($model, 'rating_items')->hiddenInput(['value'=>"[]"])->label(false);
		echo $form->field($model, 'status')->hiddenInput(['value'=>0])->label(false);
	}
    ?>
	
	
    <div class="form-group">
        <?= Html::submitButton(($model->isNewRecord) ? 'Create' : 'Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
