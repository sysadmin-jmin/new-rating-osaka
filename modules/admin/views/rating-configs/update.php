<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RatingConfigs */

$this->title = 'Update Test Set: ' . $model->test_name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $model->projectID]];
$this->params['breadcrumbs'][] = ['label' => $model->test_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rating-configs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
