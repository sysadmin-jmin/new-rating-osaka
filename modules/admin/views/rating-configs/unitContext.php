<?php 
use yii\widgets\ActiveForm;

use yii\helpers\Html;
use app\models\RatingConfigs;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Modal;

$this->title = "Unit Rating Context";

$rcModel = \app\models\RatingConfigs::findOne($rcID);

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;

$fa_trash = '<i class="fa fa-trash-o"></i>';

#An Array of descriptions
$desA = [];                 
?>


<p>
		<?= Html::a('Back',['rating-configs/view','id'=>$rcID],['class'=>'btn btn-default']) ?>

		<?= Html::a('Delete All Contexts',['rating-configs/unit-context','rcID'=>$rcID,
										   'action'=>'deleteAll'],
				     ['class'=>'btn btn-danger']) ?>
	
</p> 

<div class="">

<h1><?= $this->title ?></h1>


<?php $form = ActiveForm::begin() ?>


<div class="row">

<div class="col-md-6">

<div class="panel panel-primary">

<div class="panel-heading"> Unit Context </div>

	<div class="panel-body">

		<table class='table table-bordered'>

		<tr>
			<th style='width:50px'></th>
			<th>Unit ID</th>
			<th style='width:200px'>Context</th>
			<th>Order</th>
			<th>Action</th>
		</tr>
		<?php 
        if (count($unitA)) :

            foreach ($unitA as $key=>$row) :
                 
                
                $_contextA = @$contextA[$key] ?? [] ;
				
            usort($_contextA, function ($a, $b) {
                return $a['order'] <=> $b['order'];
            }); ?>
				<tr class='top-row'>
					<td>
                        <input type="radio" name="Contexts[unitID]" value="<?php echo $key ?>">
					</td>
					<td colspan="5"> <b><?= $row['name']; ?></b></td>
					
				</tr>

				<?php if (count($_contextA) > 0) : ?>
						<?php foreach ($_contextA as $_key => $contextRow): ?>
								<tr>

									<td><span class="glyphicon glyphicon-arrow-right" style='float:right'></span>
									</td>
									<td><?= $contextRow['name'] ?></td>
									<td><?= RatingConfigs::getContext($contextRow['context']) ?></td>
									<td>

									
									
									<?= @$contextRow['order'] ?></td>
									<td>
									<a class='openModal' data-unitID='<?=$key?>' data-key='<?=$_key?>'>
										<i class="fa fa-file-text-o"></i>
									</a>
									<?= Html::a('<i class="fa fa-edit"></i> ', ['unit-context','rcID'=>$rcID,
											 'unitID'=>$key,
											 'key'=>$_key
											 ]
											 ); ?>	

									<?= Html::a($fa_trash, ['unit-context','rcID'=>$rcID,
														'unitID'=>$key,
														'key'=>$_key,
														'action'=>'clear',
														]
										); 
									?>	
											 
									</td>		
								</tr>
								
						<?php $desA[$key][$_key] = $contextRow['des']; ?>
						<?php endforeach; ?>
				<?php endif; ?>
			<?php endforeach;?>
        <?php endif; ?>

		</table>
	</div>
</div>

</div>


<div class="col-md-6">

<div class="panel panel-default">

<div class="panel-heading"> Add Rating Context To Unit </div>
	<div class="panel-body">

		<?= $form->field($contextM,'name')->textInput() ?>

		
		<?php $contentTypeA = RatingConfigs::getContextTypeA() ?>
		<?= $form->field($contextM,'context')->dropDownList($contentTypeA) ?>

		<?php if ($contextM->unitID !='') {
				echo $form->field($contextM,'unitID')
						  ->hiddenInput()
						  ->label(false);
				
				echo Html::hiddenInput('action', 'update');
				}
			  else {
				echo Html::hiddenInput('action', 'create');  
			  }	
			?>
		
		<?= $form->field($contextM,'key')->hiddenInput()->label(false) ?>
		
		
		<?= $form->field($contextM,'des')->widget(TinyMce::className(), [
		'options' => ['rows' => 20],
		'language' => 'en',
		'clientOptions' => [
			'plugins' => [
				"advlist", "table"
			],
			'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		]
	]	)->label('Context Rating Information');
		?>

		<p style='margin-top:20px'>
			<?php echo Html::submitButton('Add', ['class'=>'btn btn-primary']) ?>
		</p>
	</div>


</div>

</div>

</div>


<?php ActiveForm::end() ?>

</div>
</div>
<?php 
/* JS using jquery */

$des_json = json_encode($desA,JSON_FORCE_OBJECT);

$js = <<<JS

var desA = {$des_json};

$('.openModal').click(function(e) {
	e.preventDefault();
	var unitID = $(this).attr("data-unitID")
	var key = $(this).attr("data-key")
	
	$('#des-block').html(desA[unitID][key])
	
	$('#des').modal({"show":true})
})

JS;

$this->registerJs($js);

?>



<?php 
Modal::begin([
    'id'=>'des',
	'header' => '<h2>Unit Context Description</h2>',
]);

echo "<span id='des-block'></span>";

Modal::end();

?>
