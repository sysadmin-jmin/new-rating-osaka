<?php 
use yii\widgets\ActiveForm;

use yii\helpers\Html;

$this->title = "Test Set - Setup Rating Units";

$rcModel = \app\models\RatingConfigs::findOne($rcID);

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;

$css = <<< CSS

li.unit {
    padding: 10px;
}
CSS;
$this->registerCss($css);


?>

<h1> <?= $this->title ?> </h1>

<p>
	<?= Html::a('Back', ['view','id'=>$rcID], ['class'=>'btn btn-default']) ?>
</p>


<div class="row">

    <div class="col-md-8">

        <div class="panel panel-default">

            <div class="panel-heading">Manage</div>

            <div class="panel-body">

                <ul class="list-unstyled">
                    <?php foreach ($list as $key => $item): ?>
                        <?= $this->render(
                            'units/_listUnit',
                            [
                                'rcID' => $rcID,
                                'id' => $key+1,
                                "key" => $key,
                                "name" => $item['name'],
                                "item" => $item,
                            ]
                        ); ?>
                    <?php endforeach ?>
                </ul>

            </div>


        </div>

    </div>

    <div class="col-md-4">
        <div class="panel panel-default">

            <div class="panel-heading">Add New Unit</div>

            <div class="panel-body">
                <?= Html::beginForm() ?>

                <label>Name</label>
                <?=Html::textInput('name', "", ['class' => 'form-control']); ?>
                <br>
                <p>
                    <?=Html::submitButton("Add Unit", ["class"=>"btn btn-primary"])?>
                </p>

                <?= Html::endForm() ?>

            </div>

        </div>

    </div>


</div>

