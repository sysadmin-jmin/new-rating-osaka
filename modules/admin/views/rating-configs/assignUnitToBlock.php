<?php 
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use yii\helpers\Html;
use app\models\RatingConfigForms\Units;

$this->title = "Assign Units To Block";

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcModel->id]];
$this->params['breadcrumbs'][] = $this->title;

function showForm($view, $blocks, $blockKey, $units)
{
    if (Yii::$app->request->get("forms")=="assignUnit") {
        $aUnits = Units::getAssignedUnits($blocks);
        return $view->render('assignUnitToBlock/_assignUnit', ["blockKey"=>$blockKey, "block"=>$blocks[$blockKey], "units"=>$units, "assignedUnits"=>$aUnits]);
    }
}

$css = <<<CSS
i {
    padding-right: 4px;
}
.block-div {
    padding-bottom: 30px;
}

.block-name {
    display:inline-block;
    padding: 0 10px;
    font-size: 20px;    
}

.block-arrow {
    margin-right:5px;
}

.group-div {
    padding:10px 50px;
}

.group-item-div {
    padding: 10px 20px;
}

.group-qfy-div {
    padding: 5px 30px;
}
.group-qfy-div li {
    font-size: 12px;
}

.updownLink {
    display: inline-block;
    width: 60px;

}

.displayhide{
    display: none;
}

CSS;

$this->registerCss($css);

$blockKey = Yii::$app->request->get("blockKey");

?>

<h1>Assign Unit To Block</h1>

<p>
	<?= Html::a('Back', ['view','id'=>$rcModel->id], ['class'=>'btn btn-default']) ?>
</p>

<div class="row">

<?php Pjax::begin(["scrollTo"=>200]); ?>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?=$rcModel->test_name?> Block Structure
            </div>
            <div class="panel-body">
                <?php
                    if (count($blocks) > 0) {
                        foreach ($blocks as $key => $block) {
                            echo $this->render('assignUnitToBlock/_block', ["rcModel"=>$rcModel, "blockKey"=>$key, "block"=>$block, "units"=>$units]);
                        }
                    } else {
                        ?>
                        <div class="block-div">
                            <div class='alert alert-warning'>
                                 No blocks found. 
                            </div>
                            <?=Html::a('<i class="fa fa-plus-circle"> </i> Add block', ['blocks', "rcID"=>$rcModel->id, "forms"=>"addBlock", "blockKey"=>0], ["class"=>"btn btn-default"]); ?>
                            <hr>
                        </div>                  
                <?php
                    } ?>
            </div>          
        </div>
    </div>
    <div class="col-md-4">
        <?=showForm($this, $blocks, $blockKey, $units)?>
    </div>
<?php Pjax::end(); ?>

</div>


<div class="div-space"></div>






