<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Raters;
use app\models\RatingConfigForms\Rater;
use app\components\Icon;

/** @var \app\models\RatingConfigs $rcModel */

$this->title = "Test: ".$rcModel->test_name." Block: ".$block['name']." Group: ".$group['name'];

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcModel->id]];
$this->params['breadcrumbs'][] = ['label'=> 'Rating Blocks', 'url'=>['rating-configs/blocks','rcID'=>$rcModel->id, "groups"=>1]];
$this->params['breadcrumbs'][] = [
                                    'label'=> 'Block Settings #'.$block["id"],
                                    'url'=>[
                                        'rating-configs/block-settings',
                                        'rcID'=>$rcModel->id,
                                        "blockKey"=>Yii::$app->request->get("blockKey")
                                    ]
                                ];
$this->params['breadcrumbs'][] = "Assign Rater to Block";


// count qualified, failed, dna member
$totalRater = Raters::assignedRaterCount($group["code"]);
$qualifiedRater = 0;
$failedRater = 0;
$dnaRater = 0;

$groupMembers = $dataProvider->query->all();
foreach ($groupMembers as $member) {
    $status = $rcModel->getRaterStatus($block["id"], $member->id);
    if ($status=="ok") {
        $qualifiedRater++;
    } elseif ($status=="fail") {
        $failedRater++;
    } elseif ($status=="dna") {
        $dnaRater++;
    }
}

$blockKey = Yii::$app->request->get("blockKey");
?>

<h1><?= $this->title ?></h1>
<p>
	<?=
        Html::a(
            'Back',
            [
                'block-settings',
                'rcID'=>$rcModel->id,
                "blockKey"=>$blockKey
            ],
            [
                'class'=>'btn btn-default'
            ]
        )
    ?>
</p>

<div class="row">
	<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-body">
		Total Member: <?=$totalRater?>
		</div>

	</div>
	</div>
	<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-body">
		Qualified: <?=$qualifiedRater?>
		</div>
	</div>
	</div>
	<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-body">
		Failed: <?=$failedRater?>
		</div>
	</div>
	</div>

	<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-body">
		Did Not Attempt: <?=$dnaRater?>
		</div>
	</div>
	</div>
</div>

<hr>

<div class="" style='padding: 10px 0'>
<?=
    Html::beginForm(
        [
            "block-raters",
            "rcID"=>$rcModel->id,
            "blockKey"=>$blockKey,
            "groupID"=>$group["code"]
        ],
        "get",
        [
            "class"=>'form-inline'
        ]
    )
?>
	<?=
        Html::textInput(
            'search_name',
            Yii::$app->request->get('search_name'),
            [
                'class'=>'form-control',
                'placeholder'=>'Name'
            ]
        );
    ?>
	<?=
        Html::dropDownList(
            'search_status',
            Yii::$app->request->get('search_status'),
            Rater::raterStatus(),
            [
                'class'=>'form-control',
                'prompt'=>'All Status'
            ]
        );
    ?>
	<?= Html::submitButton("Search", ["class"=>'btn btn-primary']); ?>
	
	<?php // ??? use model search object?>
	
<?=Html::endForm()?>
</div>

<div class="row">
    <?php Pjax::begin(["scrollTo"=>200]); ?>
		<div class="col-md-8">
			<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'attribute' => 'status',
                        'value' => function ($model) use ($blockKey, $rcModel) {
                            $status = $rcModel->getRaterStatus($blockKey, $model->id);
                            return Rater::raterStatus($status);
                        }
                    ],
                    [
                        'attribute' => 'isLeader',
                        'label' => 'Leader',
                        'value' => function ($model) use ($blockKey, $rcModel) {
                            $isLeader = $rcModel->isLeaderRater($blockKey, $model->id);
                            return $isLeader ? Icon::fa('check') : Icon::fa('times');
                        },
                        'format'=>'html'
                    ],
                    [
                         'value'=> function ($model) use ($blockKey, $group, $rcModel) {
                             $btn = "<i class='fa fa-edit'></i> ";
                             return Html::a(
                                            $btn,
                                            [
                                                'block-raters',
                                                'raterID'=>$model->id,
                                                "edit"=>"assignRaterToBlock",
                                                "rcID"=>$rcModel->id,
                                                "blockKey"=>$blockKey,
                                                "groupID"=>$group["code"]
                                            ]
                                        );
                         },
                         'format'=>'html',
                    ],
                ]
            ]);?>
		</div>
		<div class="col-md-4">
			<?php
                if (Yii::$app->request->get("edit")=="assignRaterToBlock") {
                    $raterID = Yii::$app->request->get("raterID");
                    echo $this->render(
                            "forms/_assignRaterToBlock",
                            [
                                "raterID"=>$raterID,
                                "rcModel"=>$rcModel,
                                "block"=>$block,
                                "blockKey"=> $blockKey,
                                "group"=>$group
                            ]
                        );
                }
            ?>
		</div>
    <?php Pjax::end(); ?>	
</div>



