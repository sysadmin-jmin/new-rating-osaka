<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\RatingConfigs;


/* @var $this yii\web\View */
/* @var $model app\models\RatingConfigs */

$testSetName = RatingConfigs::getName(Yii::$app->request->get('id'));
$this->title = "{$testSetName}";

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => $model->project->name . ' - Test Sets', 'url' => ['index', 'projectID' => $model->projectID]];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);

$css = <<<CSS

.table {
	margin-bottom: 50px !important;
}

table tr td:first-child, table tr th:first-child{
	width: 70%;
	padding: 15px 25px;
}



table tr td:last-child, table tr th:last-child{
	width: 15%;
}

.glyphicon {
	width: 40px;
	text-align:right;
	padding: 0px 6px;
}

i {
	margin: 0 10px 0 0;
}

.step {
	text-align: center;
	padding: 20px;
	color: #e6e6e6;
}

.black {
	color: #000;
}

.step i {
	
	font-size: 35px;
	display: block;
}


CSS;

$this->registerCSS($css);

function fa($icon)
{
    return "<i class=\"fa $icon\"></i>";
}

?>


<div class="rating-configs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
        <?= Html::a('Back', ['index', 'projectID' => $model->projectID], ['class' => 'btn btn-default',]) ?>
	
		<?= Html::a('Update',['update','id'=>$model->id],
                             ['class'=>'btn btn-default']
                        )?>
	
	
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

	

	
<section>	
<?php 
$css = <<<CSS
.step {
	text-align: center;
	padding: 20px;
	color: #e6e6e6;
}

.black {
	color: #000;
}

.step i {
	
	font-size: 35px;
	display: block;
}
CSS;

$this->registerCSS($css);	
?> 

<div class="panel panel-default">

	<div class="panel-body">
	<div class="row">
		<div class="col-md-4 black step">
			<i class='fa fa-cogs'></i>
			1. Test Items
		</div>
		<div class="col-md-4 step">
			<i class='fa fa-question-circle-o'></i>
			2. Questions
			
			
		</div>
		<div class="col-md-4">
		
		</div>
	</div>

	</div>
</div>

</section>

	
<table class='table table-bordered'>

	<tr class='top-row'>
		<th>Test Structure</th>
		<th></th>
		<th></th>
	</tr>

	<tr>
		<th>Items</th>
		<td><?= count($model->items) ? '<span class="label label-success">'.count($model->items).'</span>' : '<span class="label label-danger">Fail</span>' ?></td>
		<td><?php echo Html::a('Manage', ['items','rcID'=>$model->id], ['class'=>'btn btn-sm btn-default']) ?></td>
	</tr>

	
</table>	


<table class='table table-bordered'>
	<tr class='top-row'>
		<th>Rating Structure</th>
		<th>Pass?</th>
		<th>Options</th>
	</tr>

	
	
	<tr>
		<th>Units</th>
		<td><?= count($model->units) ? '<span class="label label-success">'.count($model->units).'</span>' : '<span class="label label-danger">Fail</span>' ?></td>
		<td><?php echo Html::a(
                            'Manage',
                            ['units','rcID'=>$model->id],
                                ['class'=>'btn btn-sm btn-default']
                        ) ?></td>
	</tr>	


	<tr>
		<th>
			<span class="glyphicon glyphicon-arrow-right"></span> 
			Assign Test Items to Unit
		</th>
		<td><?= $model->isAllUnitsHasItems() ? '<span class="label label-success">Pass</span>' : '<span class="label label-danger">Fail</span>' ?></td>
		<td><?php echo Html::a(
                                    'Manage',
                                    ['assign-item-to-unit','rcID'=>$model->id],
                                ['class'=>'btn btn-sm btn-default']
                                ) ?></td>
	</tr>	
	
	<tr>
		<th>Rating Assignment Blocks</th>
		<td><?= count($model->blocks) ? '<span class="label label-success">'.count($model->blocks).'</span>' : '<span class="label label-danger">Fail</span>' ?></td>
		<td><?php echo Html::a(
                                    'Manage',
                                    ['blocks','rcID'=>$model->id],
                                ['class'=>'btn btn-sm btn-default']
                                ) ?></td>
	</tr>
	
	<tr>
		<th>
			<span class="glyphicon glyphicon-arrow-right"></span> 
			Assign Units To Block
		</th>
		<td><?= $model->isAllBlocksHasUnit() ? '<span class="label label-success">Pass</span>' : '<span class="label label-danger">Fail</span>' ?></td>
		<td>
			<?php echo Html::a(
                                    'Manage',
                                    ['assign-unit-to-block','rcID'=>$model->id],
                                ['class'=>'btn btn-sm btn-default']
                                ) ?>	
		
		</td>
	</tr>
	</table>
	
	<div class="" style='text-align:right;padding: 15px;'>
			<?php echo Html::a('Lock Structure and Proceed to Questions ' . \app\components\Icon::fa('arrow-right'),
                                    ['questions/','rcID'=>$model->id],
									['class'=>'btn btn-primary']
                               ) ?>

	</div>

</div>

