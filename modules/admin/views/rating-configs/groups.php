<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Raters;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = "Manage Groups";

$rcModel = \app\models\RatingConfigs::findOne($rcID);

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;


?>

<h1><?= $this->title ?></h1>
<p>
    <?= Html::a('Back', ['view','id'=>$rcID], ['class'=>'btn btn-default']) ?>
</p>

<div class="row">

<div class="col-md-6">

<div class="panel panel-default">

<div class="panel-heading">Groups</div>

<div class="panel-body">
<?php Pjax::begin(); ?>

            
   <?=Html::beginForm('groups', 'get', ["id"=>"search"]); ?>
        <?=Html::input("hidden", "rcID", $rcID);?>

        <?=Html::input("text", "query", Yii::$app->request->getQueryParam('query', ''), ["id"=>"group-name", "placeholder"=> "Search"]);?>

        <?= Html::submitButton('search', ['class'=>'btn btn-primary']) ?>
    <?=Html::endForm(); ?>
        
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'emptyText' => "All groups assigned.",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        "attribute"=> "code",
                        "label" => "Code"
                    ],
                    [
                        "attribute"=> "name",
                        "label" => "Name"
                    ],
                    [
                        "label" => "Members",
                        "value" => function ($group) {
                            return Raters::find()->where(["group"=>$group["code"]])->count();
                        }
                    ],
                    [
                        'label'=>'',
                        'value'=>function ($group) use ($rcID) {
                            $s = Html::a(
                                '<span class="glyphicon glyphicon-arrow-right"></span>',
                                ['groups', 'action'=>'assign',
                                           'rcID'=>$rcID,
                                           'code'=>$group['code']],
                                ['class'=>'btn btn-xs btn-primary']
                            );
                            return $s;
                        },
                        'format'=>'html'
                    ]
                ],
            ]);

            
            ?>

<?php Pjax::end(); ?>
        </div>
    </div>
</div>
<?php

$js = <<<JS

$('#group-name').change(function(e) {
        $('#search').submit();
})
JS;
$this->registerJS($js);
?>

<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Groups in Test Set (<?=count($assignedGroups)?>)
        </div>
        <div class="panel-body">

            <table class='table table-bordered'>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Members</th>
                    <th></th>
                </tr>
                <?php

                if (isset($assignedGroups)) {
                    ?>
                    <?php foreach ($assignedGroups as $key => $item) {
                        $groups = Raters::getGroups(); ?>
                        <tr class='unit-row'>
                            <td> <?= $item ?></td>
                            <td>
                                <?=$groups[$item]["name"]; ?>
                            </td>
                            <td>
                                <?=Raters::find()->where(["group"=>$item])->count()?>
                            </td>
                            <td>
                                <?= Html::a(
                            '<i class="fa fa-trash" style="color:#ea0500"></i>',
                                                [
                                                    '/admin/rating-configs/groups',
                                                    'rcID'=>$rcID,
                                                    'action'=>'delete',
                                                    'code'=>$item
                                                ],
                                            ['class' => 'profile-link']
                    ) ?>
                            </td>
                        </tr>
                    <?php
                    } ?>
                <?php
                } ?>
            </table>        
        
        
        </div>
    </div>
</div>

</div>

<div class="div-space"></div>

<?php


$js =  <<<JS
$("#ratersinput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".table tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
JS;

$this->registerJS($js);
?>


