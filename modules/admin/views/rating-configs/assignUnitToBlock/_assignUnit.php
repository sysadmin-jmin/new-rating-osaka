<?php
/* @var $items array*/
/* @var $unitKey integer */
/* @var $unit array*/

use yii\helpers\Html;

?>

<div class="panel panel-primary">

    <div class="panel-heading">
    Assign Unit to Block

    </div>
    <div class="panel-body">
        <?= Html::beginForm(['assign-unit-to-block', "rcID"=>Yii::$app->request->get("rcID")]) ?>
            <?=Html::hiddenInput("blockKey", $blockKey)?>
            <?=Html::hiddenInput("action", "assignUnit")?>
            <div class="form-group">
                <label>Block Name</label>
                <div class="form-control-static">
                    <?=$block["name"]?>
                </div>
            </div>
            
            <div class="form-group">
                <label>Unit</label>
                <div class="form-control-static">
                    <?php
                        $unitA=[];
                        foreach ($units as $key=>$unit) {
                            if (!in_array($key, $assignedUnits)) {
                                $unitA[$key] = $unit["name"];
                            }
                        }
                        echo Html::dropDownList('unitKey', "", $unitA, ['class'=>'form-control', 'prompt' => 'Select Unit', ]);
                    ?>
                </div>
            </div>
            <p style='margin: 10px 0'>
               <button class='btn btn-primary' type="submit" id="submit">Save</button>
            </p>


        <?= Html::endForm() ?>

    </div>

</div>