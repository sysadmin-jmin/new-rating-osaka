<?php
/* @var $blockID integer */
/* @var $rcModel */

use yii\helpers\Html;
use app\components\Icon;

?>

<div class="block-div">
    <span class='block-name'>
         <?= Icon::Fa('block') ?>
		 Block: <?= $block['name'] ?> 
    </span>
	<small>(<?=count($block["units"])?> Units)</small>
	<div class="group-item-div">
            <ul>
                <?php
                    if (isset($block["units"])) {
                        foreach ($block["units"] as $unitKey) {
                            $blockID = $block["id"];
                            echo "<li>". Icon::Fa('unit'); 
							echo " Unit: ".$units[$unitKey]["name"];
							echo " ";
                            echo Html::a(
                                        '<i class="fa fa-minus-circle"></i>',
                                        [
                                            'assign-unit-to-block',
                                            "rcID"=>$rcModel->id
                                        ],
                                        [
                                            "data-confirm"=>"Are you sure want to remove this unit from block?",
                                            "data-method"=>"post",
                                            "data-params"=>[
                                                'action'=>'unassignUnit',
                                                'blockKey'=>$blockKey,
                                                "unitKey"=>$unitKey
                                            ]
                                        ]
                                )."</li>";
                        }
                    }
                ?>                
            </ul>
            
            <div class="" style='padding-left:25px'>
                <?=Html::a(
                    '<i class="fa fa-plus"></i> Assign Unit',
                    ['assign-unit-to-block', "rcID"=>$rcModel->id, "blockKey"=>$blockKey, "forms"=>"assignUnit"],
                    ['class'=>'btn btn-default btn-xs']
                ); ?>
        
            </div>  
        </div>   <!-- GROUP DIV -->
    
    
</div>  