<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RatingConfigs */

$this->title = 'Create Test Set';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $model->projectID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-configs-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<p>
    <?= Html::a('Back', 'index?projectID=' . $model->projectID, ['class' => 'btn btn-default', 'onclick' => 'history:back()']) ?>
	</p>
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
