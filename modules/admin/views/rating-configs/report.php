<?php

use yii\helpers\Html;


use app\models\RatingConfigForms\Report;
use app\models\Answers;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $Report Report */
/* @var $rcID int */
/* @var $ratingStatus array */

$this->title = 'Report';

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $Report->getProjects()->id]];
$this->params['breadcrumbs'][] = ['label' => $Report->getRatingConfig()->test_name, 'url' => ['rating-configs/view', 'id' => $rcID]];

$this->params['breadcrumbs'][] = $this->title;


$testsTotal = count($Report->getAnswers());
$divisionNum = $testsTotal < 1 ? 1 : $testsTotal;

?>
<div class="rating-configs-report">

    <h1>Report</h1>

    <div class="row">

        <div class="col-md-7">

            <h4>Rating Performance</h4>


            <table class='table table-bordered table-striped'>
                <tr>
                    <th>Item</th>
                    <th>Status</th>
                    <th>Number</th>
                    <th>% Completed</th>
                </tr>
                <tr>
                    <td rowspan='5'>Answer Sheets</td>
                    <td>In Queue</td>
                    <td align="right"><?php echo $ratingStatus[Answers::SHEETS_STATUS_OPEN] ?></td>
                    <td align="right"><?php echo round(($ratingStatus[Answers::SHEETS_STATUS_OPEN] / $divisionNum) * 100, 1) ?>%
                    </td>
                </tr>
                <tr>

                    <td>Rated</td>
                    <td align="right"><?php echo $ratingStatus[Answers::SHEETS_STATUS_RATED] ?></td>
                    <td align="right"><?php echo round(($ratingStatus[Answers::SHEETS_STATUS_RATED] / $divisionNum) * 100, 1) ?>%
                    </td>
                </tr>
                <tr>
                    <td>In Progress</td>
                    <td align="right"><?php echo $ratingStatus[Answers::SHEETS_STATUS_INPROCESS] ?></td>
                    <td align="right"><?php echo round(($ratingStatus[Answers::SHEETS_STATUS_INPROCESS] / $divisionNum) * 100, 1) ?>%
                    </td>
                </tr>
                <tr>
                    <td>Pending</td>
                    <td align="right"><?php echo $ratingStatus[Answers::SHEETS_STATUS_PENDING] ?></td>
                    <td align="right"><?php echo round(($ratingStatus[Answers::SHEETS_STATUS_PENDING] / $divisionNum) * 100, 1) ?>%
                    </td>
                </tr>
                <tr>
                    <th>Total</th>
                    <td align="right"><?php echo $testsTotal ?></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>

                <tr>
                    <td rowspan='6'>Project</td>
                    <td>Start Date</td>
                    <td><?php echo @$Report->getProjects()->starttime ?? '<span class="font-italic text-muted">Undefined</span>' ?></td>
                    <td></td>
                </tr>
                <tr>

                    <td>End Date</td>
                    <td><?php echo @$Report->getProjects()->endtime ?? '<span class="font-italic text-muted">Undefined</span>' ?></td>
                    <td>55%</td>
                </tr>
                <tr>
                    <td>Duration</td>
                    <td>
                        <?php if ($Report->getProjects()->getDateDuration() > 0): ?>
                            <?php echo $Report->getProjects()->getDateDuration(); ?> days
                        <?php else: ?>
                            End date not set , or invalid date
                        <?php endif; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Elapsed days</td>
                    <td>
                        <?php if (!empty($Report->getProjects()->starttime) && $Report->getProjects()->getElapsedDaysSinceStart() > 0): ?>
                            <?php echo $Report->getProjects()->getElapsedDaysSinceStart() ?> days
                        <?php else: ?>
                            <?php echo '<span class="font-italic text-muted">Not yet start</span>'; ?>
                        <?php endif; ?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Average Speed Per Day</td>
                    <td>{rated}/ {elapsed days}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Projected Completion</td>
                    <td>{In queue / average speed} days
                        days_add to (today)
                    </td>
                    <td></td>
                </tr>

            </table>

        </div>

        <div class="col-md-5">


            <h4>Project Performance</h4>


            <div class="panel panel-default">

                <div class="panel-heading">
                    Data
                </div>
                <div class="panel-body">
                    <table class='table'>
                        <thead>
                        <tr>
                            <th></th>
                            <th>Item</th>
                            <th>Statistics</th>
                        </tr>
                        </thead>
                        <tr>
                            <td></td>
                            <td>Project Start</td>
                            <td><?php echo $Report->getProjects()->starttime ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Time Elapsed</td>
                            <td>4 days 10 hours</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Total Units Rated</td>
                            <td>1524</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Average Time Per Unit</td>
                            <td>18m</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Projected Time to finish</td>
                            <td><span id="time-to-finish">240hours</span></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>Raters in Project</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Average Unit Per Rater</td>
                            <td></td>
                        </tr>
                    </table>
                </div>


            </div>


        </div>


    </div>

</div>
