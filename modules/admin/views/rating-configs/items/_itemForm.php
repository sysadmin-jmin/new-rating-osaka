<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="panel panel-primary">

    <div class="panel-heading"><?=$model->key!="" ? "Edit" : "Add" ?> Question Item</div>
    
    <div class="panel-body">
    
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= Html::hiddenInput('action', 'save'); ?>
			<?= $form->field($model,'key')->textInput(['readonly'=>'readonly'])
										  ->label('key for Development mode')
			?>
			
            <?= $form->field($model, 'uid') ?>

            <div id="qn-div">       
                <?= $form->field($model, 'question')->input('text') ?>
            </div>

            <?= $form->field($model, 'qnType')->dropDownList($model->qnTypeA()) ?>

            
            <?= $form->field($model, 'ansType')->dropDownList($model->ansTypeA()) ?>
            
            <?= $form->field($model, 'rcID')->hiddenInput(['value'=>$model->rcID])->label(false) ?>

        <?= Html::submitButton('Save', ['id'=>'add-items-button','class'=>'btn btn-primary']) ?>

        <?php ActiveForm::end() ?>
    

    </div>

</div>