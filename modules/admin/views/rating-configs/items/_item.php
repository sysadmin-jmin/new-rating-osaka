<?php
use yii\helpers\Html;
use app\components\Icon;

?>
<div class="item-div">
    
    <div class="item-attributes">
        <ul>
            <li class="attr">
	        <?=
            Html::a(
                'Item:'.($itemKey+1),
                [
                    "",
                    "action" => "update",
                    "rcID" => $rcID,
					"itemKey" => $itemKey
				]
            );
			?>		

			<?= 
			Html::a(Icon::Fa('delete'),
							['', 'rcID'=>$rcID, "update"=>"item"],
							["data-confirm"=>"Are you sure want to delete this item?", 
							 "data-method"=>"post", 
							 "data-params"=>[
											 'action'=>'delete', 
											 'itemKey'=>$itemKey]
							]);
			?>
		
		
			<ul class='item-info'>
				<li>Name: <?= $item["uid"] ?> </li>
				<li>Question: <?=icon($item["qnType"])?> </li>
				<li>Answer <?=icon($item["ansType"])?> </li>
			</ul>
			</li>
        </ul>
    </div>
</div>