<?php
/* @var $blockID integer */

use yii\helpers\Html;

?>
<div class="panel panel-primary">

	<div class="panel-heading">
	Add Block

	</div>
    <div class="panel-body">
        <?= Html::beginForm() ?>
            <div class="form-group">
                <label for="name">Block Name</label>
                <?=Html::textInput("name", $latestIndex, ["class"=>"form-control"])?>
            </div>

            <button class='btn btn-primary' type="submit" id="submit">Save</button>

            <?=Html::hiddenInput("action", "addBlock")?>
            <?=Html::hiddenInput("blockKey", $blockKey)?>
        <?=Html::endForm()?>

    </div>

</div>
