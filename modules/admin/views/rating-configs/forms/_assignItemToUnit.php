<?php
/* @var $items array*/
/* @var $unitKey integer */
/* @var $unit array*/

use yii\helpers\Html;

?>

<div class="panel panel-primary">

    <div class="panel-heading">
    Assign Item to Unit

    </div>
    <div class="panel-body">
        <?= Html::beginForm(['assign-item-to-unit', "rcID"=>Yii::$app->request->get("rcID")]) ?>
            <?=Html::hiddenInput("unitKey", $unitKey)?>
            <?=Html::hiddenInput("action", "assignItem")?>
            <div class="form-group">
                <label>Unit</label>
                <div class="form-control-static">
                    <?=$unit["name"]?>
                </div>
            </div>
            
            <div class="form-group">
                <label>Item</label>
                <div class="form-control-static">
                    <?php
                        $itemA=[];
                        foreach ($items as $key=>$item) {
                            if (!in_array($key, $unit["items"])) {
                                $itemA[$key] = $item["uid"];
                            }
                        }
                        echo Html::dropDownList('itemKey', "", $itemA, ['class'=>'form-control', 'prompt' => 'Select Item', ]);
                    ?>
                </div>
            </div>
            <p style='margin: 10px 0'>
               <button class='btn btn-primary' type="submit" id="submit">Save</button>
            </p>


        <?= Html::endForm() ?>

    </div>

</div>