<?php
/* @var $blockID integer */
/* @var $block */
/* @var $groups*/

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>


<div class="panel panel-primary">

	<div class="panel-heading">
	Assign Group to Block

	</div>
    <div class="panel-body">
        <?= Html::beginForm() ?>
            <?=Html::hiddenInput("blockID", $block["id"])?>
            <?=Html::hiddenInput("action", "assignGroup")?>
            <div class="form-group">
                <label>Block Name</label>
                <div class="form-control-static">
                    <?=$block["name"]?>
                </div>
    		</div>
    		
            <?= Html::dropDownList('gID', '', $groups, ['class'=>'form-control', 'prompt'=>'Select Group']); ?>
            <p style='margin: 10px 0'>
    	       <button class='btn btn-primary' type="submit" id="submit">Assign </button>
            </p>


        <?= Html::endForm() ?>

    </div>

</div>
