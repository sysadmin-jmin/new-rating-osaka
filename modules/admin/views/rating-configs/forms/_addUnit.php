<?php
/* @var $blockID integer */

use yii\helpers\Html;

?>
<div class="panel panel-primary">

	<div class="panel-heading">
    	Add Unit
	</div>
    <div class="panel-body">
        <?= Html::beginForm() ?>
            <div class="form-group">
                <label for="name">Unit Name</label>
                <?=Html::textInput("name", null, ["class"=>"form-control"])?>
            </div>

            <button class='btn btn-primary' type="submit" id="submit">Save</button>

            <?=Html::hiddenInput("action", "addUnit")?>
            <?=Html::hiddenInput("key", $key)?>
        <?=Html::endForm()?>

    </div>

</div>
