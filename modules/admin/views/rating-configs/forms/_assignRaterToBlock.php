<?php
/* @var $block array*/
/* @var $raterID integer */
/* @var $group array*/
/* @var $rcModel */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Raters;
use app\models\RatingConfigForms\Rater;

$rater = Raters::findOne($raterID);
?>


<div class="panel panel-primary">

	<div class="panel-heading">
	Assign Rater to Block

	</div>
    <div class="panel-body">
        <?=
            Html::beginForm(
                [
                    'block-raters',
                    "rcID"=>$rcModel->id,
                    "blockKey"=>$blockKey,
                    "groupID"=>$group["code"]
                ],
                "post",
                [
                    "class"=>"form-horizontal"
                ]
            );
        ?>
            <?=Html::hiddenInput("rid", $raterID)?>
            <?=Html::hiddenInput("action", "assignRater")?>
            <div class="form-group">
                <label class="col-sm-4">Block Name</label>
                <div class="col-sm-8">
                    <?=$block["name"]?>
                </div>
    		</div>

            <div class="form-group">
                <label class="col-sm-4">Group Name</label>
                <div class="col-sm-8">
                    <?=$group["name"]?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4">Rater Name</label>
                <div class="col-sm-8">
                    <?=$rater->name?>
                </div>
            </div>
    		
            <div class="form-group">
                <label class="col-sm-4">Status</label>
                <div class="col-sm-8">
                    <?=
                        Html::dropDownList(
                            'status',
                            $rcModel->getRaterStatus($blockKey, $raterID),
                            Rater::raterStatus(),
                            [
                                'class'=>'form-control'
                            ]
                        );
                    ?>
                </div>
            </div>

            <div class="checkbox">
                <div class="col-sm-4"></div>
                <label class="col-sm-8" style="padding-left:30px">
                <?=
                    Html::checkbox(
                        'isLeader',
                        $rcModel->isLeaderRater($blockKey, $raterID)
                    );
                ?> Leader
                </label>
            </div>

            <p style='margin: 10px 0'>
                <?=Html::submitButton("Save", ["class"=>"btn btn-primary"]);?>
            </p>


        <?= Html::endForm() ?>

    </div>

</div>
