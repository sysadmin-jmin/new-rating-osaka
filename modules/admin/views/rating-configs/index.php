<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectID int */

$this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['projects/']];
$this->title = Yii::$app->session->get('projectName') . ' - Test Sets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-configs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

		<?= Html::a('Back',['projects/'],['class'=>'btn btn-default']) ?>
		<?= Html::a('Create Test Set',['rating-configs/create',
										'projectID'=>Yii::$app->request->get('projectID')]
									 ,['class'=>'btn btn-success'])
		?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            ['attribute'=>'test_name',
			 'value'=> function ($model) {
						return Html::a($model->test_name,['view','id'=>$model->id]);
						},
			 'format'=>'html'
			],
			
            'date_updated:date',
			'Status',
	
			['label'=>'',
			'value'=>function($model) {
						$s = '';
						#$s .= Html::a('Manage',['view','id'=>$model->id],
						#						['class'=>'btn btn-xs btn-default']);
						#$s .= " ";
						$fa_report = "<i class='fa fa-line-chart'></i> ";
						$s .= Html::a($fa_report .'Report',['report','rcID'=>$model->id],
												['class'=>'btn btn-xs btn-warning']);
				return $s;
			},
			'format'=>'html'
			]
        ],
    ]); ?>
</div>
