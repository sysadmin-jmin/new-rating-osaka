<?php 
use app\components\Icon;
use app\models\Raters;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = "Block Settings #{$model->blockKey}";

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$rcModel->id]];
$this->params['breadcrumbs'][] = ['label'=> 'Rating Blocks', 'url'=>['rating-configs/blocks','rcID'=>$rcModel->id, "groups"=>1]];
$this->params['breadcrumbs'][] = $this->title;

function showAssignedGroups($rcGroups, $blockKey, $rcModel)
{
    $htmlResult = "";
    if (isset($rcGroups[$blockKey]) &&
        is_array($rcGroups[$blockKey])) {
        $number = 1;
        $groupsA = Raters::getGroups();
        foreach ($rcGroups[$blockKey] as $rcGroup) {
            $gid = $rcGroup["gID"];
            $totalRater = Raters::assignedRaterCount($rcGroup["gID"]);
            $qualifiedRater = $rcModel->getGroupRatersStatusCount($blockKey, $rcGroup["gID"], "ok");
            $unQualifiedRater = $totalRater - $qualifiedRater;
            $htmlResult .=" <tr>
                                <td>".$number."</td>
                                <td>".$groupsA[$gid]["name"]."</td>
                                <td>".$qualifiedRater."</td>
                                <td>".$unQualifiedRater."</td>
                                <td>".
                                    Icon::fa(
                                        'edit',
                                        [
                                            'block-raters',
                                            'rcID'=>$rcModel->id,
                                            'blockKey'=>$blockKey,
                                            'groupID'=>$gid
                                        ],
                                        [
                                            "data-pjax"=>0
                                        ]
                                    )." ".
                                    Icon::fa(
                                        'delete',
                                        [
                                            'block-settings',
                                            'rcID'=>$rcModel->id,
                                            'blockKey'=>$blockKey
                                        ],
                                        [
                                            "data-confirm"=>"Are you sure want to remove this group from block?",
                                            "data-method"=>"post",
                                            "data-params"=>[
                                                'action'=>'unassignGroup',
                                                'blockKey'=>$blockKey,
                                                "gID"=>$gid
                                            ]
                                        ]
                                    )."</td>
                            </tr>";
            $number++;
        }
    }

    return $htmlResult;
}

function showUnAssignedGroups($blockKey, $rcModel)
{
    $htmlResult = "";
    $assignedGroup = ArrayHelper::getColumn($rcModel->getGroups($blockKey), "gID");
    $groupsA = Raters::getGroups();
    $groupMap = [];
    $number = 1;
    foreach ($groupsA as $group) {
        if (!in_array($group["code"], $assignedGroup)) {
            $htmlResult .= "<tr>
                                <td>".$number."</td>
                                <td>".$group["name"]."</td>
                                <td>".
                                    Icon::Fa(
                                        'add',
                                        [
                                            "block-settings",
                                            "rcID" => $rcModel->id,
                                            "blockKey" => $blockKey
                                        ],
                                        [
                                            "data-method"=> "post",
                                            "data-params"=>[
                                                "action" => "assignGroup",
                                                "blockKey" => $blockKey,
                                                "gID" => $group["code"]
                                            ]
                                        ]
                                    ).
                                "</td>
                            </tr>";
            $number++;
        }
    }
    return $htmlResult;
}

$css = <<<CSS
.div-section{
    padding-left:22px;
}
CSS;

$this->registerCSS($css);

$blockKey = Yii::$app->request->get('blockKey');
$rcGroups = $rcModel->getGroups();
$assignedGroupsCount = isset($rcGroups[$blockKey]) ? count($rcGroups[$blockKey]) : 0;

?>
 
 
<div class="block-settings">

    <h1><?= $this->title ?></h1>
    <p>
        <?=
            Html::a(
                'Back',
                [
                    'blocks',
                    'rcID'=>$rcModel->id,
                    "groups"=>1
                ],
                [
                    'class'=>'btn btn-default'
                ]
            )
        ?>
    </p>
	
	
	<section>
	
	<h3> General RAB Settings </h3>
    <div class="row">
        <div class="col-md-8">

                <?php 
                    $form = ActiveForm::begin(
                                [
                                    "options"=> [
                                        "class"=>"form-horizontal",
                                    ],
                                    "fieldConfig"=>[
                                        "template"=> "{label}\n<div class=\"col-sm-3\">{input}</div>\n{hint}\n{error}",
                                        "labelOptions"=> [
                                            "class"=>"col-sm-3"
                                        ],
                                        "inputOptions"=>[
                                            "class"=>"form-control",
                                            "size" => 5
                                        ]
                                    ]
                                ]
                            );
                ?>
                    <?=Html::hiddenInput("action", "save")?>

                    <h4><?=Icon::fa("cog")?> Training</h4>

                    <div class="div-section">

                        <?= $form->field($model, 'hasTraining')->radioList(
                            ["1"=>'Yes', "0"=>'No'],
                            [
                                "itemOptions"=> [
                                    "labelOptions" => [
                                        "class" => "col-sm-6",
                                    ]
                                ]
                            ]
                        ) ?>

                        <?= $form->field($model, 'fEveryday')->textInput() ?>

                        <?= $form->field($model, 'afterBlocks')->textInput() ?>

                        <?= $form->field($model, 'dayLimit')->textInput() ?>

                        <?= $form->field($model, 'afterDays')->textInput() ?>

                    </div>

                    <h4><?=Icon::fa("cog")?> Quality Control</h4>
                    <div class="div-section">

                        <?= $form->field($model, 'qcLimit')->textInput() ?>
                        
                    </div>

                    <div class="div-section">
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <?= Html::submitButton("Save", ["class"=>"btn btn-primary"])?>
                            </div>
                        </div>
                    </div>

            	<?php ActiveForm::end() ?>

        </div>


        <div class="col-md-4">

        </div>

    </div>

	</section>
	
	<section>
		<h3>Add Group To Block</h3>
		
		<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Groups in Block (<?=$assignedGroupsCount?>)
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th>ID</th>
							<th>Group Name</th>
                            <th>Qualified</th>
                            <th>Unqualified</th>
							<th>Options</th>
						</tr>
                        <?=showAssignedGroups($rcGroups, $blockKey, $rcModel)?>
					</table>
				</div>
			</div>
		</div>
		

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Available Group in Project
				</div>
						<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th>ID</th>
							<th>Group Name</th>
							<th>Options</th>
						</tr>
                        <?=showUnAssignedGroups($blockKey, $rcModel)?>
					</table>
				</div>
			
			</div>
		</div>
		
		</div>
	</section>	
	
	
	
</div> 