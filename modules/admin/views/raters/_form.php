<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Raters */
/* @var $form yii\widgets\ActiveForm */

$logedInUser = Yii::$app->user->getIdentity();
?>

<div class="raters-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if ($logedInUser->role=="Admin"): ?>
        <?= $form->field($model, 'company')->dropDownList($model::getCompanies(), ["prompt"=>"=== Select Company ==="]) ?>
    <?php elseif ($logedInUser->role=="Manager"): ?>
        <?= $form->field($model, 'company')->hiddenInput(["value"=>$logedInUser->company])->label(false) ?>
    <?php endif; ?>        
    <?= $form->field($model, 'role')->dropDownList($model->getRole(), ["prompt"=>"=== Select Role ==="]) ?>

<?php if ($model->status == '') {
    $model->status = 0;
} ?>
	
    <?= $form->field($model, 'status')->radioList([0=>"Suspended", 1=>"Live"]) ?>

    <?= $form->field($model, 'password_hash')->passwordInput(["value"=>""]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
