<?php

use yii\helpers\{Html, ArrayHelper};
use yii\widgets\DetailView;
use app\components\Icon;
use app\models\ProjectForms\Brand;

/* @var $this yii\web\View */
/* @var $model app\models\Raters */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Raters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


#$model->addCertBrands("HARV");

?>
<div class="raters-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
    </p>
	
	
	
	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'email:email',
            [
                'attribute' => 'role',
                'value' => function($model) {
                    return $model->getRole($model->role);
                }
            ],
            [ 
                "attribute" => "status",
                "value" => function ($model) {
                    return $model->getStatus();
                }
            ],
            'date_created',
            'date_updated',
        ],
    ]) ?>

	<section>
	
		<div class="row">
		
			<div class="col-md-6">
			<h4>Brands Qualified</h4>
			<table class="table table-bordered">
				<tr>
					<th>Brand</th>
					<th>Status</th>
					<th>Options</th>
				</tr>
				<?php foreach($model->getCertBrands() as $key=>$status): ?>
					<tr>
						<td><?= $key ?></td>
						<td><?= $status ?></td>
						<td><?= Html::a(Icon::Fa('delete'), ['del-cert-brand'],['data'=> [
																'method'=>'post',
																'params'=>[
																	'id'=>$model->id,
																	'brandKey'=>$key
																	]
																]]);
																	
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
			</div>
			<div class="col-md-6">
			
			<div class="panel panel-default">
				<div class="panel-heading">Add A Certified Brand</div>
				<div class="panel-body">
					<?php echo Html::beginForm('add-cert-brand') ?>
					
					<?= Html::hiddenInput('id',$model->id); ?>

					<?php echo  Html::dropDownList('brandKey','',$model->getAvaBrandsA()) ?>	

					<?= Html::submitButton('add',['class'=>'btn btn-primary']) ?>
				</div>
			
			</div>
			

			
			</div>
		</div>
	
	
	</section>
	
	
	
	
</div>
