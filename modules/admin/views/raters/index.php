<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Raters;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Raters';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
i {
	margin-right: 5px;
}
</style>

<div class="raters-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rater', ['create'], ['class' => 'btn btn-success']) ?>

		<?= Html::a("<i class='fa fa-address-book-o'></i>  Assign Raters", ['assign-to-group'], ['class' => 'btn btn-default']) ?>
  
		<?= Html::a('<i class="fa fa-users"></i> Manage Groups', ['groups'], ['class' => 'btn btn-default']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            // 'email:email',
            [
                'attribute' => 'company',
                'value' => function ($model) {
                    return $model->getCompany();
                },
                'filter' => Raters::getCompanies()
            ],
            [
                'attribute' => 'group',
                'value' => function ($model) {
                    return $model->getGroup()["name"];
                },
                'filter' => ArrayHelper::map(Raters::getGroups(), "code", "name")
            ],
            [
                'attribute' => 'role',
                'value' => function ($model) {
                    return $model->getRole($model->role);
                },
                'filter' => $searchModel->getRole()
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatus();
                },
                'filter' => $searchModel->statusA()
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
