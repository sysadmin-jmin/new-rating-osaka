<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rater Groups';
$this->params['breadcrumbs'][] = ['label' => 'Raters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raters-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>
		
	 </p>

	 
	 <div class="row">
	 
	 <div class="col-md-8">
	 
	 <div class="panel panel-default">
	 
	 <div class="panel-body">
	 
	 <table class='table table-bordered'>
	 <tr>
	 	<th>Group Code</th>
		<th>Group Name</th>
		<th></th>
	 </tr>
 	<?php
        if (count($groups)):
            foreach ($groups as $group):
    ?>
			<tr>
			 	<td><?=$group["code"]?></td>
			 	<td><?=$group["name"]?></td>
				<td>
					<?= Html::a("<i class='fa fa-times'></i>", '', ['data-params'=>["action"=>"delete", "code"=>$group["code"]], "data-method"=>"post", "data-confirm"=>"Are you sure want to delete this group?"]) ?>
						
				</td>
			</tr>
	<?php
            endforeach;
        endif;
    ?>
	 
	 </table>
	 
	 
	 </div>
	 
	 
	 </div>
	 
	 
	 </div>
	 <div class="col-md-4">
	 
	 
	 <div class="panel panel-default">

		<div class="panel-body">
			<?= Html::beginForm('', 'post') ?>

			<label for="">Group Code</label>
			<?= Html::textInput('code', '', ['class'=>'form-control','placeholder'=>'JG']) ?>

			<label> Group Name </label>
			<?= Html::textInput('name', '', ['class'=>'form-control','placeholder'=>'Junior Groups']) ?>
			<?= Html::hiddenInput('action', 'add') ?>
			<p style='margin-top:20px'>
				<?= Html::submitButton('add', ['class'=>'btn btn-primary']) ?>
			</p>

			<?= Html::endForm() ?>
		</div>		 
	 
	 </div>
	 
 
	 
	 </div>
	 
	 
	 </div>

</div>
