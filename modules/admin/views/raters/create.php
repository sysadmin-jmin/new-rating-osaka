<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Raters */

$this->title = 'Create Raters';
$this->params['breadcrumbs'][] = ['label' => 'Raters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raters-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
