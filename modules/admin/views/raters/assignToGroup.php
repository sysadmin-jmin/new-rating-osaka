<?php
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assign Raters';
$this->params['breadcrumbs'][] = ['label' => 'Raters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raters-index">

    <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a('Back', ['index'], ['class' => 'btn btn-default']) ?>  
  </p>

  
  <div class="row">
  
<?php Pjax::begin(["id"=>"pjax-assign-to-group"]); ?>
<?=Html::beginForm(['assign-to-group'], 'get', ["id"=>"formAssignedRater"]);?>
  <div class="col-md-8">
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'emptyText' => "There is no unassigned rater.",
      'columns' => [
        ['class' => 'yii\grid\CheckboxColumn', "name"=>"raterID"],
        ['class' => 'yii\grid\SerialColumn'],

        // 'id',
        'name',
        'email:email',
        [
          'label'=>'',
          'value'=> function ($model) {
              $s = Html::a(
              '<span class="glyphicon glyphicon-arrow-right"></span>',
              [
                'assign-to-group',
                'action'=>'assign',
                'raterID'=>$model->id
              ],
              ['class'=>'btn btn-xs btn-primary assigned-to-group']
            );
              return $s;
          },
          'format'=>'html'
        ]
      ],
    ]); ?>
    <?=Html::input("hidden", "action", "assign");?>
    <?=Html::submitButton('<i class="glyphicon glyphicon-arrow-right"></i>', ['class' => 'btn btn-primary', "id"=>"btnAssign"]);?>
  </div>
  
  <div class="col-md-4">
      <label> Group </label>
      <?= Html::dropDownList('group', Yii::$app->request->get("group"), ArrayHelper::map($groups, "code", "name"), ['class'=>'form-control', 'prompt'=>'Select Group']); ?>
      <?php
        if (Yii::$app->request->get("group")):
        ?>
          <hr>
          <table class='table table-bordered'>
            <tr>
              <th>#</th>
              <th>Name</th>
              
              <th></th>
            </tr>
      <?php 
        if (isset($assignedRaters)):
          foreach ($assignedRaters as $key => $rater): ?>
            <tr class='unit-row'>
              <td><?= ($key+1) ?></td>
              <td><?= $rater->name ?></td>
              
              <td>
                      <?= Html::a(
            '<i class="fa fa-trash" style="color:#ea0500"></i>',
            ['assign-to-group', 'raterID'=>$rater->id, "group"=>Yii::$app->request->get("group"),'action'=>'delete' ],
            ['class' => 'profile-link']
        )
        ?>
                  </td>
            </tr>
      <?php 
          endforeach;
        endif;
      ?>

          </table>
    <?php
      endif;
    ?>
  </div>
<?= Html::endForm();?> 
<?php Pjax::end(); ?>
  
  
  </div>
  

</div>
<?php
$js =  <<<JS
  $("#pjax-assign-to-group")
  .on("click", "a.assigned-to-group", function(e){
    e.preventDefault();
    var group = $("select[name='group']").val();
    if(!group){
      alert("You have not select group to assign.");
    }else{
      var url = $(this).attr("href");
      url=url+"&group="+group;
      console.log(url);
      $.pjax({"url":url, "container":"#pjax-assign-to-group"});
    }
  })
  .on("click", "#btnAssign", function(e){
    e.preventDefault();
    var group = $("select[name='group']").val();
    if(!group){
      alert("You have not select group to assign.");
      $("input[name='raterID[]']").prop('checked', false);
      $("input[name='raterID_all']").prop('checked', false);
      return false;
    }
    var raterID = $("input[name='raterID[]']:checked");
    if(raterID.length==0){
      alert("You have not select any raters to assign.");
      return false;      
    }
    var params = $("#formAssignedRater").serialize();
    var url="assign-to-group?"+params;
    $.pjax({"url":url, "container":"#pjax-assign-to-group"});
  })
  .on("change", "select[name='group']", function(e){
    var group = $(this).val();
    var url="assign-to-group?group="+group;
    $.pjax({"url":url, "container":"#pjax-assign-to-group"});
  });
JS;
$this->registerJS($js);
?>