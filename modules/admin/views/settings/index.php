<?php 

use yii\helpers\Html;

$this->title = "Project Settings and Configurations";

$this->params['breadcrumbs'][] = $this->title;

?>
<style>
.col-md-6 {
	padding-bottom:40px;
}
h5 {
	margin-top: 50px;
	font-size: 120%;
}
</style>


<h1> <?= $this->title ?> </h1>
<div class="row">


<div class="col-md-6">

<h5> Global Settings </h5>


<ul>
	<li>
		<?= Html::a('Brands', ['projects/brand']) ?>		
	</li>
    <li>
        <?= Html::a('Companies', ['projects/company']) ?>     
    </li>
</ul>



<h5>Super Admin Rights</h5>
<ul>
	<li><?= Html::a('Clear All Question,model answer, model rating',['projects/clear-qar'])?></li>
	<li></li>
</ul>

</div>


<div class="col-md-6">

<h5>Examinees</h5>
<ul>
	<?= Html::a('Manage Examinees', ['examinees/'], ['class'=>'group-list-item']) ?>
</ul>


</div>



</div>
