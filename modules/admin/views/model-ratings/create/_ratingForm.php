<?php
use yii\helpers\Html;

?>

<div class="panel panel-primary">
	<div class="panel-heading">Rating <?=Html::a("Show all unit", "#", ["class"=>"btn btn-default btn-xs pull-right show-all-unit-context", "style"=>"display:none;"])?>
        
    </div>
	<div class="panel-body">
        <?=Html::beginForm()?>
            <?=Html::hiddenInput("action", "rating");?>
            <?=Html::hiddenInput("data[rcID]", $questionModel->rcID);?>
            <?=Html::hiddenInput("data[answerID]", Yii::$app->request->get("ansID"));?>
    		<?php
                foreach ($units as $unitKey=>$unit) {
                    $unitContext = $questionModel->getUnitContexts($unitKey);
                    echo '<div id="unit_'.$unitKey.'" class="unit-context">';
                    echo "<h4> Unit: {$unit["name"]} </h4>";
                    echo "<table class='table table-bordered'>";
                    echo "<tr>
    		                <th>Context</th>
    		                <th>1</th>
    		                <th>2</th>
    		                <th>3</th>
    		                <th>4</th>
    		                <th>5</th>
    		                <th>NR</th>
							
    		            </tr>";
                    echo showUnitContext($unit, $unitKey, $unitContext, $model);
                    echo "</table>";
                    echo "</div>";
                } #end unit
            ?>
    		<div class="" style='padding: 20px 0'>
                <?=Html::submitButton(($model ? "Update" : "Create")." Ratings", ['class'=>'btn btn-primary']);?>
			</div>
        <?=Html::endForm()?>
	</div>
</div>
