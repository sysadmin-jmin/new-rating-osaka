<?php
use yii\helpers\Html;
use app\components\Icon;

?>

<div class="block-div">					
	<h3><?=Icon::fa("cubes");?> Block: <?=$block["name"];?></h3>
	<ul class='unit'>	
		<?php
            foreach ($block["units"] as $unitKey) {
                $ratings = [];
                if ($model) {
                    $ratings = $model->getRatings($unitKey);
                }
                echo $this->render("_unit", ["unit"=>$units[$unitKey], "unitKey"=>$unitKey, "questionModel"=>$questionModel, "ratings"=>$ratings]);
            }
        ?>
	</ul>
</div>	
