<?php
use yii\helpers\Html;
use app\components\Icon;

$unitContext = $questionModel->getUnitContexts($unitKey);
?>
<li> <h4><?=Icon::fa("archive");?> Unit: <?=$unit["name"]?></h4> 
	<ul class='item'>
		<li>Context <?=Html::a("View", "#unit_$unitKey", ["class"=>"btn btn-default btn-xs toggle-unit-context"])?>
			<ul>
				<?php
                    foreach ($unitContext as $key=>$context) {
                        $rating = $ratings[$key] ?? '';
                        $rateValue = " - <small><em>(not rated)</em></small>";

                        if (isset($rating["rating"]) && $rating["rating"]!=="") {
                            $rateValue = " - <small><em>(".$rating["rating"].")</em></small>";
                        }
                        echo "<li>".$context["name"].$rateValue."</li>";
                    }
                ?>
			</ul>
		</li>
	</ul>	
</li>
