<?php
use yii\helpers\Html;
use app\components\Icon;

$this->title = "Model Ratings";


// Refactor now week after 25
function showUnitContext($unit, $unitKey, $unitContexts, $rBTModel)
{
    $str = "";

    $ratingUnits = [];
    if ($rBTModel) {
        $ratingUnits = $rBTModel->getRatings($unitKey);
    }

    if (is_array($unitContexts)) {
        foreach ($unitContexts as $contextKey=>$context) {
            $rateValue = isset($ratingUnits[$contextKey]) ? $ratingUnits[$contextKey]["rating"] : "";
            $str.= "<tr> 
						<td>
							{$context['name']}
							".Html::hiddenInput("data[rating][$unitKey][$contextKey]", "")."
						</td>";

            for ($x=1; $x<=5 ;$x++) {
                $str.=  "<td>".
                            Html::radio(
                                "data[rating][$unitKey][$contextKey]",
                                ($rateValue==$x ? true :false),
                                [
                                    "value" => $x
                                ]
                            ).
                        "</td>";
            }

            $str.= 	"<td>".
                        Html::radio("data[rating][$unitKey][$contextKey]",
                            ($rateValue=="0" ? true :false),
                            [
								"value" => 0
                            ]
                        ).
                    "</td>";
			$str.= "<td> <textarea class='form-control'> Remarks </textarea></td>";		
			$str.= "</tr>";
        }
    }

    return $str;
}

$css = <<<CSS
.item > li {
	padding-bottom: 15px;
}

.item > li > ul li {
	margin: 4px 0;
}
CSS;

$this->registerCSS($css);

$blocks = $questionModel->getBlocks();
$units = $questionModel->getUnits();

?>

<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?=
            Html::a(
                'Back',['answers/training-index',
					'rcID'=>$questionModel->rcID,
					'qnID'=>$questionModel->id
                ],
                [
                    'class'=>'btn btn-default'
                ]
            );
        ?>
    </p>
	
	<div class="row">

		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">General Information</div>
				<div class="panel-body">
					<?php
                        foreach ($blocks as $block) {
                            echo $this->render(
                                    "create/_block",
                                    [
                                        "block"=>$block,
                                        "units"=>$units,
                                        "questionModel"=>$questionModel,
                                        "model"=>$model
                                    ]
                                );
                        }
                    ?>
				</div>			
			</div>
		</div>
	
		<div class="col-md-8">
			<?php echo $this->render("create/_ratingForm",[
                        "units"=>$units,
                        "model"=>$model,
                        "questionModel"=>$questionModel
                    ]
                );
            ?>
		</div>
	</div>
</div>

<?php
$js = <<<JS
	$(".toggle-unit-context").click(function(e){
		var hash = $(this).prop("hash");

		$(".unit-context").hide();
		$(hash).show(300);
		$(".show-all-unit-context").show();
	});

	$(".show-all-unit-context").click(function(e){
		$(".unit-context").show(200);
		$(".show-all-unit-context").hide();
	});
JS;

$this->registerJS($js);
?>