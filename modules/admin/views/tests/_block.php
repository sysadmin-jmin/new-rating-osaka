<?php
use yii\helpers\Html;
use app\models\RatedUnits;

$tick = "<i class='fa fa-check'></i>";
$arrow = "<i class='fa fa-arrow-right' style='margin: 0px 10px 0px 20px'></i>";
$cross = "<i class='fa fa-times'></i>";
$glass = "<i class='fa fa-search'></i>";

$ansBlocks = $model->getBlocks();
$ansUnits = $model->getUnits();
$ansItems = $model->getItems();
?>


<div class="panel panel-primary">
<div class="panel-heading">
Block <?=$block["id"]?>
	<div class="" style='float:right;margin-right:10px'>
	Status:
	<?= (!empty($ansBlocks[$blockKey]["status"]) && $ansBlocks[$blockKey]["status"]=="closed") ? $tick : $cross ?>
	</div>
</div>

<?php
if (count($block["units"])): ?>
<?php
    foreach ($block["units"] as $key => $blockUnit):
        $unit = $units[$blockUnit];
		$_ansUnit = $ansUnits[$key];
		$_ansUnitStatus = $_ansUnit["status"];
?>
	<div class="panel panel-body">
		
		<div class="row">
			<div class="col-md-6">
			<h4> Unit (<?= $key ?>): <?=$unit["name"]?> </h4>
			</div>
			<div class="col-md-6">
				<h5 style='float:right'>
				Status:
				<?= ($_ansUnitStatus =="closed") ? $tick : $cross ?>
				</h5>
			</div>
		</div>
		
		
		<div class="row">
				<div class="col-md-6">

					<table class='table table-bordered'>

						<?php
                            if (count($unit["items"])):
                                foreach ($unit["items"] as $unitItem):
                                    $item = $items[$unitItem];
                        ?>
									<tr>
										<td><?= $arrow ?>Item <?=$item['uid']?></td>
										<td>
											<?php if (!empty($ansItems[$unitItem]["src"])): ?>
												<?= Html::a($glass, ['item-image','id'=>$model->id, "itemID"=> $unitItem]) ?>
											<?php endif; ?>
										</td>
									</tr>
						<?php
                                endforeach;
                            endif;
                        ?>
					</table>			
				</div>
			
				<div class="col-md-6">

					<table class='table table-bordered'>
						<tr class='info'>
							<th>Rating Context</th>
							<th style='width:20px'>Score</th>
						</tr>

						<?php
                            if (count($contexts[$blockUnit])):
                                foreach ($contexts[$blockUnit] as $key=>$context):
                                    $runit = RatedUnits::find()->where(["answerID"=>$model->id, "unitID"=>$blockUnit])->one();
                                    if ($runit):
                                        $ratings = $runit->getRatings();
                        ?>
										<tr>
											<td><?= $arrow ?> <?=$context["name"]?></td>
											<td><?=$ratings[$key]?></td>
										</tr>
							  <?php else: ?>	
										<tr>
											<td><?=$context["name"]?></td>
											<td><i class='fa fa-battery-0'></i></td>
										</tr>								
							<?php
                                    endif;
                                endforeach;
                            endif;
                        ?>
					</table>
				</div>		
		</div>
	</div>
<?php
    endforeach;
?>
<?php
endif;
?>
</div>
