<?php

use yii\helpers\Html;

$this->title = "View Answer Sheet";

$rcModel = \app\models\RatingConfigs::findOne($model->rcID);

$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['rating-configs/index', 'projectID' => $rcModel->projectID]];
$this->params['breadcrumbs'][] = ['label'=> $rcModel->test_name, 'url'=>['rating-configs/view','id'=>$model->rcID]];
$this->params['breadcrumbs'][] = ['label' => 'Test Result Data', 'url' => ['index','rcID'=>$model->rcID]];
$this->params['breadcrumbs'][] = $this->title;

if ($model->rcModel) {
    $blocks = $model->rcModel->blocks;
    $units = $model->rcModel->units;
    $items = $model->rcModel->items;
    $contexts = $model->rcModel->contexts;
}

?>

<h1><?= $this->title ?></h1>
<h4><?= "Test-Set: {$rcModel->test_name}" ?>  </h4>
<p><?= "Answer ID: #{$model->id}" ?> </p>

<p>
	<?= Html::a('Back', ['index','rcID'=>Yii::$app->request->get('rcID')], ['class'=>'btn btn-default']) ?>
</p>


<div class="row">

    <?php 
        if (count($blocks)):
            foreach ($blocks as $key=>$block):
    ?>
            	<div class="col-md-9">
            	<?= $this->render('_block', ["model"=>$model, "blockKey"=> $key, "block"=>$block, "units"=>$units, "items"=>$items, "contexts"=>$contexts]); ?>
            	</div>
    <?php 
            endforeach;
        endif;
    ?>
	<div class="col-md-3">


	</div>


</div>





