<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Answers Result Data';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/']];
$this->params['breadcrumbs'][] = ['label' => 'Test Sets', 'url' => ['rating-configs/']];
$this->params['breadcrumbs'][] = ['label'=> \app\models\RatingConfigs::findOne($rcID)->test_name, 'url'=>['rating-configs/view','id'=>$rcID]];
$this->params['breadcrumbs'][] = $this->title;
$tick = "<i class='fa fa-check'></i>";
$cross = "<i class='fa fa-times'></i>";
$glass = "<i class='fa fa-search'></i>";


function loopItems($itemList,$itemA) {
	
	echo "<ul>";
	foreach($itemList as $key=>$itemID) {
		$_itemA = $itemA[$itemID];
		
		?>
		<li><?= $_itemA['uid'] ?></li>
		<?php
	}
	echo "</ul>";
}	
	

function loopUnits($unitList,$unitA,$items) {
	 
	
	
	foreach($unitList as $key=>$unitID) {
		$_unitA = $unitA[$unitID];
	
		?>
		
			<li><?= $_unitA['name'] ?>
				<ul>
				<?php loopItems($_unitA['items'],$items) ?>
				</ul>
			</li>

		<?php
	}
}

$css = <<<CSS

.unit-list > li {
	margin:0 -25px;
	font-size: 14px;
}

.unit-list li ul li {
	margin: 5px -40px;
	font-size: 12px;
}

.table-small {
	width:100px;
}

.table-small tr th{
}

CSS;
$this->registerCss($css);

?>




<h1><?= Html::encode($this->title) ?></h1>


<p>
<?php echo Html::beginForm(); ?>
<?php echo Html::hiddenInput('rcID', $rcID); ?>
<?php echo Html::hiddenInput('returnPath', Url::current()); ?>

<?php echo Html::a('Gen image', ['answergen/image'], ['class'=>'btn btn-default']) ?>


<?= Html::submitButton('Generate Test Answer Sheets',
    [
        'formaction' => Url::to('/admin/answergen/answer'),
        'formmethod'=>'post',
        'class'=>'btn btn-warning',
    ]);
?>


<?= Html::a(
    'Delete All',
    ['delete','rcID'=> \Yii::$app->request->get('rcID')],
    [
        'class'=>'btn btn-danger',
        'data' => ['confirm' => 'Are you sure you want to delete ALL data ?'],
    ]
);

?>
<?php echo Html::endForm(); ?>
</p>

<div class="panel panel-default">

<div class="panel-heading">Test Structure</div>
<div class="panel-body">

	<div class="row">

<?php foreach($blocks as $key=>$block): ?>
	<div class="col-md-4">
		<div class="panel panel-primary">
			<div class="panel-heading"><?= $block['name'] ?></div>
			<div class="panel-body">
				<ul class='unit-list'>
				<?php loopUnits($block['units'],$units,$items) ?>
				</ul>
			</div>
		</div>
	</div>
<?php endforeach; ?>
	</div>
</div>

</div>

<hr>


<table class='table table-bordered table-striped'>

<tr>
	<th width="80">Answer ID</th>
	<th>Blocks </th>
	<th>Units </th>
	<th width="25">View</th>
</tr>

<?php foreach ($list as $key=>$test): ?>
<tr>
	<td><?=$test->id?></td>
	<td>
		<?php
        $ansBlocks = $test->getBlocks();
        ?>
		
		<table class='table table-bordered table-small'> 
			<tr>
				<th style='width:200px'>Block ID</th>
				<th>Status</th>
			</tr>
			<?php foreach ($ansBlocks as $key =>$block) : ?>
				<tr>
					<td><?= $blocks[$key]["id"] ?></td>
					<td><?=	($block["status"]=="closed") ? $tick : $cross?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</td>
	
	<td>

    <?php $ansUnits = $test->getUnits(); ?>

		<table class='table table-bordered table-small'>
			<tr>
				<th>Unit ID</th>
				<th>Status</th>
			</tr>
			<?php foreach ($ansUnits as $key=>$unit): ?>
					<tr>
						<td><?= $units[$key]["id"] ?></td>
						<td><?=($unit["status"]=="closed") ? $tick : $cross?></td>
					</tr>
			<?php endforeach ?>		
		</table>
	</td>

	<td><?= Html::a($glass, ['view','id'=>$test->id,'rcID'=>$rcID]) ?></td>
</tr>
<?php endforeach ?>

</table>
