<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pending Reasons';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="brand">
<h1><?= $this->title ?></h1>
<div class="row">

<div class="col-md-8">

<table class='table table-bordered'>
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	
	<?php foreach($reasons as $key=>$value): ?>
	<tr>
		<td><?= $key+1 ?></td>
		<td><?= $value['text']; ?></td>
		<td> <?php echo Html::a(Icon::fa('edit'), ['brand-settings','brandKey'=>$key]); ?></td>
		<td><?php 
			echo Html::a(Icon::fa('trash'), ['brand'],
                ['data-method' => 'POST',
                 'data-params' => [
                    'key'=>$key,
                    'action'=>'delete'
                ]
            ]);
			?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>

</div>

<div class="col-md-4">

	<div class="panel panel-default">

	<div class="panel-heading">
		Add new reason
	</div>

	<div class="panel-body">
	<?php echo Html::beginForm(Url::to(['pendings', 'id' => $id]),'POST'); ?>
        
		<?= Html::textInput('text', '', ['placeholder' => 'Reasons', 'class' => 'form-control']) ?>
        
        <p style='margin-top:20px'>
		    <?= Html::submitButton('Add', ['name' => 'action', 'value' => 'add', 'class'=>'btn btn-primary']) ?>
		</p>

	<?php echo Html::endForm(); ?>
	</div>

	</div>
</div>

</div>
