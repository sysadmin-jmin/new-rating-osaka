<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Project Companies';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="brand">
<h1><?= $this->title ?></h1>
<div class="row">

<div class="col-md-8">

<table class='table table-bordered'>
    <tr>
        <th style='width:100px'>Short Code</th>
        <th>Name</th>
        <th> </th>
    </tr>
    
    <?php foreach ($companies as $key=>$value): ?>
    <tr>
        <td><?= $key ?></td>
        <td><?= $value; ?></td>
        <td>
            <?=Html::a("<i class='fa fa-trash-o'></i>", ['company'], ['data-method' => 'POST', 'data-params' => ['code'=>$key, 'action'=>'delete']]);?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

</div>

<div class="col-md-4">

    <div class="panel panel-default">

    <div class="panel-heading">
        Add Another Company
    </div>

    <div class="panel-body">
        <?= Html::beginForm('', 'post', ['class'=>'form-inline']) ?>
        <label for="">Company Code</label>
        <?= Html::textInput('code', '', ['class'=>'form-control','placeholder'=>'GOOGLE']) ?>

        <label> Company Full Name </label>
        <?= Html::textInput('name', '', ['class'=>'form-control','placeholder'=>'Google Inc.']) ?>
        <?= Html::hiddenInput('action', 'add') ?>
        <p style='margin-top:20px'>
            <?= Html::submitButton('add', ['class'=>'btn btn-primary']) ?>

        </p>

        <?= Html::endForm() ?>
    </div>

    </div>
</div>

</div>
