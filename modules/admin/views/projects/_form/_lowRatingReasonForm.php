<?php
use yii\helpers\Html;
use app\components\Icon;

/* @var $model app\models\Projects */

function showLowRatingReason($model)
{
    $htmlResult = "";
    $lowRatingReasons = $model->getLowRatingReasons();
    foreach ($lowRatingReasons as $key=>$reason) {
        $htmlResult .= "
			<tr>
				<td>".$reason."</td>
				<td>".Icon::fa(
                        "trash",
                        [
                            "",
                            "id"=>$model->id
                        ],
                        [
                            "data-method"=>"post",
                            "data-params"=>[
                                "action" => "deleteLowRatingReasons",
                                "key" => $key
                            ],
                            "data-confirm" => "Are you sure want to delete this reason?"
                        ]
                    ).
                "</td>
			</tr>";
    }

    return $htmlResult;
}
?>
<section>
	<hr>

	<h3>Set Low Rating Reason</h3>
		
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Low Rating Reason
				</div>
				<div class="panel-body">
					<table class="table table-bordered table-strip">
						<tr>
							<th>Reason</th>
							<th></th>
						</tr>
						<?=showLowRatingReason($model);?>
					</table>
				</div>
			</div>			
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					Add Reason
				</div>
				<div class="panel-body">
					<?=Html::beginForm(["", "id"=>$model->id])?>
						<?=Html::hiddenInput("action", "addLowRatingReasons")?>
						<div class="form-group">
							<label class="label-control">Reason</label>
							<?=Html::textInput("reason", "", ["class"=> "form-control"])?>
						</div>
						<div class="form-group">
							<?=Html::submitButton("Save", ["class"=>"btn btn-default"])?>
						</div>
					<?=Html::endForm()?>
				</div>
			</div>						
		</div>
	</div>	
</section>