<?php
use app\models\Projects;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Icon;
/**
 * @var Projects $model
 */

?>
<section>
    
    <hr>
    
    <?php $projectGroupCodes = $model->getGroups(); ?>
    
    <h3>Add Group To Project</h3>
    
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Groups in Project (<?php echo count($projectGroupCodes); ?>)
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>Group Name</th>
                            <th>Options</th>
                        </tr>
                        <?php if (count($projectGroupCodes) > 0): ?>
                            <?php foreach ($projectGroupCodes as $idx => $code ): ?>
                                <tr>
                                    <td><?php echo $idx+1; ?></td>
                                    <td><?php echo @$groups[$code]['name'] ?? 'Removed group?'; ?></td>
                                    <td><?= Html::a(
                                            Icon::Fa('delete'),
                                            Url::to(['update-group','id'=>$model->id, 'code' => $code, 'mode' => 'remove']),
                                            ['data-confirm' => 'Are you sure you want to remove this group ?']
                                        ); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3">No assigned</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
        
        
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Assign Group To Project
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>Group Name</th>
                            <th>Options</th>
                        </tr>
                        <?php if (count($groups) > 0): ?>
                            <?php $idx = 1; ?>
                            <?php foreach ($groups as $code => $group): ?>
                                <?php if (in_array($code, $projectGroupCodes))
                                    continue; ?>
                                <tr>
                                    <td><?php echo $idx++ ?></td>
                                    <td><?php echo $group['name']; ?></td>
                                    <td><?= Html::a(
                                            Icon::Fa('add'),
                                            Url::to(['update-group','id'=>$model->id, 'code' => $code, 'mode' => 'add'])
                                        ); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="3">No groups</td>
                            </tr>
                        <?php endif; ?>
                    </table>
                    </table>
                </div>
            
            </div>
        </div>
    
    </div>
</section>