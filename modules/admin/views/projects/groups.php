<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\Raters;

$this->title = 'Add Group to Project';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="projects-groups">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index'], ['class'=>'btn btn-default']) ?>
    </p>


    <div class="row">

        <div class="col-md-6">

            <div class="panel panel-default">

                <div class="panel-heading">Groups</div>

                <div class="panel-body">
                    <?php Pjax::begin(); ?>
                        <?=Html::beginForm('groups', 'get', ["id"=>"search"]); ?>
                            <?=Html::input("hidden", "id", $projectModel->id);?>

                            <?=Html::input("text", "query", Yii::$app->request->getQueryParam('query', ''), ["id"=>"group-name", "placeholder"=> "Search"]);?>

                            <?= Html::submitButton('search', ['class'=>'btn btn-primary']) ?>
                        <?=Html::endForm(); ?>

                        <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'emptyText' => "All groups assigned.",
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        "attribute"=> "code",
                                        "label" => "Code"
                                    ],
                                    [
                                        "attribute"=> "name",
                                        "label" => "Name"
                                    ],
                                    [
                                        "label" => "Members",
                                        "value" => function ($group) {
                                            return Raters::find()->where(["group"=>$group["code"]])->count();
                                        }
                                    ],
                                    [
                                        'label'=>'',
                                        'value'=>function ($group) use ($projectModel) {
                                            $s = Html::a(
                                                '<span class="glyphicon glyphicon-arrow-right"></span>',
                                                ['groups', 'action'=>'assign',
                                                           'id'=>$projectModel->id,
                                                           'code'=>$group['code']],
                                                ['class'=>'btn btn-xs btn-primary']
                                            );
                                            return $s;
                                        },
                                        'format'=>'html'
                                    ]
                                ],
                            ]);
                        ?>                        

                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="panel panel-default">

                <div class="panel-heading">Groups in Project (<?=count($assignedGroups)?>)</div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Members</th>
                            <th></th>
                        </tr>
                        <?php if (count($assignedGroups)): ?>
                            <?php foreach ($assignedGroups as $key=>$code): ?>
                                <tr>
                                    <td><?=$code?></td>
                                    <td>
                                        <?php 
                                            $group = Raters::getGroupByCode($code);
                                            if ($group) {
                                                echo $group["name"];
                                            }
                                        ?>
                                    </td>
                                    <td><?=Raters::find()->where(["group"=>$code])->count()?></td>
                                    <td>
                                        <?=Html::a('<i class="fa fa-trash" style="color:#ea0500"></i>', ["groups", "action"=>"delete", "id"=>$projectModel->id, "code"=>$code])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4"><div class="empty">No Groups in this Project.</div></td>
                            </tr>
                        <?php endif;?>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>