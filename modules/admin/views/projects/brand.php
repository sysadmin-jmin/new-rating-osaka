<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Brands';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="brand">
<h1><?= $this->title ?></h1>
<div class="row">

<div class="col-md-8">

<table class='table table-bordered'>
	<tr>
		<th style='width:100px'>Short Code</th>
		<th>Name</th>
		<th>Settings</th>
		<th>Delete</th>
	</tr>
	
	<?php foreach($brandA as $key=>$value): ?>
	<tr>
		<td><?= $key ?></td>
		<td><?= $value['name']; ?></td>
		<td> <?php echo Html::a(Icon::Fa('settings'), ['brand-settings','brandKey'=>$key]); ?></td>
		<td><?php 
			echo Html::a("<i class='fa fa-trash-o'></i>", ['brand'],
							['data-method' => 'POST',
							 'data-params' => [
												'key'=>$key,
												'action'=>'delete'
												]
							]);
			?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>

</div>

<div class="col-md-4">

	<div class="panel panel-default">

	<div class="panel-heading">
		Add Another Key Value
	</div>

	<div class="panel-body">
	
	<?php $form = ActiveForm::begin() ?>
		<?= $form->field($Brand,'key') ?>
	
		<?= $form->field($Brand,'name') ?>

		<?= Html::hiddenInput('action','add') ?>

		<p style='margin-top:20px'>
			<?= Html::submitButton('add',['class'=>'btn btn-primary']) ?>

		</p>

	<?php ActiveForm::end() ?>
	</div>

	</div>
</div>

</div>
