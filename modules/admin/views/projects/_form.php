<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
//use yii\jui\DatePicker;
use nex\datepicker\DatePicker;
use app\components\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

?>
<style> 
section {
	padding: 50px 0;
}
 </style>
<div class="projects-form">
	<?php $form = ActiveForm::begin(); ?>
    <div class="row">
		
        <div class="col-md-8">
			<h3>General Settings</h3>
			
			<?= $form->field($model, 'brand')->dropDownList($model->getBrandDropList()) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'skipblock')->radioList( [0=>'No',1=>'Yes']) 
					 ->hint("Allow Raters To Skip A Block"); ?>
            
            <?= $form->field($model, 'pending_mode')->radioList([0=>'Off',1=>'On'])
                ->hint("Allow pending")->label('Pending mode'); ?>

			
			<div class="row">
				<div class="col-md-6">
					<?php echo $form->field($model, 'starttime')->widget(DatePicker::className(),[
						'model' => $model,
						'attribute' => 'starttime',
						'options' => ['class' => 'form-control'],
						'language' => 'en',
                        'size' => 'md',
                        'placeholder' => 'Start DateTime',
						'clientOptions' => [
                            'format' => 'YYYY-MM-DD HH:mm',
						],
						//'dateFormat' => 'yyyy-MM-dd',
					]); ?>				
				</div>
				<div class="col-md-6">
					<?php echo $form->field($model, 'endtime')->widget(DatePicker::className(),[
						'model' => $model,
						'attribute' => 'endtime',
						'options' => ['class' => 'form-control '],
						'language' => 'en',
                        'size' => 'md',
                        'placeholder' => 'End DateTime',
                        'clientOptions' => [
						    'format' => 'YYYY-MM-DD HH:mm',
						],
						//'dateFormat' => 'yyyy-MM-dd',

					]); ?>
				
				</div>
			</div>
			
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
	</div>	
	
	
	
		
	

    </div>
	<?php ActiveForm::end(); ?>
</div>
