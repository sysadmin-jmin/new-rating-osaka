<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\components\Icon;
use app\assets\VueAsset;
//print_r($model->attributes());

VueAsset::register($this);

?>
<style>

.short {
	width: 50px;
}

</style>
<div id='app' class="project-settings">

<h1>Brand Settings</h1>




<?php $form = ActiveForm::begin() ?>

<section>
	<h3>General Settings	</h3>
	
	<?= $form->field($model,'key') ?>
	<?= $form->field($model,'name') ?>
</section>
	
	<hr>

<section>
	<h3>Initial Training</h3>
	
	<?= $form->field($model,'isInTraining')->radioList([0=>"no",1=>"yes"]) ?>

	<?= $form->field($model,'inTrainNo')->textInput(['class'=>'form-control short']) ?>	
</section>	

<hr>


	
<section>
	<h3>Certification Test</h3>
	

	<?= $form->field($model,'certByBrand')->radioList([1=>"yes"]) ?>

	<?= $form->field($model,'certNo')->textInput(['class'=>'form-control short']) ?>
	
	<?= $form->field($model,'lowScoreOption')->dropDownList($model->lowScoreOptionA()) ?>
	
	Lowest score is counted as 'within'

	<?= $form->field($model,'certTolerance')->textInput(['class'=>'form-control short']) ?>
	
	
	<?= $form->field($model,'certPerfectPerc')->textInput(['class'=>'form-control short']) ?>
	
	<?= $form->field($model,'certWithinPerc')->textInput(['class'=>'form-control short']) ?>
	
	<?= $form->field($model,'certLimit')->textInput(['class'=>'form-control short']) ?>
	
	<?= $form->field($model,'attemptIntDays')->textInput(['class'=>'form-control short']) ?>
	
	<?= $form->field($model,'attemptLimit')->textInput(['class'=>'form-control short']) ?>
</section>
	
<hr>	
	
<section>
	<h3>Refresh Training</h3>
	
	<p> Setting the Milestone For Refresher Training (#number of rated blocks)</p>
	<p> On the ... </p>
	<ul>
		<li v-for=" (no,key) in t_interval"> #{{no}} Rating
			<a href="#" @click='delTintv(key,$event)'> <?= Icon::Fa('delete') ?> </a>
		</li>
	</ul>		

	<div class="form-inline">
		<?= $form->field($model,'training_interval')->hiddenInput(['v-model'=>'t_interval'])
			->label(false)
		?>
		
		<input class='form-control' style='width:50px' id='t_intv' v-model="t_intv"> 
		<button @click='add($event)' class='btn btn-default'>Add</button>
		
	</div>
	
	<?= $form->field($model,'trainDummieNo')->textInput(['class'=>'form-control short']) ?>	
	
	<?= $form->field($model,'trainActNo')->textInput(['class'=>'form-control short']) ?>	
</section>
	
	<hr>
	
<section>
	<h3> Quality Control Tests </h3>

	<p> Setting the Milestone For Quality Control (#number of rated blocks)</p>
	<p> On the ... </p>
	<ul>
		<li v-for=" (no,key) in qc_interval"> #{{no}} Rating
			<a href="#" @click='delQcIntv(key,$event)'> <?= Icon::Fa('delete') ?> </a>
		</li>
	</ul>	
	
	<?= $form->field($model,'qc_interval')->hiddenInput(['v-model'=>'qc_interval'])->label(false) ?>	
	
	
	<!-- copy here -->
	
	<div class="form-inline">
		<input class='form-control' style='width:50px' id='qc_intv' v-model="qc_intv"> 
		<button @click='addQc($event)' class='btn btn-default'>Add</button>
	</div>

	<?= $form->field($model,'qcTolerance')->textInput(['class'=>'form-control short']) ?>	
	
	<?= $form->field($model,'qcPerfectPerc')->textInput(['class'=>'form-control short']) ?>	

	<?= $form->field($model,'qcWithinPerc')->textInput(['class'=>'form-control short']) ?>	
		
		

</section>	

	<?= Html::submitButton('Save Brand Configuration',['class'=>'btn btn-primary'])?>
	
<?php ActiveForm::end() ?>

</div>


<script>

var app = new Vue({
  el: '#app',
  data: {
	t_interval: <?= $model->getTrainingInterval() ?>,
	qc_interval: <?= $model->getQcInterval() ?>,
	t_intv:'',
	qc_intv:'',
	reasons: <?= $model->getReasons ?>,
	reason:''
  },
  
  methods: {
	  
	  add(e) {
		  e.preventDefault()
		  
		  var isInArray = this.t_interval.indexOf(this.t_intv);
		  console.log(isInArray);
		  if(isInArray!="1") {
			this.t_interval.push(this.t_intv) 
			this.t_interval.sort(function(a, b){return a-b});
			this.t_intv = '';
		  }  
	  },
	  
	  delTintv(key,e) {
		  e.preventDefault();
		  this.t_interval.splice(key,1)
		  
	  },
	  
	  addQc(e) {
		  e.preventDefault()
		  
		  var isInArray = this.qc_interval.indexOf(this.qc_intv);
		  console.log(isInArray);
		  if(isInArray!="1") {
			this.qc_interval.push(this.qc_intv) 
			this.qc_interval.sort(function(a, b){return a-b});
			this.qc_intv = '';
		  }  
	  },

	  delQcIntv(key,e) {
		  e.preventDefault();
		  this.qc_interval.splice(key,1)
		  
	  },	  
	  
  } //methods 
})

</script>


