<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = 'Update Projects: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="projects-update">

    <h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Back', ['index'], ['class'=>'btn btn-default']) ?>
	</p>
	
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
    <?php echo $this->render('_form/_groupForm', [
        'model' => $model,
        'groups' => $groups,
    ]);
    ?>

    <?=$this->render(
        "_form/_lowRatingReasonForm",
        [
            "model"=>$model
        ]
    );?>
</div>
