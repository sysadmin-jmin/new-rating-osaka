<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use app\models\RatingConfigs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
     <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php 	

$actionColumn = [	
		'class' => 'yii\grid\ActionColumn',
		'template' => ' {update} {delete}'
];

?>
		
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
			 
			 'attribute'=>'name',
			 'value'=> function ($model) {
						return Html::a($model->name,['rating-configs/', 'projectID' =>$model->id]);
						},
			 'format'=>'html',
			 'contentOptions'=>['style'=>'width:30%']
			],
			'brand',
			['label'=>'Test Sets',
			 'value'=> function($model) {
							$count = RatingConfigs::find()->where(['projectID'=>$model->id])
														->count();
							return "<span class='badge badge-primary'>{$count}</span>";
						},
			 'format'=>'html'
			],
			
			
            ['label'=>'Period',
                'value'=> function($model) {
                    return sprintf('%s <br>~ %s'
                        , date('j M G:i', strtotime($model->starttime))
                        , date('j M G:i', strtotime($model->endtime)));
                },
                'format'=>'html',
            ],
            ['label'=>'Pending',
                'value'=> function($model) {
                    return sprintf('%s <span class="badge badge-default">%d</span>'
                        , ($model->pending_mode == 1 ? 'Allow' : 'Not allow')
                        , count(json_decode($model->pending_reasons,true))
                    );
                },
                'format'=>'html',
            ],
			$actionColumn,
        ],
    ]); ?>
	
</div>
