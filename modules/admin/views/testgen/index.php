<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Test Generator';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="">

This will generate a 10 sample tests 

<p>
Build item json based on the question and lorem50
</p>

</div>

<p>
<?php echo Html::beginForm() ?>

<?php echo Html::hiddenInput('action', 'examinees'); ?>

<?php echo Html::submitButton('Generate Examinees', ['class'=>'btn btn-primary']); ?>
<?php echo Html::endForm() ?>

</p>

<p>
<?php echo Html::beginForm() ?>

<?php echo Html::hiddenInput('action', 'test'); ?>

<?php echo Html::submitButton('Generate Test', ['class'=>'btn btn-primary']); ?>
<?php echo Html::endForm() ?>
</p>

