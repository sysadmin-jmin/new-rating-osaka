<?php

namespace app\modules\admin\controllers;

use Yii;
use Faker;
use yii\db\Expression;
use yii\helpers\FileHelper;
use app\models\Examinees;
use app\models\Answers;
use app\models\Raters;
use app\models\RatingConfigs;
use app\components\AppHelper;

class TestgenController extends \yii\web\Controller
{
    public $layout = '@app/modules/admin/views/layout/columns2';
    
    
    public function actionIndex()
    {
        $action = Yii::$app->request->post('action');


        if ($action =='test') {
            // empty test table and rated units, set all raters inpro_testblock to null
            Yii::$app->db
                    ->createCommand()
                    ->truncateTable('tests')
                    ->execute();

            Yii::$app->db
                    ->createCommand()
                    ->truncateTable('rated_units')
                    ->execute();

            Raters::updateAll(["inpro_testblock" => null]);


            $faker = Faker\Factory::create();

            $examinees = Examinees::find()->all();

            $is_generated = false;

            if ($examinees) {
                // generate tests for each examinee
                foreach ($examinees as $examinee) {
                    //// get one random rating-config
                    $rc = RatingConfigs::find()->orderBy(new Expression('rand()'))->one();

                    if ($rc) {
                        $test = new Answers;
                        $test->rcID = $rc->id;
                        $test->examineeID = $examinee->id;
                        // need to save test first to get test ID, use to generate folderPath
                        $test->save();

                        if ($test->createFolderPath()) {
                            $items = $rc->items;
                            if (count($items)) {
                                // generate answer for items
                                $answerItems = [];
                                foreach ($items as $key=>$item) {
                                    if ($test->createAnswerImg($test->id, $key)) {
                                        $answerItems[$key] = [
                                            "src" => $key.".png",
                                            "type" => "png"
                                        ];
                                    }
                                    usleep(500);
                                }
                                $test->items = json_encode($answerItems, JSON_FORCE_OBJECT);
                            }
                        }

                        $units = $rc->units;
                        if (count($units)) {
                            //generate rated for unit;
                            $ratedUnits = [];
                            foreach ($units as $key=>$unit) {
                                $ratedUnits[$key] = [
                                    "status" => "open"
                                ];
                            }
                            $test->units = json_encode($ratedUnits, JSON_FORCE_OBJECT);
                        }

                        $blocks = $rc->blocks;
                        if ($blocks) {
                            //generate rated for blocks;
                            $ratedBlocks = [];
                            foreach ($blocks as $key=>$block) {
                                $ratedBlocks[$key] = [
                                    "status" => "open"
                                ];
                            }
                            $test->blocks = json_encode($ratedBlocks, JSON_FORCE_OBJECT);
                        }

                        // save test after
                        if ($test->save()) {
                            $is_generated = true;
                        }
                    } else {
                        Yii::$app->session->setFlash('warning', 'No rating config found. please create at least 1 rating config.');
                        break;
                    }
                }
            } else {
                Yii::$app->session->setFlash('warning', 'No Examinee found, please create or generate examinee.');
            }

            if ($is_generated) {
                Yii::$app->session->setFlash('success', 'Answers successfully generated.');
            }
        }
        
        if ($action =='examinees') {
            Yii::$app->db
                    ->createCommand()
                    ->truncateTable('examinees')
                    ->execute();
            
            $faker = Faker\Factory::create();

            echo $faker->name;
            echo $faker->address;

            echo $faker->text;
    
            $count = 0;
            
            while ($count < 10) {
                $count++;
                $model = new Examinees();
                $model->name = $faker->name;
                $model->school = $faker->city;
                $model->save();
            }
            
            Yii::$app->session->setFlash('success', 'Examinee successfully generated');
        }
        
        return $this->redirect(['tests','rcID'=>$rcID]);
    }
    
    public function actionImage()
    {
        (new Answers)->createAnswerImg(2, 1);
    }

    public function actionTest()
    {

        $rcID = Yii::$app->request->post('rcID');


        // empty test table and rated units
        Yii::$app->db
            ->createCommand()
            ->truncateTable('tests')
            ->execute();

        Yii::$app->db
            ->createCommand()
            ->truncateTable('rated_units')
            ->execute();

        $faker = Faker\Factory::create();

        $examinees = Examinees::find()->all();

        $is_generated = false;

        // get one from rating-config
        $rc = RatingConfigs::findOne($rcID);

        if ($examinees) {
            // generate tests for each examinee
            foreach ($examinees as $examinee) {

                if ($rc) {
                    $test = new Answers;
                    $test->rcID = $rc->id;
                    $test->examineeID = $examinee->id;
                    // need to save test first to get test ID, use to generate folderPath
                    $test->save();

                    if ($test->createFolderPath()) {
                        $items = $rc->items;
                        if (count($items)) {
                            // generate answer for items
                            $answerItems = [];
                            foreach ($items as $key=>$item) {
                                if ($test->createAnswerImg($test->id, $key)) {
                                    $answerItems[$key] = [
                                        "src" => $key.".png",
                                        "type" => "png"
                                    ];
                                }
                                usleep(500);
                            }
                            $test->items = json_encode($answerItems, JSON_FORCE_OBJECT);
                        }
                    }

                    $units = $rc->units;
                    if (count($units)) {
                        //generate rated for unit;
                        $ratedUnits = [];
                        foreach ($units as $key=>$unit) {
                            $ratedUnits[$key] = [
                                "status" => "0"
                            ];
                        }
                        $test->units = json_encode($ratedUnits, JSON_FORCE_OBJECT);
                    }

                    $blocks = $rc->blocks;
                    if ($blocks) {
                        //generate rated for blocks;
                        $ratedBlocks = [];
                        foreach ($blocks as $key=>$block) {
                            $ratedBlocks[$key] = [
                                "status" => "0"
                            ];
                        }
                        $test->blocks = json_encode($ratedBlocks, JSON_FORCE_OBJECT);
                    }

                    // save test after
                    if ($test->save()) {
                        $is_generated = true;
                    }
                } else {
                    Yii::$app->session->setFlash('warning', 'No rating config found. please create at least 1 rating config.');
                    break;
                }
            }
        } else {
            Yii::$app->session->setFlash('warning', 'No Examinee found, please create or generate examinee.');
        }

        if ($is_generated) {
            Yii::$app->session->setFlash('success', 'Answers successfully generated.');
        }

        $returnPath = Yii::$app->request->post('returnPath', 'index');


        return $this->redirect($returnPath);
    }
}
