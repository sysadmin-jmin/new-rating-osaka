<?php
# Fuji
namespace app\modules\admin\controllers;

use app\models\AnswerForms\Create;

use app\models\Raters;
use Faker\Provider\File;
use Yii;
use app\models\Answers;
use app\models\Questions;
use app\models\RatedBlock;
use app\models\RatingConfigs;
use app\models\AnswerForms\Import;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\UnsupportedMediaTypeHttpException;
use yii\web\UploadedFile;

class AnswersController extends Controller
{

    /**
     * @param int $rcID
     * @param int $qnID
     * @return string
     */
    public function actionActualIndex($rcID, $qnID)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Answers::find()->where(['qnID' => $qnID]),
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        
        $qnType = Questions::findOne($qnID)->type;


        return $this->render('actual/actual-index', [
            'dataProvider'  => $dataProvider,
            'qnID'  => $qnID,
            'rcID'  => $rcID,
            'filterType' => Answers::getTypeList(),
            'filterStatus' => Answers::getStatusList(),
            'qnType'=>$qnType
        ]);
    }
    
    public function actionActualPendingRab($rcID, $qnID)
    {
        $answerModel = Answers::find()
                        ->where(["rcID"=>$rcID, "qnID"=>$qnID])
                        ->one();
        $query = RatedBlock::find()
                            ->where(["ansID"=>$answerModel->id])
                            ->andWhere(["block_status"=>0]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $data["answerModel"] = $answerModel;
        $data["dataProvider"] = $dataProvider;

        return $this->render('actual/pending-rabs', $data);
    }
    
    
    
    public function actionTrainingIndex($rcID, $qnID)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Answers::find()->where(['qnID' => $qnID]),
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        
        $qnType = Questions::findOne($qnID)->type;


        return $this->render('training-index', [
            'dataProvider'  => $dataProvider,
            'qnID'  => $qnID,
            'rcID'  => $rcID,
            'filterType' => Answers::getTypeList(),
            'filterStatus' => Answers::getStatusList(),
            'qnType'=>$qnType
        ]);
    }
    
    
    
    
    public function actionCreate($rcID, $qnID)
    {
        $params['rcID'] = $rcID;

        $params['qnID'] = $qnID;

        $params['rcM'] = RatingConfigs::findOne($rcID);
    
        $params['qnM'] = Questions::findOne($qnID);


        $model = new Answers;

        $model->rcID = $rcID;
        $model->qnID = $qnID;
        
        $model->units = $model->generateUnits($params['qnM']->getUnits());
        $model->blocks = $model->generateBlocks($params['qnM']->getBlocks());
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            self::flash('success', 'answer added'); #from controller
            return $this->redirect(['update','id'=>$model->id]);
        }

        $params['model'] = $model;

        return $this->render('create', $params);
    }


    /**
     * Answer file uploading function
     * @param int $rcID
     * @param int $qnID
     * @param int $ansID
     * @return false|string
     * @throws \yii\base\Exception
     */
    public function actionUpdateUpload($rcID, $qnID, $ansID)
    {
        // Disable layout
        $this->layout = '';

        $params['rcID'] = $rcID;
        $params['qnID'] = $qnID;
        $params['ansID'] = $ansID;

        \Yii::$app->response->content = 'text/json';

        $model = new Create;

        $response = [
            'status' => 'error',
            'message' => 'Something went wrong',
            'content' => []
        ];

        if (\Yii::$app->request->isPost) {
            $uploadFile = UploadedFile::getInstance($model, 'answerFile');
            $postdata = \yii::$app->request->post();
            $model->rcID = $rcID;
            $model->qnID = $qnID;
            $model->key = $postdata['Create']['key'];
            $model->index = $postdata['Create']['index'];


            if ($uploadFile->getHasError()) {
                $response['message'] = 'Failed to upload';
            } else {

                // Load models
                $questions = Questions::findOne($qnID);
                $answers = Answers::findOne($ansID);

                // Save path
                // ex. /data/answers/<answers.id>/1.mp3
                //     /data/answers/<answers.id>/2.png
                $savePath = sprintf('%s/%d', Url::to('@app/data/answers'), $ansID);

                FileHelper::createDirectory($savePath, 0777, true);

                // text/plain => $type = text, $ext = plain
                // image/jpeg => $type = image, $ext = jpeg
                list($type, $ext) = explode('/', $uploadFile->type);

                $response['content'] = [
                    'mime_type' => $uploadFile->type,
                    'type' => $type,
                    'size' => $uploadFile->size,
                    'src' => $uploadFile->name,
                ];

                $model->answerFile = $uploadFile;

                $filename = sprintf('%d.%s', $model->index, $uploadFile->extension);


                // Delete old file same index named before save the new
                {
                    $filterName = $model->index.'.';
                    $foundFiles = FileHelper::findFiles($savePath, ['filter' => function ($path) use ($savePath,$filterName) {
                        if (!is_file($path)) {
                            return false;
                        }
                        return StringHelper::startsWith(basename($path), $filterName);
                    }]);

                    if (count($foundFiles) > 0) {
                        foreach ($foundFiles as $existFilename) {
                            unlink($existFilename);
                        }
                    }
                }


                if ($uploadFile->saveAs($savePath . DIRECTORY_SEPARATOR . $filename)) {
                    $response['status'] = 'success';
                    $response['message'] = 'Success upload';

                    {
                        // Update Answers model
                        $items = $answers->getItems();
                        if (!is_array($items) || count($items) < 1) {
                            $items = [];
                        }

                        $itemIndex = $model->index - 1;
                        if (!isset($items[$itemIndex])) {
                            $items[$itemIndex] = ['src'=>'','type'=>'','ansType'=>$questions->getItems($itemIndex)['ansType']];
                        }

                        $items[$itemIndex]['src'] = $filename;
                        $items[$itemIndex]['type'] = $ext;
                        $answers->folderPath = str_replace(Url::to('@app'), '', $savePath);
                        $answers->items = json_encode($items, JSON_FORCE_OBJECT);

                        if (!$answers->save()) {
                            Yii::$app->response->setStatusCode(500);
                            $response['status'] = 'error';
                            $response['message'] = 'Failed to save data';
                            $response['content']['error'] = $answers->getFirstErrors();
                        }
                    }
                } else {
                    Yii::$app->response->setStatusCode(500);
                    $response['status'] = 'error';
                    $response['message'] = 'Failed to save uploaded file';
                }

                $contentRaw = file_get_contents($savePath . DIRECTORY_SEPARATOR . $filename);
                $response['content']['name'] = $filename;
                if ($type == 'text') {
                    $response['content']['raw'] = sprintf('data:%s;charset=UTF-8,%s', $uploadFile->type, rawurlencode($contentRaw));
                //$response['content']['raw'] = rawurlencode($contentRaw);
                } else {
                    $response['content']['raw'] = sprintf('data:%s;base64,%s', $uploadFile->type, base64_encode($contentRaw));
                }
            }
        }
        return json_encode($response);
    }


    /**
     * Answer upload view and remove each items
     * @param int $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Answers $model */
        $model = $this->findModel($id);

        if ($action = Yii::$app->request->get('action')) {
            // Delete files
            if ($action == 'remove') {
                $key = ((int)Yii::$app->request->get('key') - 1);
                // Remove item
                $items = $model->getItems();
                if (!isset($items[$key])) {
                    Yii::$app->session->setFlash('danger', 'Selected item does not found.');
                    return $this->redirect(['update','id' => $id]);
                } else {
                    // exists
                    $filepath = sprintf('%s/%s/%s', Url::to('@app'), $model->folderPath, $items[$key]['src']);
                    if (file_exists($filepath)) {
                        if (!unlink($filepath)) {
                            Yii::$app->session->setFlash('warning', 'The file could not remove.');
                            return $this->redirect(['update', 'id' => $id]);
                        }
                    }
                    $items[$key]['src'] = '';
                    $model->items = json_encode($items, JSON_FORCE_OBJECT);

                    if (!$model->save()) {
                        Yii::$app->session->setFlash('danger', 'The file was removed, could not update record.');
                        return $this->redirect(['update','id' => $id]);
                    }
                    Yii::$app->session->setFlash('success', 'File was removed');
                    return $this->redirect(['update','id' => $id]);
                }
            }
        }

        
        $params['model'] = $model;
        $params['rcM'] = RatingConfigs::findOne($model->rcID);
        $params['qnM'] = Questions::findOne($model->qnID);
        $params['ansID'] = Questions::findOne($id);
        $params['supportUploadBytes'] = self::convertToBytes(ini_get('upload_max_filesize'));

        $params['formModel'] = new Create;

        return $this->render('update', $params);
    }


    /**
     * Delete answers
     * @param int $rcID
     * @param int $qnID
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($rcID, $qnID)
    {
        $models = Answers::find()->where(["qnID"=>$qnID])->all();
       
        
        
        // base path
        $basePath = Url::to(sprintf('@app/data/rc/%d', $qnID));


        if (count($models) < 1) {
            Yii::$app->session->setFlash('danger', 'Empty data');
            return $this->redirect(['index','rcID'=>$rcID,'qnID'=>$qnID]);
        }


        foreach ($models as $model) {
            // delete data
            $model->delete();
        }
        // delete files and directories under /data/rc/{id}
        if (file_exists($basePath)) {
            BaseFileHelper::removeDirectory($basePath);
        }

        Yii::$app->session->setFlash('success', 'Successfully all data deleted');
        return $this->redirect(['index','rcID'=>$rcID,'qnID'=>$qnID]);
    }
    
    
    
    
    
    
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ["model"=>$model]);
    }

    public function actionAssign($id)
    {
        $model = $this->findModel($id);
        
        /** @var Raters[] $raters */
        $raters = [];
        foreach (Raters::find()->where('1')->all() as $rater)
            $raters[$rater->id] = $rater;
        
        if (Yii::$app->request->isPost) {
            $action = Yii::$app->request->post('action');
            $raterID = Yii::$app->request->post('rid');
            $blockKey = Yii::$app->request->post('blockkey');
            $assigns = $model->getAssign();
            if ($action == 'assign') {
                // Assign user to block
                if (isset($assigns[$blockKey]) && in_array($raterID,$assigns[$blockKey])) {
                    Yii::$app->session->setFlash('danger', 'Already assigned');
                    $this->redirect(Url::to(['assign', 'id' => $id]));
                } else {
                    if (!isset($assigns[$blockKey]))
                        $assigns = [$blockKey => []];
                    
                    $assigns[$blockKey][] = $raterID;
                }
            } elseif ($action == 'unassign') {
                // Unassign user from block
                if (!isset($assigns[$blockKey])) {
                    Yii::$app->session->setFlash('danger', 'Not assigned');
                    $this->redirect(Url::to(['assign', 'id' => $id]));
                } else {
                    if ($idx = array_search($raterID,$assigns[$blockKey]) !== false)
                        unset($assigns[$blockKey][$idx]);
                }
            }
            // Save updates
            Yii::$app->session->setFlash('success', 'Success update');
            $model->assign = json_encode($assigns,JSON_FORCE_OBJECT);
            $model->save();
            $this->redirect(Url::to(['assign', 'id' => $id]));
        }
        
        
        return $this->render('assign', [
            'model'=>$model,
            'raters' => $raters,
        ]);
    }
    
    
    public function actionItemImage($id, $itemID)
    {
        $model = $this->findModel($id);
        $item = $model->getItems()[$itemID];
        
        $path = $model->getItemImage($itemID);
        
        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/'.$item["type"]);
        $img_data = file_get_contents($path);
        $response->data = $img_data;
        return $response;
    }

    /**
     * @param $id
     * @return Answers|array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Answers::find()->with('rcModel')->where(["id"=>$id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @param $itemID
     * @return \yii\console\Response|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionItemAudio($id, $itemID)
    {
        $model = $this->findModel($id);
        $path = $model->getItemAudio($itemID);
        $item = $model->getItems()[$itemID];
        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('Content-Type', 'audio/'.$item['type']);
        $response->data = file_get_contents($path);
        return $response;
    }

    /**
     * @param $id
     * @param $itemID
     * @return \yii\console\Response|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionItemText($id, $itemID)
    {
        $model = $this->findModel($id);
        $item = $model->getItems()[$itemID];
        if ($item['ansType'] != 'text') {
            throw new UnsupportedMediaTypeHttpException('Invalid item type');
        }
        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('Content-Type', 'text/plain');
        $response->data = file_get_contents($path);
        return $response;
    }
    

    /**
     * Generate dummy actual answers
     * Generate dummie answers for testing
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionGenDummy()
    {
        $qnID = Yii::$app->request->get('qnID');

        // 'model' or 'actual'
        $type = Yii::$app->request->get('type');

        // Return path
        $returnPath = Yii::$app->request->get('returnPath', null);
        if (null === $returnPath) {
            $returnPath = Yii::$app->request->getReferrer();
        }

        if (empty($type)) {
            Yii::$app->session->setFlash('danger', 'Please specify type param');
            Yii::$app->response->redirect($returnPath);
            return;
        }

        // empty test table and rated units
        Yii::$app->db
            ->createCommand()
            ->truncateTable('answers')
            ->execute();

        Yii::$app->db
            ->createCommand()
            ->truncateTable('rated_units')
            ->execute();


        $faker = \Faker\Factory::create();

        $examinees = \app\models\Examinees::find()->all();

        $is_generated = false;


        /** @var Questions $qn get one from Questions */
        $qn = Questions::findOne($qnID);

        if (!$examinees) {
            Yii::$app->session->setFlash('warning', 'No Examinee found, please create or generate examinee.');
            Yii::$app->response->redirect($returnPath);
            return;
        }
        // generate tests for each examinee
        foreach ($examinees as $examinee) {
            if (!$qn) {
                Yii::$app->session->setFlash('warning', 'No questions found. please create at least 1 question.');
                break;
            }

            $answer = new Answers;
            $answer->qnID = $qn->id;
            $answer->examineeID = $examinee->id;
            $answer->type = $type;
            // need to save test first to get test ID, use to generate folderPath
            $answer->save();

            if ($answer->createFolderPath()) {
                $items = $qn->items;
                if (count($items)) {
                    // generate answer for items
                    $answerItems = [];
                    foreach ($items as $key=>$item) {
                        if ($answer->createAnswerImg($answer->id, $key)) {
                            $answerItems[$key] = [
                                "src" => $key.".png",
                                "type" => "png"
                            ];
                        }
                        usleep(500);
                    }
                    $answer->items = json_encode($answerItems, JSON_FORCE_OBJECT);
                }
            }

            $units = $qn->units;
            if (count($units)) {
                //generate rated for unit;
                $ratedUnits = [];
                foreach ($units as $key=>$unit) {
                    $ratedUnits[$key] = [
                        "status" => "0"
                    ];
                }
                $answer->units = json_encode($ratedUnits, JSON_FORCE_OBJECT);
            }

            $blocks = $qn->blocks;
            if ($blocks) {
                //generate rated for blocks;
                $ratedBlocks = [];
                foreach ($blocks as $key=>$block) {
                    $ratedBlocks[$key] = [
                        "status" => "0"
                    ];
                }
                $answer->blocks = json_encode($ratedBlocks, JSON_FORCE_OBJECT);
            }

            // save test after
            if ($answer->save()) {
                $is_generated = true;
            }
        }

        if ($is_generated) {
            Yii::$app->session->setFlash('success', 'Answers successfully generated.');
        }


        return $this->redirect($returnPath);
    }
    
    // Delete all answers + related image files
    // related to this qnID

    public function actionTruncate()
    {
        $qnID = \yii::$app->request->get('qnID');
        $models = Answers::findAll(["qnID"=>$qnID]);

        if (count($models) < 1) {
            Yii::$app->session->setFlash('danger', 'Empty data');
            return $this->redirect(['index?qnID='.$qnID]);
        }

        $rcID = $models[0]->rcID;

        // delete all data
        Answers::deleteAll(['qnID' => $qnID]);


        // base path
        $targetPath = Url::to(sprintf('@app/data/rc/%d/qn/%d', $rcID, $qnID));

        // delete files and directories under /data/tests/{id}
        if (file_exists($targetPath)) {
            BaseFileHelper::removeDirectory($targetPath);
        }

        Yii::$app->session->setFlash('success', 'Successfully all data deleted');
        return $this->redirect(['index?qnID='.$qnID]);
    }

    /**
     * @param int $rcID
     * @param int $qnID
     * @return string
     */
    public function actionImport($rcID, $qnID)
    {
        $supportUploadBytes = self::convertToBytes(ini_get('upload_max_filesize'));

        $model = new Import();
        $model->qnID = $qnID;

        return $this->render('import', [
            'model'=>$model, 'rcID' => $rcID,
            'supportUploadBytes' => $supportUploadBytes,
        ]);
    }


    /**
     * CSV upload view
     * @param int $rcID
     * @deprecated integrated with import
     */
    public function actionUploadCsv($rcID)
    {
        $supportUploadBytes = self::convertToBytes(ini_get('upload_max_filesize'));

        return $this->render('uploadCsv', [
            'rcID' => $rcID,
            'supportUploadBytes' => $supportUploadBytes,
        ]);
    }


    /**
     * Process chunk uploading large files
     * @see FlowJS
     * @param int $qnID
     */
    public function actionUpload($qnID = null)
    {
        // TODO change layout for REST like display
        //$this->layout = '';

        if (!$qnID && Yii::$app->request->isPost) {
            $qnID = Yii::$app->request->post('qnID');
        }


        // RequestInterface for FlowJS
        $request = new \Flow\Request;

        // Requested _FILES params
        $files = $request->getFile();
        $extension = pathinfo($request->getFileName(), PATHINFO_EXTENSION);
        // save file path
        if (StringHelper::startsWith($files['type'], 'text/') && $extension == 'csv') {
            // CSV file
            $savePath = Url::to(sprintf('@app/data/answers/import_temp/%d/list.csv', $qnID));
        } else {
            // ZIP file
            $savePath = Url::to(sprintf('@app/data/answers/import_temp/%d/%d.%s', $qnID, $qnID, $extension));
        }
        $saveDir = dirname($savePath);
        if (!file_exists($saveDir)) {
            BaseFileHelper::createDirectory($saveDir);
        }
        if (!is_dir($saveDir)) {
            // if the path does not directory
            // delete file before process
            BaseFileHelper::unlink($saveDir);
            // create new directory
            BaseFileHelper::createDirectory($saveDir);
        }
        // to upload chunk files
        $workingPath = Url::to(sprintf('@app/data/tmp/chunks/%s', Yii::$app->session->id));
        if (!file_exists($workingPath) || is_dir($workingPath)) {
            BaseFileHelper::createDirectory($workingPath);
        }

        // Flow configure
        $config = new \Flow\Config;
        $config->setTempDir($workingPath);
        $config->setDeleteChunksOnSave(true);
        $file = new \Flow\File($config, $request);
        Yii::info($_FILES);


        if (Yii::$app->request->isGet) {
            // check uploaded chunks for resume
            if ($file->checkChunk()) {
                Yii::$app->response->setStatusCode(200);
            } else {
                Yii::$app->response->setStatusCode(204);
                $var_export = var_export($request->getFile(), true);
                $var_export .= PHP_EOL.'  CurrentChunkSize => ' . $request->getCurrentChunkSize();
                return $var_export;
            }
        } else {
            // To upload chunk files
            if ($file->validateChunk()) {
                $file->saveChunk();
            } else {
                Yii::$app->response->setStatusCode(400);
                $var_export = var_export($request->getFile(), true);
                $var_export .= PHP_EOL.'  CurrentChunkSize => ' . $request->getCurrentChunkSize();
                $var_export .= PHP_EOL.'_FILES = ' . PHP_EOL . print_r($_FILES, true);
                return $var_export;
            }
        }


        if ($file->validateFile() && $file->save($savePath)) {
            // File upload was completed
            // Remove chunked files
            \Flow\Uploader::pruneChunks($workingPath);
            BaseFileHelper::removeDirectory($workingPath);
        } else {
            // This is not a final chunk, continue to upload
            return Yii::$app->response->setStatusCode(200);
        }
    }

    /**
     * @param int $id Answer.ID
     * @param int $idx index of in items
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionRenderRaw($id, $idx)
    {
        $answers = $this->findModel($id);
        if (null == $answers) {
            throw new NotFoundHttpException('Requested item does not found on this server.');
        }

        if (!isset($answers->getItems()[$idx])) {
            throw new NotFoundHttpException('Requested item does not found on this server.');
        }
        $item = $answers->getItems()[$idx];

        if (!is_array($item) || count($item) < 1) {
            throw new NotFoundHttpException('Requested item does not found on this server.');
        }

        $resPath = sprintf(Url::to('@webroot/%s/%s'), $answers->folderPath, $item['src']);
        if (!file_exists($resPath)) {
            throw new NotFoundHttpException('Requested item does not found on this server.');
        }

        // MIME Type
        $mimeType = sprintf('%s/%s', $item['ansType'], $item['type']);
        if (strlen($mimeType) < 3) {  // invalid MIME
            $mimeType = 'application/octet-stream';
        } // default MIME

        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('Content-Type', $mimeType);
        $response->data = file_get_contents($resPath);
        return $response;
    }


    /**
     * Convert string of byte size to bytes integer.
     * @param string $val
     * @return int|string
     */
    private static function convertToBytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        $val = intval(substr($val, 0, -1));
        switch ($last) {
            case 't': $val *= 1024;
            // no break
            case 'g': $val *= 1024;
            // no break
            case 'm': $val *= 1024;
            // no break
            case 'k': $val *= 1024;
        }
        return $val;
    }
}
