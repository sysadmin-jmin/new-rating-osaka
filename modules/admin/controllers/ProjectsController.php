<?php

namespace app\modules\admin\controllers;

use app\models\ProjectForms\PendingReasons;
use Yii;
use app\models\Projects;
use app\models\Raters;
use app\models\projectForms\Brand;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use app\modules\admin\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    public $layout = '@app/modules/admin/views/layout/columns2';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Projects::find()->orderBy('brand'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $groups = Raters::getGroups();
        
        // default value
        $model->pending_mode = 0;
        
        return $this->render('create', [
            'model' => $model,
            'groups' => $groups,
        ]);
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var Projects $model */
        $model = $this->findModel($id);

        $action = Yii::$app->request->post("action");

        if ($action == "addLowRatingReasons") {
            $reason = Yii::$app->request->post("reason");
            $model->addLowRatingReasons($reason);
        } elseif ($action == "deleteLowRatingReasons") {
            $key = Yii::$app->request->post("key");
            $model->removeLowRatingReasons($key);
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        // format datetime
        if ($model->starttime !== null)
            $model->starttime = Yii::$app->formatter->asDatetime($model->starttime, 'YYYY-MM-dd HH:mm');
        if ($model->endtime !== null)
            $model->endtime = Yii::$app->formatter->asDatetime($model->endtime, 'YYYY-MM-dd HH:mm');
        
        
        $groups = Raters::getGroups();
        
        return $this->render('update', [
            'model' => $model,
            'groups' => $groups,
        ]);
    }
    
    /**
     * @param int $id Projects ID
     * @param string $mode process mode
     * @param string $code Group ID
     */
    public function actionUpdateGroup($id, $mode, $code)
    {
        $model = $this->findModel($id);
        
        if ($mode == 'add') {
            if (!$model->addGroup($code)) {
                Yii::$app->session->setFlash('warning', 'This group has been already added.');
            }
        } elseif ($mode == 'remove') {
            if (!$model->deleteGroup($code)) {
                Yii::$app->session->setFlash('danger', 'This group could not remove.');
            }
        }
        
        if (!$model->save()) {
            Yii::$app->session->setFlash('danger', 'Failed to save changes');
        }
        
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionBrand()
    {
        $projects = new Projects;
        $Brand = new Brand;
        $brandA = $Brand->getBrandA();
        
        $req = Yii::$app->request;
        if ($req->post('action')=='add') {
            $Brand->load($req->post()) && $Brand->save();
            self::flash('success', 'Brand saved or updated!');
            return $this->redirect(['brand']);
        }
        
        if ($req->post('action')=='delete') {
            $key = $req->post('key');
            $Brand->deleteBrand($key);
            self::flash('success', 'Brand deleted!');
            return $this->redirect(['brand']);
        }
        return $this->render('brand', ['brandA'=>$brandA,'Brand'=>$Brand]);
    }
    
    
    public function actionBrandSettings($brandKey)
    {
        $model = new Brand;
        $model->find($brandKey);
        $req = Yii::$app->request;
        if ($model->load($req->post()) && $model->save()) {
            self::flash('success', 'Brand saved or updated!');
            return $this->redirect(['brand-settings','brandKey'=>$brandKey]);
        }
        
        
        return $this->render('brand-settings', ['model'=>$model]);
    }
    
    

    public function actionCompany()
    {
        $action = Yii::$app->request->post("action");
        if ($action=='add') {
            $code = Yii::$app->request->post('code');
            $name = Yii::$app->request->post('name');
            Raters::addCompany($code, $name);
            self::flash('success', 'Company saved or updated!');
            return $this->redirect(['company']);
        } elseif ($action=='delete') {
            $code = Yii::$app->request->post('code');
            Raters::deleteCompany($code);
            self::flash('success', 'Company deleted!');
            return $this->redirect(['company']);
        }
        
        $companies = Raters::getCompanies();
        
        return $this->render('company', ['companies'=>$companies]);
    }

    public function actionGroups()
    {
        $action = Yii::$app->request->get("action");

        $projectID = Yii::$app->request->get("id");
        $projectModel = $this->findModel($projectID);


        if ($action) {
            $code = Yii::$app->request->get("code");
            if ($action == "assign") {
                $projectModel->addGroup($code);
                $projectModel->save();
                $this->flash("success", "Group has been successfully added to Project.");
            } elseif ($action== "delete") {
                $projectModel->deleteGroup($code);
                $projectModel->save();
                $this->flash("success", "Group has been successfully removed from Project.");
            }
            return $this->redirect(["groups", "id"=>$projectID]);
        }

        // get assigned groups on project
        $assignedGroups = $projectModel->getGroups();
        
        // get unassigned groups
        $availableGroups = array_diff_key(Raters::getGroups(), array_flip($assignedGroups));

        // filter available group name by query params
        $filterGroups = $this->filterGroups($availableGroups, "name", "query");

        $dataProvider = new ArrayDataProvider([
            'key'=>'code',
            'allModels' => $filterGroups,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);


        return $this->render('groups', ["projectModel"=> $projectModel, "dataProvider"=>$dataProvider, "assignedGroups"=>$assignedGroups]);
    }
    
    public function actionPendings($id)
    {
        $projects = Projects::findOne(['id' => $id]);
        if ($projects == null) {
            throw new NotFoundHttpException('Project not found');
        }
        $reasons = $projects->getPendingReasons();
        PendingReasons::setReasons($reasons);
        $req = Yii::$app->request;
        
        
        if ($req->isPost) {
            $pr = new PendingReasons();
            $action = $req->post('action');
            
            if ($action == 'add') {
                if (!$pr->add($req->post('text'))) {
                    self::flash('danger', 'Failed to add reason');
                } else {
                    $projects->setPendingReasons($pr->getReasons());
                    if (!$projects->save()) {
                        self::flash('danger', 'Failed to save');
                    } else {
                        self::flash('success', 'Reason saved');
                    }
                }
            } elseif ($action == 'remove') {
                $key = $req->post('key');
                if (!$pr->remove($key)) {
                    self::flash('danger', 'Failed to remove reason');
                } else {
                    $projects->setPendingReasons($pr->getReasons());
                    if (!$projects->save()) {
                        self::flash('danger', 'Failed to save');
                    } else {
                        self::flash('success', 'Reason removed!');
                    }
                }
            }
            
            return $this->redirect(['pendings', 'id' => $id]);
        }
        
        return $this->render('pendings', ['id' => $id, 'reasons' => $reasons]);
    }
 
 
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function filterGroups($groups=[], $col, $paramName)
    {
        $result = [];
        if (count($groups)) {
            $result = array_filter($groups, function ($item) use ($col, $paramName) {
                $query = strtolower(Yii::$app->request->getQueryParam($paramName, ''));
                if (strlen($query) > 0) {
                    if (strpos(strtolower($item[$col]), $query) !== false) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            });
        }
        return $result;
    }
	
	// Super Admin 
	
	public function actionClearQar() {
		
		//truncate
		
		// 1 questions 
		// 2 answers
		// 3 model ratings
		// 4 rating blocks 
		
	}
	
}
