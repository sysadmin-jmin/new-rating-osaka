<?php
/**
 * Created by PhpStorm.
 * User: fujimoto
 * Date: 2018-12-28
 * Time: 14:57
 */

namespace app\modules\admin\controllers;

use app\components\UploadImageComponent;
use app\models\AnswerForms\Import;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;

class MediaController extends Controller
{

    public function actionUpload()
    {
        $model = new Import;
        $imageFile = UploadedFile::getInstance($model, 'dataFile');

        $directory = Yii::getAlias('@app/data/tmp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }

        if ($imageFile) {

            // Convert to png
            $filePath = Yii::$app->convertImage->saveImageForm($imageFile, $directory, 480);
            $fileName = basename($filePath);


            $path = '/data/tmp/' . Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;
            return Json::encode([
                'files' => [
                    [
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        'url' => $path,
                        'thumbnailUrl' => $path,
                        'deleteUrl' => 'image-delete?name=' . $fileName,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
        }

        return '';
    }
}