<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\ModelRatings;
use app\models\Questions;
use app\models\Answers;

class ModelRatingsController extends Controller
{
    public function actionView()
    {
        #$this->layout = '@app/modules/admin/views/layout/columns2';
        return $this->render('view');
    }
    
    public function actionCreate($ansID, $qnID)
    {
        $action = Yii::$app->request->post("action");

        if ($action=="rating") {
            $postData = Yii::$app->request->post("data");

            if (ModelRatings::saveRating($postData)) {
                $this->flash("success", "Rating successfully saved.");
                return $this->redirect(["create", "ansID"=>$ansID, "qnID"=>$qnID]);
            }
        }

        $answerModel = Answers::findOne($ansID);
        $questionModel = Questions::findOne($qnID);
        $model = ModelRatings::find()->where(["answerID"=>$ansID,  "rcID"=>$answerModel->rcID])->one();

        return $this->render('create', ["model"=>$model, "questionModel"=>$questionModel]);
    }

    protected function findModel($id)
    {
        if (($model = ModelRatings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
