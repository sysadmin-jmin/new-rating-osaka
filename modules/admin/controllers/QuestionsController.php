<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Questions;
use app\models\RatingConfigs as RC;
use app\models\QuestionForms\Items;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex($rcID)
    {
        $this->layout = '@app/modules/admin/views/layout/columns2';
        
        
        $dataProviderActual = new ActiveDataProvider([
            'query' => Questions::find()->where(['rcID'=>$rcID,'type'=>'actual'])
        ]);
        
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Questions::find()->where(['rcID'=>$rcID])
                                        ->andWhere(['<>','type','actual'])
                                            ,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'dataProviderActual' => $dataProviderActual,
            'rcID'=>$rcID,

        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $key=null;
        $update = Yii::$app->request->get("update");
        $questionModel = $this->findModel($id);

        $data = [];
        $data["formObj"]= null;
        
        
        if ($update == "items") {
            $key = Yii::$app->request->get("key");
            $itemM = $questionModel->findItems($key);

            $action = Yii::$app->request->post("action");

            if ($action == "save") {
                $post = Yii::$app->request->post();
                if ($itemM->load($post) && $itemM->save()) {
                    $this->flash("success", "Items has successfully updated.");
                    return $this->redirect(['view','id'=>$id, "update"=>$update, "key"=>$key]);
                }
            } elseif ($action == "delete") {
                if ($itemM->delete()) {
                    $this->flash("warning", "Items has successfully deleted.");
                    return $this->redirect(['view','id'=>$id]);
                }
            }

            $data["formObj"]= $itemM;
        }

        $data["model"] = $questionModel;
        $data["update"] = $update;
        $data["key"] = $key;

        return $this->render('view', $data);
    }

    public function actionItems($id)
    {
        $questionModel = $this->findModel($id);
        $data = [];
        $data["formObj"] = null;

        $update = Yii::$app->request->get("update");

        if ($update=="item") {
            $itemKey= Yii::$app->request->post("itemKey");

            $itemM = $questionModel->findItems($itemKey);
            $action= Yii::$app->request->post("action");

            if ($action=="save") {
                $post = Yii::$app->request->post();

                if ($itemM->load($post) && $itemM->save()) {
                    $this->flash("success", "Item has successfully updated.");
                    return $this->redirect(['items','id'=>$id]);
                }
            } elseif ($action == "delete") {
                if ($itemM->delete()) {
                    $this->flash("success", "Item has successfully deleted.");
                    return $this->redirect(['items','id'=>$id]);
                }
            }

            $data["formObj"] = $itemM;
        }


        $data["model"] = $questionModel;
        return $this->render('items', $data);
    }

    public function actionUnits($id)
    {
        $questionModel = $this->findModel($id);
        $data = [];
        $data["formObj"] = null;
        $data["model"] = $questionModel;

        $update = Yii::$app->request->get("update");

        if ($update=="unit") {
            $unitKey= Yii::$app->request->post("unitKey");

            $unitM = $questionModel->findUnit($unitKey);
            $action= Yii::$app->request->post("action");

            if ($action=="save") {
                $post = Yii::$app->request->post();

                if ($unitM->load($post) && $unitM->save()) {
                    $this->flash("success", "Unit has successfully updated.");
                    return $this->redirect(['units','id'=>$id]);
                }
            }

            $data["formObj"] = $unitM;
        }

        return $this->render('units', $data);
    }

    public function actionContext($id)
    {
        $key=null;
        $update = Yii::$app->request->get("update");
        $questionModel = $this->findModel($id);

        $data = [];
        $data["formObj"]= null;

        if ($update == "context") {
            $key = Yii::$app->request->get("key");
            $unitKey = Yii::$app->request->get("unitKey");
            $contextM = $questionModel->findUnitContext($unitKey, $key);

            $action = Yii::$app->request->post("action");

            if ($action=="save") {
                $post = Yii::$app->request->post();
                if ($contextM->load($post) && $contextM->save()) {
                    $this->flash("success", "Context has successfully added / updated.");
                    return $this->redirect(['context','id'=>$id]);
                }
            } elseif ($action=="delete") {
                if ($contextM->delete()) {
                    $this->flash("warning", "Context has successfully deleted.");
                    return $this->redirect(['context','id'=>$id]);
                }
            }

            $data["formObj"]= $contextM;
        }

        $data["model"] = $questionModel;
        $data["update"] = $update;
        $data["key"] = $key;

        return $this->render('context', $data);
    }

    public function actionGenInstructions($id)
    {
        $data = [];
        $data["formObj"]= null;
        $update = Yii::$app->request->get("update");

        $questionModel = $this->findModel($id);

        if ($update=="genInstruction") {
            $blockKey = Yii::$app->request->post("blockKey");
            $key = Yii::$app->request->post("key");
            $genInstructionsM = $questionModel->findGenInstruction($blockKey, $key);

            $action = Yii::$app->request->post("action");
            if ($action=="save") {
                $post = Yii::$app->request->post();
                if ($genInstructionsM->load($post) && $genInstructionsM->save()) {
                    $this->flash("success", "General Instruction has successfully added / updated.");
                    return $this->redirect(['gen-instructions','id'=>$id]);
                }
            } elseif ($action=="delete") {
                if ($genInstructionsM->delete()) {
                    $this->flash("warning", "General Instruction has successfully deleted.");
                    return $this->redirect(['gen-instructions','id'=>$id]);
                }
            }

            $data["formObj"] = $genInstructionsM;
        }
        $data["questionModel"] = $questionModel;
        
        return $this->render('genInstructions', $data);
    }
    
    public function actionExamples($id)
    {
        $data = [];
        $data["formObj"]= null;
        $update = Yii::$app->request->get("update");

        $questionModel = $this->findModel($id);
        
        if ($update=="example") {
            $unitKey = Yii::$app->request->post("unitKey");
            $key = Yii::$app->request->post("key");
            $exampleM = $questionModel->findExample($unitKey, $key);
    
            $action = Yii::$app->request->post("action");
            if ($action=="save") {
                $post = Yii::$app->request->post();
                if ($exampleM->load($post) && $exampleM->save()) {
                    $this->flash("success", "Example has successfully added / updated.");
                    return $this->redirect(['examples','id'=>$id]);
                }
            } elseif ($action=="delete") {
                if ($exampleM->delete()) {
                    $this->flash("warning", "Example has successfully deleted.");
                    return $this->redirect(['examples','id'=>$id]);
                }
            }

            $data["formObj"] = $exampleM;
        }

        $data["model"] = $questionModel;
        
        return $this->render('examples', $data);
    }
    
    // Sets Qn Status to lock
    public function actionLock()
    {
        $id = Yii::$app->request->post('id');
        $status_ = Yii::$app->request->post('status');

        $model = $this->findModel($id);
        $model->status = $status_;
        
        $model->save(false);
        
        $this->flash("warning", "Question Set {$id} Now {$status_}");
        return $this->redirect(['index','rcID'=>$model->rcID]);
    }
    
    
    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($rcID)
    {
        $model = new Questions();

        $model->rcID = $rcID;
        
        $rcModel = RC::findOne($rcID);
        $model->projectID = $rcModel->projectID;
        
        
        $model->items = $rcModel->rating_items;
        $model->units = $rcModel->rating_units;
        $model->blocks = $rcModel->rating_blocks;
        
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this::flash('success', "Created A Question Set!");
            return $this->redirect(['index','rcID'=>$model->rcID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    


    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index','rcID'=>$model->rcID]);
    }

    public function actionItemImage($id)
    {
        $model = $this->findModel($id);
        $itemKey = Yii::$app->request->get("itemKey");

        $path = $model->getItemsDir()."/".$itemKey.".png";

        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/png');
        $img_data = file_get_contents($path);

        $response->data = $img_data;
        return $response;
    }

    public function actionContextImage($id)
    {
        $model = $this->findModel($id);
        $unitKey = Yii::$app->request->get("unitKey");
        $contextKey = Yii::$app->request->get("contextKey");

        $path = $model->getContextPath($unitKey, $contextKey);

        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/png');
        $img_data = file_get_contents($path);

        $response->data = $img_data;
        return $response;
    }

    public function actionGenInstructionsImage($id)
    {
        $model = $this->findModel($id);
        $blockKey = Yii::$app->request->get("blockKey");
        $key = Yii::$app->request->get("key");

        $path = $model->getGenInstructionsDir()."/".$blockKey."_".$key.".png";

        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/png');
        $img_data = file_get_contents($path);

        $response->data = $img_data;
        return $response;
    }

    public function actionExamplesImage($id)
    {
        $model = $this->findModel($id);
        $unitKey = Yii::$app->request->get("unitKey");
        $key = Yii::$app->request->get("key");

        $path = $model->getExamplesDir()."/".$unitKey."_".$key.".png";

        $response = \Yii::$app->response;
        $response->format = yii\web\Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/png');
        $img_data = file_get_contents($path);

        $response->data = $img_data;
        return $response;
    }

    public function actionAssignItemToUnit($id)
    {
        $data = [];
        $questionModel = $this->findModel($id);

        $action = Yii::$app->request->post("action");
        $unitKey= Yii::$app->request->post("unitKey");
        $itemKey= Yii::$app->request->post("itemKey");

        if ($action=="assignItem") {
            if ($questionModel->assignItemToUnit($itemKey, $unitKey)) {
                self::flash('success', "Item successfuly assigned.");
            } else {
                self::flash('danger', implode("", $questionModel->getErrors("units")));
            }
            return $this->redirect(['assign-item-to-unit','id'=>$id]);
        } elseif ($action=="unassignItem") {
            if ($questionModel->unassignItemFromUnit($itemKey, $unitKey)) {
                self::flash('success', "Item successfuly unassigned.");
            } else {
                self::flash('danger', implode("", $questionModel->getErrors("units")));
            }
            return $this->redirect(['assign-item-to-unit','id'=>$id]);
        }

        $data["model"] = $questionModel;
        return $this->render('assignItemToUnit', $data);
    }

    public function actionAssignUnitToBlock($id)
    {
        $data = [];
        $questionModel = $this->findModel($id);

        $action = Yii::$app->request->post('action');
        $unitKey= Yii::$app->request->post("unitKey");
        $blockKey= Yii::$app->request->post("blockKey");

        if (Yii::$app->request->isPost && $blockKey == null) {
            self::flash('danger', 'Please choose block');
            return $this->redirect(['assign-unit-to-block','id'=>$id]);
        }
        if (Yii::$app->request->isPost && $unitKey == null) {
            self::flash('danger', 'Please choose unit');
            return $this->redirect(['assign-unit-to-block','id'=>$id,'blockKey'=>$blockKey,'forms'=>'assignUnit']);
        }

        if ($action == 'assignUnit') {
            if ($questionModel->assignUnitToBlock($unitKey, $blockKey)) {
                self::flash('success', 'Unit successfully assigned to Block.');
            }
            return $this->redirect(['assign-unit-to-block','id'=>$id]);
        } elseif ($action == 'unassignUnit') {
            if ($questionModel->unassignUnitFromBlock($unitKey, $blockKey)) {
                Yii::$app->session->setFlash('warning', 'Units successfully removed from Block.');
            }
            return $this->redirect(['assign-unit-to-block','id'=>$id]);
        }

        $data["model"] = $questionModel;
        return $this->render('assignUnitToBlock', $data);
    }


    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function flash($header, $msg)
    {
        Yii::$app->session->setFlash($header, $msg);
    }
}
