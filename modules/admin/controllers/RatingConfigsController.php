<?php

namespace app\modules\admin\controllers;

use app\models\RatingConfigForms\Report;
use app\models\RatingConfigForms\Group;
use app\models\RatingConfigForms\Rater;
use app\models\RatingConfigForms\Items;
use app\models\Answers;
use app\models\RatingConfigForms\Units;
use Yii;

use app\models\Projects;
use app\models\RatingConfigs;
use app\models\Raters;


use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use app\modules\admin\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use yii\helpers\Url;

/**
 * RatingConfigsController implements the CRUD actions for RatingConfigs model.
 */
class RatingConfigsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RatingConfigs models.
     * @return mixed
     */
    public function actionIndex($projectID)
    {
        $this->layout = '@app/modules/admin/views/layout/columns2';
        
        Yii::$app->session->set('projectID', $projectID);
        Yii::$app->session->set('projectName', Projects::getName($projectID));
        
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => RatingConfigs::find()->where(['projectID' => $projectID]),
        ]);

        return $this->render('index', [
            'projectID' => $projectID,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RatingConfigs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/modules/admin/views/layout/columns2';
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RatingConfigs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RatingConfigs;
        $model->projectID = Yii::$app->request->get('projectID');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','projectID'=>$model->projectID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RatingConfigs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RatingConfigs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $projectID = $model->projectID;
        $model->delete();
        self::flash('warning', 'Rating Set Configutation Deleted!');
        
        return $this->redirect(['index','projectID'=>$projectID]);
    }

    // *** Rating Structure ****
    public function actionItems($rcID)
    {
        $data = [];
        $rcModel = $this->findModel($rcID);
        $req = Yii::$app->request;
        
        $itemM = new Items($rcID);

        //Delete
        if ($req->post('action') == 'delete') {
            $itemM->key = $req->post('itemKey');
            $itemM->delete();
            $this->flash("success", "Item has been deleted");
            return $this->redirect(['items','rcID'=>$rcID]);
        }
        
        //Update
        if ($req->get('action')=='update') {
            $itemKey = $req->get('itemKey');
            $item_ = $rcModel->getItems()[$itemKey];
            $itemM->attributes = $item_;
            $itemM->key = $itemKey;
        }
        
        //Update | New
        if ($req->post('action')=='save') {
            if ($itemM->load($req->post()) && $itemM->save()) {
                $this->flash("success", "Item has successfully added / updated.");
                return $this->redirect(['items','rcID'=>$rcID]);
            }
        }

        $data['itemM'] = $itemM;
        $data['rcModel'] = $rcModel;
        $data['items'] = $rcModel->getItems();
                
        return $this->render('items', $data);
    }
    
    //displays image data
    public function actionItemImage($rcID, $itemID)
    {
        $rcModel = $this->findModel($rcID);
        $model = new Items($rcID, $rcModel);
        $itemObj = $model->findOne($itemID);
        $path = Url::to('@webroot') .'/'.$itemObj->question;
        
        $response = \Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $response->headers->add('content-type', 'image/png');
        $img_data = file_get_contents($path);
        $response->data = $img_data;
        return $response;
    }

    public function actionUnits($rcID)
    {
        $postData = Yii::$app->request->post();
        $action = Yii::$app->request->get("action");
        $id = Yii::$app->request->get("id");
        $rcModel = $this->findModel($rcID);

        if (!empty($postData)) {
            if ($postData['name'] !='') {
                $rcModel->saveUnits($postData);
                self::flash('success', "Unit added successfuly");
            } else {
                self::flash('error', "Unit name can not be empty");
            }
        } elseif ($action == 'delete' && $id!='') {
            $rcModel->deleteUnit($id);
            self::flash('warning', "Unit Deleted");
            return $this->redirect(['units','rcID'=>$rcID]);
        }

        $unitsA = $rcModel->getUnits();
        $maxKey = (count($unitsA)>0) ? max(array_keys($unitsA)) + 1 : 0;
        $maxKey = $maxKey + 1;
        
        
        return $this->render(
                    'units',
                    [
                        'list'=>$unitsA,
                        'rcID'=>$rcID,
                        'rcM' => $rcModel,
                        'maxKey' => $maxKey
                    ]
                );
    }
    
        
    public function actionAssignItemToUnit($rcID)
    {
        $rcModel = $this->findModel($rcID);

        $action = Yii::$app->request->post("action");
        $unitKey= Yii::$app->request->post("unitKey");
        $itemKey= Yii::$app->request->post("itemKey");

        if ($action=="up") {
            $rcModel->unitUp($unitKey);
        } elseif ($action=="down") {
            $rcModel->unitDown($unitKey);
        } elseif ($action=="delete") {
            $rcModel->deleteUnit($unitKey);
        } elseif ($action=="assignItem") {
            if ($rcModel->assignItemToUnit($itemKey, $unitKey)) {
                self::flash('success', "Item successfuly assigned.");
            } else {
                self::flash('danger', implode("", $group->getErrors("rating_units")));
            }
            return $this->redirect(['assign-item-to-unit','rcID'=>$rcID]);
        } elseif ($action=="unassignItem") {
            if ($rcModel->unassignItemFromUnit($itemKey, $unitKey)) {
                self::flash('success', "Item successfuly unassigned.");
            } else {
                self::flash('danger', implode("", $group->getErrors("rating_units")));
            }
            return $this->redirect(['assign-item-to-unit','rcID'=>$rcID]);
        }
        // items
        $items = $rcModel->getItems();
        // units
        $units = $rcModel->getUnits();

        return $this->render('assignItemToUnit', [
            'rcID' => $rcID,
            'items'=> $items,
            'units'=> $units,
        ]);
    }

    public function actionBlocks($rcID)
    {
        $action = Yii::$app->request->post("action");
        $rcModel = $this->findModel($rcID);


        if ($action=="addBlock") {
            $data = Yii::$app->request->post();
            if ($data["name"] != '') {
                $rcModel->saveBlock($data);
                self::flash('success', "Block added successfuly");
                return $this->redirect(['blocks','rcID'=>$rcID]);
            } else {
                self::flash('error', "Block name can not be empty");
            }
        } elseif ($action=="up") {
            $blockKey= Yii::$app->request->post("blockKey");
            $rcModel->blockUp($blockKey);
        } elseif ($action=="down") {
            $blockKey= Yii::$app->request->post("blockKey");
            $rcModel->blockDown($blockKey);
        } elseif ($action=="delete") {
            $blockKey= Yii::$app->request->post("blockKey");
            $rcModel->deleteBlock($blockKey);
        }


        return $this->render('blocks', ['model'=>$rcModel]);
    }
    
    
    public function actionBlockSettings($rcID, $blockKey)
    {
        $rcModel = $this->findModel($rcID);
        $blockSettingsModel = new \app\models\RatingConfigForms\BlockSettings($rcModel, $blockKey);
        
        $action = Yii::$app->request->post("action");

        if ($action=="save") {
            $postData = Yii::$app->request->post();
            if ($blockSettingsModel->load($postData) && $blockSettingsModel->save()) {
                self::flash('success', 'Block Settings successfully saved.');
            }
        } elseif ($action=="assignGroup") {
            $postData = Yii::$app->request->post();
            $postData["rcID"] = $rcID;
            $postData["blockKey"] = $blockKey;
            $group = new Group;

            if ($group->load($postData, "") && $group->save()) {
                self::flash('success', "Group successfuly assigned.");
                return $this->redirect(['block-settings','rcID'=>$rcID, "blockKey"=>$blockKey]);
            } else {
                self::flash('danger', implode("", $group->getErrors("gID")));
                return $this->redirect(['block-settings','rcID'=>$rcID, "blockKey"=>$blockKey]);
            }
        } elseif ($action=="unassignGroup") {
            $gid = Yii::$app->request->post("gID");

            $group = Group::findOne($rcModel, $blockKey, $gid);

            if ($group->delete()) {
                self::flash('success', "Group successfuly unassigned.");
                return $this->redirect(['block-settings','rcID'=>$rcID, "blockKey"=>$blockKey]);
            }
        }
        
        return $this->render('block-settings', ['model'=>$blockSettingsModel, "rcModel" => $rcModel]);
    }
    
    
    
    public function actionAssignUnitToBlock($rcID)
    {
        $rcModel = $this->findModel($rcID);

        // units
        $units = $rcModel->getUnits();
        // block
        $blocks = $rcModel->getBlocks();

        $action = Yii::$app->request->post('action');
        $unitKey= Yii::$app->request->post("unitKey");
        $blockKey= Yii::$app->request->post("blockKey");

        if (Yii::$app->request->isPost && $blockKey == null) {
            self::flash('danger', 'Please choose block');
            return $this->redirect(['assign-unit-to-block','rcID'=>$rcID]);
        }
        if (Yii::$app->request->isPost && $unitKey == null) {
            self::flash('danger', 'Please choose unit');
            return $this->redirect(['assign-unit-to-block','rcID'=>$rcID,'blockKey'=>$blockKey,'forms'=>'assignUnit']);
        }
        
        if ($action == 'assignUnit') {
            if ($rcModel->assignUnitToBlock($unitKey, $blockKey)) {
                self::flash('success', 'Unit successfully assigned to Block.');
            }
            return $this->redirect(['assign-unit-to-block','rcID'=>$rcID]);
        }
        if ($action == 'unassignUnit') {
            if ($rcModel->unassignUnitFromBlock($unitKey, $blockKey)) {
                self::flash('warning', 'Units successfully removed from Block.');
            }
            return $this->redirect(['assign-unit-to-block','rcID'=>$rcID]);
        }
        
        //Units will not show units that has already been assigned
        
        
        return $this->render('assignUnitToBlock', ['rcModel'=>$rcModel,
                                                'units'=>$units,
                                                'blocks'=>$blocks
        
        ]);
    }
    
    public function actionBlockRaters($rcID, $blockKey, $groupID)
    {
        $action = Yii::$app->request->post("action");

        if ($action=="assignRater") {
            $postData = Yii::$app->request->post();
            $postData["rcID"] = $rcID;
            $postData["blockKey"] = $blockKey;
            $rater = new Rater;

            if ($rater->load($postData, "") && $rater->save()) {
                self::flash('success', "Rater status successfuly updated.");
            }
            
            return $this->redirect(
                [
                    'block-raters',
                    'rcID'=>$rcID,
                    "blockKey"=>$blockKey,
                    "groupID"=>$groupID
                ]
            );
        }

        $rcModel= $this->findModel($rcID);
        $block = $rcModel->getBlocks($blockKey);
        $group = Raters::getGroupByCode($groupID);

        $query = Raters::find()->where(["group"=>$groupID]);
        if ($searchName = trim(Yii::$app->request->get("search_name"))) {
            $query->andWhere("name LIKE '%$searchName%'");
        }
        if ($searchStatus= Yii::$app->request->get("search_status")) {
            $members = $rcModel->getGroupRatersStatus($blockKey, $groupID, $searchStatus);
            $member_ids = array_column($members, "id");
            if (count($member_ids)) {
                $query->andWhere("id in (".implode(",", $member_ids).")");
            } else {
                $query->andWhere("id in (0)");
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $params['rcModel'] = $rcModel;
        $params['dataProvider'] = $dataProvider;
        $params['block'] = $block;
        $params['group'] = $group;

        return $this->render('blockRaters', $params);
    }

    // Reports
    public function actionReport()
    {
        $rcID = Yii::$app->request->get('rcID');


        $Report = new Report(['rcID' => $rcID]);

        // update tests staths
        $Report->updateSheetStatus();


        /** @var Answers[status] $classified */
        $classified = $Report->getClassifyTestStatus();
        $ratingStatus = array_map(function ($test) {
            return count($test);
        }, $classified);


        return $this->render('report', [
            'rcID'=>$rcID,
            'Report' => $Report,
            'ratingStatus' => $ratingStatus,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = RatingConfigs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    

    private function filterGroups($data=[], $col, $paramName)
    {
        $result = [];
        if (count($data)) {
            $result = array_filter($data, function ($item) use ($col, $paramName) {
                $query = strtolower(Yii::$app->request->getQueryParam($paramName, ''));
                if (strlen($query) > 0) {
                    if (strpos(strtolower($item[$col]), $query) !== false) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            });
        }
        return $result;
    }
}
