<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Raters;
use yii\data\ActiveDataProvider;
use app\modules\admin\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use app\components\AccessRule;
use yii\filters\AccessControl;

/**
 * RatersController implements the CRUD actions for Raters model.
 */
class RatersController extends Controller
{
    public $layout = '@app/modules/admin/views/layout/columns2';
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                     'class' => AccessRule::className(),
                 ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Admin', 'Manager'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Raters models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Raters();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Raters model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionAddCertBrand() {
		
		$id = Yii::$app->request->post('id');
		$model = $this->findModel($id);
		$brandKey = Yii::$app->request->post('brandKey');
		
		if($model->addCertBrands($brandKey)) {
			Yii::$app->session->setFlash('success','Certifcation For Brand removed');
		}
		
		return $this->redirect(['view','id'=>$id]);
	}
	
	
	public function actionDelCertBrand() {
		
		$id = Yii::$app->request->post('id');
		$model = $this->findModel($id);
		$brandKey = Yii::$app->request->post('brandKey');
		
		if($model->delCertBrands($brandKey)) {
			Yii::$app->session->setFlash('success','Certifcation For Brand removed');
		}
		
		return $this->redirect(['view','id'=>$id]);
	}
	
    
    public function actionResetAdmin()
    {
        $password = 'admin';
        #$hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        $model = $this->findModel(1);
        $model->setPassword($password);
        $model->save(false);
        echo "reset";
    }

    /**
     * Creates a new Raters model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Raters();

        $model->setCustomRules([ [["password_hash"], "required"] ]);

        $data = Yii::$app->request->post("Raters");
        if (count($data)) {
            // use password algorithm from Yii2
            if (trim($data["password_hash"])) {
                $model->setPassword(trim($data["password_hash"]));
            }
            // unset the password hash so can not to be loaded again by model
            unset($data["password_hash"]);
            if ($model->load($data, "") && $model->save()) {
                #return $this->redirect(['view', 'id' => $model->id]);
                Yii::$app->session->setFlash('success', 'Rater created!');
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Raters model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $logedInUser = Yii::$app->user->getIdentity();

        // if logged in user is manager only can update rater for same company.
        if ($logedInUser->role=="Manager" && $model->company!=$logedInUser->company) {
            throw new ForbiddenHttpException("You do not allowed to update this rater. Your company not match with current rater.");
        }

        $data = Yii::$app->request->post("Raters");
        if (count($data)) {
            // if password is filled change the password otherwise dont change the password
            // use password algorithm from Yii2
            if (trim($data["password_hash"])) {
                $model->setPassword(trim($data["password_hash"]));
            }
            // unset the password hash so can not to be loaded again by model
            unset($data["password_hash"]);
            if ($model->load($data, "") && $model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Raters model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $logedInUser = Yii::$app->user->getIdentity();

        // if logged in user is manager only can delete rater for same company.
        if ($logedInUser->role=="Manager" && $model->company!=$logedInUser->company) {
            throw new ForbiddenHttpException("You do not allowed to delete this rater. Your company not match with current rater.");
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Raters model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Raters the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
     
    public function actionGroups()
    {
        $action = Yii::$app->request->post("action");

        if ($action=="add") {
            $code = Yii::$app->request->post("code");
            $name = Yii::$app->request->post("name");

            if (Raters::addGroup($code, $name)) {
                Yii::$app->session->setFlash("success", "Group has successfully added.");
            } else {
                Yii::$app->session->setFlash("danger", "Cannot add group.");
            }
            return $this->refresh();
        } elseif ($action == "delete") {
            $code = Yii::$app->request->post("code");
            if (Raters::deleteGroup($code)) {
                Yii::$app->session->setFlash("success", "Group has successfully deleted.");
            } else {
                Yii::$app->session->setFlash("danger", "Cannot delete group.");
            }
            return  $this->refresh();
        }

        $groups = Raters::getGroups();

        return $this->render('groups', ["groups"=>$groups]);
    }

    public function actionAssignToGroup()
    {
        $logedInUser = Yii::$app->user->getIdentity();

        $action =  Yii::$app->request->get("action");
        $group = Yii::$app->request->get("group");
        $raterID = Yii::$app->request->get("raterID");

        $assignedRaters = [];


        if ($action=="assign") {
            Raters::assignRaterToGroup($raterID, $group);
        } elseif ($action=="delete") {
            Raters::unAssignRaterFromGroup($raterID);
        }

        if ($group) {
            $assignedRaters = $logedInUser->findByGroup($group);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => Raters::find()->where("`group` is NULL OR `group`=''"),
        ]);
        
        $groups = Raters::getGroups();

        return $this->render('assignToGroup', [
            'dataProvider' => $dataProvider,
            'groups' => $groups,
            'assignedRaters' => $assignedRaters
        ]);
    }
     
    protected function findModel($id)
    {
        if (($model = Raters::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
