<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers\Controller;

class SettingsController extends Controller
{
    public $layout = '@app/modules/admin/views/layout/columns2';
	
	
	public function actionIndex()
    {
        return $this->render('index');
    }

	
	public function actionBrand() {
		
		
		return $this->render('brand');
	}
}
