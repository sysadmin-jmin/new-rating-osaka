<?php
namespace app\modules\admin\controllers;

use yii\filters\AccessControl;
use app\components\AccessRule;
use Yii;

class Controller extends \yii\web\Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    // 'ruleConfig' => [
    //             	'class' => AccessRule::className(),
    //         	],
    //             'rules' => [
    //                 [
    //                     'allow' => true,
    //                     'roles' => ['Admin'],
    //                 ],
    //             ],
    //         ]
    //     ];
    // }

    #public $layout = '@app/modules/admin/views/layout/columns2';

    /* public function __construct()	 {
        Yii::$app->homeUrl = ['/admin'];
    } */
	
    //private functions used internally
    public function flash($header, $msg)
    {
        Yii::$app->session->setFlash($header, $msg);
    }
}
