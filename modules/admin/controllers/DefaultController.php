<?php

namespace app\modules\admin\controllers;

use app\modules\admin\controllers;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['projects/']);
    }
}
